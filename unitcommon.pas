{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  This unit concentrates global constants, varibles and resource strings.
}
unit unitcommon;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, GraphType;

const
  SECTION_DEFAULT       = 'options';
  SECTION_GEOMETRY      = 'geometry';
  SECTION_FONTS         = 'fonts';
  SECTION_MRECOMMANDS   = 'MRECommands';

const
  CMDSEP = '&sep;';

  SEARCHIN_COMMIT       = 1;
  SEARCHIN_AUTHOR       = 2;
  SEARCHIN_SUBJECT      = 3;
  SEARCHIN_DATE         = 4;

  ACTION_CONTINUE         = 1;
  ACTION_SKIP             = 2;
  ACTION_ABORT            = 3;
  ACTION_QUIT             = 4;
  ACTION_BISECT_START     = 5;
  ACTION_BISECT_GOOD      = 6;
  ACTION_BISECT_BAD       = 7;
  ACTION_BISECT_GOOD_SET  = 8;
  ACTION_BISECT_BAD_SET   = 9;
  ACTION_BISECT_LOG       = 10;
  ACTION_BISECT_RESET     = 11;

  BIN_BUFSIZE           = 1024;

const
  //LOG_CMD = '--pretty="format:%ct'#2'%P'#2'%H'#2'%an'#2'%ae'#2'%s'#2#3'" --all';
  LOG_CMD = '--pretty="format:%ct'#2'%P'#2'%H'#2'%an'#2'%ae'#2'%s'#2#3'" --exclude=refs/stash --all';
  //LOG_CMD = '--pretty="format:%ct'#2'%P'#2'%H'#2'%an'#2'%ae'#2'%s'#2#3'" --branches --remotes --tags';
  LOG_SINGLE_CMD = '--pretty="format:%ct'#2'%P'#2'%H'#2'%an'#2'%ae'#2'%s'#2#3'" -n 1 --exclude=refs/stash';

var
  gblInvalidateCache: boolean = false;          // Global variable to enable invalidate cache and start from 0
  gblRecordsToUpdate: Integer = 25;             // Update progress indicator (counter) every this number of received records
  gblCacheScreens: Integer = 11;                // for new log records, how many screens will the graph will be calculated
  gblRecordsToRowCount: Integer = 10;           // for new log records, every how many records will be grid.RowCount get updated
  gblCutterMode: boolean = false;               // Experimental feature for simplying graph (by eliminating intermediary log records)
  gblTopologicalMode: boolean = true;           // Enables retrieving log records in topologica sort order (this resolves concurrent commits)
  gblAllowDeleteChanged: boolean = false;       // Enables deleting of modified tracked files (not just untracked files)
  gblMaxGraphColumnWidth: Integer = 300;        // After graph calc, this is the maximun graph column width
  gblMaxGraphRows: Integer = 10000;             // A default maximun row count showing the graph
  gblReportLooseObjects: boolean = true;        // In the reposository information report include also loose objects
  gblBisectGoodColor: TGraphicsColor = $C8FFC8; //
  gblBisectBadColor:  TGraphicsColor = $C8C8FF; //
  gblBisectTryColor:  TGraphicsColor = $C8C800; //
  gblLastDiffToolIndex: Integer = -1;           // Last diff tool index selected (used when double click an entry)

resourcestring
  rsNewBranch = 'New Branch';
  rsRenameBranch = 'Rename current branch';
  rsRenameBranchTo = 'Rename %s to:';
  rsInvalidBranchName = 'Invalid branch name';
  rsAddWorkTree = 'Add branch %s to a worktree';
  rsRemoveWorkTree = 'Remove this worktree';
  rsNewTag = 'Create a tag at this point';
  rsReload = 'Reload';
  rsPushingYourCommits = 'Pushing your commits';
  rsThereAreCommitsBehind = 'There are commits behind, are you sure you want to push?';
  rsRestoringWorkFiles = 'Restoring working files';
  rsRestoringWorkFilesWarning =
    'You are about to discard changes in %s,'^M^M+
    'data will be lost and this action cannot be undone'^M^M+
    'Are you sure to continue?';
  rsDeletingWorkFiles = 'Deleting untracked files';
  rsDeletingWorkFilesWarning =
    'You are about to delete %s,'^M^M+
    'This action cannot be undone'^M^M+
    'Are you sure to continue?';
  rsSIsAlreadyInIgnoredList = '''%s'' is already in ignored list';
  rsTheTypeSIsAlreadyInTheIgnoredList = 'The type ''*%s'' is already in the ignored list';
  rsDFiles = '%d files';
  rsDiscardChanges = 'Discard changes';
  rsDeleteFiles = 'Delete Files';
  rsCancel = 'Cancel';
  rsViewUntrackedFiles = 'View Untracked Files';
  rsViewIgnoredFiles = 'View Ignored Files';
  rsViewTrackedFiles = 'View Tracked Files';
  rsStageS = 'Stage ''%s''';
  rsUnstageS = 'Unstage ''%s''';
  rsStageChanged = 'Stage Changed';
  rsStageAll = 'Stage All';
  rsUnstageAll = 'Unstage All';
  rsAddSToIgnoreList = 'Add ''%s'' to ignore list';
  rsAddSFilesToIgnoreList = 'Add ''*%s'' Files to ignore list';
  rsRestoreS = 'Restore ''%s''';
  rsRestoreAllChanged = 'Restore All Changed';
  rsNotYetImplementedForUnstagedS = 'Not yet implemented for Unstaged: %s';
  rsNotYetImplementedForStagedS = 'Not yet implemented for Staged: %s';
  rsSDoesNotExists = '%s does not exists';
  rsTheFileSIsBinaryDBytes = 'The file ''%s'' is binary, %d bytes';
  rsThisFeatureWillBeImplementedASAP = 'This feature will be implemented ASAP';
  rsCouldnTGetToplevelDirectoryOfS = 'Couldn''t get toplevel directory of %s';
  rsYouHaveToStageSomething = 'You have to stage something in order to commit';
  rsYourCommitMessageIsEmpt = 'Your commit message is empty!';
  rsThisRepositoryHasNoRemotes = 'This repository has no remotes defined'^M+
                                 'I''m not yet prepared to handle this';
  rsPushingBranchWithout = 'Pushing branch without tracking information';
  rsDoYouWantToPushSToSA = 'Do you want to push "%s" to "%s"'^M+
                           'and setup tracking information? I will do:'^M^M+
                           'git push --set-upstream %1:s %0:s';
  rsYesDoIt = 'yes, do it';
  rsPushingToRemoteS = 'Pushing to remote: %s';
  rsFetchingFromRemote = 'Fetching from remote: ';
  rsPullingFromRemote = 'pulling from remote: ';
  rsExecutingACustomCommand = 'Executing a custom command';
  rsYouAreAboutToExecute = 'You are about to execute %s: '^M^M+
                           '%s'^M^M+
                           'Do you want to proceed?';
  rsMERGING = 'Merging';
  rsMERGINGCONFLICT = 'Merging Conflict';
  rsHasDCommits = 'has %d commits';
  rsDCommitsAhead = '%d commits ahead';
  rsDCommitsBehind = '%d commits behind';
  rsOf = 'of';
  rsNoTagAvailable = 'No Tag available';
  rsAtTag = 'At tag';
  rsDCommitsSince = '%d commits since';
  rsResetABranch = 'Reset a branch';
  rsMergingBranches = 'Merging branches';
  rsDeletingRemoteBranch = 'Deleting remote branch';
  rsCreateABranchAtThisCommit = 'Create a branch at this commit';
  rsMergeSToS = 'Merge %s to %s';
  rsRebaseSOnTopOfS = 'Rebase %s on top of %s';
  rsSwitchToS = 'Switch to %s';
  rsDeleteBranchS = 'Delete branch %s';
  rsDeleteRemoteBranchS = 'Delete remote branch %s';
  rsCreateATagAtThisCommit = 'Create a tag at this commit';
  rsSwitchToTagS = 'Switch to tag %s';
  rsDeleteTagS = 'Delete tag %s';
  rsResetSToThisCommit = 'Reset %s to this commit';
  rsUnableToLocateDbIndex = 'Unable to locate db index';
  rsBuildingGraph = 'Building graph..';
  rsBranchNameIsEmpty = 'Branch name is empty';
  rsInvalidCharacterInBranchName = 'Invalid character in branch name';
  rsBranchNameAlreadyExists = 'Branch name already exists';
  rsWillDoGitCheckoutBSS = 'will do: git checkout -b %s %s';
  rsWillDoGitCheckoutSDetached = 'will do: git checkout %s'^M'Repo will be left in detached HEAD state';
  rsCommandDS = 'command %d: %s';
  rsDescriptionIsEmpty = 'Description is empty';
  rsInvalidDescription = 'Invalid description';
  rsCommandMustStartWithGit = 'Command must start with ''git ''';
  rsInvalidGitCommand = 'Invalid git command';
  rsIncompleteCommand = 'Incomplete command';
  rsHistoryOfS = 'History of %s';
  rsLocalBranches = 'Local Branches';
  rsTrackingBranches = 'Tracking Branches';
  rsTags = 'Tags';
  rsCommit = 'Commit';
  rsBranchSAlredyExists = 'Branch ''%s'' alredy exists';
  rsNoReferingBranchSelected = 'No refering branch selected';
  rsNotRefersToATrackingBranch = 'Not refers to a tracking branch';
  rsNoReferingTagSelected = 'No refering tag selected';
  rsNoCommitSet = 'No commit set';
  rsWorktreeS = 'worktree: %s';
  rsTagS2 = 'Tag: %s';
  rsCommitS = 'Commit: %s';
  rsErrorWhileGettingListOfBranches = 'Error while getting list of branches';
  rsInvalid = 'Invalid';
  rsTagNameIsEmpty = 'Tag name is empty';
  rsInvalidCharacterInTagName = 'Invalid character in tag name';
  rsResetBranchS = 'Reset branch %s';
  rsYouAreResettingBranch = 'You are resetting branch %s to revision %s:'^M+
                            '%s'^M+
                            'Do you want to proceed?';
  rsResetSoft = 'Neither the working copy nor the index are altered.'^M^M^M+
    'git doc:'^M+
    'Does not touch the index file or the working tree at all (but '+
    'resets the head to <commit>, just like all modes do). This leaves all '+
    'your changed files "Changes to be committed", as git status would put it.';
  rsResetMixed = 'Reset index, don''t touch working copy.'^M^M^M+
    'git doc: '^M+
    'Resets the index but not the working tree (i.e., the ' +
    'changed files are preserved but not marked for commit)' +
    ' and reports what has not been updated. This is the ' +
    'default action.';
  rsResetHard = 'Reset index and working copy.'^M+
    'LOCAL CHANGES WILL BE LOST!'^M^M^M+
    'git doc: '^M+
    'Resets the index and working tree. Any changes to tracked ' +
    'files in the working tree since <commit> are discarded. Any' +
    ' untracked files or directories in the way of writing any ' +
    'tracked files are simply deleted.';
  rsStartingCommandPleaseWait = 'Starting command, please wait ....';
  rsWorking = 'Working ....';
  rsSucceed = 'Succeed';
  rsFailed = 'Failed';
  rsTheCommandIsEmpty = 'The command is empty';
  rsABranchIsNotSelected = 'A branch is not selected';
  rsTheTargetRemoteRepositoryIsNotSelected = 'The target remote repository is '
    +'not selected';
  rsInvalidTargetUrl = 'Invalid target url';
  rsNewRemoteName = '<New Remote Name>';
  rsInvalidRemoteName = 'remote %d has an invalid name';
  rsInvalidFetchURL = '%s has an invalid Fetch URL';
  rsInvalidPushURL = '%s has an invalid Push URL';
  rsTheBranchesListIsEmpty = 'The branches list is empty';
  rsSIsAnInvalidBranchName = '%s is an invalid branch name';
  rsCopy = 'Copy';
  rsSelectAll = 'Select All';
  rsDeleteS = 'Delete ''%s''';
  rsSomeFilesCouldNotBeDeleted = 'Some files could not be deleted';
  rsCheckOutACommit = 'Check out a commit';
  rsSwitchToThisCommit = 'Switch to this commit';
  rsCreateAPatchFileFromS = 'Create a patch file from %s';
  rsCopyPatchFromSToTheClipboard = 'Copy patch from %s to the clipboard';
  rsApplyingSerialPatchesIsNotYetImplemented = 'Applying serial patches is not'
    +' yet implemented';
  rsInvalidAmountOfPatchesTooApply = 'Invalid amount of patches too apply';
  rsPatchingTheWorkArea = 'Patching the work area';
  rsYouAreTryingToApply = 'You are trying to apply the patch file: %s to '+
    'your working area.'^M^M+
    'This operation will not make a commit.'^M^M+
    'Do you want to apply the patch?';
  rsApplyPatch = 'Apply patch';
  rsApplyingAPatch = 'Applying a patch';
  rsTheRepositoryURLIsEmpty = 'The repository URL is empty';
  rsInvalidCloneURL = 'Invalid clone URL';
  rsInvalidDirectory = 'Invalid directory';
  rsInvalidRepositoryName = 'Invalid repository name';
  rsInto = 'into: ';
  rsInvalidFileUrl = 'Invalid file url';
  rsTheFileUrlPointsToAnInvalidDirectory = 'The file url points to an invalid '
    +'directory';
  rsLogS = 'Log - %s';
  rsSProducedAnErrorExecutionStoped = '%s produced an error, execution stoped';
  rsCommandSS = 'command %s %s';
  rsWasSuccessful = 'Was successful';
  rsDeletingBranchWarning =
    'You are about to delete branch'^M^M+
    '%s'^M^M+
    'Are you sure to continue?';
  rsDeletingABranch = 'Deleting a branch';
  rsDeleteBranch = 'Delete branch';
  rsSwitchingBranchFailed = 'Switching branch failed';
  rsDeletingTagFailed = 'Deleting tag failed';
  rsDeletingBranchFailed = 'Deleting branch failed';
  rsDeletingARemoteBranchFailed = 'Deleting a remote branch failed';
  rsShowFileHistoryOfS = 'Show File History of %s';
  rsREBASE = 'Rebase';
  rsAM = 'AM';
  rsAMREBASE = 'AM/Rebase';
  rsINTERACTIVEREBASE = 'Interactive Rebase';
  rsREBASEMERGE = 'Rebase/Merge';
  rsCHERRYPICKING = 'Cherry Picking';
  rsREVERTING = 'Reverting';
  rsBISECTING = 'Bisecting';
  rsContinueRebasing = 'continue rebasing';
  rsSkipThisPatch = 'skip this patch';
  rsAbortTheRebase = 'abort the rebase';
  rsQuitTheRebase = 'quit the rebase';
  rsRebaseAction = 'Rebase action';
  rsREVERT = 'Revert';
  rsContinueReverting = 'continue reverting';
  rsAbortTheRevert = 'abort the revert';
  rsQuitTheRevert = 'quit the revert';
  rsRevertAction = 'Revert action';
  rsYouSelectedToSIWillDo = 'You selected to %s, I will do:'^M^M+
              'git %s'^M^M+
              'Do you want to proceed?';
  rsMergeAction = 'Merging action';
  rsAbortTheMerge = 'abort the merge';
  rsPleaseResolveAllConflictsFirst = 'Please resolve all conflicts first';
  rsFixConflictsAndCommitYourChanges = 'Fix conflicts and commit your changes';
  rsContinue = 'Continue';
  rsSkip = 'Skip';
  rsAbort = 'Abort';
  rsQuit = 'Quit';
  rsSInProgress = '%s in progress: ';
  rsCherryPickThisCommit = 'Cherry pick this commit';
  rsRevertThisCommit = 'Revert this commit';
  rsBisect = 'Bisect';
  rsBisectStart = 'Start';
  rsBisectReset = 'Reset';
  rsSelectThisCommitAsS = 'Select this commit as %s';
  rsSSelectedS = '%s selected (%s)';
  rsBisectThisCommitIsS = 'Bisect: This commit is %s';
  rsBisectShowBisectLog = 'Bisect: Show bisect log';
  rsBisectReset2 = 'Bisect: Reset';
  rsTheBisectOperationHasEnded = 'The bisect operation has ended';
  rsNoMatchingRefs = 'no matching refs';
  rsCompareWithSUsingS = 'Compare with %s using %s';
  rsCompareWithWorkingTreeUsingS = 'Compare with working tree using %s';
  rsCompareWithSUsing = 'Compare with %s using';
  rsCompareWithWorkingTreeUsing = 'Compare with working tree using';
  rsOpen = 'Open';
  rsOpenSWithS = 'Open %s with %s';
  rsCompareWithWorkingTree = 'Compare with working tree';
  rsMarkThisForComparision = 'Mark this for comparision';
  rsShowLog = 'Show Log';
  rsSaveFileTo = 'Save file to ...';
  rsSaveDirTo = 'Save directory to ...';
  rsExistingDir = 'Directory already exists';
  rsExistingDirWarning =
    'The directory %s already exists'^M^M+
    'If you proceed, existing files would be overwritten'^M^M+
    'Are you sure to continue?';
  rsProceed = 'Proceed';
  rsCopyPath = 'Copy path';
  rsCopySHA1 = 'Copy SHA-1';
  rsTheFileSDoesNotExistsInTheWorkingArea = 'The file %s does not exists in the working area';
  rsComparingSVsS = 'Comparing %s vs %s';
  rsBase = 'Base';
  rsWorkingTree = 'Working tree';
  rsMarked = 'Marked';
  rsCurrent = 'Current';
  rsOlder = 'Older';
  rsNewer = 'Newer';
  // diff actions
  rsRevertHunk = 'Revert Hunk';
  rsUndoRevertHunk = 'Undo Reverted Hunk';
  rsUndoRevertHunkOn = 'Undo Reverted Hunk on %s';
  rsStageHunk = 'Stage Hunk';
  rsUnstageHunk = 'UnStage Hunk';
  rsStageLine = 'Stage Line';
  rsStageLines = 'Stage Lines';
  rsUnStageLine = 'UnStage Line';
  rsUnStageLines = 'UnStage Lines';
  rsRevertLine = 'Revert line';
  rsRevertLines = 'Revert lines';
  rsUndoRevertLineOn = 'Undo Reverted Line on %s';
  rsUndoRevertLinesOn = 'Undo Reverted Lines on %s';

implementation

end.

