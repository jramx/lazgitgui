{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  Shows the git Log, tree, changes and file history.
}
unit unitformlog;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LCLType, LazLoggerBase,
  Forms, Controls, Clipbrd, Graphics, Dialogs, ComCtrls, grids, ButtonPanel,
  unitcommon, unitifaces, unitconfig, unithighlighterhelper, unitgitmgr, unitframelog,
  unitsyneditextras, unitformmgr, unitgittypes, unitwidgetset;

type

  { TfrmLog }

  TfrmLog = class(TForm, IObserver)
    btnPanel: TButtonPanel;
    fLog: TframeLog;
    sbar: TStatusBar;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure txtSearchEnter(Sender: TObject);
    procedure txtSearchExit(Sender: TObject);
  private
    fGitMgr: TGitMgr;
    fhlHelper: THighlighterHelper;
    function GetPickCommit: boolean;
    function  GetStandAlone: boolean;
    procedure OnCommitDblCLick(Sender: TObject);
    procedure SetGitMgr(AValue: TGitMgr);
    procedure SetHlHelper(AValue: THighlighterHelper);
    procedure ObservedChanged(Sender: TObject; what: Integer; data: PtrInt);
    procedure SetPickCommit(AValue: boolean);
    procedure SetStandAlone(AValue: boolean);
    procedure UpdateCaption;
  public
    procedure Clear;
    property GitMgr: TGitMgr read fGitMgr write SetGitMgr;
    property HlHelper: THighlighterHelper read fhlHelper write SetHlHelper;
    property StandAlone: boolean read GetStandAlone write SetStandAlone;
    property PickCommit: boolean read GetPickCommit write SetPickCommit;
  end;

  procedure LogLauncher(sender:TObject);
  function PickCommit(gitMgr: TGitMgr; hlHelper: THighlighterHelper; out item: TLogItem): boolean;
  procedure ShowLog(gitMgr: TGitMgr; hlHelper: THighlighterHelper; selCommit:string);

var
  frmLog: TfrmLog = nil;

implementation

procedure LogLauncher(sender: TObject);
var
  formMgr: TformMgr absolute sender;
  F: TfrmLog;
begin

  if not formMgr.OpenDirectory(formMgr.Target) then begin
    Application.Terminate;
    exit;
  end;

  F := TfrmLog.Create(Application);
  F.GitMgr := formMgr.GitMgr;
  F.HlHelper := formMgr.HlHelper;
  F.StandAlone := true;
  try
    F.ShowModal;
  finally
    F.Free;

    PreserveClipboard;

    Application.Terminate;
  end;

end;

// This function launches a modal log window for picking a commit hash
function PickCommit(gitMgr: TGitMgr; hlHelper: THighlighterHelper; out
  item: TLogItem): boolean;
var
  F: TfrmLog;
  res: Integer;
  intfLog: ILog;
begin
  F := TfrmLog.Create(Application);
  F.GitMgr := gitMgr;
  F.HlHelper := hlHelper;
  F.StandAlone := true;
  F.PickCommit := true;
  try
    res := F.ShowModal;
    result := res=mrOk;
    if result then begin
      intfLog := F.fLog;
      if intfLog.LocateCurrentItem then
        item := intfLog.CurrentItemPtr^;
      intfLog := nil;
    end;
  finally
    F.Free;
  end;
end;

procedure ShowLog(gitMgr: TGitMgr; hlHelper: THighlighterHelper;
  selCommit: string);
begin
  if frmLog=nil then begin
    frmLog := TfrmLog.Create(Application);
    frmLog.GitMgr := GitMgr;
    frmLog.HlHelper := HlHelper;
  end;
  frmLog.Show;
  frmLog.fLog.SearchLog(selCommit, true, 1, [SEARCHIN_COMMIT]);
  frmLog.BringToFront;
end;

{$R *.lfm}

{ TfrmLog }

procedure TfrmLog.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  CloseAction := caHide;
end;

procedure TfrmLog.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  fConfig.OpenConfig;
  fConfig.WriteWindow(Self, 'frmlog', SECTION_GEOMETRY);
  fConfig.WriteInteger('frmlog.panbrowser.height', flog.panBrowser.Height, SECTION_GEOMETRY);
  fConfig.WriteInteger('frmlog.panfiles.width', flog.panFiles.Width, SECTION_GEOMETRY);
  fConfig.CloseConfig;
end;

procedure TfrmLog.FormCreate(Sender: TObject);
var
  col: TCollectionItem;
  gcol: TGridColumn;
  i: Integer;
begin
  // assign tags to columns, they are used instead of column titles to locate columns
  // as titles are translatable, we don't use columns index as columns can be moved
  for i:=0 to fLog.gridLog.Columns.Count-1 do
    fLog.gridLog.Columns[i].Tag := i;

  fConfig.OpenConfig;
  fLog.Config := fConfig;
  fConfig.ReadWindow(Self, 'frmlog', SECTION_GEOMETRY);
  fLog.panBrowser.Height := fConfig.ReadInteger('frmlog.panbrowser.height', fLog.panBrowser.Height, SECTION_GEOMETRY);
  fLog.panFiles.Width := fConfig.ReadInteger('frmlog.panfiles.width', fLog.panFiles.width, SECTION_GEOMETRY);
  for col in fLog.gridLog.Columns do begin
    gcol := TGridColumn(col);
    if gcol.SizePriority=0 then
      gcol.Width := fConfig.ReadInteger('frmlog.grid.coltag'+IntToStr(gCol.Tag)+'.width', gcol.width, SECTION_GEOMETRY);
  end;
  fConfig.CloseConfig;

  fLog.OnCommitDblClick := @OnCommitDblCLick;

  AddPopupMenu(fLog.txtViewer);
end;

procedure TfrmLog.FormDestroy(Sender: TObject);
begin
  fLog.Clear;
  GitMgr := nil;
end;

procedure TfrmLog.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState
  );
begin
  if key=VK_ESCAPE then
    close;
end;

procedure TfrmLog.FormShow(Sender: TObject);
begin
  fLog.Active := true;

  if StandAlone then
    fGitMgr.UpdateStatus();
end;

procedure TfrmLog.txtSearchEnter(Sender: TObject);
begin
  if PickCommit then
    btnPanel.DefaultButton := pbNone;
end;

procedure TfrmLog.txtSearchExit(Sender: TObject);
begin
  if PickCommit then
    btnPanel.DefaultButton := pbOk;
end;

procedure TfrmLog.SetGitMgr(AValue: TGitMgr);
begin
  if fGitMgr = AValue then Exit;

  if (AValue=nil) and (fGitMgr<>nil) then
    fGitMgr.RemoveObserver(self);

  fGitMgr := AValue;

  if fGitMgr<>nil then
    fGitMgr.AddObserver(self);

  fLog.GitMgr := fGitMgr;
end;

function TfrmLog.GetStandAlone: boolean;
begin
  result := fLog.StandAlone;
end;

procedure TfrmLog.OnCommitDblCLick(Sender: TObject);
begin
  if PickCommit then
    ModalResult := mrOk;
end;

function TfrmLog.GetPickCommit: boolean;
begin
  result := btnPanel.Visible;
end;

procedure TfrmLog.SetHlHelper(AValue: THighlighterHelper);
begin
  if fhlHelper = AValue then Exit;
  fhlHelper := AValue;

  fLog.HlHelper := fhlHelper;
end;

procedure TfrmLog.ObservedChanged(Sender: TObject; what: Integer; data: PtrInt);
begin
  case what of
    GITMGR_EVENT_UpdateStatus,
    GITMGR_EVENT_REFLISTCHANGED:
      UpdateCaption;
  end;
end;

procedure TfrmLog.SetPickCommit(AValue: boolean);
begin
  btnPanel.Visible := AValue;
end;

procedure TfrmLog.SetStandAlone(AValue: boolean);
begin
  fLog.StandAlone := AValue;
end;

procedure TfrmLog.UpdateCaption;
var
  s: string;
begin
  s := fGitMgr.GitState.AsString;
  if s<>'' then begin
    if fGitMgr.GitState.BisectFinished then
      s += ' - ' + rsTheBisectOperationHasEnded;
    s := ' (' + s + ')';
  end;

  if fGitMgr.GitState.DetachedHead then
    s := ' - ' + fGitMgr.Branch + ' @ ' + copy(fGitMgr.BranchOID, 1, 7) + s
  else
    s := ' - ' + fGitMgr.Branch + s;

  Caption := format(rsLogS, [ fGitMgr.Git.TopLevelDir + s]);
end;

procedure TfrmLog.Clear;
begin
  fLog.Clear;
end;

end.

