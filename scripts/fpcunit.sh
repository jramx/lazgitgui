#!/bin/bash

# [options] <message> [<file> [<contents> [<tag>]]]
fpcunit_testing () {
  # Aparently gitk won't draw this right
  # LazGitGui, TortoiseGit and git log --graph --all do it ok  init_repository
  # Setup
  test_commit A
  test_commit --both-tag B
  git checkout -b topic
  test_commit E
  test_commit F
  git checkout -b fix1
  test_commit I
  test_commit --both-tag J
  git checkout topic
  test_commit G
  git checkout -b fix2
  test_commit --both-tag K
  git checkout topic
  test_commit H
  git checkout main
  git merge --no-ff -m "C" topic
  git tag C
  git tag -a -m "Cx" Cx
  test_commit D
}

