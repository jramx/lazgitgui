#!/bin/bash

TRASH_DIR=~/temp/trash
indir=$TRASH_DIR/workdir
REPO_DIR=$TRASH_DIR/repo.dir
DEFAULT_BRANCH=main
LAZGITGUI_DIR=~/work/bit/lazgitgui
TESTTOOL=~/git/git/t/helper/test-tool

if [ "$OSTYPE" == "msys" ]; then
  EOL="\r"
fi

make_dir () {
  rm -rf $TRASH_DIR
  mkdir -p $indir
  cd $indir
}

init_repository () {

  if [ "$1" == "--bare" ]; then
    # setup the remote repository
    shift
    git init --bare $REPO_DIR
    git -C $REPO_DIR symbolic-ref HEAD refs/heads/$DEFAULT_BRANCH
    
    # clone the remote repository
    git clone file://$REPO_DIR $indir
  else 
    # only work 2.28.0
    # https://stackoverflow.com/q/42871542
    # git init --initial-branch=$DEFAULT_BRANCH
    git init
    git checkout -b $DEFAULT_BRANCH
  fi
}

# Usage: write_line [options] <afile> <message>
write_line () {

  append=
  
	while test $# != 0
	do
		case "$1" in
      --append)
        append=yes
        ;;
      *)
        break
        ;;
		esac
		shift
	done
  
  if test -n $append; then
    echo -e "$2$EOL" >> "$1"
  else
    echo -e "$2$EOL" > "$1"
  fi
  
}

# from test-lib.sh

exec 4>&2
exec 5>&1
exec 6<&0
exec 7>&2

error () {
  echo "error: $*"
}

BUG () {
	error >&7 "bug in the test script: $*"
}

# from test-lib-functions.sh

# Usage: test_commit [options] <message> [<file> [<contents> [<tag>]]]
# <file>, <contents>, and <tag> all default to <message>.
test_commit () {

  tag=light
  append=
  
	while test $# != 0
	do
		case "$1" in
      --append)
        append=yes
        ;;
      --no-tag)
        tag=none
        ;;
      --annotate)
        tag=annotate
        ;;
      --both-tag)
        tag=both
        ;;
      *)
        break
        ;;
		esac
		shift
	done
  
  file=${2:-"$1.t"}
  
  if test -n $append; then
    echo -e "${3-$1}$EOL" >> "$file"
  else
    echo -e "${3-$1}$EOL" > "$file"
  fi
  
  git add "$file"
  git commit -m "$1"
  
	case "$tag" in
    none)
      ;;
    light)
      git tag "${4:-$1}"
      ;;
    annotate)
      git tag -a -m "$1" "${4:-$1}"
      ;;
    both)
      git tag "${4:-$1}"
      git tag -a -m "$1x" "${4:-$1}x"
      ;;
	esac
  
}


# Tests that its two parameters refer to the same revision, or if '!' is
# provided first, that its other two parameters refer to different
# revisions.
test_cmp_rev () {
	local op='=' wrong_result=different

	if test $# -ge 1 && test "x$1" = 'x!'
	then
	    op='!='
	    wrong_result='the same'
	    shift
	fi
	if test $# != 2
	then
		BUG "test_cmp_rev requires two revisions, but got $#"
	else
		local r1 r2
		r1=$(git rev-parse --verify "$1") &&
		r2=$(git rev-parse --verify "$2") || return 1

		if ! test "$r1" "$op" "$r2"
		then
			cat >&4 <<-EOF
			error: two revisions point to $wrong_result objects:
			  '$1': $r1
			  '$2': $r2
			EOF
			return 1
		fi
	fi
}

# debugging-friendly alternatives to "test [-f|-d|-e]"
# The commands test the existence or non-existence of $1
test_path_is_file () {
	# test "$#" -ne 1 && BUG "1 param"
	if ! test -f "$1"
	then
		echo "File $1 doesn't exist"
		false
	fi
}


# Wrapper for grep which used to be used for
# GIT_TEST_GETTEXT_POISON=false. Only here as a shim for other
# in-flight changes. Should not be used and will be removed soon.
test_i18ngrep () {
	eval "last_arg=\${$#}"

	test -f "$last_arg" ||
	BUG "test_i18ngrep requires a file to read as the last parameter"

	if test $# -lt 2 ||
	   { test "x!" = "x$1" && test $# -lt 3 ; }
	then
		BUG "too few parameters to test_i18ngrep"
	fi

	if test "x!" = "x$1"
	then
		shift
		! grep "$@" && return 0

		echo >&4 "error: '! grep $@' did find a match in:"
	else
		grep "$@" && return 0

		echo >&4 "error: 'grep $@' didn't find a match in:"
	fi

	if test -s "$last_arg"
	then
		cat >&4 "$last_arg"
	else
		echo >&4 "<File '$last_arg' is empty>"
	fi

	return 1
}

# Deprecated wrapper for "git init", use "git init" directly instead
# Usage: test_create_repo <directory>
test_create_repo () {
	git init "$@"
}
