#!/bin/bash

# from t7060-wstatus.sh

# A/A conflict
conflict_AA () {
  init_repository
  # Setup
  test_commit A
  test_commit B oneside added
  git checkout A^0
  test_commit C oneside created
  git checkout B^0
  git merge C
}

# M/D conflict does not segfault
conflict_MD () {
  init_repository
  test_commit initial foo ""
  test_commit modify foo foo
  git checkout -b side HEAD^
  git rm foo
  git commit -m delete
  git merge main
}

conflict_renameunmerged () {
  conflict_MD
  
  # Setup
  git rm -f -r .
  cat "$LAZGITGUI_DIR/readme.md" > ONE
  git add ONE
  git commit -m "One commit with ONE"
  echo -e Modified > TWO
  cat ONE >> TWO
  cat ONE >> THREE
  git add TWO THREE
  sha1=$(git rev-parse :ONE)
  git rm --cached ONE
  (
    echo "100644 $sha1 1	ONE" &&
    echo "100644 $sha1 2	ONE" &&
    echo "100644 $sha1 3	ONE"
  ) | git update-index --index-info
  echo Further >> THREE
  
  # Status
  git status -suno
  git diff-index --cached --name-status HEAD
  git diff-index --cached -M --name-status HEAD
  git diff-index --cached -C --name-status HEAD
}

# conflicts with add and rm advice (deleted by them)
conflict_AA_UD () {

  conflict_renameunmerged
  
  git reset --hard
  git checkout main
  test_commit init main.txt init
  git checkout -b second_branch
  git rm main.txt
  git commit -m "main.txt deleted on second_branch"
  test_commit second conflict.txt second
  git checkout main
  test_commit on_second main.txt on_second
  test_commit main conflict.txt main
  git merge second_branch
  git status --untracked-files=no
}

# conflicts with add and rm advice (both deleted)
conflict_DD_UA_AU () {

  conflict_AA_UD
  
  git reset --hard
  git checkout -b conflict  
  test_commit one main.txt one
  git branch conflict_second
  git mv main.txt sub_main.txt
  git commit -m "main.txt renamed in sub_main.txt"
  git checkout conflict_second
  git mv main.txt sub_second.txt
  git commit -m "main.txt renamed in sub_second.txt"
  git merge conflict
  git status --untracked-files=no
}

# conflicts with only rm advice (both deleted)
conflict_DD () {
  conflict_DD_UA_AU
  
  git reset --hard conflict_second
  git merge conflict
  git add sub_main.txt
  git add sub_second.txt
  git status --untracked-files=no
  git reset --hard
  git checkout main
}
