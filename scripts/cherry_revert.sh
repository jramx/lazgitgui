#!/bin/bash

# from t3501-revert-cherry-pick.sh

# Setup
cherryrevert_setup () {
  init_repository

  for l in a b c d e f g h i j k l m n o
	do
		echo $l$l$l$l$l$l$l$l$l || return 1
	done > oops
  git add oops
  git commit -m initial
  git tag initial

  echo "Add extra line at the end" >>oops
  git commit -a -m added
  git tag added

  git mv oops spoo
  git commit -m rename1
  git tag rename1

  git checkout -b side initial
  git mv oops opos
  git commit -m rename2
  git tag rename2
}

cherryrevert_cherry_pick_nonsense () {

  cherryrevert_setup

  pos=$(git rev-parse HEAD)
  git diff --exit-code HEAD
  git cherry-pick --nonsense 2>msg
  git diff --exit-code HEAD "$pos"
  echo "[Uu]sage: "
  echo msg
}

cherryrevert_revert_nonsense () {

  cherryrevert_cherry_pick_nonsense

  pos=$(git rev-parse HEAD)
  git diff --exit-code HEAD
  git revert --nonsense 2>msg
  git diff --exit-code HEAD "$pos"
  test_i18ngrep "[Uu]sage:" msg
}


# the following two test cherry-pick and revert with renames
#
# --
#  + rename2: renames oops to opos
# +  rename1: renames oops to spoo
# +  added:   adds extra line to oops
# ++ initial: has lines in oops

# cherry-pick after renaming branch
cherryrevert_cherry_pick_rename_branch () {

  cherryrevert_revert_nonsense

  git checkout rename2
  git cherry-pick added
  test_cmp_rev rename2 HEAD^
  grep "Add extra line at the end" opos
  git reflog -1 | grep cherry-pick

}

# revert after renaming branch
cherryrevert_revert_rename_branch () {

  cherryrevert_cherry_pick_rename_branch

  git checkout rename1
  git revert added
  test_cmp_rev rename1 HEAD^
  test_path_is_file spoo
  test_cmp_rev initial:oops HEAD:spoo
  git reflog -1 | grep revert

}

# cherry-pick on stat-dirty working tree
cherryrevert_stat_dirty () {

  cherryrevert_revert_rename_branch

  git clone . copy
  (
    cd copy
    git checkout initial
    $TESTTOOL chmtime +40 oops
    git cherry-pick added
  )
}

# revert forbidden on dirty working tree
cherryrevert_revert_forbidden () {

  cherryrevert_stat_dirty

	echo content >extra_file
	git add extra_file
	git revert HEAD 2>errors
	test_i18ngrep "your local changes would be overwritten by " errors
}

# cherry-pick on unborn branch
cherryrevert_cherry_pick_unborn () {

  cherryrevert_revert_forbidden

	git checkout --orphan unborn
	git rm --cached -r .
	rm -rf *
	git cherry-pick initial
	git diff --quiet initial
	test_cmp_rev ! initial HEAD

}

# cherry-pick "-" to pick from previous branch'
cherryrevert_cherry_pick_previous () {

  cherryrevert_cherry_pick_unborn

	git checkout unborn
	test_commit to-pick actual content
	git checkout main
	git cherry-pick -
	echo content >expect
	test_cmp expect actual

}

# cherry-pick "-" is meaningless without checkout
cherryrevert_cherry_pick_meaningless () {

  cherryrevert_cherry_pick_previous

	test_create_repo afresh
	(
		cd afresh
		test_commit one
		test_commit two
		test_commit three
		git cherry-pick -
	)

}

# cherry-pick "-" works with arguments
cherryrevert_cherry_pick_works () {

  cherryrevert_cherry_pick_meaningless

	git checkout -b side-branch
	test_commit change actual change
	git checkout main
	git cherry-pick -s -
	echo "Signed-off-by: C O Mitter <committer@example.com>" >expect
	git cat-file commit HEAD | grep ^Signed-off-by: >signoff
	test_cmp expect signoff
	echo change >expect
	test_cmp expect actual
}
