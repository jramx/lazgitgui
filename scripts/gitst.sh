#!/bin/bash

. ./base.sh
. ./status.sh
. ./cherry_revert.sh
. ./fpcunit.sh
. ./misc.sh

make_dir

#conflict_AA
#conflict_MD
#conflict_renameunmerged
#conflict_AA_UD
#conflict_DD_UA_AU
#conflict_DD
#fpcunit_testing
#cherryrevert_cherry_pick_works
#bisect_tester
#both_modified
ammend_tester