#!/bin/bash

#REPO=$HOME/temporal/repo.git
REPO=$(pwd)/repo.git
MAXCOMMITS=2
DRY=

DELAY=0
REPOA=1
REPOB=0
REPOC=0
COUNT=0

# Cases
DoBaseCommits=0
SampleMergeToRight=0
SampleTwoBranches=0
SampleRebaseSimple=0
SampleRebaseConflict=0
SampleRebaseManSimple=0
SampleRebaseManSimpleCommon=0
SampleRebaseManOnto=0
SampleRebaseManOnto2=0
SampleRebaseRemoval=0
SampleSimpleRepo=1

if [ "$OSTYPE" == "msys" ]; then
  EOL="\r"
fi

GIT () {
  if [ "$DRY" == "" ]; then
    case "$#" in
      1)
      git "$1"
      ;;
      2)
      git "$1" "$2"
      ;;
      3)
      git "$1" "$2" "$3"
      ;;
    esac
  else
    echo "git $1 $2 $3 $4 $5 $6 $7 $8"
  fi
}

CD () {
  if [ "$DRY" == "" ]; then
    cd $1
  else
    echo "cd $1"
  fi
}

makerepo () {
  if [ ! -d repo.git ]; then
    echo "Initializing a bare repository"
    GIT init --bare repo.git
  else
    echo "The repository repo.git already exists"
  fi
}

clonerepo () {
  if [ ! -d $1 ]; then
    echo "Cloning sample repository $1"
    GIT clone file://$REPO $1
  else
    echo "The reposository $1 already exists"
  fi  
}

clonerepos () {
  (( REPOA )) && clonerepo gita
  (( REPOB )) && clonerepo gitb
  (( REPOC )) && clonerepo gitc
}

docommits () {

  echo "Creating commits in $1"

  CD $1

  FILE=notes_$1.txt
  if [ "$DRY" == "" ]; then
    BRANCH=$(git symbolic-ref --short HEAD)
  else
    BRANCH=XYZ
  fi

  test -n "$2" && EXTRA=" - $2"


  if [ ! -f $FILE ]; then
    (( COUNT++ ))
    echo -e "Initial text$EOL" > $FILE
    GIT add $FILE
    GIT commit -m "$COUNT-$1-$BRANCH: Added file $FILE"
  else
    if [[ "$OSTYPE" == "darwin"* ]]; then
      FILESIZE=$(stat -f %z "$FILE")
    else
      FILESIZE=$(stat -c%s "$FILE")
    fi
    MSG=" - now with $FILESIZE bytes"
  fi

  for i in $(seq 1 $MAXCOMMITS); do
    (( COUNT++ ))
    echo -e "Line $i$MSG$EXTRA$EOL" >> $FILE
    GIT add $FILE
    GIT commit -m "$COUNT - $1-$BRANCH: added Line $i$MSG$EXTRA"
    if [ "$DRY" == "" ]; then
      (( DELAY )) && sleep 1
    else
      echo "sleep 1"
    fi
  done

  EXTRA=

  CD ..

}

simplecommit () {

  # $1 Dir/Repository
  # $2 File Name
  # $3 File Content
  # $4 Commit msg
  
  if [ "$#" == "2" ]; then
    FILE=$2.txt
    CONT="This is $FILE"
    CMSG=$2
  elif [ "$#" == "3" ]; then
    FILE=$2.txt
    CONT="This is $FILE"
    CMSG=$3
  else
    FILE=$2
    CONT="$3"
    CMSG="$4"
  fi

  echo "Simple Commit: Repo=$1 File=$FILE Content=$CONT Msg=CMSG"

  CD $1
  
  echo -e "$CONT$EOL" >> $FILE
  GIT add $FILE
  GIT commit -m "$CMSG"
  (( DELAY )) && sleep 1

  EXTRA=

  CD ..

}

createbranch () {
  CD $1
  GIT branch $2
  test -n "$3" && GIT checkout $2
  CD ..
}

switchbranch () {
  CD $1
  GIT checkout $2
  CD ..
}

mergebranch () {
  CD $1
  GIT merge $2
  CD ..
}

# A simple repository for testing stuff
SimpleRepository () {

  echo "Creating commits in $1"

  CD $1

  FILE=notes_$1.txt
  if [ "$DRY" == "" ]; then
    BRANCH=$(git symbolic-ref --short HEAD)
  else
    BRANCH=XYZ
  fi

  for i in $(seq 1 10); do
    (( COUNT++ ))
    echo -e "$COUNT$EOL" >> $FILE
  done

  GIT add $FILE

  GIT commit -m "$COUNT-$1-$BRANCH: Added file $FILE"
}

# Recreate Merge to right
MergeToRight () {
  docommits $1
  docommits $1
  docommits $1
  createbranch $1 algo checkout
  docommits $1
  switchbranch $1 master
  docommits $1
  mergebranch $1 algo
}

TwoBranches () {
  docommits $1
  docommits $1
  docommits $1
  createbranch $1 algo checkout
  docommits $1
  switchbranch $1 master
  docommits $1
  switchbranch $1 algo
  docommits $1
  switchbranch $1 master
}

RebaseSimple () {
  docommits $1
  docommits $1
  docommits $1
  createbranch $1 algo checkout
  docommits $1
  switchbranch $1 master
  docommits $1
  switchbranch $1 algo
  docommits $1
  switchbranch $1 master
}

RebaseConflict () {
  docommits $1
  docommits $1
  docommits $1
  createbranch $1 algo checkout
  docommits $1 AAA
  switchbranch $1 master
  docommits $1 BBB
  switchbranch $1 algo
  docommits $1
  switchbranch $1 master
}

RebaseManSimple () {
  simplecommit $1 D
  simplecommit $1 E
  createbranch $1 topic
  simplecommit $1 F
  simplecommit $1 G
  switchbranch $1 topic
  simplecommit $1 A
  simplecommit $1 B
  simplecommit $1 C
}

RebaseManSimpleCommon () {
  simplecommit $1 D
  simplecommit $1 E
  createbranch $1 topic
  simplecommit $1 A "A'"  
  simplecommit $1 F
  switchbranch $1 topic
  simplecommit $1 A
  simplecommit $1 B
  simplecommit $1 C
}

RebaseManOnto () {
  simplecommit $1 A
  simplecommit $1 B
  createbranch $1 next checkout
  simplecommit $1 E
  simplecommit $1 F
  simplecommit $1 G
  simplecommit $1 H
  simplecommit $1 I
  createbranch $1 topic checkout
  simplecommit $1 J
  simplecommit $1 K
  simplecommit $1 L
  switchbranch $1 master
  simplecommit $1 C
  simplecommit $1 D
  simplecommit $1 E
}

RebaseManOnto2 () {
  simplecommit $1 A
  simplecommit $1 B
  simplecommit $1 C
  simplecommit $1 D
  createbranch $1 topicA checkout
  simplecommit $1 E
  simplecommit $1 F
  simplecommit $1 G
  createbranch $1 topicB checkout
  simplecommit $1 H
  simplecommit $1 I
  simplecommit $1 J
}

RebaseRemoval () {
  simplecommit $1 X
  createbranch $1 topicA checkout
  simplecommit $1 E
  simplecommit $1 F
  simplecommit $1 G
  simplecommit $1 H
  simplecommit $1 I
  simplecommit $1 J
}

BaseCommits () {
  (( REPOA )) && docommits gita
  (( REPOB )) && docommits gitb
  (( REPOC )) && docommits gitc
}

if [ "$1" == "clean" ]; then
    echo "cleaning..."
    rm -rf repo.git
    rm -rf gita
    rm -rf gitb
    rm -rf gitc
fi

makerepo
clonerepos

(( REPOC )) && repo=gitc
(( REPOB )) && repo=gitb
(( REPOA )) && repo=gita

(( DoBaseCommits )) && BaseCommits

(( SampleMergeToRight )) && MergeToRight $repo
(( SampleTwoBranches )) && TwoBranches $repo
(( SampleRebaseSimple )) && RebaseSimple $repo
(( SampleRebaseConflict )) && RebaseConflict $repo
(( SampleRebaseManSimple )) && RebaseManSimple $repo
(( SampleRebaseManSimpleCommon )) && RebaseManSimpleCommon $repo
(( SampleRebaseManOnto )) && RebaseManOnto $repo
(( SampleRebaseManOnto2 )) && RebaseManOnto2 $repo
(( SampleRebaseRemoval )) && RebaseRemoval $repo
(( SampleSimpleRepo )) && SimpleRepository $repo

