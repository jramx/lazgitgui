#!/bin/bash

DIR=$HOME/tt
REPO=$DIR/repo.git
NAME=gita
GITDIR=$DIR/$NAME

MAXCOMMITS=2
DRY=

DELAY=0
COUNT=0

#set -x

# Cases
DoBaseCommits=0
SampleMergeToRight=0
SampleTwoBranches=0
SampleRebaseSimple=0
SampleRebaseConflict=1
SampleRebaseManSimple=0
SampleRebaseManSimpleCommon=0
SampleRebaseManOnto=0
SampleRebaseManOnto2=0
SampleRebaseRemoval=0

if [ "$OSTYPE" == "msys" ]; then
  EOL="\r"
fi

GIT () {
  if [ "$DRY" == "" ]; then
    case "$#" in
      1)
      git -C $GITDIR "$1"
      ;;
      2)
      git -C $GITDIR "$1" "$2"
      ;;
      3)
      git -C $GITDIR "$1" "$2" "$3"
      ;;
    esac
  else
    echo "git $1 $2 $3 $4 $5 $6 $7 $8"
  fi
}

makerepo () {
  mkdir -p $DIR 
  if [ ! -d $REPO ]; then
    echo "Initializing a bare repository"
    
    git init --bare $REPO
  else
    echo "The repository $REPO already exists"
  fi
}

clonerepo () {
  if [ ! -d $GITDIR ]; then
    echo "Cloning sample repository $1"
    if [ "$DRY" == "" ]; then 
      git clone file://$REPO $GITDIR
    else
      echo "git clone file://$REPO $GITDIR"
    fi
  else
    echo "The reposository $REPODIR already exists"
  fi  
}

docommits () {

  echo "Creating commits in $GITDIR"

  FILE=notes_$NAME.txt
  if [ "$DRY" == "" ]; then
    BRANCH=$(git -C $GITDIR symbolic-ref --short HEAD)
  else
    BRANCH=XYZ
  fi
 
  test -n "$1" && EXTRA=" - $1"


  if [ ! -f $GITDIR/$FILE ]; then
    (( COUNT++ ))
    echo -e "Initial text$EOL" > $GITDIR/$FILE
    GIT add $FILE
    GIT commit -m "$COUNT-$NAME-$BRANCH: Added file $FILE"
  else
    if [[ "$OSTYPE" == "darwin"* ]]; then
      FILESIZE=$(stat -f %z "$GITDIR/$FILE")
    else
      FILESIZE=$(stat -c%s "$GITDIR/$FILE")
    fi
    MSG=" - now with $FILESIZE bytes"
  fi

  for i in $(seq 1 $MAXCOMMITS); do
    (( COUNT++ ))
    echo -e "Line $i$MSG$EXTRA$EOL" >> $GITDIR/$FILE
    GIT add $FILE
    GIT commit -m "$COUNT - $NAME-$BRANCH: added Line $i$MSG$EXTRA"
    if [ "$DRY" == "" ]; then
      (( DELAY )) && sleep 1
    else
      echo "sleep 1"
    fi
  done

  EXTRA=

}

simplecommit () {

  # $1 File Name
  # $2 File Content
  # $3 Commit msg
  
  if [ "$#" == "1" ]; then
    FILE=$1.txt
    CONT="This is $FILE"
    CMSG=$1
  elif [ "$#" == "2" ]; then
    FILE=$1.txt
    CONT="This is $FILE"
    CMSG=$2
  else
    FILE=$1
    CONT="$2"
    CMSG="$3"
  fi

  echo "Simple Commit: Repo=$GITDIR File=$FILE Content=$CONT Msg=CMSG"

  echo -e "$CONT$EOL" >> $GITDIR/$FILE
  GIT add $FILE
  GIT commit -m "$CMSG"
  (( DELAY )) && sleep 1

  EXTRA=

}


createbranch () {
  GIT branch $1
  test -n "$2" && GIT checkout $1
}

switchbranch () {
  GIT checkout $1
}

mergebranch () {
  GIT merge $1
}

# Recreate Merge to right
MergeToRight () {
  docommits
  docommits
  docommits
  createbranch algo checkout
  docommits
  switchbranch master
  docommits
  mergebranch algo
}

TwoBranches () {
  docommits
  docommits
  docommits
  createbranch algo checkout
  docommits
  switchbranch master
  docommits
  switchbranch algo
  docommits
  switchbranch master
}

RebaseSimple () {
  docommits
  docommits
  docommits
  createbranch algo checkout
  docommits
  switchbranch master
  docommits
  switchbranch algo
  docommits
  switchbranch master
}

RebaseConflict () {
  docommits
  docommits
  docommits
  createbranch algo checkout
  docommits AAA
  switchbranch master
  docommits BBB
  switchbranch algo
  docommits
  #switchbranch master
  git rebase master algo
}

RebaseManSimple () {
  simplecommit D
  simplecommit E
  createbranch topic
  simplecommit F
  simplecommit G
  switchbranch topic
  simplecommit A
  simplecommit B
  simplecommit C
}

RebaseManSimpleCommon () {
  simplecommit D
  simplecommit E
  createbranch topic
  simplecommit A "A'"  
  simplecommit F
  switchbranch topic
  simplecommit A
  simplecommit B
  simplecommit C
}

RebaseManOnto () {
  simplecommit A
  simplecommit B
  createbranch next checkout
  simplecommit E
  simplecommit F
  simplecommit G
  simplecommit H
  simplecommit I
  createbranch topic checkout
  simplecommit J
  simplecommit K
  simplecommit L
  switchbranch master
  simplecommit C
  simplecommit D
  simplecommit E
}

RebaseManOnto2 () {
  simplecommit A
  simplecommit B
  simplecommit C
  simplecommit D
  createbranch topicA checkout
  simplecommit E
  simplecommit F
  simplecommit G
  createbranch topicB checkout
  simplecommit H
  simplecommit I
  simplecommit J
}

RebaseRemoval () {
  simplecommit X
  createbranch topicA checkout
  simplecommit E
  simplecommit F
  simplecommit G
  simplecommit H
  simplecommit I
  simplecommit J
}

BaseCommits () {
  docommits
}

test -z "$DIR" && echo "invalid dir" && exit
  
[ "$DIR" == "$HOME" ] && echo "invalid dir" && exit

if [ "$1" == "clean" ]; then
  echo "cleaning $DIR ..."  
  rm -rf $DIR
fi

makerepo
clonerepo

# trap read debug

(( DoBaseCommits )) && BaseCommits

(( SampleMergeToRight )) && MergeToRight $repo
(( SampleTwoBranches )) && TwoBranches $repo
(( SampleRebaseSimple )) && RebaseSimple $repo
(( SampleRebaseConflict )) && RebaseConflict $repo
(( SampleRebaseManSimple )) && RebaseManSimple $repo
(( SampleRebaseManSimpleCommon )) && RebaseManSimpleCommon $repo
(( SampleRebaseManOnto )) && RebaseManOnto $repo
(( SampleRebaseManOnto2 )) && RebaseManOnto2 $repo
(( SampleRebaseRemoval )) && RebaseRemoval $repo
