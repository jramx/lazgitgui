#!/bin/bash

# [options] <message> [<file> [<contents> [<tag>]]]
bisect_tester () {
  init_repository
  # Setup
  test_commit --no-tag A
  test_commit B
  test_commit --no-tag "Started log.txt" log.txt "Line 1"
  git checkout -b topic
  test_commit --no-tag E
  test_commit --no-tag F
  git checkout main
  write_line --append log.txt "Line 2"
  write_line --append log.txt "Line 3"
  git add log.txt
  git commit -m "Updated log.txt"
  test_commit --no-tag C
  test_commit D
  #git bisect start
  #git bisect good B
}

both_modified () {
  init_repository
  # Setup
  test_commit A

  cat >testregexpr.lpr <<EOF
program testregexpr;

{$mode objfpc}{$H+}

uses
  SysUtils, Classes, RegExpr;

const
  INPUTSTRING = 'Docs: LCL/forms. Updates topics for code changes in c187dc8f, cbafa073, 5f82f6ba, c5c2329f.';
  EXPRESSION  = '(\s|#)([0-9a-fA-F]{6,})';

procedure UseRegExpr;
var
  reg: TRegExpr;
  offset: Integer;
  curTicks: QWord;
begin
  WriteLn;
  WriteLn('Using RegExpr:TRegExpr');
  curTicks := GetTickCount64;
  reg := TRegExpr.Create;
  try
    reg.InputString := INPUTSTRING;
    reg.Expression:= EXPRESSION;
    offset := 1;
    with reg do
      while Exec(offset) do begin
        WriteLn(Format('  Match %s found at %d len=%d', [QuotedStr(Match[0]), MatchPos[0], MatchLen[0]]));
        offset := MatchPos[0] + MatchLen[0];
      end;
  finally
    reg.Free;
    WriteLn('  It took ',GetTickCount64-curTicks, ' ticks');
  end;
end;

begin
  WriteLn('Compiler Version: ' + {$I %FPCVERSION%} + '  Target: ' + {$I %FPCTARGETCPU} + '-' + {$I %FPCTARGETOS});
  UseRegExpr;
end.
EOF
  git add testregexpr.lpr
  git commit -m "Added testregexpr.lpr"
  git checkout -b topic
  sed -i "s/reg.Expression:= EXPRESSION;/reg.Expression:= ''+EXPRESSION;/" testregexpr.lpr
  sed -i "s/reg.InputString := INPUTSTRING;/reg.InputString := ''+INPUTSTRING;/" testregexpr.lpr
  git add testregexpr.lpr
  git commit -m "Pre quotes"
  git checkout main
  sed -i "s/reg.Expression:= EXPRESSION;/reg.Expression:= EXPRESSION''+;/" testregexpr.lpr
  sed -i "s/reg.InputString := INPUTSTRING;/reg.InputString := INPUTSTRING+'';/" testregexpr.lpr
  git add testregexpr.lpr
  git commit -m "Post quotes"
  git merge topic
}

# 
ammend_tester () {
  init_repository --bare
  # Setup
  test_commit --no-tag A
  test_commit B
  test_commit --no-tag "Started log.txt" log.txt "Line 1"
  test_commit --no-tag C
  test_commit --no-tag D
  write_line --append log.txt "Line 2"
  write_line --append log.txt "Line 3"
  git add log.txt
  git rm A.t
  git mv B.t b.t
  echo -e "New File$EOL" > new.txt
  git add new.txt
  #return 0
  git commit -m "Several changes
Other Line"
  echo -e "Archivo Nuevo$EOL" > nuevo.txt
  echo -e "New A.t file, coincidental with the commited one$EOL" > A.t
}
