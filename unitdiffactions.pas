{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  Several actions that can be made on the current diff of a modified file
}
unit unitdiffactions;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Dialogs, Menus, SynEdit,
  unitcommon, unitgitutils, unitcommit, unitentries, unitifaces, unitgitmgr,
  unitguitools;

type
  THunkType = (htHeader, htHunk, htLine, htLines);
  THunkRec = record
    hunkType: THunkType;
    fromLine, toLine: Integer;
    diff: string;
    path: string;
  end;

  TLineRange = record
    lineStart,
    lineEnd: Integer;
  end;

  THunkSizes = record
    left, leftSize: Integer;
    right, rightSize: Integer;
  end;

  { TDiffActions }

  TDiffActions = class
  private
    fEnabled: boolean;
    fGitMgr: TGitMgr;
    fHunks: array of THunkRec;
    fUndoHunks: array of THunkRec;
    fTxt: TSynEdit;
    fLastEntry: PFileEntry;
    fUnstaged: Boolean;
    fLineRange: TLineRange;
    procedure OnRevertHunk(Sender: TObject);
    procedure OnStageHunk(Sender: TObject);
    procedure OnStageLine(Sender: TObject);
    procedure OnRevertLine(Sender: TObject);
    procedure OnUndoRevertHunk(Sender: TObject);
    procedure AppendRange(aStart, aEnd: Integer; var s:string; le:string=LineEnding);
    procedure AppendRangeIfStarts(aStart, aEnd: Integer; var s:string; aSet:TSysCharSet; le:string=LineEnding);
    function  ApplyPatch(patch: string; action: string): boolean;
    procedure OnUnStageHunk(Sender: TObject);
    function  LastUndoHunksIndex(aPath: string): Integer;
    function  LineRangeChanges: boolean;
    function  HunkHeaderToSizes(hdr: string; out sizes: THunkSizes): boolean;
    function  AddUndoRevert(cmd, aPath: string; aType: THunkType): Integer;
    function  GetChangedLinesPatch(revert: boolean; out patch, action: string; out hasMoreChanges: boolean): boolean;
    procedure UpdateStateLineChanges(target: Integer; revert: boolean; patch: string; hasMoreChanges: boolean);
  public
    constructor Create(aEditor: TSynEdit);
    destructor Destroy; override;
    procedure Clear;
    procedure ParseDiff(entry: PFileEntry; unstaged: boolean);
    procedure AddPopupItems(mousePos: TPoint);
    function  MouseToLine(mousePos: TPoint): Integer;
    function  MouseToHunk(mousePos: TPoint; out range:TLineRange): Integer;
    function  LineToHunk(aLine: Integer): Integer;

    property GitMgr: TGitMgr read fGitMgr write fGitMgr;
    property Enabled: boolean read fEnabled write fEnabled;
  end;

implementation

const
  MENUITEM_TAG            = 1000;
  DIFFACTION_REVERSE      = '--reverse';
  DIFFACTION_REVERSEUNDO  = '';
  DIFFACTION_STAGE        = '--cached';
  DIFFACTION_STAGEUNDO    = '--reverse --cached';
  TARGET_UNSTAGED         = 0;
  TARGET_STAGED           = 1;

type
  TDiffActionsMenuItem = class(TMenuItem);

{ TDiffActions }

procedure TDiffActions.OnUndoRevertHunk(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  i: Integer;
  path: String;
begin

  i := mi.Tag - MENUITEM_TAG;
  if ApplyPatch(fUndoHunks[i].diff, '') then begin
    // success
    path := fUndoHunks[i].path;

    // remove the undo entry
    Delete(fUndoHunks, i, 1);

    // update the current diff view
    if not Enabled then
      // this means we are starting with an unchanged entry path
      // first update the status so our file appear. Use the 'sync'
      // version so the UpdateDiff next is applied in order.
      fGitMgr.UpdateStatusSync
    else
      // just update the current file
      path := '';

    fGitMgr.UpdateDiff(path, TARGET_UNSTAGED);
  end;
end;

procedure TDiffActions.AppendRange(aStart, aEnd: Integer; var s: string; le:string=LineEnding);
var
  i: Integer;
begin
  for i := aStart to aEnd do
    s += fTxt.Lines[i] + le;
end;

procedure TDiffActions.AppendRangeIfStarts(aStart, aEnd: Integer;
  var s: string; aSet: TSysCharSet; le: string);
var
  i: Integer;
  line: string;
begin
  for i := aStart to aEnd do begin
    line := fTxt.Lines[i];
    if (line<>'') and (line[1] in aset) then
      s += line + le;
  end;
end;

function TDiffActions.ApplyPatch(patch: string; action: string): boolean;
var
  st: TStringStream;
  aFile: String;
  cmdOut: RawByteString;
  j: Integer;
begin

  // get temp file holding the patch
  st := TStringStream.Create(patch);
  aFile := GetTempFilename('revert-hunk', action);
  st.SaveToFile(aFile);
  st.free;

  try
    patch := format('apply %s %s',[action, Sanitize(aFile)]);
    result := fGitmgr.Git.Any(patch, cmdOut)<=0;
    if not result then
      ShowMessage(fGitMgr.Git.ErrorLog);
  finally
    DeleteFile(aFile);
  end;

end;

procedure TDiffActions.OnUnStageHunk(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  i: Integer;
  cmd: string;
begin

  i := mi.tag - MENUITEM_TAG;
  cmd := '';
  AppendRange(fHunks[0].fromLine, fHunks[0].toLine, cmd, #10);
  AppendRange(fHunks[i].fromLine, fHunks[i].toLine, cmd, #10);

  if ApplyPatch(cmd, DIFFACTION_STAGEUNDO) then begin

    // Is this the last hunk?
    if (Length(fHunks)<3) then begin
      fGitMgr.UpdateStatusSync;
      Enabled := false;
    end else
      fGitMgr.UpdateDiff('', TARGET_STAGED);

  end;
end;

function TDiffActions.LastUndoHunksIndex(aPath: string): Integer;
var
  i: Integer;
begin
  result := -1;
  for i:=Length(fUndoHunks)-1 downto 0 do begin
    if fUndoHunks[i].path=aPath then begin
      result := i;
      break;
    end;
  end;
end;

function TDiffActions.LineRangeChanges: boolean;
var
  i: Integer;
  s: String;
begin
  result := true;
  for i:=fLineRange.lineStart to fLineRange.lineEnd do begin
    s := fTxt.Lines[i];
    if s='' then continue; // what???
    if not (s[1] in ['-','+']) then begin
      result := false;
      break;
    end;
  end;
end;

function TDiffActions.HunkHeaderToSizes(hdr: string; out sizes: THunkSizes
  ): boolean;

  function StrToSize(s:string; out line, size: Integer): boolean;
  var
    p: Integer;
  begin
    p := pos(',', s);
    if p<=0 then begin
      size := 1;
      result := TryStrToInt(s, line);
    end else begin
      result := TryStrToInt(copy(s, p+1, MAXINT), size);
      if result then
        result := TryStrToInt(copy(s, 1, p-1), line);
    end;
  end;
var
  m, p: Integer;
begin
  result := false;
  p := pos('@@ -', hdr);
  if p<=0 then exit;
  delete(hdr, 1, p+3);
  m := pos(' +', hdr);
  if m<=0 then exit;
  p := pos(' @@', hdr);
  if p<=0 then exit;
  result := StrToSize(copy(hdr, 1, m-1), sizes.left, sizes.leftSize);
  result := result and StrToSize(copy(hdr, m+2, p-m-2), sizes.right, sizes.rightSize);
end;

function TDiffActions.AddUndoRevert(cmd,aPath:string;aType:THunkType):Integer;
begin
  result := Length(fUndoHunks);

  SetLength(fUndoHunks, result+1);
  fUndoHunks[result].diff := cmd;
  fUndoHunks[result].path := aPath;
  fUndoHunks[result].hunkType := aType;
end;

// for Stage/Unstage lines we use the algorithm used by git-gui
// see: diff.tcl proc apply_or_revert_range_or_line
function TDiffActions.GetChangedLinesPatch(revert: boolean; out patch,
  action: string; out hasMoreChanges: boolean): boolean;
var
  h, i, m, n: Integer;
  sizes: THunkSizes;
  toContext: Char;
  context, line, patchHdr: string;
begin

  result := false;

  // find first line of current hunk
  // in what hunk are we?
  h := LineToHunk(fLineRange.lineStart);

  // get the header hunk sizes
  if not HunkHeaderToSizes(fTxt.Lines[fHunks[h].fromLine], sizes) then begin
    ShowMessage('Invalid patch: no hunk header');
    exit;
  end;

  // are the lines in the lineStart-lineRange the only changes in the hunk?
  // ingnore the hunk header and the lines in the lineStart-lineEnd range
  hasMoreChanges := false;
  for i:=fHunks[h].fromLine+1 to fHunks[h].toLine do
    if (i>=fLineRange.lineStart) and (i<=fLineRange.lineEnd) then
      continue
    else
    if fTxt.Lines[i]='' then begin
      ShowMessage('Invalid hunk detected');
      exit;
    end else if fTxt.Lines[i][1]<>' ' then begin
      hasMoreChanges := true;
      break;
    end;

  if not fUnstaged then begin
    toContext := '+';
    action := DIFFACTION_STAGEUNDO;
  end else begin
    if revert then begin
      toContext := '+';
      action := DIFFACTION_REVERSE;
    end else begin
      toContext := '-';
      action := DIFFACTION_STAGE;
    end;
  end;

  patch := '';
  context := '';
  m := 0;
  n := 0;

  // loop through all the hunk lines but skip the hunk header
  for i := fHunks[h].fromLine + 1 to fHunks[h].toLine do begin

    line := fTxt.Lines[i];

    if (i>=fLineRange.lineStart) and (i<=fLineRange.lineEnd) and (line[1] in ['+','-']) then
    begin
      // a line to stage/unstage
      if line[1]='-' then begin
        inc(n);
        patch += context + line + #10;
        context := '';
      end else begin
        inc(m);
        patch += line + #10;
      end;
    end else
    if not (line[1] in ['+','-']) then begin
      // context line
      patch += context + line + #10;
      if pos('\ ', line)=0 then begin
        inc(m);
        inc(n);
      end;
      context := '';
    end else
    if line[1] = toContext then begin
      // turn change line into context line
      line[1] := ' ';
      if toContext='-' then
        context += line + #10
      else
        patch += line + #10;

      inc(m);
      inc(n);
    end else begin
      // a change in the opposite direction of
      // to_context which is outside the range of
      // lines to apply.
      patch += context;
      context := '';
    end;
  end;

  patchHdr := '';
  AppendRange(fHunks[0].fromLine, fHunks[0].toLine, patchHdr, #10);

  patch := patchHdr + format('@@ -%d,%d +%d,%d @@',[sizes.left, n, sizes.left, m]) + #10 + patch;

  result := true;
end;

procedure TDiffActions.UpdateStateLineChanges(target: Integer; revert: boolean;
  patch: string; hasMoreChanges: boolean);
var
  needFullUpdate, needDiffUpdate: Boolean;
begin
  needFullUpdate := false;
  needDiffUpdate := false;

  if target=TARGET_UNSTAGED then begin

    // and undo line revert
    if revert then begin
      if fLineRange.lineStart=fLineRange.lineEnd then
        AddUndoRevert(patch, fLastEntry^.path, htLine)
      else
        AddUndoRevert(patch, fLastEntry^.path, htLines)
    end;

    // Is this entry already in the index?
    if fLastEntry^.EntryTypeStaged in [etNotUpdatedA, etNotUpdatedD, etNotUpdatedM] then
      // no, have to do a full update
      needFullUpdate := true;

    // to know if this is the last hunk, we have to know if this lines are the
    // only change in hunk, if there are no more changes only then check
    if (not hasMoreChanges) and (Length(fHunks)<3) then needFullUpdate := true
    else                                                needDiffUpdate := true;

  end else begin
    // if is not yet in the unstaged list, do a full update
    if not EntryTypeInList(fLastEntry^.EntryTypeUnStaged, false) then
      needFullUpdate := true;

    if (not hasMoreChanges) and (length(fHunks)<3) then needFullUpdate := true
    else                                                needDiffUpdate := true;
  end;

  // if need full update, check if we will need diff update
  // if yes, then do update in sync mode, if no, update and disable
  if needFullUpdate then begin
    if needDiffUpdate then
      fGitMgr.UpdateStatusSync
    else begin
      fGitMgr.UpdateStatus;
      Enabled := false;
    end;
  end;

  // if need diff update but full update was not needed just update
  // the current entry, on contrary, locate first the entry in the list
  if needDiffUpdate then begin
    if not needFullUpdate then fGitMgr.UpdateDiff('', target)
    else                       fGitMgr.UpdateDiff(fLastEntry^.path, target);
  end;

end;

procedure TDiffActions.OnRevertHunk(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  i, j: Integer;
  cmd: string;
begin
  i := mi.tag - MENUITEM_TAG;
  cmd := '';
  AppendRange(fHunks[0].fromLine, fHunks[0].toLine, cmd);
  AppendRange(fHunks[i].fromLine, fHunks[i].toLine, cmd);

  if ApplyPatch(cmd, '--reverse') then begin

    AddUndoRevert(cmd, fLastEntry^.Path, htHunk);

    // if currently there are 2 or less fHunks it means that
    // we just reverted the last hunk, queue a full update status
    // if there are enough hunks, the file is still modified and
    // just reload the current patch
    if Length(fHunks)<3 then begin
      fGitMgr.UpdateStatus;
      // the patch content is not valid anymore
      Enabled := false;
    end else
      fGitMgr.UpdateDiff('', TARGET_UNSTAGED);

  end;
end;

procedure TDiffActions.OnStageHunk(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  i, j: Integer;
  cmd: string;
  needFullUpdate, needDiffUpdate: boolean;
begin

  i := mi.tag - MENUITEM_TAG;
  cmd := '';
  AppendRange(fHunks[0].fromLine, fHunks[0].toLine, cmd, #10);
  AppendRange(fHunks[i].fromLine, fHunks[i].toLine, cmd, #10);

  if ApplyPatch(cmd, DIFFACTION_STAGE) then begin

    needFullUpdate := false;
    needDiffUpdate := false;

    // Is this entry already in the index?
    if fLastEntry^.EntryTypeStaged in [etNotUpdatedA, etNotUpdatedD, etNotUpdatedM] then
      // no, have to do a full update
      needFullUpdate := true;

    // Is this the last hunk?
    if Length(fHunks)<3 then  needFullUpdate := true
    else                      needDiffUpdate := true;

    // if need full update, check if we will need diff update
    // if yes, the do update in sync mode, if no, update and disable
    if needFullUpdate then begin
      if needDiffUpdate then
        fGitMgr.UpdateStatusSync
      else begin
        fGitMgr.UpdateStatus;
        Enabled := false;
      end;
    end;

    // if need diff update but full update was not needed just update
    // the current entry, on contrary, locate first the entry in the list
    if needDiffUpdate then begin
      if not needFullUpdate then fGitMgr.UpdateDiff('', TARGET_UNSTAGED)
      else                       fGitMgr.UpdateDiff(fLastEntry^.path, TARGET_UNSTAGED);
    end;

  end;
end;

procedure TDiffActions.OnStageLine(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  patch, action: string;
  hasMoreChanges: Boolean;
begin

  if not GetChangedLinesPatch(false, patch, action, hasMoreChanges) then
    exit;

  if ApplyPatch(patch, action) then
    UpdateStateLineChanges(mi.Tag, false, patch, hasMoreChanges);

end;

procedure TDiffActions.OnRevertLine(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  patch, action: string;
  hasMoreChanges: Boolean;
begin

  if not GetChangedLinesPatch(true, patch, action, hasMoreChanges) then
    exit;

  if ApplyPatch(patch, action) then
    UpdateStateLineChanges(mi.Tag, true, patch, hasMoreChanges);

end;

constructor TDiffActions.Create(aEditor: TSynEdit);
begin
  inherited Create;
  fTxt := aEditor;
end;

destructor TDiffActions.Destroy;
begin
  if fLastEntry<>nil then
    Dispose(fLastEntry);
  inherited Destroy;
end;

procedure TDiffActions.Clear;
begin
  fHunks := nil;
end;

// this is called when a selection in the unstaged or staged lists is detected
// if fTxt is loaded with diff content, store the state in fLastEntry and fUnstaged
// then find all the available hunks and their line ranges.
procedure TDiffActions.ParseDiff(entry: PFileEntry; unstaged: boolean);
var
  fromLine, i, j: Integer;
  newHunk: Boolean;
  lines: TStrings;
begin

  fEnabled := true;

  if (fLastEntry=nil) then
    New(fLastEntry);

  fLastEntry^ := entry^;
  fUnstaged := unstaged;

  lines := fTxt.Lines;

  fHunks := nil;

  fromLine := 0;
  for i := 0 to lines.Count-1 do begin
    newHunk := (i=lines.Count-1) or (pos('@@', lines[i])=1);
    if newHunk then begin
      j := Length(fHunks);
      SetLength(fHunks, j+1);
      if fromLine = 0 then  fHunks[j].hunkType := htHeader
      else                  fHunks[j].hunkType := htHunk;
      fHunks[j].fromLine := fromLine;
      fHunks[j].toLine := i;
      if i<lines.Count-1 then
        fHunks[j].toLine -= 1;
      fromLine := i;
    end;
  end;
end;

// Add context menu items to the diff control popup menu
procedure TDiffActions.AddPopupItems(mousePos: TPoint);
var
  pop: TPopupMenu;
  i, j, ix: Integer;
  mi: TMenuItem;
  onPath: string;
  sep, lrChanges: boolean;
begin

  // exit if a diff file has not been parsed yet
  if fLastEntry=nil then
    exit;

  pop := fTxt.PopupMenu;

  // first remove any previously added diff actions menu items so we
  // start with a fresh popup menu. After this, it will contains only
  // the std 'copy' and 'select all' menu items.
  for i:=pop.Items.Count-1 downto 0 do begin
    mi := pop.Items[i];
    if mi is TDiffActionsMenuItem then
      pop.Items.Delete(i);
  end;

  // find (right) clicked hunk index and line range
  i := MouseToHunk(mousePos, fLineRange);

  // we don't just append the menu items to the popup menu, we insert them
  // in an orderly fashion, before the standard menu items.
  ix := 0;

  // check first a hunk was (right) clicked.
  if i>0 then begin

    // if the source is the unstaged list add a Stage hunk menu item else
    // we are using the staged list and add an UnStage hunk menu item
    if fUnStaged then
      AddPopItem(pop, rsStageHunk, @OnStageHunk, MENUITEM_TAG + i, '', TDiffActionsMenuItem, ix)
    else
      AddPopItem(pop, rsUnStageHunk, @OnUnStageHunk, MENUITEM_TAG + i, '', TDiffActionsMenuItem, ix);
    inc(ix);

    // if there is no selection and the mouse cursor is over a changed line
    // or if there is a selection and the selection has only changed lines
    // insert a menu item for the respective action. 'j' (as the menuitem tag)
    // below, is used to decide if the action will stage or unstage lines
    // but only if lines with changes were clicked.
    lrChanges := LineRangeChanges;
    if lrChanges then with fLineRange do begin
      if fUnStaged then begin
        j := TARGET_UNSTAGED;
        if lineStart=lineEnd then onPath := rsStageLine
        else                      onPath := rsStageLines;
      end else begin
        j := TARGET_STAGED;
        if lineStart=lineEnd then onPath := rsUnStageLine
        else                      onPath := rsUnStageLines;
      end;
      AddPopItem(pop, onPath, @OnStageLine, j, '', TDiffActionsMenuItem, ix);
      inc(ix);
    end;

    // add a separator
    AddPopItem(pop, '-', nil, MENUITEM_TAG, '', TDiffActionsMenuItem, ix);
    inc(ix);
  end;

  // if the source is the Unstaged list, and a hunk is under the mouse cursor
  // add a revert hunk (m.i) and check if we will need a separator item later
  sep := (i>0) and fUnstaged;
  if sep then begin
    // Add Revert hunk menu item
    // We have to add MENUITEM_TAG to tag because the popup menu is a
    // TSEPopupMenu which uses tag from 0 to 1 to enable or disable
    // the menu items according to the current selection.
    AddPopItem(pop, rsRevertHunk, @OnRevertHunk, MENUITEM_TAG + i, '', TDiffActionsMenuItem, ix);
    inc(ix);
  end;

  if lrChanges and fUnstaged then with fLineRange do begin
    // Add revert line(s) menu item
    if lineStart=lineEnd then onPath := rsRevertLine
    else                      onPath := rsRevertLines;
    AddPopItem(pop, onPath, @OnRevertLine, TARGET_UNSTAGED, '', TDiffActionsMenuItem, ix);
    inc(ix);
  end;

  // Add Undo Revert hunk and line(s) menu item
  if (Length(fUndoHunks)>0) then begin

    // get the last undo hunk that matches the currently selected file entry
    // (fLastEntry), if none checks, just return the last
    j := LastUndoHunksIndex(fLastEntry^.path);
    if j<0 then
      j := Length(fUndoHunks)-1;

    // add the menu item for a hunk or line(s)
    if fUndoHunks[j].hunkType=htHunk then
      onPath := format(rsUndoRevertHunkOn, [QuotedStr(fUndoHunks[j].path)])
    else
    if fUndoHunks[j].hunkType=htLine then
      onPath := format(rsUndoRevertLineOn, [QuotedStr(fUndoHunks[j].path)])
    else
      onPath := format(rsUndoRevertLinesOn, [QuotedStr(fUndoHunks[j].path)]);

    AddPopItem(pop, onPath , @OnUndoRevertHunk, MENUITEM_TAG + j, '', TDiffActionsMenuItem, ix);
    inc(ix);

    // make sure we add a separator menu item
    sep := true;
  end;

  // Add final a separator
  if sep then
    AddPopItem(pop, '-', nil, MENUITEM_TAG, '', TDiffActionsMenuItem, ix);

end;

// given some pixel coordinates returns what line (index, so zero based) was it
// if diff actions are disabled (when viewer is not showing a diff for example)
// returns -1
function TDiffActions.MouseToLine(mousePos: TPoint): Integer;
var
  p: TPoint;
begin
  if not Enabled then
    result := -1
  else begin
    p := fTxt.PixelsToRowColumn(mousePos);
    p.Offset(-1, -1);
    result := p.y;
  end;
end;

// mouse (client coords) to hunk index and range of selected lines (if any)
function TDiffActions.MouseToHunk(mousePos: TPoint; out range: TLineRange
  ): Integer;
begin

  result := -1;

  // what is the (right) clicked line?
  range.lineStart := MouseToLine(MousePos);
  range.lineEnd := range.lineStart;

  // if a valid line was pointed, find if there is a selection and set the
  // range to the starting and ending selected lines.
  if range.lineStart>=0 then begin
    result := LineToHunk(range.lineStart);
    if fTxt.SelAvail then begin
      range.lineStart := fTxt.BlockBegin.Y - 1;
      range.lineEnd := fTxt.BlockEnd.Y - 1;
    end;
  end;

end;

// given a Line (index, base 0) find what hunk corresponds in the diff control
function TDiffActions.LineToHunk(aLine: Integer): Integer;
var
  i: Integer;
begin
  result := -1;
  for i:=0 to Length(fHunks)-1 do begin
    if (fHunks[i].fromLine<=aLine) and (aLine<=fHunks[i].toLine) then begin
      result := i;
      break;
    end;
  end;
end;

end.

