{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  TFormMgr.
  This class is used to launch some forms in LazGitGui as standalone modules.
}
unit unitformmgr;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LazLoggerBase, Forms, Dialogs,
  unitcommon, unitifaces, unitconfig, unitgitmgr, unithighlighterhelper,
  unittextchunks;

type

  TLaunchProcHandler = procedure(sender: TObject);
  PFormLaunchInfo = ^TFormLaunchInfo;
  TFormLaunchInfo = record
    data: string;
  end;

  { TFormMgr }

  TFormMgr = class(TComponent, IObserver)
  private
    fLaunchProc: TLaunchProcHandler;
    fGitMgr: TGitMgr;
    fGit: IGit;
    fhlHelper: THighlighterHelper;
    fTarget: string;
    procedure LaunchForm(Data: PtrInt);
    procedure ObservedChanged(Sender:TObject; what: Integer; data: PtrInt);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function  OpenDirectory(aDir: string): boolean;
    procedure Launch;

    property GitMgr: TGitMgr read fGitMgr write fGitMgr;
    property HlHelper: THighlighterHelper read fhlHelper;
    property LaunchProc: TLaunchProcHandler read fLaunchProc write fLaunchProc;
    property Target: string read fTarget write fTarget;
  end;


implementation

{ TFormMgr }

procedure TFormMgr.ObservedChanged(Sender: TObject; what: Integer;
  data: PtrInt);
begin

end;

function TFormMgr.OpenDirectory(aDir: string): boolean;
begin
  result := false;
  aDir := ExpandFileName(aDir);
  fGit.OpenDir(aDir);
  if fGit.TopLevelDir='' then begin
    ShowMessageFmt(rsCouldnTGetToplevelDirectoryOfS, [aDir]);
    exit;
  end;

  fTextLinks.LoadFromConfig(fGit.TopLevelDir);

  result := true;
end;

procedure TFormMgr.Launch;
begin
  Application.QueueAsyncCall(@LaunchForm, 0);
end;

procedure TFormMgr.LaunchForm(Data: PtrInt);
begin
  if Assigned(fLaunchProc) then
    fLaunchProc(Self);
end;

constructor TFormMgr.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  fhlHelper := THighlighterHelper.create;

  fGitMgr := TGitMgr.Create;
  fGitMgr.Config := fConfig;
  fGitMgr.AddObserver(Self);

  fGit := fGitMgr.Git;
  fConfig.OpenConfig;
  if not fGitMgr.Initialize then begin
    fConfig.CloseConfig;
    DebugLn('Error: Could not find git command');
    Application.Terminate;
    exit;
  end;

  fTextLinks := TTextLinks.Create;
  fConfig.CloseConfig;
end;

destructor TFormMgr.Destroy;
begin
  fGit := nil;
  fGitMgr.RemoveObserver(Self);
  fhlHelper.Free;
  fGitMgr.Free;
  fTextLinks.Free;

  inherited Destroy;
end;

end.

