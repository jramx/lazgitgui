{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  Repository Browser
  This module implements a repository browser, that it's, given a commit hash
  it will show the repository content as it was at that particular commit,
  It's a re-implementation of Log/show changes/tree, there the functionality
  is limited to show file content, here much more operations are possible.
}
unit unitrepobrowser;

{$mode ObjFPC}{$H+}
{$ModeSwitch advancedrecords}

interface

uses
  Classes, SysUtils, fpjson,
  LCLType, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  Grids, Clipbrd, ActnList, StdCtrls, Buttons, Menus, LazLoggerBase,
  Types, unitcommon, unitgittypes, unitgitutils, unitifaces, unitconfig,
  unithighlighterhelper, unitgitmgr, unitvfs, unitcommitbrowser, unitformmgr,
  unitformlog, unitsrceditor, unitrunthread, unitfilehistory,
  unitguitools, unitdifftool, unitmarkeditem, unitwidgetset;

type

  TDummyDirNode = class(TTreeNode)
  end;

  { TfrmRepoBrowser }

  TfrmRepoBrowser = class(TForm, IObserver)
    actSelectCommit: TAction;
    ActionList1: TActionList;
    ImgList: TImageList;
    Label1: TLabel;
    lblDate: TLabel;
    lblSubject: TLabel;
    lblAuthor: TLabel;
    lblRefs: TLabel;
    lblCommit: TLabel;
    panTools: TPanel;
    btnSelectCommit: TSpeedButton;
    popEditor: TPopupMenu;
    sDlg: TSaveDialog;
    sdirDlg:TSelectDirectoryDialog;
    Splitter1: TSplitter;
    sBar: TStatusBar;
    treeFiles: TTreeView;
    view: TDrawGrid;
    procedure actSelectCommitExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure treeFilesExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure treeFilesSelectionChanged(Sender: TObject);
    procedure viewColRowExchanged(Sender: TObject; IsColumn: Boolean; sIndex,
      tIndex: Integer);
    procedure viewCompareCells(Sender: TObject; ACol, ARow, BCol,
      BRow: Integer; var Result: integer);
    procedure viewContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure viewDblClick(Sender: TObject);
    procedure viewDrawCell(Sender: TObject; aCol, aRow: Integer; aRect: TRect;
      aState: TGridDrawState);
    procedure viewHeaderSized(Sender: TObject; IsColumn: Boolean; Index: Integer
      );
  private
    fStartCommit: string;
    fCommitBrowser: TCommitBrowser;
    fGitMgr: TGitMgr;
    fHlHelper: THighlighterHelper;
    fList: TFPList;
    fRoot: TTreeNode;
    fStandAlone: boolean;
    fLogItem: TLogItem;
    fRepoHelper: TMarkedItemHelper;
    procedure CreateDummyDirNodeClass(Sender: TCustomTreeView;
      var NodeClass: TTreeNodeClass);
    function  ExploreNode(Node: TTreeNode; expanding: boolean): boolean;
    function  GetTreeNodePath(Node: TTreeNode): string;
    procedure OnCompareWithWorkingTree(Sender: TObject);
    procedure OnCopyPath(Sender: TObject);
    procedure OnCopySha(Sender: TObject);
    procedure OnCustomEditFile(Sender: TObject);
    procedure OnDiffToolMarked(Sender: TObject);
    procedure OnDiffToolWorkingArea(Sender: TObject);
    procedure OnMarkForComparison(Sender: TObject);
    procedure OnViewFile(Sender: TObject);
    procedure OnOpen(Sender: TObject);
    procedure OnSaveFileTo(Sender: TObject);
    procedure OnSaveDirectoryTo(Sender: TObject);
    procedure OnShowLog(Sender: TObject);
    procedure ShowNode(const node: TTreeNode);
    procedure SetGitMgr(AValue: TGitMgr);
    procedure ObservedChanged(Sender: TObject; what: Integer; data: PtrInt);
    procedure ApplyCommit;
    procedure ReloadTreeFile;
    procedure PopulateTree(node: PvfsNode; parentNode: TTreeNode);
    procedure UpdateCaption;
    procedure ShowFiles(node: PvfsNode);
    function  GetNodeImageIndex(node: PvfsNode): Integer;
    function  ColumnByTag(aTag: Integer): TGridColumn;
    procedure ClearTree;
    procedure DoViewFile(node: PvfsNode);
    procedure DoOpen(node: PvfsNode);
    function  GetGridSelectedNode: PvfsNode;
    function  GetGridNodePath(node: PvfsNode): string;
  public
    property GitMgr: TGitMgr read fGitMgr write SetGitMgr;
    property HlHelper: THighlighterHelper read fHlHelper write fHlHelper;
    property StandAlone: boolean read fStandAlone write fStandAlone;
    property Commit: string read fStartCommit write fStartCommit;
  end;

  { TDrawGridHelper }

  TDrawGridHelper = class helper for TDrawGrid
  public
    procedure StartColumnSort(aIndex: Integer; aSortOrder: TSortOrder);
  end;


  procedure ShowRepoBrowser(gitMgr:TGitMgr; HlHelper: THighlighterHelper; startCommit:string);
  procedure RepoBrowserLauncher(sender:TObject);

var
  frmRepoBrowser: TfrmRepoBrowser;

implementation

{$R *.lfm}

const
  COLTAG_NAME                 = 0;
  COLTAG_EXTENSION            = 1;
  COLTAG_SIZE                 = 2;
  COLTAG_MODE                 = 3;

  PAD_SPACE                   = 3;

  ICON_REPOSITORY             = 0;
  ICON_FILE                   = 1;
  ICON_FOLDERCLOSED           = 2;
  ICON_FOLDEROPENED           = 3;
  ICON_FOLDERPENDING          = 4;

procedure ShowRepoBrowser(gitMgr: TGitMgr; HlHelper: THighlighterHelper;
  startCommit: string);
var
  F: TfrmRepoBrowser;
begin
  F := TfrmRepoBrowser.Create(Application);
  F.Commit := startCommit;
  F.GitMgr := gitMgr;
  F.HlHelper := HlHelper;
  F.StandAlone := true;
  try
    F.ShowModal;
  finally
    F.Free;
  end;
end;

procedure RepoBrowserLauncher(sender: TObject);
var
  formMgr: TformMgr absolute sender;
  F: TfrmRepoBrowser;
begin

  if not formMgr.OpenDirectory(formMgr.Target) then begin
    Application.Terminate;
    exit;
  end;

  F := TfrmRepoBrowser.Create(Application);
  F.GitMgr := formMgr.GitMgr;
  F.StandAlone := true;
  try
    F.ShowModal;
  finally
    F.Free;

    PreserveClipboard;

    Application.Terminate;
  end;

end;

{ TfrmRepoBrowser }

procedure TfrmRepoBrowser.FormCreate(Sender: TObject);
var
  col: TCollectionItem;
  gcol: TGridColumn;
  i: Integer;
  n: TTreeNode;
begin
  // assign tags to columns, they are used instead of column titles to locate columns
  // as titles are translatable, we don't use columns index as columns can be moved
  for i:=0 to view.Columns.Count-1 do
    view.Columns[i].Tag := i;

  fConfig.OpenConfig;

  fConfig.ReadWindow(Self, 'frmRepoBrowser', SECTION_GEOMETRY);
  treeFiles.Width := fConfig.ReadInteger('frmRepoBrowser.tree.width', treeFiles.width, SECTION_GEOMETRY);
  for col in view.Columns do begin
    gcol := TGridColumn(col);
    if gcol.SizePriority=0 then
      gcol.Width := fConfig.ReadInteger('frmRepoBrowser.grid.coltag'+IntToStr(gCol.Tag)+'.width', gcol.width, SECTION_GEOMETRY);
  end;

  fCommitBrowser := TCommitBrowser.Create;
  fCommitBrowser.Config := fConfig;
  fCommitBrowser.Mode := cbmTree;
  fCommitBrowser.ObserverMgr.AddObserver(self);
  fCommitBrowser.WithSizes := true;

  fConfig.CloseConfig;

  view.FocusRectVisible := false;
  view.ColumnClickSorts := true;
  view.Clear;

  fList := TFpList.Create;
end;

procedure TfrmRepoBrowser.FormDestroy(Sender: TObject);
begin
  fList.Free;
  fCommitBrowser.Free;
  fRepoHelper.Free;
end;

procedure TfrmRepoBrowser.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then
    close;
end;

procedure TfrmRepoBrowser.FormShow(Sender: TObject);
begin

  if fStartCommit<>'' then begin
    if fGitMgr.GetCommit(fStartCommit, fLogItem)<=0 then begin
      ApplyCommit;
      exit;
    end;
  end;

  if StandAlone then begin
    fGitMgr.UpdateStatus();
    fGitMgr.UpdateRefList;
  end else begin
    if fGitMgr.RefList.Count=0 then
      fGitMgr.UpdateRefList;
    fGitMgr.GetCommit(fGitMgr.BranchOID, fLogItem);
    ApplyCommit;
  end;
end;

procedure TfrmRepoBrowser.treeFilesExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  //DebugLn('Expanding node %s',[Node.Text]);
  AllowExpansion := ExploreNode(Node, true);
end;

procedure TfrmRepoBrowser.treeFilesSelectionChanged(Sender: TObject);
var
  node: TTreeNode;
begin
  node := treeFiles.Selected;
  ExploreNode(node, false);
end;

procedure TfrmRepoBrowser.viewColRowExchanged(Sender: TObject;
  IsColumn: Boolean; sIndex, tIndex: Integer);
begin
  fList.Exchange(sIndex-1, tIndex-1);
end;

function FileTypeToOrd(Info: PInfoNode; ascending: boolean): Integer;
begin
  if Info=nil then
    result := 2
  else if Info^.filetype='tree' then
    result := 0
  else
    result := 1;

  if not ascending then
    result := - result;
end;

procedure TfrmRepoBrowser.viewCompareCells(Sender: TObject; ACol, ARow, BCol,
  BRow: Integer; var Result: integer);
var
  aIndex, bIndex: Integer;
  aNode, bNode: PvfsNode;
  aOrd, bOrd: Integer;
  ascending: boolean;
  aInfo, bInfo: PInfoNode;
begin

  ascending := view.SortOrder<>soDescending;

  aIndex := ARow - view.FixedRows;
  aNode := PvfsNode(fList[aIndex]);
  aInfo := PInfoNode(aNode^.Data);
  aOrd := FileTypeToOrd(aInfo, ascending);

  bIndex := BRow - view.FixedRows;
  bNode := PvfsNode(fList[bIndex]);
  bInfo := PInfoNode(bNode^.Data);
  bOrd := FileTypeToOrd(bInfo, ascending);

  result := aOrd - bOrd;

  if result=0 then begin

    case view.Columns[ACol].tag of

      COLTAG_NAME:
        result := compareText(aNode^.name, bNode^.name);

      COLTAG_EXTENSION:
        begin
          result := CompareText(ExtractFileExt(aNode^.name), ExtractFileExt(bNode^.name));
          if result=0 then begin
            // same extension, compare filenames always ascending
            result := compareText(aNode^.name, bNode^.name);
            exit;
          end;
        end;

      COLTAG_SIZE:
        begin
          result := aInfo^.fileSize - bInfo^.fileSize;
          if result=0 then begin
            // same size, compare filenames always ascending
            result := compareText(aNode^.name, bNode^.name);
            exit;
          end;
        end;

      COLTAG_MODE:
        begin
          result := CompareText(aInfo^.filemode, bInfo^.filemode);
          if result=0 then begin
            // same mode, compare filenames always ascending
            result := compareText(aNode^.name, bNode^.name);
            exit;
          end;
        end;

    end;
  end;

  if view.SortOrder=soDescending then
    result := -result;
end;

procedure TfrmRepoBrowser.viewContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
var
  i, count: Integer;
  vfsNode: PvfsNode;
  info: PInfoNode;
  isDir: Boolean;
  cEditor: string;
  arr: TJSONArray;
  obj: TJSONObject;
  aGit: IGit;
begin

  popEditor.Items.Clear;
  aGit := fGitMgr.Git;

  vfsNode := GetGridSelectedNode;
  info := PInfoNode(vfsNode^.Data);
  isDir := info^.filetype='tree';
  //DebugLn('%s: tree: %s',[vfsNode^.name, info^.fileTree]);

  AddPopItem(popEditor, rsOpen, @OnOpen, PtrInt(vfsNode));
  if not isDir then begin
    cEditor := fConfig.ReadString('name', '', 'editor');
    if cEditor<>'' then
      AddPopItem(popEditor,
                 format(rsOpenSWithS, [QuotedStr(vfsNode^.Name), QuotedStr(cEditor)]),
                 @OnCustomEditFile,
                 PtrInt(vfsNode));
  end;
  AddPopItem(popEditor, '-', nil, 0);
  if not isDir then begin
    AddPopItem(popEditor, rsCompareWithWorkingTree, @OnCompareWithWorkingTree, PtrInt(vfsNode));
    fConfig.OpenConfig;
    fRepoHelper.AddPopMenuCompare(popEditor, @OnDiffToolWorkingArea, false);
    AddPopItem(popEditor, '-', nil, 0);
    AddPopItem(popEditor, rsMarkThisForComparision, @OnMarkForComparison, PtrInt(vfsNode));

    fRepoHelper.AddPopMenuCompare(popEditor, @OnDiffToolMarked, true);
    fConfig.CloseConfig;

    AddPopItem(popEditor, '-', nil, 0);
  end;

  AddPopItem(popEditor, rsShowLog, @OnShowLog, PtrInt(vfsNode));

  AddPopItem(popEditor, '-', nil, 0);
  if not isDir then
    AddPopItem(popEditor, rsSaveFileTo, @OnSaveFileTo, PtrInt(vfsNode))
  else
    AddPopItem(popEditor, rsSaveDirTo, @OnSaveDirectoryTo, PtrInt(vfsNode));

  AddPopItem(popEditor, '-', nil, 0);
  AddPopItem(popEditor, rsCopyPath, @OnCopyPath, PtrInt(vfsNode));
  AddPopItem(popEditor, rsCopySHA1, @OnCopySha, PtrInt(vfsNode));
end;

procedure TfrmRepoBrowser.viewDblClick(Sender: TObject);
var
  vfsNode: PvfsNode;
begin
  vfsNode := GetGridSelectedNode;
  DoOpen(vfsNode);
end;

procedure TfrmRepoBrowser.viewDrawCell(Sender: TObject; aCol, aRow: Integer;
  aRect: TRect; aState: TGridDrawState);
var
  imgIndex: Integer;
  alignment: TAlignment;
  s: String;
  vfsNode: PvfsNode;
  info: PInfoNode;

  procedure TextOut(txt: string);
  var
    ts: TTextStyle;
  begin
    ts := view.Canvas.TextStyle;
    ts.Alignment := alignment;
    view.Canvas.TextRect(aRect, aRect.Left, aRect.Top, txt);
    view.Canvas.TextStyle := ts;
  end;

begin
  if aRow>=view.FixedRows then begin

    vfsNode := PvfsNode(fList[aRow - view.FixedRows]);
    if vfsNode=nil then
      exit; // WTF??
    info := PInfoNode(vfsNode^.Data);

    alignment := view.Columns[aCol].Alignment;
    aRect.Right := aRect.Right - 3;

    case view.Columns[aCol].tag of
      COLTAG_NAME:
        begin
          imgIndex := GetNodeImageIndex(vfsNode);
          ImgList.Draw(view.Canvas, aRect.Left, aRect.Top, imgIndex);
          aRect.Left := aRect.Left + 16 + PAD_SPACE;
          TextOut(vfsNode^.name);
        end;
      COLTAG_EXTENSION:
        begin
          ARect.Left := ARect.Left + PAD_SPACE;
          TextOut(ExtractFileExt(vfsNode^.name));
        end;
      COLTAG_SIZE:
        if info<>nil then begin
          ARect.Right := aRect.Right - PAD_SPACE;
          TextOut(FormatFloat('#,###', info^.fileSize));
        end;
      COLTAG_MODE:
        if info<>nil then begin
          ARect.Left := aRect.Left + PAD_SPACE;
          TextOut(info^.filemode);
        end;
    end;
  end;
end;

procedure TfrmRepoBrowser.viewHeaderSized(Sender: TObject; IsColumn: Boolean;
  Index: Integer);
var
  col: TGridColumn;
begin
  if isColumn then begin
    col := view.Columns[Index];
    fConfig.WriteInteger('frmRepoBrowser.grid.coltag'+IntToStr(col.tag)+'.width', col.Width, SECTION_GEOMETRY);
  end;
end;

procedure TfrmRepoBrowser.SetGitMgr(AValue: TGitMgr);
begin
  if fGitMgr = AValue then Exit;

  if (AValue=nil) and (fGitMgr<>nil) then
    fGitMgr.RemoveObserver(self);

  fGitMgr := AValue;

  if fGitMgr<>nil then
    fGitMgr.AddObserver(self);

  fCommitBrowser.GitMgr := fGitMgr;

  if fGitMgr<>nil then
    fRepoHelper := TMarkedItemHelper.Create(fGitMgr, 'repobrowser');
end;

procedure TfrmRepoBrowser.CreateDummyDirNodeClass(Sender: TCustomTreeView;
  var NodeClass: TTreeNodeClass);
begin
  NodeClass := TDummyDirNode;
end;

function TfrmRepoBrowser.ExploreNode(Node: TTreeNode; expanding: boolean
  ): boolean;
var
  aPath: string;
  child: PvfsNode;
  vfsNode: PvfsNode;
  N: TTreeNode;
begin
  result := true;

  aPath := GetTreeNodePath(Node);

  N := Node.GetFirstChild;
  if N is TDummyDirNode then begin
    Node.ImageIndex := ICON_FOLDEROPENED;
    Node.SelectedIndex := ICON_FOLDEROPENED;
    result := false;
    if Node.Data<>nil then begin
      // ask the commit browser we need expanding this node
      vfsNode := fCommitBrowser.ExpandVfs(aPath);
      // this vfsNode should correspond to the data of treenode 'node'
      // add the childs of vfsNode as childs of 'node'
      result := vfsNode<>nil;
      if result then begin
        N.Delete;
        child := vfsNode^.childs;
        while child<>nil do begin
          PopulateTree(child, Node);
          child := child^.next;
        end;
      end;
    end;

    if expanding then
      exit;
  end;

  ShowNode(Node);
end;

function TfrmRepoBrowser.GetTreeNodePath(Node: TTreeNode): string;
begin
  result := Node.GetTextPath;
  result := copy(result, Length(fRoot.Text) + 2, MAXINT);
end;

procedure TfrmRepoBrowser.OnCompareWithWorkingTree(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  node: PvfsNode;
  aFile, tempFile, workFile: String;
begin
  node := PvfsNode(mi.Tag);
  aFile := GetGridNodePath(node);

  tempFile := GetTempFilename(node^.name, fLogItem.CommitOID);
  workFile := fGitMgr.Git.TopLevelDir + aFile;

  if not FileExists(workFile) then begin
    ShowMessageFmt(rsTheFileSDoesNotExistsInTheWorkingArea, [aFile]);
    exit;
  end;

  ShowMessageFmt(rsComparingSVsS, [ExtractFileName(tempFile), ExtractFileName(aFile)]);

  // TODO: launch comparing workFile vs DoDirSeparators(workfile);
end;

procedure TfrmRepoBrowser.OnCopyPath(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  node: PvfsNode;
begin
  node := PvfsNode(mi.Tag);
  Clipboard.AsText :=GetGridNodePath(node);
end;

procedure TfrmRepoBrowser.OnCopySha(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  node: PvfsNode;
begin
  node := PvfsNode(mi.Tag);
  Clipboard.AsText := PInfoNode(node^.Data)^.fileTree;
end;

procedure TfrmRepoBrowser.OnCustomEditFile(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  node: PvfsNode;
  cmd, args, aFile: String;
  cmdOut: RawByteString;
  info: PInfoNode;
begin

  cmd := fConfig.ReadString('command', '', 'editor');
  if cmd='' then
    exit;

  node := PvfsNode(mi.Tag);
  info := PInfoNode(node^.data);
  aFile := GetTempFilename(node^.name, fLogItem.CommitOID);

  if fGitMgr.ExtractToFile(info^.fileTree, aFile) then begin

    args := fConfig.ReadString('args', '', 'editor');

    if args<>'' then begin
      if not args.EndsWith('=') then
        args += ' ';
      args := ' ' + args;
    end else
      args := ' ';

    cmd += args + Sanitize(aFile);

    RunCommand(cmd, '', cmdOut, true);

  end;

end;

procedure TfrmRepoBrowser.OnDiffToolMarked(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  node: PvfsNode;
  info: PInfoNode;
  labelOlder, labelNewer: string;
  cmdOut: RawByteString;
  dtl: TDiffToolLauncherRec;

  marked1, marked2: TMarkedItem;
  older, newer: PMarkedItem;
  asPicked: Boolean;
begin

  node := GetGridSelectedNode;
  if node=nil then
    exit; // should not happen
  info := PInfoNode(node^.Data);

  marked1 := fRepoHelper.MarkedItem;

  marked2.filename := node^.name;
  marked2.treehash := info^.fileTree;
  marked2.CommitOID := fLogItem.CommitOID;
  marked2.CommiterDate := fLogItem.CommiterDate;

  asPicked := fConfig.ReadBoolean('frmRepoBrowser.CompareVersionsAsPicked', true);

  if asPicked or (marked1.CommiterDate <= marked2.CommiterDate) then begin
    older := @marked1;
    newer := @marked2;
  end else begin
    older := @marked2;
    newer := @marked1;
  end;

  if asPicked then begin
    labelOlder := rsMarked;
    labelNewer := rsCurrent;
  end else begin
    labelOlder := rsOlder;
    labelNewer := rsNewer;
  end;

  initialize(dtl);

  dtl.toolIndex := mi.Tag;
  dtl.gitMgr := fGitMgr;
  dtl.base.filename := ExtractFilename(older^.filename);
  dtl.base.tag := older^.CommitOID;
  dtl.base.tree := older^.treehash;
  dtl.base.meta := format('%s: %s',[labelOlder, GetTempFilename(dtl.base.filename, dtl.base.tag)]);
  dtl.comp.filename := ExtractFilename(newer^.filename);
  dtl.comp.tag := newer^.CommitOID;
  dtl.comp.tree := newer^.treehash;
  dtl.comp.meta := format('%s: %s',[labelNewer, GetTempFilename(dtl.comp.filename, dtl.comp.tag)]);

  LaunchDiffTool(dtl, cmdOut);
end;

procedure TfrmRepoBrowser.OnDiffToolWorkingArea(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  node: PvfsNode;
  info: PInfoNode;
  cmdOut: RawByteString;
  dtl: TDiffToolLauncherRec;
begin

  node := GetGridSelectedNode;
  if node=nil then
    exit; // should not happen
  info := PInfoNode(node^.Data);

  initialize(dtl);

  dtl.toolIndex := mi.Tag;
  dtl.gitMgr := fGitMgr;
  dtl.base.filename := node^.name;
  dtl.base.tag := fLogItem.CommitOID;
  dtl.base.tree := info^.fileTree;
  dtl.base.meta := rsBase;
  dtl.comp.filename := fGitMgr.Git.TopLevelDir + GetGridNodePath(node);
  dtl.comp.meta := rsWorkingTree;

  LaunchDiffTool(dtl, cmdOut);
end;

procedure TfrmRepoBrowser.OnMarkForComparison(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  node: PvfsNode;
  info: PInfoNode;
begin
  node := PvfsNode(mi.Tag);
  info := PInfoNode(node^.Data);
  fRepoHelper.SetMarkedItem(node^.name, info^.fileTree, fLogItem); // TODO: full path
end;

procedure TfrmRepoBrowser.OnViewFile(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  node: PvfsNode;
begin
  node := PvfsNode(mi.Tag);
  DoViewFile(node);
end;

procedure TfrmRepoBrowser.OnOpen(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  node: PvfsNode;
begin
  node := PvfsNode(mi.Tag);
  DoOpen(node);
end;

procedure TfrmRepoBrowser.OnSaveFileTo(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  node: PvfsNode;
  info: PInfoNode;
  stream: TStream;
begin
  node := PvfsNode(mi.Tag);
  info := PInfoNode(node^.data);

  stream := fGitMgr.GetFileStream(info^.fileTree);
  if stream<>nil then begin
    try
      sDlg.FileName := node^.name;
      if sDlg.Execute then
        TMemoryStream(stream).SaveToFile(sDlg.FileName);
    finally
      stream.free;
    end;
  end;
end;

// reference: https://stackoverflow.com/a/69199776
procedure TfrmRepoBrowser.OnSaveDirectoryTo(Sender:TObject);
var
  mi: TMenuItem absolute Sender;
  node: PvfsNode;
  info: PInfoNode;
  aPath, aDest, cmd, aDir: string;
  cmdOut:RawByteString;
  git:IGit;
  res:TModalResult;
begin
  // TODO: check under windows...
  node := PvfsNode(mi.Tag);
  info := PInfoNode(node^.data);
  if info^.filetype<>'tree' then
    exit;
  aPath := GetGridNodePath(node);
  if sdirDlg.execute then begin
    aDir :=  ExtractFileName(aPath);
    aDest := IncludeTrailingPathDelimiter(sDirDlg.FileName) + aDir;
    if DirectoryExists(aDest) then begin
      res := QuestionDlg(rsExistingDir, format(rsExistingDirWarning, [QuotedStr(aDir)]), mtWarning,
                [mrYes, rsProceed, mrCancel, rsCancel], 0 );
      if res<>mrYes then
        exit;
    end else
      ForceDirectories(aDest);
    git := fGitMgr.Git;
    cmd := format('--work-tree=%s restore --source=%s:%s .', [Sanitize(aDest), fLogItem.CommitOID, Sanitize(aPath)]);
    if git.Any(cmd, cmdOut)<=0 then
      ShowMessage('done');
  end;
end;

procedure TfrmRepoBrowser.OnShowLog(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  node: PvfsNode;
  aPath: String;
begin
  node := PvfsNode(mi.Tag);
  aPath := GetGridNodePath(node);
  ShowFileHistory(aPath, fGitMgr, fHlHelper, true);
end;

procedure TfrmRepoBrowser.ShowNode(const node: TTreeNode);
begin
  if node<>nil then begin
    if node = fRoot then
      ShowFiles(nil)
    else begin
      node.ImageIndex := ICON_FOLDEROPENED;
      node.SelectedIndex := ICON_FOLDEROPENED;
      ShowFiles(PvfsNode(node.Data));
    end;
  end;
end;

procedure TfrmRepoBrowser.ObservedChanged(Sender: TObject; what: Integer;
  data: PtrInt);
begin
  case what of
    GITMGR_EVENT_UpdateStatus:
      if StandAlone then begin
        if fLogItem.CommitOID='' then begin
          fGitMgr.GetCommit(fGitMgr.BranchOID, fLogItem);
          ApplyCommit;
        end;
      end;

    COMMITBROWSER_EVENT_RELOAD:
      begin
        ClearTree;
        if data<>0 then
          ReloadTreeFile;
      end;
  end;
end;

procedure TfrmRepoBrowser.ApplyCommit;
var
  refsMap: TRefsMap;
  arr: TRefInfoArray;
  info: PRefInfo;
  s: string;
  n: Integer;

  procedure Add(sub: string);
  begin
    if s<>'' then s += ', ';
    s += sub;
  end;

begin

  // commit header info
  lblCommit.Caption := Copy(fLogItem.CommitOID, 1, 10);
  lblDate.Caption := UnixTimestampToStr(fLogItem.CommiterDate);
  lblAuthor.Caption := format('%s <%s>',[fLogItem.Author, fLogItem.Email]);
  lblSubject.Caption := fLogItem.Subject;

  //DebugLn('CommitID=%s', [fLogItem.CommitOID]);

  // commit references
  refsMap := fGitMgr.RefsMap;
  if (refsMap<>nil) and refsMap.Find(fLogItem.CommitOID, n) then begin

    arr := refsMap.Data[n];

    s := '';
    for info in arr do
      case info^.subType of
        rostTag:
          add(format('tag: %s',[info^.refName]));

        rostTracking:
          add(info^.refName);

        rostLocal:
          if info^.head then  Add('HEAD: '+info^.refName)
          else                Add(info^.refName);
      end;

    if s<>'' then
      s := '(' + s + ')';

  end else
    s := rsNoMatchingRefs;

  lblRefs.Caption := s;

  // feed the commit browser to get vfs data
  fCommitBrowser.LogItem := fLogItem;
  fCommitBrowser.ApplyMode;
  fRoot.Selected := true;
  fRoot.Expand(false);
  //fCommitBrowser.Vfs.Dump;

  UpdateCaption;
end;

procedure TfrmRepoBrowser.ReloadTreeFile;
var
  sibling: PvfsNode;
begin

  treeFiles.Items.Clear;
  fRoot := treeFiles.Items.AddChild(nil, '/');
  fRoot.ImageIndex := ICON_REPOSITORY;
  fRoot.SelectedIndex := ICON_REPOSITORY;

  sibling := fCommitBrowser.vfs.root;
  while sibling<>nil do begin
    PopulateTree(sibling, fRoot);
    sibling := sibling^.Next;
  end;
end;

procedure TfrmRepoBrowser.PopulateTree(node: PvfsNode; parentNode: TTreeNode);
var
  child: PvfsNode;
  info: PInfoNode;
  oldCreateNodeClass: TTVCreateNodeClassEvent;
  newNode: TTreeNode;
begin
  if node=nil then exit;

  info := node^.data;
  if info^.filetype='tree' then begin

    newNode := treeFiles.Items.AddChildObject(parentNode, node^.Name, node);
    newNode.ImageIndex := ICON_FOLDERCLOSED;
    newNode.SelectedIndex := ICON_FOLDERCLOSED;

    if (info<>nil) and (info^.filetype='tree') then begin
      oldCreateNodeClass := treeFiles.OnCreateNodeClass;
      TreeFiles.OnCreateNodeClass := @CreateDummyDirNodeClass;
      TreeFiles.Items.AddChild(newNode, '');
      TreeFiles.OnCreateNodeClass  := oldCreateNodeClass;
      newNode.ImageIndex := ICON_FOLDERPENDING;
      newNode.SelectedIndex := ICON_FOLDERPENDING;
    end else begin
      child := node^.Childs;
      while child<>nil do begin
        PopulateTree(child, newNode);
        child := child^.Next;
      end;
    end;

  end;
end;

procedure TfrmRepoBrowser.UpdateCaption;
begin

end;

procedure TfrmRepoBrowser.ShowFiles(node: PvfsNode);
var
  info: PInfoNode;
  col: TGridColumn;
  i: Integer;
begin
  if node=nil then
    node := fCommitBrowser.vfs.root
  else
    node := node^.childs;

  fList.clear;
  while node<>nil do begin
    info := PInfoNode(node^.Data);
    if (info<>nil) {and (info^.filetype<>'tree')} then
      fList.Add(node);
    node := node^.Next;
  end;

  view.RowCount := view.FixedRows + fList.Count;

  col := ColumnByTag(COLTAG_NAME);
  i := view.Columns.IndexOf(col);

  View.StartColumnSort(i, soAscending);
end;

function TfrmRepoBrowser.GetNodeImageIndex(node: PvfsNode): Integer;
begin
  if PInfoNode(node^.Data)^.filetype='tree' then
    result := ICON_FOLDERCLOSED
  else
    result := ICON_FILE;
end;

function TfrmRepoBrowser.ColumnByTag(aTag: Integer): TGridColumn;
var
  i: Integer;
begin
  result := nil;
  for i:=0 to view.Columns.Count-1 do
    if view.Columns[i].Tag=aTag then begin
      result := view.Columns[i];
      break;
    end;
end;

procedure TfrmRepoBrowser.ClearTree;
begin
  treeFiles.OnSelectionChanged := nil;
  treeFiles.Items.Clear;
  treeFiles.OnSelectionChanged := @treeFilesSelectionChanged;
end;

procedure TfrmRepoBrowser.DoViewFile(node: PvfsNode);
var
  meta: string;
  F: TfrmEditor;
  info: PInfoNode;
  stream: TStream;
begin
  info := PInfoNode(node^.Data);
  meta := copy(info^.fileTree, 1, 7);

  stream := fGitMgr.GetFileStream(info^.fileTree);
  if stream=nil then
    exit;

  try
    stream.Position := 0;

    F := TfrmEditor.Create(Self);
    F.ViewerMgr.Readonly := true;
    F.ViewerMgr.LoadFromStream(stream, format('%s - %s', [meta, node^.name]));

    try
      F.ShowModal;
    finally
      F.Free;
    end;

  finally
    stream.free;
  end;

end;

procedure TfrmRepoBrowser.DoOpen(node: PvfsNode);
var
  info: PInfoNode;
  treeNode: TTreeNode;
begin

  info := PInfoNode(node^.Data);

  if info^.filetype='tree' then begin

    treeNode := treeFiles.Selected.FindNode(node^.name);
    if treeNode<>nil then
      treeNode.Selected := true

  end else
    DoViewFile(node);
end;

function TfrmRepoBrowser.GetGridSelectedNode: PvfsNode;
var
  aIndex: Integer;
begin
  aIndex := view.Row - view.FixedRows;
  result := PvfsNode(fList[aIndex]);
end;

function TfrmRepoBrowser.GetGridNodePath(node: PvfsNode): string;
begin
  if treeFiles.Selected=fRoot then
    result := node^.name
  else
    result := GetTreeNodePath(treeFiles.Selected) + '/' + node^.name;
end;

procedure TfrmRepoBrowser.FormCloseQuery(Sender: TObject; var CanClose: Boolean
  );
begin
  fConfig.OpenConfig;
  fConfig.WriteWindow(Self, 'frmRepoBrowser', SECTION_GEOMETRY);
  fConfig.WriteInteger('frmRepoBrowser.tree.width', treeFiles.Width, SECTION_GEOMETRY);
  fConfig.CloseConfig;
end;

procedure TfrmRepoBrowser.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  CloseAction := caHide;
end;

procedure TfrmRepoBrowser.actSelectCommitExecute(Sender: TObject);
var
  aCommit: string;
  aLogItem: TLogItem;
begin
  if PickCommit(GitMgr, HlHelper, aLogItem) then begin
    fLogItem := aLogItem;
    ApplyCommit;
  end;
end;

{ TDrawGridHelper }

procedure TDrawGridHelper.StartColumnSort(aIndex: Integer;
  aSortOrder: TSortOrder);
begin
  if aIndex=SortColumn then begin
    if aSortOrder=soAscending then
      SortOrder := soDescending
    else
      SortOrder := soAscending;
  end else
    ; // aSortOrder is ignored it will be ascending
  HeaderClick(true, aIndex);
end;

end.

