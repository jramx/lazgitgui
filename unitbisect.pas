{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  Bisect helper.
  Handles most of the bisect functionality.
}
unit unitbisect;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Math,
  Graphics, Menus, Grids,
  unitcommon,unitgittypes, unitgitutils, unitifaces, unitgitmgr, unitgitstate,
  unitdbindex, unitlogcache;

type

  { TBisect }

  TBisect = class
  private
    fBisectGood, fBisectGoodData, fBisectBad, fBisectBadData: string;
    fBisectRowGood, fBisectRowBad: Integer;
    fGitMgr: TGitMgr;
    fGrid: TDrawGrid;
    fLog: ILog;
    fLogCache: TLogCache;
    fOrgOnPrepareCanvas: TOnPrepareCanvasEvent;
    procedure GridPrepareCanvas(Sender: TObject; aCol, aRow: Integer;
      aState: TGridDrawState);
    procedure OnBisectClick(Sender: TObject);
    procedure OnBisectStartClick(Sender: TObject);
    procedure BisectReset;
    procedure BisectCurrentGood;
    procedure BisectCurrentBad;
  public
    constructor Create(aGrid: TDrawGrid);

    function Recover: boolean;
    procedure AddMenu(popLog: TPopupMenu; mnuSeparatorLast: TMenuItem);
    function CheckFinished: boolean;

    property LogCache: TLogCache read fLogCache write fLogCache;
    property GitMgr: TGitMgr read fGitMgr write fGitMgr;
    property Log: ILog read fLog write fLog;
  end;

implementation

{ TBisect }

procedure TBisect.GridPrepareCanvas(Sender: TObject; aCol, aRow: Integer;
  aState: TGridDrawState);
var
  isGood, isBad, minGood, maxGood: boolean;
  bgColor: TColor;
  aIndex, minRow, maxRow: Integer;
  aItem: TLogItem;
begin
  if assigned(fOrgOnPrepareCanvas) then
    fOrgOnPrepareCanvas(Sender, aCol, aRow, aState);

  if (aCol>=fGrid.FixedCols) and (aRow>=fGrid.FixedRows) and (fLogCache.DbIndex<>nil) then begin

    aIndex := aRow - fGrid.FixedRows;
    if not fLogCache.DbIndex.LoadItem(aIndex, aItem) then
      exit;

    aIndex := 0;

    if (fBisectRowGood<fGrid.FixedRows) or (fBisectRowBad<fGrid.FixedRows) then begin

      isGood := (aItem.CommitOID=fBisectGood);
      isBad  := (aItem.CommitOID=fBisectBad);
      if isGood or isBad then begin
        if isGood then  aIndex := 1
        else            aIndex := 2;
      end;

    end else begin

      minRow := min(fBisectRowGood, fBisectRowBad);
      maxRow := max(fBisectRowGood, fBisectRowBad);
      minGood := fBisectRowGood<fBisectRowBad;
      maxGood := fBisectRowGood>fBisectRowBad;

      if (aRow<=minRow) then begin
        if minGood then aIndex := 1
        else            aIndex := 2;
      end else
      if (aRow>=maxRow) then begin
        if maxGood then aIndex := 1
        else            aIndex := 2;
      end;

    end;

    if aIndex>0 then begin
      if aIndex=1 then bgColor := ColorToRGB(gblBisectGoodColor)
      else             bgColor := ColorToRGB(gblBisectBadColor);
      if gdRowHighlight in aState then
        bgColor := bgColor xor $1F1F1F;
    end else
      exit;

    fGrid.Canvas.Brush.Color := bgColor;
  end;

end;

procedure TBisect.OnBisectClick(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  cmdOut: RawByteString;
  bAct, aRow: Integer;
begin

  aRow := fGrid.Row;
  bAct := mi.Tag;

  case bAct of
    ACTION_BISECT_GOOD:
      if (fBisectGood='') then begin
        bAct := ACTION_BISECT_GOOD_SET;
        BisectCurrentGood;
      end;
    ACTION_BISECT_BAD:
      if (fBisectBad='') then begin
        bAct := ACTION_BISECT_BAD_SET;
        BisectCurrentBad;
      end;
  end;

  fGitMgr.Bisect(bAct, cmdOut, fBisectBad, fBisectGood);

  case mi.Tag of
    ACTION_BISECT_GOOD:
      begin
        fBisectRowGood := aRow;
        fGrid.Invalidate;
      end;
    ACTION_BISECT_BAD:
      begin
        fBisectRowBad := aRow;
        fGrid.Invalidate;
      end;
    ACTION_BISECT_RESET:
      BisectReset;
  end;
end;

procedure TBisect.OnBisectStartClick(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  data: string;
begin
  case mi.Tag of
    ACTION_BISECT_GOOD:
      BisectCurrentGood;

    ACTION_BISECT_BAD:
      BisectCurrentBad;

    ACTION_BISECT_RESET:
      BisectReset;
  end;
end;

procedure TBisect.BisectReset;
begin
  fBisectGood := '';
  fBisectBad := '';
  fBisectRowGood := -1;
  fBisectRowBad := -1;
  fGrid.Invalidate;
end;

procedure TBisect.BisectCurrentGood;
var
  data: string;
  item: PLogItem;
begin
  item := fLog.CurrentItemPtr;

  data := format('%s %s',[
    copy(item^.CommitOID, 1, 7),
    BoolToStr(Length(item^.Subject)<=15, item^.Subject, copy(item^.Subject, 1, 15)+'...')
    ]);

  fBisectGood := item^.CommitOID;
  fBisectGoodData := data;

  if not fGitMgr.GitState.Bisecting then begin
    fBisectRowGood := fGrid.Row;
    if fBisectRowBad<>0 then fGrid.Invalidate;
  end;
end;

procedure TBisect.BisectCurrentBad;
var
  data: string;
  item: PLogItem;
begin
  item := fLog.CurrentItemPtr;

  data := format('%s %s',[
    copy(item^.CommitOID, 1, 7),
    BoolToStr(Length(item^.Subject)<=15, item^.Subject, copy(item^.Subject, 1, 15)+'...')
    ]);

  fBisectBad := item^.CommitOID;
  fBisectBadData := data;

  if not fGitMgr.GitState.Bisecting then begin
    fBisectRowBad := fGrid.Row;
    if fBisectRowGood<>0 then fGrid.Invalidate;
  end;
end;

constructor TBisect.Create(aGrid: TDrawGrid);
begin
  inherited Create;

  fGrid := aGrid;

  fOrgOnPrepareCanvas := fGrid.OnPrepareCanvas;

  fGrid.OnPrepareCanvas := @GridPrepareCanvas;

  fBisectRowGood := -1;
  fBisectRowBad := -1;

end;

function TBisect.Recover: boolean;
var
  lastBad, lastGood: string;
  commit: QWord;
begin

  result :=
     (fGitMgr.GitState<>nil) and fGitMgr.GitState.Bisecting and
     (fBisectGood='') and (fBisectBad='') and
     (fBisectRowBad<fGrid.FixedRows) and (fBisectRowGood<fGrid.FixedRows);

  //DebugLn('TframeLog.BisectRecover %s: state=%s bisecting=%s bad=%s good=%s rowBad=%d rowGood=%d',
  //  [BoolToStr(result, 'Need Recover', 'No recover'),
  //   dbgs(fGitMgr.GitState<>nil), dbgs((fGitMgr.GitState<>nil) and fGitMgr.GitState.Bisecting),
  //  fBisectBad, fBisectGood, fBisectRowBad, fBisectRowGood]);

  if result then begin
     // the repository is in bisecting state, but apparenly we didn't start it
     // use the information
    with fGitMgr.GitState do begin

      fBisectBad := BisectStartBad;
      fBisectGood := BisectStartGood;

      if (fBisectBad<>'') and (BisectLastBad<>'') then begin
        commit := OIDToQWord(BisectLastBad);
        fBisectRowBad := fLog.FindCommitRow(commit);
      end;

      if (fBisectGood<>'') and (BisectLastGood<>'') then begin
        commit := OIDToQWord(BisectLastGood);
        fBisectRowGood := fLog.FindCommitRow(commit);
      end;

    end;

  end;

end;

procedure TBisect.AddMenu(popLog: TPopupMenu; mnuSeparatorLast: TMenuItem);
var
  bis, mi: TMenuItem;
  s, aTermBad, aTermGood: string;
  item: PLogItem;
begin
  item := fLog.CurrentItemPtr;
  aTermBad := fGitMgr.GitState.BisectTermBad;
  aTermGood := fGitMgr.GitState.BisectTermGood;


  mi := TMenuItem.Create(fGrid.Owner);
  mi.Caption := '-';
  popLog.Items.Insert(mnuSeparatorLast.MenuIndex, mi);

  if fGitMgr.GitState.StateType=gstNone then begin

    bis := TMenuItem.Create(fGrid.Owner);
    bis.Caption := rsBisect;
    popLog.Items.Insert(mnuSeparatorLast.MenuIndex, bis);

    // bisect not yet started, only three menus are available
    // select bisect good, select bisect bad and bisect start
    // bisect start is only enabled when bisect good and
    // bisect bad had been selected.

    mi := TMenuItem.Create(fGrid.Owner);
    if fBisectBad = '' then s := format(rsSelectThisCommitAsS, [QuotedStr(aTermBad)])
    else                  s := format(rsSSelectedS, [QuotedStr(TitleCase(aTermBad)), fBisectBadData]);
    mi.Caption := s;
    mi.Tag := ACTION_BISECT_BAD;
    mi.OnClick := @OnBisectStartClick;
    mi.Enabled := item^.CommitOID<>fBisectBad;
    bis.Insert(0, mi);

    mi := TMenuItem.Create(fGrid.Owner);
    if fBisectGood = '' then s := format(rsSelectThisCommitAsS, [QuotedStr(aTermGood)])
    else                   s := format(rsSSelectedS, [QuotedStr(TitleCase(aTermGood)), fBisectGoodData]);
    mi.Caption := s;
    mi.Tag := ACTION_BISECT_GOOD;
    mi.OnClick := @OnBisectStartClick;
    mi.Enabled := item^.CommitOID<>fBisectGood;
    bis.Insert(1, mi);

    mi := TMenuItem.Create(fGrid.Owner);
    mi.Caption := rsBisectStart;
    mi.Tag := ACTION_BISECT_START;
    mi.OnClick := @OnBisectClick;
    mi.Enabled := (fBisectGood<>'') and (fBisectBad<>'') and (fBisectGood<>fBisectBad);
    bis.Insert(2, mi);

    if (fBisectBad<>'') or (fBisectGood<>'') then begin
      mi := TMenuItem.Create(fGrid.Owner);
      mi.Caption := rsBisectReset;
      mi.Tag := ACTION_BISECT_RESET;
      mi.OnClick := @OnBisectStartClick;
      bis.Insert(3, mi);
    end;

  end else
  if fGitMgr.GitState.Bisecting then begin

    // while bisecting make available good, bad, log and reset

    if not fGitMgr.GitState.BisectFinished then begin

      mi := TMenuItem.Create(fGrid.Owner);
      mi.Caption := format(rsBisectThisCommitIsS, [QuotedStr(aTermGood)]);
      mi.Tag := ACTION_BISECT_GOOD;
      mi.OnClick := @OnBisectClick;
      popLog.Items.Insert(mnuSeparatorLast.MenuIndex, mi);

      mi := TMenuItem.Create(fGrid.Owner);
      mi.Caption := format(rsBisectThisCommitIsS, [QuotedStr(aTermBad)]);
      mi.Tag := ACTION_BISECT_BAD;
      mi.OnClick := @OnBisectClick;
      popLog.Items.Insert(mnuSeparatorLast.MenuIndex, mi);

    end;

    mi := TMenuItem.Create(fGrid.Owner);
    mi.Caption := rsBisectShowBisectLog;
    mi.Tag := ACTION_BISECT_LOG;
    mi.OnClick := @OnBisectClick;
    popLog.Items.Insert(mnuSeparatorLast.MenuIndex, mi);

    mi := TMenuItem.Create(fGrid.Owner);
    mi.Caption := rsBisectReset2;
    mi.Tag := ACTION_BISECT_RESET;
    mi.OnClick := @OnBisectClick;
    popLog.Items.Insert(mnuSeparatorLast.MenuIndex, mi);

  end;
end;

function TBisect.CheckFinished: boolean;
var
  commit: QWord;
begin
  result := fGitMgr.GitState.Bisecting and fGitMgr.GitState.BisectFinished;
  if result then begin
    commit := OIDToQWord(fGitMgr.GitState.BisectLastBad);
    fLog.LocateCommit(commit);
  end;
end;

end.

