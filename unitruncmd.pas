{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  Runs a command in a thread o runs a command presenting the user a window
  with the result.
}
unit unitruncmd;

{$mode ObjFPC}{$H+}
{$ModeSwitch nestedprocvars}
{.$define Debug}

interface

uses
  Classes, SysUtils, Math, LazLogger, SynEdit, Forms, Controls, Graphics,
  Dialogs, StdCtrls, ButtonPanel,
  unitcommon, unitifaces, unitconfig, unitprocess, unitansiescapes,
  unitsyneditextras, unitrunthread;

type

  { TfrmRunCommand }

  TfrmRunCommand = class(TForm)
    ButtonPanel1: TButtonPanel;
    chkCloseOk: TCheckBox;
    lblResult: TLabel;
    lblCaption: TLabel;
    txtOutput: TSynEdit;
    procedure chkCloseOkClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    fCommand: string;
    fLastIndex: Integer;
    fCaretY, fCaretX: Integer;
    fResult: Integer;
    fRunThread: TRunThread;
    fStartDir: string;
    fAnsiHandler: TAnsiEscapesHandler;
    procedure OnDone(Sender: TObject);
    procedure OnOutput(sender: TObject; var interrupt: boolean);
  public
    property Command: string read fCommand write fCommand;
    property StartDir: string read fStartDir write fStartDir;
    property Result: Integer read fResult;
  end;

  function RunInteractive(command, startdir, title, caption:string): Integer;

var
  frmRunCommand: TfrmRunCommand;

implementation

function RunInteractive(command, startdir, title, caption: string): Integer;
var
  rf: TfrmRunCommand;
begin
  rf := TfrmRunCommand.Create(Application);
  rf.Command := command;
  rf.StartDir := startdir;
  rf.lblCaption.Caption := Caption;
  rf.Caption := title;
  try
    rf.ShowModal;
    result := rf.Result;
  finally
    rf.Free;
  end;
end;

{$R *.lfm}

{ TfrmRunCommand }

procedure TfrmRunCommand.FormShow(Sender: TObject);
begin
  lblResult.Caption := rsStartingCommandPleaseWait;
  txtOutput.Clear;
  Application.ProcessMessages;

  fRunThread.Command := fCommand;
  fRunThread.StartDir := fStartDir;
  fRunThread.Start;
end;

procedure TfrmRunCommand.FormCreate(Sender: TObject);
begin

  fConfig.OpenConfig;

  fConfig.ReadWindow(Self, 'runcommandform', SECTION_GEOMETRY);
  fConfig.ReadFont(txtOutput.Font, 'RunCmdOutput', fpFixed, SECTION_FONTS);
  chkCloseOk.Checked := fConfig.ReadBoolean('CloseOnSuccess', false, 'RunCommandForm');

  fRunThread := TRunThread.Create;
  fRunThread.FreeOnTerminate := true;
  fRunThread.OnOutput := @OnOutput;
  fRunThread.OnTerminate := @OnDone;

  fLastIndex := -1;

  fAnsiHandler := TAnsiEscapesHandler.Create(txtOutput);
  AddPopupMenu(txtOutput);

  fConfig.CloseConfig;
end;

procedure TfrmRunCommand.FormDestroy(Sender: TObject);
begin
  fAnsiHandler.Free;
end;

procedure TfrmRunCommand.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  fConfig.WriteWindow(Self, 'runcommandform', SECTION_GEOMETRY);
end;

procedure TfrmRunCommand.chkCloseOkClick(Sender: TObject);
begin
  fConfig.WriteBoolean('CloseOnSuccess', chkCloseOk.Checked, 'RunCommandForm');
end;

procedure TfrmRunCommand.OnOutput(sender: TObject; var interrupt: boolean);
var
  thread: TRunThread absolute sender;
begin
  fAnsiHandler.ProcessLine(thread.Line, thread.LineEnding);
  lblResult.Caption := rsWorking;
end;

procedure TfrmRunCommand.OnDone(Sender: TObject);
begin
  {$IFDEF DEBUG}
  debugln('TfrmRunCommand.OnDone');
  {$ENDIF}
  fResult := fRunThread.Result;
  if fResult<=0 then begin
    lblResult.Color := clGreen;
    lblResult.Font.Color := clWhite;
    lblResult.Caption := rsSucceed;
    if chkCloseOk.Checked then Close;
  end else begin
    txtOutput.Append(fRunThread.ErrorLog);
    lblResult.Color := clRed;
    lblResult.Font.Color := clWhite;
    lblResult.Caption := rsFailed;
  end;
end;

end.

