{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  TGitState.
  This class handles what is called the 'git state', that is, if the currently
  the repository is rebasing, cherry picking, reverting, etc.
}
unit unitgitstate;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  lazloggerbase,
  unitcommon, unitgitutils, unitifaces, unitparsers;

type

  TGitStateType = (gstNone, gstRebase, gstAM, gstAMRebase, gstRebaseInteractive,
                   gstRebaseMerge, gstMerging, gstCherryPicking, gstReverting,
                   gstBisecting);

  { TGitState }

  TGitState = class
  private
    fDetachedHead: Boolean;
    fGit: IGit;
    fMergingConflict: boolean;
    fRebaseBranch: String;
    fRebaseStep: Integer;
    fRebaseTotal: Integer;
    fStateType: TGitStateType;
    fGitDir: string;
    fBisectStartBad, fBisectStartGood, fBisectLastBad, fBisectLastGood: string;
    fBisectTermBad, fBisectTermGood: string;
    fBisectFinished: Boolean;
    function GetBisecting: boolean;
    function GetInWorkTree: boolean;
    function GetMerging: boolean;
    function GetRebasing: boolean;
    function GetReverting: boolean;
    function GetStateAsString: string;
    function SequencerTodo(gitDir:string; out todo:string): boolean;
    function CheckSequencerStatus: boolean;
    procedure UpdateBisectInfo;
  public
    constructor create;
    destructor destroy; override;
    procedure Update;

    property AsString: string read GetStateAsString;
    property git: IGit read fGit write fGit;
    property StateType: TGitStateType read fStateType;
    property Rebasing: boolean read GetRebasing;
    property Merging: boolean read GetMerging;
    property Reverting: boolean read GetReverting;
    property Bisecting: boolean read GetBisecting;
    property DetachedHead: boolean read fDetachedHead;
    property InWorkTree: boolean read GetInWorkTree;
    property MergingConflict: boolean read fMergingConflict write fMergingConflict;
    //
    property BisectTermGood: string read fBisectTermGood;
    property BisectTermBad: string read fBisectTermBad;
    property BisectStartGood: string read fBisectStartGood;
    property BisectStartBad: string read fBisectStartBad;
    property BisectLastGood: string read fBisectLastGood;
    property BisectLastBad: string read fBisectLastBad;
    property BisectFinished: boolean read fBisectFinished;
  end;

implementation

{ TGitState }

function TGitState.SequencerTodo(gitDir: string; out todo: string): boolean;
begin
  gitDir := gitDir + 'sequencer' + PathDelim + 'todo';
  result := FileExists(gitDir);
  if result then
    todo := GetFileContent(gitDir, true);
end;

function TGitState.GetStateAsString: string;
begin
  case fStateType of
    gstNone:              result := '';
    gstRebase:            result := rsREBASE;
    gstAM:                result := rsAM;
    gstAMRebase:          result := rsAMREBASE;
    gstRebaseInteractive: result := rsINTERACTIVEREBASE;
    gstRebaseMerge:       result := rsREBASEMERGE;
    gstMerging:           result := rsMERGING;
    gstCherryPicking:     result := rsCHERRYPICKING;
    gstReverting:         result := rsREVERTING;
    gstBisecting:         result := rsBISECTING;
  end;

  if Rebasing then
    result += format(' %d/%d', [fRebaseStep, fRebaseTotal]);

  if MergingConflict then
    result += ', ' + rsMERGINGCONFLICT;
end;

function TGitState.GetRebasing: boolean;
begin
  result := fStateType in [gstRebaseInteractive, gstRebaseMerge, gstRebase, gstAM, gstAMRebase];
end;

function TGitState.GetReverting: boolean;
begin
  result := fStateType = gstReverting;
end;

function TGitState.GetMerging: boolean;
begin
  result := fStateType = gstMerging;
end;

function TGitState.GetBisecting: boolean;
begin
  result := fStateType = gstBisecting;
end;

function TGitState.GetInWorkTree: boolean;
begin
  result := fGitDir<>fGit.GitDir;
end;

function TGitState.CheckSequencerStatus: boolean;
var
  todo: string;
begin
  if FileExists(fGitDir + 'CHERRY_PICK_HEAD') then begin
    fStateType := gstCherryPicking;
    exit(true);
  end else if FileExists(fGitDir + 'REVERT_HEAD') then begin
    fStateType := gstReverting;
    exit(true);
  end else if SequencerTodo(fGitDir, todo) then begin
    if (pos('pick', todo)=1) or (pos('p  ', todo)=1) then begin
      fStateType := gstCherryPicking;
      exit(true);
    end else
    if (pos('revert', todo)=1) then begin
      fStateType := gstReverting;
      exit(true);
    end;
  end;
  result := false;
end;

constructor TGitState.create;
begin
  inherited Create;
  fBisectTermGood := 'good';
  fBisectTermBad := 'bad';
end;

destructor TGitState.destroy;
begin
  fGit := nil;
  inherited destroy;
end;

procedure TGitState.Update;
var
  cmd, aDir: string;
  cmdOut: RawByteString;
  arr: TStringArray;
begin

  fStateType := gstNone;

  // This code is based on /usr/lib/git-core/git-sh-prompt @ __git_ps1
  //
  // we need GitDir here instead of fGit.GitDir because fGit.GitDir is really
  // GIT_COMMON_DIR and points to the repository .git/ directory, while GIT_DIR
  // should point (in case of checked out work tree) to .git/worktrees/<worktree>
  //
  cmd := 'rev-parse --absolute-git-dir --is-inside-git-dir --is-bare-repository --is-inside-work-tree --symbolic-full-name HEAD';
  if fGit.Any(cmd, cmdOut)<=0 then begin

    cmd := cmdOut;
    arr := cmd.Split([#10]);

    if arr[0]='--absolute-git-dir' then
      arr[0] := fGit.GitDir;

    fGitDir := IncludeTrailingBackslash(SetDirSeparators(arr[0]));
    if DirectoryExists(fGitDir + 'rebase-merge') then begin
      aDir := fGitDir + 'rebase-merge' + pathDelim;
      fRebaseBranch := GetFileContent(aDir + 'head-name');
      fRebaseStep := StrToIntDef(GetFileContent(aDir + 'msgnum'), 0);
      fRebaseTotal := StrToIntDef(GetFileContent(aDir + 'end'), 0);
      if FileExists(aDir + 'interactive') then
        fStateType := gstRebaseInteractive
      else
        fStateType := gstRebaseMerge;
    end else
    if DirectoryExists(fGitDir + 'rebase-apply') then begin
      aDir := fGitDir + 'rebase-apply' + pathDelim;
      fRebaseStep := StrToIntDef(GetFileContent(aDir + 'next'), 0);
      fRebaseTotal := StrToIntDef(GetFileContent(aDir + 'last'), 0);
      if FileExists(aDir + 'rebasing') then begin
        fRebaseBranch := GetFileContent(aDir + 'head-name');
        fStateType := gstRebase
      end else if FileExists(aDir + 'applying') then
        fStateType := gstAM
      else
        fStateType := gstAMRebase;
    end else
    if FileExists(fGitDir + 'MERGE_HEAD') then
      fStateType := gstMerging
    else if CheckSequencerStatus then
      //
    else if FileExists(fGitDir + 'BISECT_LOG') then begin
      fStateType := gstBisecting;
      UpdateBisectInfo;
    end;

    fDetachedHead := arr[4]='HEAD';

  end else begin
  end;
end;

procedure TGitState.UpdateBisectInfo;
var
  cmdOut: RawByteString;
begin

  fBisectStartBad := '';
  fBisectStartGood := '';
  fBisectLastBad := '';
  fBisectLastGood := '';
  fBisectFinished := false;

  // the correct way is asking git for the bisect log, but this is much slower
  // than reading the log from the .git directory.
  // (well at least in windows where1700ms versus 20ms.
  // Anyway, if we are unable to get the .git dir, try the slower version.

  if (fGitDir='') or not DirectoryExists(fGitDir) then begin

    if fGit.Any('rev-parse --absolute-git-dir', cmdOut)<=0 then begin

      if trim(cmdOut)='--absolute-git-dir' then
        cmdOut := fGit.GitDir;

      fGitDir := IncludeTrailingBackslash(SetDirSeparators(cmdOut));

    end else
      fGitDir := '';

  end;

  if fGitDir='' then begin

    if fGit.Any('bisect log', cmdOut)>0 then
      exit;

  end else
    cmdOut := GetFileContent(fGitDir + 'BISECT_LOG', false);

  ParseBisectLog(cmdOut, fBisectStartBad, fBisectStartGood, fBisectLastBad, fBisectLastGood,
                         fBisectTermBad, fBisectTermGood, fBisectFinished);
end;

end.

