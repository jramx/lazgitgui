unit unitwidgetset;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  {$ifdef linux}gtk2, gdk2,{$endif}
  Clipbrd;

  procedure PreserveClipboard;

implementation

// clipboard empty on exit workaround
// reference: https://wiki.lazarus.freepascal.org/Clipboard#How_to_fix_empty_GTK2_clipboard_on_exit
procedure PreserveClipboard;
{$ifdef linux}
var
  c: PGtkClipboard;
  t: string;
{$endif}
begin
  {$ifdef linux}
  c := gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
  t := Clipboard.AsText;
  gtk_clipboard_set_text(c, PChar(t), Length(t));
  gtk_clipboard_store(c);
  {$endif}
end;

end.

