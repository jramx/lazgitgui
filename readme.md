**LazGitGui**

This is a [git](https://git-scm.com/book/en/v2) tool resembling the popular 'git gui' program, with some additional features.

If you don't know what git gui is, it is a graphical representation of the git status, where one can do basic git operations like watch what files under the work space have been modified and see the differences with respect to the original files. It also allow move files between the unstaged and the staged area and make commits from it.

Git Gui has some cool features not yet available in this program, for example the selective apply or revert of hunk and line changes from the diff viewer.

Contrary to Git Gui which relies on gitk for that feature, LazGitGui has a log feature that allows you to see the history of the repository and do some operation on branches and commits.

**Screenshots.**

![main](images/main.png)

![log](images/log.png)

![filehistory](images/filehistory.png)

For more information see the [Documentation](docs/documentation.md)
