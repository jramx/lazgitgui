{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  Shows the file history and changes
}
unit unitfilehistory;

{$mode ObjFPC}{$H+}

{.$define DebugFileHistory}

interface

uses
  Classes, SysUtils, DateUtils,
  LCLIntf, LCLType, LazLoggerBase, Clipbrd,
  Forms, Controls, Graphics, Dialogs, Grids, ExtCtrls, Menus, SynEdit,
  unitcommon, unitgittypes, unitconfig, unitifaces, unitgitutils, unitgitmgr,
  unithighlighterhelper, unittextchunks, unitlinkmgr, unitsyneditextras,
  unitformmgr, unitguitools, unitmarkeditem, unitparsers, unitdifftool, Types,
  unitwidgetset;

type

  THistoryFlags = set of (hfChange, hfCreate, hfRename, hfDelete, hfCopy);
  THistoryItem = record
    Date,
    CommitOID,
    Author,
    Subject, Filename, Refs: RawByteString;
    Flags: THistoryFlags;
  end;

  { TfrmFileHistory }

  TfrmFileHistory = class(TForm, IObserver)
    grid: TDrawGrid;
    popGrid: TPopupMenu;
    Splitter1: TSplitter;
    txtDiff: TSynEdit;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure gridContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure gridDrawCell(Sender: TObject; aCol, aRow: Integer; aRect: TRect;
      aState: TGridDrawState);
    procedure gridHeaderSized(Sender: TObject; IsColumn: Boolean; Index: Integer
      );
    procedure gridPrepareCanvas(Sender: TObject; aCol, aRow: Integer;
      aState: TGridDrawState);
    procedure gridSelection(Sender: TObject; aCol, aRow: Integer);
  private
    fFilePath: string;
    fGit: IGit;
    fGitMgr: TGitMgr;
    fhlHelper: THighlighterHelper;
    fHistory: array of THistoryItem;
    fIndex: Integer;
    fIsDir: Boolean;
    fLastRow: Integer;
    fLinkMgr: TLinkMgr;
    fMarkedHelper: TMarkedItemHelper;
    fMenuIndex: Integer;
    procedure OnBrowseCommit(Sender: TObject);
    procedure OnCompareMarked(Sender: TObject);
    procedure OnCopyAllInfo(Sender: TObject);
    procedure OnCopyCommitHash(Sender: TObject);
    procedure OnCopySubject(Sender: TObject);
    procedure OnGetLogItemData(sender: TObject; aIndex: Integer; out aCommit,
      aSubject: RawByteString);
    procedure OnGotoCommit(Sender: TObject);
    procedure OnLinkClick(sender: TObject; link: TTextChunksItem);
    procedure OnMarkedItem(Sender: TObject);
    procedure ProcessFilePath;
    procedure SetFilePath(AValue: string);
    procedure SetGitMgr(AValue: TGitMgr);
    procedure ObservedChanged(Sender:TObject; what: Integer; data: PtrInt);
    procedure SetHlHelper(AValue: THighlighterHelper);
    procedure DoRowSelection(aRow: Integer);
    procedure SearchLog(txt: string; forward: boolean; startRow:Integer=-1; searchIn:TSetOfByte=[]);
    function  GetTreeInfo(aIndex: Integer; out prevTree, curTree: string): boolean;
    function  GetMarkedRow: Integer;
  public
    property FilePath: string read fFilePath write SetFilePath;
    property GitMgr: TGitMgr read fGitMgr write SetGitMgr;
    property HlHelper: THighlighterHelper read fhlHelper write SetHlHelper;
  end;

  procedure ShowFileHistory(aFile: string; GitMgr: TGitMgr; HlHelper:THighlighterHelper; modal:boolean=false);

  procedure FileHistoryLauncher(sender:TObject);

var
  frmFileHistory: TfrmFileHistory;

implementation

uses
  unitformlog, unitrepobrowser;

{$R *.lfm}

const
  COLTAG_DATE                 = 0;
  COLTAG_AUTHOR               = 1;
  COLTAG_SUBJECT              = 2;
  COLTAG_SHA1                 = 3;

procedure ShowFileHistory(aFile: string; GitMgr: TGitMgr;
  HlHelper: THighlighterHelper; modal: boolean);
var
  f: TfrmFileHistory;
begin
  f := TfrmFileHistory.Create(Application);
  f.GitMgr := GitMgr;
  f.HlHelper := HlHelper;
  f.FilePath := aFile;
  if modal then f.ShowModal
  else          f.Show;
end;

procedure FileHistoryLauncher(sender: TObject);
var
  formMgr: TformMgr absolute sender;
  aPath: string;
  fGit: IGit;
  cmdOut: RawByteString;
  isDir: Boolean;
begin

  try
    fGit := formMgr.GitMgr.Git;

    // TODO: check borderline cases

    aPath := formMgr.Target;
    isDir := DirectoryExists(aPath);
    if not isDir then
      aPath := ExtractFilePath(aPath);

    if not formMgr.OpenDirectory(aPath) then begin
      Application.Terminate;
      exit;
    end;

    aPath := formMgr.Target;
    Delete(aPath, 1, Length(fGit.TopLevelDir));

    if fGit.Any('ls-files --error-unmatch ' + aPath, cmdOut)>0 then begin
      if not Application.HasOption('q', 'quiet') then
        ShowMessageFmt('%s is untracked',[formMgr.Target]);
      Application.Terminate;
      exit;
    end;

    ShowFileHistory(aPath, formMgr.GitMgr, formMgr.HlHelper, true);

    PreserveClipboard;

    Application.Terminate;

  finally
    fGit := nil;
  end;
end;

{ TfrmFileHistory }

procedure TfrmFileHistory.FormCloseQuery(Sender: TObject; var CanClose: Boolean
  );
begin
  fConfig.WriteWindow(Self, 'frmFileHistory', SECTION_GEOMETRY);
  fConfig.WriteInteger('frmFileHistory.grid.height', grid.Height, SECTION_GEOMETRY);
end;

procedure TfrmFileHistory.FormCreate(Sender: TObject);
begin
  AddPopupMenu(txtDiff);
end;

procedure TfrmFileHistory.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  CloseAction := caFree;
end;

procedure TfrmFileHistory.FormDestroy(Sender: TObject);
begin
  fMarkedHelper.Free;
  GitMgr := nil;
  fLinkMgr.Free;
end;

procedure TfrmFileHistory.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then
    close;
end;

procedure TfrmFileHistory.FormShow(Sender: TObject);
var
  col: TCollectionItem;
  gcol: TGridColumn;
  i: Integer;
begin
  for i:=0 to grid.Columns.Count-1 do
    grid.Columns[i].Tag := i;

  fConfig.OpenConfig;
  fConfig.ReadWindow(Self, 'frmFileHistory', SECTION_GEOMETRY);
  fConfig.ReadInteger('frmFileHistory.grid.height', grid.Height, SECTION_GEOMETRY);
  for col in grid.Columns do begin
    gcol := TGridColumn(col);
    if gcol.SizePriority=0 then
      gcol.Width := fConfig.ReadInteger('frmFileHistory.grid.coltag'+IntToStr(gcol.tag)+'.width', gcol.width, SECTION_GEOMETRY);
  end;
  fConfig.CloseConfig;

  fLinkMgr := TLinkMgr.Create(grid, COLTAG_SUBJECT);
  fLinkMgr.GitMgr := fGitMgr;
  fLinkMgr.OnLinkClick := @OnLinkClick;
  fLinkMgr.OnGetLogItemData := @OnGetLogItemData;
end;

procedure TfrmFileHistory.gridContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
var
  aRow, aCol: Longint;
  //isCurRow: Boolean;
begin
  grid.MouseToCell(MousePos.x, MousePos.y, aCol, aRow);
  fIndex := aRow - grid.FixedRows;
  if (fIndex<0) or (fIndex>Length(fHistory)-1) then
    exit;

  //isCurRow := grid.Row=aRow;

  popGrid.Items.clear;

  AddPopItem(popGrid, 'Go to commit', @OnGotoCommit, fIndex);
  AddPopItem(popGrid, 'Browse repository at this commit', @OnBrowseCommit, fIndex);

  AddPopItem(popGrid, '-', nil, 0);
  AddPopItem(popGrid, 'Mark this for comparision', @OnMarkedItem, fIndex);
  fMarkedHelper.AddPopMenuCompare(popGrid, @OnCompareMarked, true);

  AddPopItem(popGrid, '-', nil, 0);
  AddPopItem(popGrid, 'Copy commit SHA-1', @OnCopyCommitHash, fIndex);
  AddPopItem(popGrid, 'Copy Subject', @OnCopySubject, fIndex, 'Copy');
  AddPopItem(popGrid, 'Copy all commit info', @OnCopyAllInfo, fIndex, 'Copy');
end;

procedure TfrmFileHistory.gridDrawCell(Sender: TObject; aCol, aRow: Integer;
  aRect: TRect; aState: TGridDrawState);
var
  s: RawByteString;
  aIndex, x: Integer;
  Chunks: TTextChunks;
  chunk: TTextChunksItem;
  aStyle: TFontStyles;
begin
  if (fGitMgr=nil) or (aRow<grid.FixedRows) then
    exit;
  aIndex := aRow - grid.FixedRows;
  x := aRect.Left + 7;
  case grid.Columns[aCol].tag of
    COLTAG_DATE:    s := fHistory[aIndex].Date;
    COLTAG_AUTHOR:  s := fHistory[aIndex].Author;
    COLTAG_SUBJECT:
      begin
        Chunks := GetTextChunks(grid.Canvas, aRect, x, fGitMgr.RefsMap, fHistory[aIndex].CommitOID, fHistory[aIndex].Subject);
        for chunk in Chunks do begin
          grid.Canvas.Brush.Style := chunk.brushStyle;
          grid.Canvas.Brush.Color := chunk.brushColor;
          grid.Canvas.Pen.Style := chunk.penStyle;
          grid.Canvas.Pen.Color := chunk.penColor;
          grid.Canvas.Pen.Width := chunk.penWidth;
          if chunk.itemType=tcitBox then grid.Canvas.Rectangle(chunk.r);
          grid.Canvas.Brush.Style := bsClear;
          grid.Canvas.Font.Color := chunk.fontColor;
          aStyle := grid.Canvas.Font.Style;
          if chunk.itemType=tcitLink then Include(aStyle, fsUnderline)
          else                            Exclude(aStyle, fsUnderline);
          grid.Canvas.Font.Style := aStyle;
          grid.Canvas.TextOut(chunk.r.Left + 3, chunk.r.Top, chunk.text);
        end;
        grid.Canvas.Brush.Style := bsSolid;
        grid.Canvas.Pen.Style := psSolid;
        exit;
      end;
    COLTAG_SHA1:    s := fHistory[aIndex].CommitOID;
  end;
  grid.Canvas.Brush.Style := bsClear;
  grid.Canvas.TextOut(x, aRect.Top, s);
  grid.Canvas.Brush.Style := bsSolid;
end;

procedure TfrmFileHistory.gridHeaderSized(Sender: TObject; IsColumn: Boolean;
  Index: Integer);
var
  col: TGridColumn;
begin
  if isColumn then begin
    col := grid.Columns[Index];
    fConfig.WriteInteger('frmFileHistory.grid.coltag'+IntToStr(col.Tag)+'.width', col.Width, SECTION_GEOMETRY);
  end;
end;

procedure TfrmFileHistory.gridPrepareCanvas(Sender: TObject; aCol,
  aRow: Integer; aState: TGridDrawState);
var
  aIndex: Integer;
  aColor: TColor;
begin
  if (fGitMgr=nil) or (aRow<grid.FixedRows) then
    exit;
  aIndex := aRow - grid.FixedRows;
  aColor := grid.Canvas.Brush.Color;
  if fHistory[aIndex].CommitOID=fMarkedHelper.MarkedItem.CommitOID then begin
    aColor := clMoneyGreen;
    if gdRowHighlight in aState then
      aColor := clSkyBlue;
  end;
  grid.Canvas.Brush.Color := aColor;
end;

procedure TfrmFileHistory.gridSelection(Sender: TObject; aCol, aRow: Integer);
begin
  DoRowSelection(aRow);
end;

procedure TfrmFileHistory.SetGitMgr(AValue: TGitMgr);
begin
  if fGitMgr = AValue then Exit;

  if fGitMgr<>nil then
    fGitMgr.RemoveObserver(Self);

  fGitMgr := AValue;

  if fGitMgr<>nil then begin
    fGitMgr.AddObserver(Self);
    fGit := fGitMgr.Git;
  end else
    fGit := nil;

  if fGitMgr<>nil then
    fMarkedHelper := TMarkedItemHelper.Create(fGitMgr, 'filehistory');
end;

procedure TfrmFileHistory.SetFilePath(AValue: string);
begin
  if fFilePath = AValue then Exit;
  fFilePath := AValue;
  fIsDir := DirectoryExists(fGit.TopLevelDir + fFilePath);
  ProcessFilePath;
end;

procedure TfrmFileHistory.ProcessFilePath;
var
  i, j, k: Integer;
  L: TStringList;
  item: THistoryItem;
  {o, }s: RawByteString;
  p, q, r, m, n, t: pchar;
  Ms: TMemoryStream;

  function NextEOL: boolean;
  begin
    n := strpos(p, #10);
    result := n<>nil;
    if result then
      n^ := #0;
  end;

  function GetString(start:pchar; len:Integer): string;
  begin
    SetString(Result, start, len);
  end;

  function AdvanceLine(findEOL:boolean=true): boolean;
  begin
    p := n + 1;
    while (p<t) and (p^ in [#10, #13]) do
      inc(p);
    if findEOL then
      result := NextEOL
    else
      result := true;
  end;

begin
  Caption := format(rsHistoryOfS, [fFilePath]);
  L := TStringList.Create;
  Ms := TMemoryStream.Create;
  try

    {$ifdef DebugFileHistory}
    if FileExists('filehistory.raw') then

      Ms.LoadFromFile('filehistory.raw')

    else begin

      // parse this: .....
      if fGit.Any('log --follow --stat --summary -z -- ' + fFilePath, Ms)>0 then
        exit; // error

      Ms.SaveToFile('filehistory.raw');

    end;
    {$else}
    // parse this: .....
    if fGit.Any('log --follow --stat --summary -z -- ' + fFilePath, Ms)>0 then
      exit; // error
    {$endif}

    p := Ms.Memory;
    t := p + Ms.Size;
    while p < t do begin

      // prepare location of next record
      r := p + strlen(p) + 1;

      // locate EOL
      if not NextEOL then break;

      // Find start of record
      if strlcomp(p, 'commit ', 7)<>0 then
        break;

      // found, save commit oid
      SetString(item.CommitOID, p+7, 40);
      inc(p, 47);
      // save refs if there is any
      item.Refs := Trim(GetString(p, n - p));
      if not AdvanceLine then break;

      // skip 'Author: '
      q := strpos(p + 8, ' ');
      if q=nil then break;
      item.Author := GetString(p + 8, q - (p + 8));
      if not AdvanceLine then break;

      // skip 'Date: ';
      item.Date := Trim(GetString(p + 6, (n - (p + 6))));
      if not AdvanceLine then break;

      // skip empty line
      while (p<t) and (p^ in [#10, #13]) do inc(p);

      // collect subject lines
      item.Flags := [];
      item.Subject := '';
      while (p<t) and (StrLComp(p, '    ', 4)=0) do begin
        item.Subject += Trim(GetString(p, n-p));
        AdvanceLine;
        if p+1=n then break;
      end;

      // file section
      // TODO: multiple files (directory history)
      q := strpos(p, '|');
      if q=nil then break;
      (q-1)^ := #0;

      // try to detect OrigPath{ => NewPath}/filename
      // Ref: https://git-scm.com/docs/git-diff#_other_diff_formats
      q := strpos(p, '{ => ');
      if q<>nil then begin
        item.Filename := Trim(GetString(p, strlen(p)));
        i := pos('}', item.Filename);
        if i>0 then Delete(item.Filename, i, 1);
        i := q - p;
        Delete(item.Filename, i, 5);
      end else begin
        q := strpos(p, '=>');
        if q<>nil then
          p := q + 2;
        item.Filename := Trim(GetString(p, strlen(p)));
      end;
      if not AdvanceLine then break;

      // skip line 'x file changed, y insertions, z deletions
      if not AdvanceLine(false) then break;

      if strlcomp(p, ' rename', 7)=0 then Include(item.flags, hfRename) else
      if strlcomp(p, ' delete', 7)=0 then Include(item.flags, hfDelete) else
      if strlcomp(p, ' copy', 5)=0   then Include(item.flags, hfCopy) else
      if strlcomp(p, ' create', 7)=0 then Include(item.flags, hfCreate)
      else                                Include(item.Flags, hfChange);

      j := Length(fHistory);
      SetLength(fHistory, j+1);
      fHistory[j] := item;

      p := r;
    end;

    grid.RowCount := grid.FixedRows + Length(fHistory);

    fLastRow := -1;
    DoRowSelection(grid.FixedRows);

  finally
    Ms.Free;
    L.Free;
  end;
end;

procedure TfrmFileHistory.OnLinkClick(sender: TObject; link: TTextChunksItem);
begin
  case link.linkAction of
    'goto':
        SearchLog(link.linkDest, true, 0,[SEARCHIN_COMMIT]);

    'open':
      begin
        if link.linkDest='' then  OpenURL(link.text)
        else                      OpenURL(link.linkDest);
      end;
  end;
end;

procedure TfrmFileHistory.OnMarkedItem(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  aIndex, mRow: Integer;
  prevTree, curTree: string;
  aDate: Int64;
begin
  aIndex := mi.Tag;
  if not GetTreeInfo(aIndex, prevTree, curTree) then begin
    ShowMessage('Unable to mark this commit!');
    exit;
  end;
  mRow := GetMarkedRow;
  if mRow>=grid.FixedRows then
    grid.InvalidateRow(mRow);
  aDate := GitDateToUnixTimestamp(fHistory[aIndex].Date);
  fMarkedHelper.SetMarkedItem(fHistory[aIndex].Filename, curTree, fHistory[aIndex].CommitOID, aDate);
  grid.InvalidateRow(aIndex + grid.FixedRows);
end;

procedure TfrmFileHistory.OnGetLogItemData(sender: TObject; aIndex: Integer;
  out aCommit, aSubject: RawByteString);
begin
  aCommit := fHistory[aIndex].CommitOID;
  aSubject := fHistory[aIndex].Subject;
end;

procedure TfrmFileHistory.OnGotoCommit(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
begin
  ShowLog(fGitMgr, fhlHelper, fHistory[mi.Tag].CommitOID);
end;

procedure TfrmFileHistory.OnCopyCommitHash(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
begin
  Clipboard.AsText := fHistory[mi.Tag].CommitOID;
end;

procedure TfrmFileHistory.OnCopyAllInfo(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
begin
  Clipboard.AsText := fHistory[mi.Tag].Subject;
end;

procedure TfrmFileHistory.OnBrowseCommit(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
begin
  ShowRepoBrowser(fGitMgr, fHlHelper, fHistory[mi.Tag].CommitOID);
end;

procedure TfrmFileHistory.OnCompareMarked(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  marked1, marked2: TMarkedItem;
  prevTree, curTree, labelOlder, labelNewer: string;
  aDate: Int64;
  older, newer: PMarkedItem;
  dtl: TDiffToolLauncherRec;
  asPicked: Boolean;
  cmdOut: RawByteString;
begin
  if not GetTreeInfo(fIndex, prevTree, curTree) then begin
    ShowMessage('Invalid tree info for comparing');
    exit;
  end;
  aDate := GitDateToUnixTimestamp(fHistory[fIndex].Date);

  marked1 := fMarkedHelper.MarkedItem;

  marked2.filename := fHistory[fIndex].Filename;
  marked2.treehash := curTree;
  marked2.CommitOID := fHistory[fIndex].CommitOID;
  marked2.CommiterDate := aDate;

  if marked1.CommiterDate <= marked2.CommiterDate then begin
    older := @marked1;
    newer := @marked2;
  end else begin
    older := @marked2;
    newer := @marked1;
  end;

  if asPicked then begin
    labelOlder := rsMarked;
    labelNewer := rsCurrent;
  end else begin
    labelOlder := rsOlder;
    labelNewer := rsNewer;
  end;

  initialize(dtl);

  dtl.toolIndex := mi.Tag;
  dtl.gitMgr := fGitMgr;
  dtl.base.filename := ExtractFilename(older^.filename);
  dtl.base.tag := older^.CommitOID;
  dtl.base.tree := older^.treehash;
  dtl.base.meta := format('%s: %s',[labelOlder, GetTempFilename(dtl.base.filename, dtl.base.tag)]);
  dtl.comp.filename := ExtractFilename(newer^.filename);
  dtl.comp.tag := newer^.CommitOID;
  dtl.comp.tree := newer^.treehash;
  dtl.comp.meta := format('%s: %s',[labelNewer, GetTempFilename(dtl.comp.filename, dtl.comp.tag)]);

  LaunchDiffTool(dtl, cmdOut);
end;

procedure TfrmFileHistory.OnCopySubject(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
begin
  Clipboard.AsText := fHistory[mi.Tag].Subject;
end;

procedure TfrmFileHistory.ObservedChanged(Sender: TObject; what: Integer;
  data: PtrInt);
begin
  case what of
    GITMGR_EVENT_REFLISTCHANGED:
      begin
        grid.Invalidate;
      end;
  end;

end;

procedure TfrmFileHistory.SetHlHelper(AValue: THighlighterHelper);
begin
  if fhlHelper = AValue then Exit;
  fhlHelper := AValue;
  if fhlHelper<>nil then
    fhlHelper.SetHighlighter(txtDiff, 'x.diff');
end;

procedure TfrmFileHistory.DoRowSelection(aRow: Integer);
var
  aIndex: Integer;
  L: TStringList;
  cmd: string;
begin
  if (fLastRow<>aRow) and (Length(fHistory)>0) then begin
    aIndex := aRow - grid.FixedRows;
    L := TStringList.Create;
    try
      cmd := ' --format=fuller '+fHistory[aIndex].CommitOID;
      if not fIsDir then
        cmd += ' -- ' + fHistory[aIndex].Filename;
      fGit.Show(cmd, L);
      txtDiff.Lines.Assign(L);
    finally
      L.Free;
    end;
    fLastRow := aRow;
  end;
end;

procedure TfrmFileHistory.SearchLog(txt: string; forward: boolean;
  startRow: Integer; searchIn: TSetOfByte);
var
  L: TStringList;
  i, delta, aRow, aIndex, anyRow, count: Integer;
  found: boolean;
begin

  if SearchIn=[] then
    SearchIn := [SEARCHIN_COMMIT, SEARCHIN_AUTHOR, SEARCHIN_SUBJECT];

  L := TStringList.Create;
  try
    L.DelimitedText := lowercase(txt);
    if forward then delta := 1 else delta := -1;
    if startRow<0 then aRow := grid.Row
    else               aRow := startRow;

    anyRow := -1;
    aRow := aRow + delta;
    while (aRow >= grid.FixedRows) and (aRow <= grid.RowCount-1) do begin
      aIndex := aRow - grid.FixedRows;

      count := 0;
      for i:=0 to L.Count-1 do begin

        found := (SEARCHIN_COMMIT in SearchIn) and (pos(L[i], fHistory[aIndex].CommitOID)>0);
        if not found then
          found := (SEARCHIN_AUTHOR in SearchIn) and (pos(L[i], lowercase(fHistory[aIndex].author))>0);
        if not found then
          found := (SEARCHIN_SUBJECT in SearchIn) and (pos(L[i], lowercase(fHistory[aIndex].Subject))>0);

        if found then begin
          if (anyRow<0) then
            anyRow := aIndex;
          inc(count);
        end;

      end;

      if count=L.Count then begin
        // found all terms
        grid.Row := aRow;
        //btnNext.Enabled := true;
        //btnPrev.Enabled := true;
        exit;
      end;

      aRow := aRow + delta;
    end;

    // not found, but check anyRow
    if anyRow>=grid.FixedRows then begin
      // partial find
      grid.Row := aRow;
      //btnNext.Enabled := true;
      //btnPrev.Enabled := true;
    end;

    ShowMessageFmt('Could''t find "%s"',[txt]);

  finally
    L.Free;
  end;
end;

function TfrmFileHistory.GetTreeInfo(aIndex: Integer; out prevTree,
  curTree: string): boolean;
var
  aRow, m1, m2: Integer;
  cmdOut: RawByteString;
  head, tail: PChar;
  c: char;
  path, origPath, score: string;
begin

  result := false;
  prevTree := '';
  curTree := '';

  aRow := aIndex + grid.FixedRows;
  if aRow=grid.Row then begin
    // this is the selected row, get the info from the log...
    for m1 := 0 to txtDiff.Lines.Count-1 do begin
      score := txtDiff.Lines[m1];
      if pos('index ', score)=1 then begin
        m2 := pos('..', score);
        if m2>0 then begin
          // found
          prevTree := copy(score, 7, m2-7);
          aRow := score.IndexOf(' ', m2+2);
          if aRow>0 then
            curTree := copy(score, m2+2, aRow-(m2+2)+1)
          else
            curTree := copy(score, m2+2, MAXINT);
          result := true;
          break;
        end;
      end;
    end;

  end else begin
    // have to query git about it...
    score := fHistory[aIndex].CommitOID + ' -- ' + fHistory[aIndex].Filename;
    if fGit.Any('diff-tree --no-commit-id -r '+ score, cmdOut)<=0 then begin
      head := {%H-}@cmdOut[1];
      tail := head + Length(cmdOut);
      ParseTreeItem(head, tail, m1, m2, prevTree, curTree, c, path, origPath, score);
      result := true;
    end;
  end;

end;

function TfrmFileHistory.GetMarkedRow: Integer;
var
  i: Integer;
begin
  result := -1;
  for i:=0 to Length(fHistory)-1 do begin
    if fMarkedHelper.MarkedItem.CommitOID=fHistory[i].CommitOID then begin
      result := i + grid.FixedRows;
      break;
    end;
  end;

end;

end.

