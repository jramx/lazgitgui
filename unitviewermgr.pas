unit unitviewermgr;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  Controls, Forms,
  unitifaces, unitgitutils, unitframeeditor, unitframeimage;

type

  TViewerMgrFlags = set of (vmfCanEdit, vmfCanOpen, vmfIsText);

  { TViewerMgr }

  TViewerMgr = class
  private
    fFileCanEdit: Boolean;
    fFileCanOpen: Boolean;
    fFileIsText: Boolean;
    fViewers: array of IViewer;
    fViewer: IViewer;
    fControl: TWinControl;
    fEmbedded: boolean;
    fHlHelper: TObject;
    fImgFrame: TfrmImage;
    fOnQuit: TNotifyEvent;
    fReadOnly: boolean;
    fTxtFrame: TframeEditor;
    procedure HideAllBut(viewer: IViewer);
    procedure AttachViewer;
    procedure SetEmbedded(AValue: boolean);
    procedure SetHlHelper(AValue: TObject);
    procedure SetOnQuit(AValue: TNotifyEvent);
    procedure SetProperty(aProperty: string; aValue: TNotifyEvent); overload;
    procedure SetProperty(aProperty: string; aValue: Variant); overload;
    procedure SetProperty(aProperty: string; aValue: TObject); overload;
    procedure SetReadOnly(AValue: boolean);
    procedure AddViewer(aViewer: IViewer);
    function  CanOpen(aFile: string; isText: boolean): boolean;
    function  CanEdit(aFile: string; isText: boolean): boolean;
  public
    constructor create;
    destructor Destroy; override;

    procedure LoadFromFile(aFile: string);
    procedure LoadFromStream(stream: TStream; meta:string='');
    procedure Hide;
    function Analyze(aFile: string): TViewerMgrFlags;

    property AttachTo: TWinControl read fControl write fControl;
    property OnQuit: TNotifyEvent read fOnQuit write SetOnQuit;
    property Embedded: boolean read fEmbedded write SetEmbedded;
    property Readonly: boolean read fReadOnly write SetReadOnly;
    property HlHelper: TObject read fHlHelper write SetHlHelper;
  end;


implementation

{ TViewerMgr }

procedure TViewerMgr.HideAllBut(viewer: IViewer);
var
  v: IViewer;
begin
  for v in fViewers do
    if v<>viewer then
      v.SetVisible(false);
end;

procedure TViewerMgr.AttachViewer;
begin
  HideAllBut(nil);
  if fViewer<>nil then begin
    fViewer.AttachTo(fControl);
    fViewer.SetVisible(true);
  end;
end;

procedure TViewerMgr.SetEmbedded(AValue: boolean);
begin
  if fEmbedded = AValue then Exit;
  fEmbedded := AValue;
  SetProperty('embedded', AValue);
end;

procedure TViewerMgr.SetHlHelper(AValue: TObject);
begin
  if fHlHelper = AValue then Exit;
  fHlHelper := AValue;
  SetProperty('hlhelper', AValue);
end;

procedure TViewerMgr.SetOnQuit(AValue: TNotifyEvent);
begin
  if fOnQuit = AValue then Exit;
  fOnQuit := AValue;
  SetProperty('onquit', AValue);
end;

procedure TViewerMgr.SetProperty(aProperty: string; aValue: TNotifyEvent);
var
  v: IViewer;
begin
  for v in fViewers do
    v.SetProperty(aProperty, aValue);
end;

procedure TViewerMgr.SetProperty(aProperty: string; aValue: Variant);
var
  v: IViewer;
begin
  for v in fViewers do
    v.SetProperty(aProperty, aValue);
end;

procedure TViewerMgr.SetProperty(aProperty: string; aValue: TObject);
var
  v: IViewer;
begin
  for v in fViewers do
    v.SetProperty(aProperty, aValue);
end;

procedure TViewerMgr.SetReadOnly(AValue: boolean);
begin
  if fReadOnly = AValue then Exit;
  fReadOnly := AValue;
  SetProperty('readonly', AValue);
end;

constructor TViewerMgr.create;
begin
  inherited Create;

  fImgFrame := TfrmImage.Create(nil);
  fTxtFrame := TframeEditor.Create(nil);

  AddViewer(fImgFrame);
  AddViewer(fTxtFrame);
end;

destructor TViewerMgr.Destroy;
begin
  fTxtFrame.Free;
  fImgFrame.Free;
  inherited Destroy;
end;

procedure TViewerMgr.AddViewer(aViewer: IViewer);
var
  i: Integer;
begin
  i := Length(fViewers);
  SetLength(fViewers, i+1);
  fViewers[i] := aViewer;
end;

function TViewerMgr.CanOpen(aFile: string; isText: boolean): boolean;
var
  v: IViewer;
begin
  result := false;
  for v in fViewers do
    if v.CanOpen(aFile, isText) then begin
      result := true;
      break;
    end;
end;

function TViewerMgr.CanEdit(aFile: string; isText: boolean): boolean;
var
  v: IViewer;
begin
  result := false;
  for v in fViewers do
    if v.CanEdit(aFile, isText) then begin
      result := true;
      break;
    end;
end;

procedure TViewerMgr.LoadFromFile(aFile: string);
var
  i: Integer;
begin

  fViewer := nil;
  for i:=0 to Length(fViewers)-1 do begin
    if fViewers[i].LoadFromFile(aFile) then begin
      fViewer := fViewers[i];
      break;
    end;
  end;

  AttachViewer;
end;

procedure TViewerMgr.LoadFromStream(stream: TStream; meta: string);
var
  i: Integer;
begin

  fViewer := nil;
  for i:=0 to Length(fViewers)-1 do begin
    if fViewers[i].LoadFromStream(stream, meta) then begin
      fViewer := fViewers[i];
      break;
    end;
  end;

  AttachViewer;
end;

procedure TViewerMgr.Hide;
begin
  HideAllBut(nil);
end;

function TViewerMgr.Analyze(aFile: string): TViewerMgrFlags;
var
  isText: Boolean;
begin
  result := [];
  isText := IsTextFile(aFile);
  if isText then Include(result, vmfIsText);
  if CanOpen(aFile, isText) then Include(result, vmfCanOpen);
  if CanEdit(aFile, isText) then Include(result, vmfCanEdit);
end;

end.

