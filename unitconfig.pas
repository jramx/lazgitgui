{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  Config unit
}
unit unitconfig;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, {$ifdef UseINI}IniFiles,{$endif} fpjson, jsonparser,
  LazLogger, Graphics, Menus, Forms,
  unitcommon, unitifaces, unitcustomconfig;

type

  { TConfig }

  TConfig = class(TCustomConfig)
  private
  public
    procedure ReadWindow(aForm: TForm; aKey:string; section:string=SECTION_DEFAULT);
    procedure WriteWindow(aForm: TForm; aKey:string; section:string=SECTION_DEFAULT);
    procedure ReadFont(aFont: TFont; aKey:string; defPitch:TFontPitch=fpFixed; section:string=SECTION_DEFAULT);
    function MenuMRE(aMRE: TComponent; save: boolean; onclick:TNotifyEvent; aCaption: string; section:string=SECTION_DEFAULT): TMenuItem;
  end;

var
  fConfig: TConfig;

implementation


procedure TConfig.ReadWindow(aForm: TForm; aKey: string; section: string);
begin
  aForm.Left :=   fConfig.ReadInteger(aKey + '.Left',    aForm.Left,   section);
  aForm.Top :=    fConfig.ReadInteger(aKey + '.Top',     aForm.Top,    section);
  aForm.Width :=  fConfig.ReadInteger(aKey + '.Width',   aForm.Width,  section);
  aForm.Height := fConfig.ReadInteger(aKey + '.Height',  aForm.Height, section);
  if fConfig.ReadBoolean(aKey + '.maximized', false, section) then
    aForm.WindowState := wsMaximized;
end;

procedure TConfig.WriteWindow(aForm: TForm; aKey: string; section: string);
var
  isMaximized: Boolean;
begin
  isMaximized := aForm.WindowState=wsMaximized;
  fConfig.WriteBoolean(aKey + '.maximized', isMaximized, SECTION_GEOMETRY);
  if not isMaximized then begin
    fConfig.WriteInteger(aKey + '.Left', aForm.Left, SECTION_GEOMETRY);
    fConfig.WriteInteger(aKey + '.Top', aForm.Top, SECTION_GEOMETRY);
    fConfig.WriteInteger(aKey + '.Width', aForm.Width, SECTION_GEOMETRY);
    fConfig.WriteInteger(aKey + '.Height', aForm.Height, SECTION_GEOMETRY);
  end;
end;

procedure TConfig.ReadFont(aFont: TFont; aKey: string; defPitch: TFontPitch;
  section: string);
var
  s: String;
  aQuality: TFontQuality;
begin
  with aFont do begin
    s := Name;
    aQuality := Quality;
    if defPitch=fpFixed then begin
      {$ifdef Darwin}
      s := 'Menlo';
      aQuality := fqAntialiased;
      {$endif}
      {$ifdef MsWindows}
      s := 'Courier New';
      {$endif}
    end;
    Name := fConfig.ReadString(aKey+'.font.name', s, section);
    Size := fConfig.ReadInteger(aKey+'.font.size', 10, section);
    if fConfig.ReadBoolean(aKey+'.font.antialiased', aQuality=fqAntialiased, section) then
      Quality := fqAntialiased
    else
      Quality := fqNonAntialiased;
  end;
end;

function TConfig.MenuMRE(aMRE: TComponent; save: boolean;
  onclick: TNotifyEvent; aCaption: string; section: string): TMenuItem;
var
  i, n: Integer;
  menu: TMenuItem;
  arr: TJSONArray;

  procedure NewMRE(newCaption:string);
  begin
    result := TMenuItem.Create(aMRE.Owner);
    result.Caption := newCaption;
    result.OnClick := onclick;
    menu.Insert(0, result);
  end;

begin

  if save and (aCaption='') then
    exit(nil);

  if (not (aMRE is TMenuItem)) and (not (aMRE is TMenu)) then
    raise Exception.Create('aMRE must be either a T[Pop]Menu or a TMenuItem');

  if aMRE is TMenu then
    menu := TMenu(aMRE).Items
  else
    menu := TMenuItem(aMRE);

  OpenConfig;
  if Save then begin

    i := menu.IndexOfCaption(aCaption);
    if i<0 then
      NewMRE(aCaption)
    else
      result := menu.Items[i];

    result.MenuIndex := 0;

    {$IFDEF UseINI}
    WriteInteger('MRECount', menu.Count, section);
    for i := 1 to menu.Count do
      WriteString(IntToStr(i), menu.Items[i - 1].Caption, section);
    {$ENDIF}

    arr := TJsonArray.Create;
    for i:=1 to menu.Count do
      arr.Add(menu.Items[i-1].Caption);

    WriteArray(section, arr);

  end else begin

    menu.Clear;
    {$IFDEF UseINI}
    n := ReadInteger('MRECount', 0, section);
    for i:=n downto 1 do
      NewMRE(ReadString(IntToStr(i), '' , section));
    {$ELSE}
    arr := ReadArray(section);
    if arr<>nil then
      for i:=0 to arr.Count-1 do
        NewMRE(arr.Strings[i]);
    {$ENDIF}

  end;
  CloseConfig;
end;

end.

