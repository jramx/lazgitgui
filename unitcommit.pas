{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  TCommit.
  A class that knows how to load a commit
}
unit unitcommit;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  unitcommon, unitgittypes, unitifaces, unitentries, unitparsers, unitgitmgr;

type

  { TCommit }

  TCommit = class
  private
    fAuthor: String;
    fCommit: String;
    fDate: String;
    fEmail: String;
    fGitMgr: TGitMgr;
    fSubject: String;
    fEntries: TStringList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Load(aCommit: string);

    property GitMgr: TGitMgr read fGitMgr write fGitMgr;
    property Author: string read fAuthor;
    property SHA1: string read fCommit;
    property Date: string read fDate;
    property Email: string read fEmail;
    property Subject: string read fSubject;
    property Entries: TStringList read fEntries;
  end;

implementation

{ TCommit }

constructor TCommit.Create;
begin
  inherited Create;
  fEntries := TStringList.Create;
end;

destructor TCommit.Destroy;
begin
  ClearEntries(fEntries);
  fEntries.Free;
  inherited Destroy;
end;

procedure TCommit.Load(aCommit: string);
var
  fGit: IGit;
  cmdOut: RawByteString;
  head, tail: pchar;
  rCommit: string;
begin

  ClearEntries(fEntries);

  fAuthor := '';
  fEmail := '';
  fDate := '';
  fSubject := '';

  fGit := fGitMgr.Git;
  fCommit := aCommit;

  rCommit := 'show --raw --no-abbrev ' + aCommit;
  if fGit.Any(rCommit, cmdOut)<=0 then begin
    head := @cmdOut[1];
    tail := head + Length(cmdOut);
    ParseCommit(head, tail, fEntries, rCommit, fAuthor, fEmail, fDate, fSubject);
  end;

  fGit := nil;
end;

end.

