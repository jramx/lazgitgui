unit tstsmain;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  unitcommon, unitcustomconfig, unitgitmgr, unitgit;

type

  { TMain }

  TMain = class
  private
    fGitMgr: TGitMgr;
    fConfig: TCustomConfig;
  public
    constructor Create;
    destructor Destroy; override;

    property GitMgr: TGitMgr read fGitMgr;
    property Config: TCustomConfig read fConfig;
  end;

var
  fMain: TMain;

implementation

{ TMain }

constructor TMain.Create;
begin
  inherited Create;

  fGitMgr := TGitMgr.Create;
  fConfig := TCustomConfig.Create;

  fConfig.OpenConfig;

  fConfig.CloseConfig;
end;

destructor TMain.Destroy;
begin
  fConfig.Free;
  fGitMgr.Free;

  inherited Destroy;
end;

end.

