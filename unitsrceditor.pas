unit unitsrceditor;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Math,
  LCLType, Forms, Controls, Graphics, Dialogs,
  unitcommon, unitgitutils, unitconfig, unithighlighterhelper, unitviewermgr;

type

  { TfrmEditor }

  TfrmEditor = class(TForm)
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    fHlHelper: THighlighterHelper;
    fViewerMgr: TViewerMgr;
    procedure OnEditorQuit(Sender: TObject);
  public
    //procedure LoadFromFile(aFile:string);
    //procedure LoadFromStream(stream: TStream; metaFilename:String='');

    property ViewerMgr: TViewerMgr read fViewerMgr;
  end;

var
  frmEditor: TfrmEditor;

implementation

{$R *.lfm}

{ TfrmEditor }

procedure TfrmEditor.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  fConfig.WriteWindow(Self, 'frmEditor', SECTION_GEOMETRY);
end;

procedure TfrmEditor.FormCreate(Sender: TObject);
begin
  fHlHelper := THighlighterHelper.create;
  fConfig.ReadWindow(Self, 'frmEditor', SECTION_GEOMETRY);

  fViewerMgr := TViewerMgr.create;
  fViewerMgr.AttachTo := Self;
  fViewerMgr.OnQuit := @OnEditorQuit;
  fViewerMgr.HlHelper := fHlHelper;
end;

procedure TfrmEditor.FormDestroy(Sender: TObject);
begin
  fViewerMgr.Free;
  fHlHelper.Free;
end;

procedure TfrmEditor.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then
    Close;
end;

procedure TfrmEditor.OnEditorQuit(Sender: TObject);
begin
  close;
end;

//procedure TfrmEditor.LoadFromFile(aFile: string);
//begin
//  if not image.LoadFromFile(aFile) then begin
//    if IsTextFile(aFile) then begin
//      editor.ReadOnly := ReadOnly;
//      editor.LoadFromFile(aFile);
//      image.Visible := false;
//    end else
//      // TODO: hex dump
//  end else
//    editor.visible := false;
//end;
//
//procedure TfrmEditor.LoadFromStream(stream: TStream; metaFilename: String);
//var
//  aSize: Integer;
//begin
//  if not image.LoadFromStream(stream) then begin
//    // TODO: ATM we only use TMemoryStream for this ...
//    if stream is TMemoryStream then begin
//      aSize := Min(1024, stream.Size);
//      if IsBinBuffer(TMemoryStream(stream).Memory, aSize) then begin
//        // TODO: hex dump
//      end else begin
//        editor.ReadOnly := ReadOnly;
//        editor.LoadFromStream(stream, metaFilename);
//        image.Visible := false;
//      end;
//    end;
//  end else begin
//    editor.Visible := false;
//  end;
//end;

end.

