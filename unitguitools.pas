unit unitguitools;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  Menus;

  function AddPopItem(pop: TPopupMenu; caption:string; onClick:TNotifyEvent;
                      tag: Ptrint; submenu:string=''; miclass: TMenuItemClass = nil;
                      atIndex:Integer=-1): TMenuItem;

implementation

function AddPopItem(pop: TPopupMenu; caption: string; onClick: TNotifyEvent;
  tag: Ptrint; submenu: string; miclass: TMenuItemClass; atIndex: Integer
  ): TMenuItem;
var
  mnu: TMenuItem;
begin

  if miclass=nil then
    miclass := TMenuItem;

  if submenu<>'' then begin
    mnu := pop.Items.Find(submenu);
    if mnu=nil then begin
      mnu := miclass.Create(pop.Owner);
      mnu.Caption := submenu;
      if atIndex>=0 then  pop.Items.Insert(atIndex, mnu)
      else                pop.Items.Add(mnu);
    end;
  end else
    mnu := pop.Items;

  if caption='-' then begin
    if mnu[mnu.Count-1].Caption='-' then begin
      result := mnu[mnu.Count-1];
      exit;
    end;
  end;

  result := miclass.Create(pop.Owner);
  result.Caption := caption;
  result.OnClick := onClick;
  result.Tag := tag;

  if atIndex>=0 then mnu.Insert(atIndex, result)
  else               mnu.Add(result);
end;

end.

