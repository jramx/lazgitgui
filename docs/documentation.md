**LazGitGui**

This is a [git](https://git-scm.com/book/en/v2) tool resembling the popular 'git gui' program, with some additional features.

If you don't know what git gui is, it is a graphical representation of the git status, where one can do basic git operations like watch what files under the work space have been modified and see the differences with respect to the original files. It also allow move files between the unstaged and the staged area and make commits from it.

Git Gui has some cool features not yet available in this program, for example the selective apply or revert line changes from the diff viewer.

Contrary to Git Gui which relies on gitk for that feature, LazGitGui has a log feature that allows you to see the history of the repository and do some operation on branches and commits.

**Features.**

  * Clone a repository.
  * Handle remotes dialog.
  * State panel that shows current and upstream branches. Commits ahead and commits behind. Tag description.
  * User defined buttons to assign click and run git commands.
  * List of untracked and changed files, selecting a file shows its changes or if is untracked the file content. A context menu contain numerous features like:
    * Stage one, some or all files.
    * Restore one, some or all changed files.
    * Delete files.
    * Edit files in an internal or external editor.
    * Show Picture files.
    * Create a patch from the selection and save it to a file.
    * Show the file history.
    * Modify the ignore file with information of the selected file.
    * Show tracked and ignored files.
  * List of staged files ready to be committed.
  * Buttons for reload, fetch, pull, push and push with dialog.
  * Amend a commit.
  * A repository information button.
  * <u>A Log window</u> listing commits with the following features:
    * Filter or Search commits by multiple terms.
    * Show the changes made by a commit.
    * Show the repository tree at some commit in history, this helps to check what was the content of certain file at some commit in particular.
    * Show the file history of the selected file.
    * Show the graph detailing the relationship between commits.
    * Regular expression rules for matching bug, commit and URL links in commit subject (commit links are valuable for pointing at previous commits in history).
  * The log window context menu has the following features:
    * Go to the checked out commit (HEAD or Detached HEAD), go to parent or child commit.
    * Create a tag a this commit.
    * Delete tags.
    * Switch to a tag.
    * Create a branch a this commit.
    * Switch to a branch.
    * Delete local and remote branches.
    * Start a Rebase on top of the selected commit.
    * Cherry Pick this commit.
    * Revert this commit.
    * Merge.
    * Start a Bisect operation by selecting a pair of commits.
    * Reset current branch to a commit.
    * Switch to a commit.
    * Copy commit information: full, sha, subject, etc.

* Other features:
    * Lock to start directory. While launching LazGitGui with the --locked argument, it will show the repository restricted to the given directory or parent of given file path instead of the whole repository.
    * Dropping a patch file in LazGitGui allows to apply the patch to the repository (no commit is created by this operation).
    * The log window title show information about the repository state.
    * Git state and links to actions. LazGitGui now detects if the repository is doing any of the following operations in some intermediate state: Rebase, Cherry pick, Revert, Merge and Bisect. It will show some options for taking a corresponding action, here an example of a rebase with conflicts:
    ![Rebase with conflicts state](../images/rebasingconflict.png)
    * Repository browser

**Screenshots.**

***Status Screen***
![Main Screen](../images/main.png)

***Log Screen***
![Log Window](../images/log.png)

***File History Screen***
![File History Window](../images/filehistory.png)

**Download**

There are no install packages at this time, see [Building LazGitGui]()

**Report issues**

Please file a report in the [bug tracker](https://gitlab.com/jramx/lazgitgui/-/issues)

**Building LazGitGui**

At the moment there are no binary releases available so the program have to be compiled and used from the compiling directory.

This is a [Lazarus/Free Pascal](https://www.lazarus-ide.org) program so you will need Lazarus in order to compile it, a standard  install will be enough as LazGitGui does not use special packages.

The most recent Lazarus release should be fine but if it's not please let me know.

Currently LazGitGui is being developed using the following Lazarus/Free Pascal environment:

`Lazarus 3.99 @ d5031b1eaf FPC 3.2.2 i386-win32-win32/win64`<br>
`Lazarus 2.3.0 (rev main-2_3-2876-gf79a5d6718) FPC 3.2.2 x86_64-darwin-cocoa`<br>
`Lazarus 2.3.0 (rev main-2_3-3436-gda12f4b7d5) FPC 3.2.2 aarch64-darwin-cocoa`<br>
`Lazarus 2.3.0 (rev main-2_3-2889-g1b9d3d4cbf) FPC 3.2.2 x86_64-linux-gtk2`<br>
`Lazarus 3.99 (rev main_3_99-27-g0d7fc55be1) FPC 3.2.3 x86_64-linux-gtk2`<br>

In Lazarus, open the project file lazgitgui.lpi and compile it.

**How to use.**

LazGitGui can use a supplied path passed as the last parameter in the command line, it can be a directory or a file residing within the working area of the repository at any level, if a path is not specified it will use the current working directory. Some suggestions for invoking LazGitGui:

From Lazarus, an external tool can be configured with the following properties:

(Linux + Windows)

    Title: LazGitGui
    Program Filename: PATH/TO/LazGitGui executable
    Parameters: --logfile=PATH/TO/LazGitGui/ide.log $EdFile()
    Shortcut: CTRL+ALT+G

(macOS)

    Title: LazGitGui
    Program Filename: open
    Parameters: -a PATH/TO/LazGitGui.app --args --logfile=PATH/TO/LazGitGui/ide.log $EdFile()
    Shortcut: CMD + G

The logfile parameter is optional.

*LazGitGui does not handle authorization yet (or any interactive commands)*.

**Launching LazGitGui**

The program tries to find the git executable in the system (or user) path, once located, it gets stored in the LazGitGui config file. LazGitGui does this only once, the next time the program is started, the git executable path is read from the config file.

In the case LazGitGui was unable to locate the git program, or if one specific version of git is needed, one have to edit the config file and introduce the git location there. At the moment, LazGitGui does not have a config screen.

The config file can be found in the following locations:

    Windows:    %LOCALAPPDATA%\appdata\lazgitgui\config\lazgitgui.cfg.json
    Linux/mac:  $HOME/.config/lazgitgui\lazgitgui.cfg.json

for example (this works under Linux):

  "lazgitgui.cfg" : {
    "git" : "/usr/bin/git"
  }

As an alternative for editing the config file, a command line parameter `--git=PATH/TO/GIT_EXECUTABLE` is available.

By default, LazGitGUI will show the [Status Screen]() mode, if the -l or --log argument is supplied it will be launched in the [Standalone Log]() mode where only the log window will be shown. If the -f &lt;file&gt; or --filehistory=&lt;file&gt; argument is supplied, it will be run in [File History]() mode which shows all the changes of the given file. In this case &lt;file&gt; have to be an existing tracked file within the working area, if&lt;file&gt; is not supplied it exists with an error.

**Status screen**

The main screen is a graphic representation of the git status together with some extra information that allows us, with a quick look, to obtain the general status of the repository.

***Branch status***

![Branch status labels](../images/branchstatus.png)

In this section, the current branch (at the left) is presented, next, information about how many not yet pushed local commits are (commits ahead) and how many commits the remote end has that you don't (commits behind), next is the name of the remote (tracking) branch. If enabled, next is information about how many commits are there since the last tag in the branch.

Hovering the mouse on the upstream label will show you the repository URL.

The "current branch" label, has a context menu:

![Branch context menu](../images/branchcontextmenu.png)

From there a new branch can be created, or the repository can be switched to an existing local branch, the current branch is selected in this list.

Note. Deleting branches is implemented in the log window.


***Direct commands and Custom Commands***

LazGitGui supports two kinds of user defined commands, Direct Commands and Custom Commands.

![commands](../images/commands.png)

Direct command.<br>
Are simple (non interactive) commands, it is an alternative to open the terminal and type git commands, which can optionally be recorded and are available in the [>] button menu. This commands are always unconditionally run in a window.

Custom commands.<br>
Are also user defined commands but they appear at the right of the [+] button, in fact, each command creates a button with a user defined caption or image (not implemented yet), commands can be reordered, renamed or deleted. As a difference with direct commands, each command (or script) can have a security option so a prompt may appear before execution and the command may or may not show a window. Each command may consist of any number of git commands, each command on a separate line, lines that do not start with 'git ' are considered comments and are ignored.
Custom commands support some variables which are substituted before command execution. All variables start with a $ symbol and are case insensitive. At the moment, LazGitGui understand the following variables:

| Variable    | Meaning                                                                                   |
|-------------|-------------------------------------------------------------------------------------------|
|$branch      |The current branch name                                                                    |
|$branchoid   |The SHA-1 value identifying the current branch                                             |
|$upstream    |The upstream branch which the local branch is tracking, is always in the remote/branch form|
|$remote      |The remote portion of the upstream branch (i.e. the fraction before the '/')               |
|$remotebranch|The branch name of the upstream branch (i.e. the fraction after the '/')                   |
|$remoteurl   |The URL that the remote is pointing to                                                     |
|$tag         |The previous tag or annotated tag obtained by git describe                                 |
|$tagoid      |The SHA-1 value corresponding to the $tag                                                  |
|             |                                                                                           |


***Unstaged list***

![Unstaged List](../images/unstagedlist.png)

This list show the changes in the working area, the kind of change is represented with a distinctive icon at the start of the file name. All changed files are presented as relative to the top level directory of the repository. Files in this list can be "moved" to the staged list by clicking the icon or by using other options available in the context menu:

![Unstaged List Context Menu](../images/unstagedcontextmenu.png)

Here, options to move the selected file, all changed files or all files including files not yet tracked. Also, changes on files can be discarded using the restore functions.

The context menu changes dynamically based on the selected files, a contiguous group can be selected by SHIFT+clicking a range of files or CTRL+Clicking individual files.

If only one untracked file is selected (a file with a plain icon), the menu presents some options for ignoring the file or all files of the same type. Ignoring files in this list will modify the repository top level .gitignore file:

![Ignore menu items](../images/unstagedignore.png)

***Staged list***

The staged list shows all new (untracked) and changed files that have been selected for the next commit. In this list the context menu have options for "undoing" the selected files, that is, to unstage items so they are 'returned' to the unstaged list.

The [git book](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository) has a nice diagram that shows the life cycle of the status of your files

***Viewer area***

This area show the changes of the selected files in either the staged or unstaged list. If the selected file is a new in the repository (untracked), the file content (if the file is text) or a binary indicator is shown in this area. This area sometimes also show error messages from git commands on the staged or unstaged list.

If the viewer show diff content, the context menu opened by a right click, will offer the following options depending on if the source of the diff is the unstaged list or the staged list:

* **Stage Hunk**. This menu item will appear if the mouse click was under a hunk, on action this hunk will be applied to the stage area.
* **UnStage Hunk**. This menu will appear when the diff content comes from a file in the staged area.
* **Revert Hunk**. With this option, the hunk under the the mouse pointer will be reverted from the current diff, this hunk revert is applied to the file in the working dir immediately. The change introduced by the patch will be added to a undo list.
* **Undo Revert Hunk**. Whenever a hunk is reverted, it is added to the undo list, and can be restored by this command in the order they were applied. Note that this undo list is available only while the program is running.


***Commit Area***

![Commit Area](../images/commitarea.png)

Once there are some items in the staged list and a commit subject is typed in the message text area, a commit can be made by pressing the Commit button, then when you are ready commits can be pushed to the upstream repository by pressing the Push button.

While you are typing at the commit area pressing the CTRL+B combo will insert the current branch name, also SHIFT+CTRL+UP will bring back the last used commit message.

***Buttons***

The <u>rescan</u> button performs an git status and updates the staged and unstaged lists, it also updates the current branch information.

The <u>Fetch</u> button does a git fetch followed by a rescan. Fetch retrieves the latest changes from the remote repository but does not actually modify the working areas so it is save and recommended to periodically fetch from the remote repository, as this action updates the tracking information, one can see how many commits the remote end has that you don't.

The <u>Pull</u> button performs a git pull command.

The <u>Push</u> button performs a git push using the current tracking information configured for the current branch. Push sends all (not pushed) local commits under the current branch to the remote repository, if the current branch has not yet configured tracking branch, it will show a dialog box asking if you want to push your changes to the current remote and additionally set up tracking information using the same name, after that, pushing will be done without any dialog.

![Push Without tracking information](../images/pushnotracking.png)

Pushing a new branch when there are multiple remotes will trigger the push dialog.

The <u>Push...</u> button display the push dialog where you can make use of some custom options.

![Push Without tracking information](../images/pushdialog.png)

***Information***

The [I] button dumps in the viewer area general information about the repository

![Information Dump](../images/information.png)

**The log Window**

This window is invoked by the [NL] (new log) button, or in a standalone way by launching LazGitGui with the -l or --log arguments.

It features a commit listing showing newer entries at the top. Each commit line has a graph item representing the commit hierarchy, the commit subject, author and SHA1 identifier. In the Subject column at the start of the line, there are any references (tags, local or remote branches) pointing to that commit. The current commit (the commit HEAD is pointing at) row, is highlighted in bold style, the current branch is drawn in a red background while other local branches are displayed with green background, remote branches are shown in the origin/branch way and have brown background. Light tags have a yellow background while annotated tags have a distinctive mark, also, annotated tags will show the tag message in a hint.

![Log window references](../images/logwinreferences.png)

Initially, only commits are listed, but by pressing the [SC] (Show Changes) button, the changes introduced by the commit are displayed in a split panel below, where at the left, there is a tree view listing all the files modified by the commit (in PATCH mode) or (in TREE mode) the directory structure (or tree) of the repository as it was at that particular moment in time.

Clicking a file in this tree view, enables the [FH] button which let us see the file history of the selected file, not only from this commit and older but whole file history in the repository. 

![Log changes](../images/logchanges.png)

In patch mode the corresponding selected file diff introduced by the commit is displayed in the viewer area at the right. In Tree mode, the content of the file is show instead.

***The log cache***

The log content is retrieved automatically in full from the git log command listing, in large repositories, this command is a fairly slow operation which is potentially repeated every time the log feature is requested, the strategy used in LazGitGui is asking for the log and save it in a cache, and if it is allowed to retrieve the whole log, the next time only the more recent changes will be queried and newer commits will be updated almost instantaneously, in this way the whole log is available immediately which allow us to do some operations like searching or filtering faster.

This strategy has also some disadvantages, the current cache design relies on the git log to return new commits so they are added on top of the existing ones, but this is not always the case, when this happen the cache is left out of sync and you will see things in the graph which makes no sense, an example of this, is when a branch is being reset to a previous commit and the log is updated, the cache would still hold the old commits and will show them. The same happen when rebasing, a serie of commits will be moved on top of other but the old ones would still appear in the log. For this purpose, LazGitGui has a "rebuild" cache feature which is enabled in three ways:

 1. The first time that the [NL] button and the keyboards SHIFT key are pressed.
 2. Likewise, in the log window, if the SHIFT key is pressed at the same time the [R] button is clicked.
 3. A command line parameter, --ClearCache. Starting LazGitGui with this parameter it's instructed to invalidate the cache once an update is requested.

LazGitGui sometimes know when something will modify the log structure and it will automatically queue a cache rebuild. For the cases where LazGitGui is not smart enough, you have to trigger the cache rebuild manually as explained.

Another disadvantage, if you see it that way, is the duplicate of information in both the cache and the git records. Also, because the cache file holds the subject, dates, author, commit and parent's commit SHA-1s, it may grow "big", here are some examples:

  * Lazarus, 23 years of development, 71045 commits:
    * lazgitgui.logcache: 13.2M, lazgitgui.logindex: 693.8K
  * Free Pascal, 23+ years of development, 71442 commits:
    * lazgitgui.logcache: 15.8M, lazgitgui.logindex: 741.3K
  * Git, 18 years, 73150 commits:
    * lazgitgui.logcache: 12.9M, lazgitgui.logindex: 714.4K
  * LazGitGui, 5 months of development, 796 commits:
    * lazgitgui.logcache: 136.2K, lazgitgui.logindex: 7.8K

Another problem is that some temporary situations, like stash commits, are no easily represented, as if we retrieve them in the log, they would create a record which is not easy to remove later.

Most of these problems can be solved or at least be reduced, this are pending tasks. 

***Filter and Search***

By default search is active as indicated by the [S] button pressed look, just typing terms enable the [<] and [>] buttons, which starts searching backwards or forwards relative to the currently selected commit. Typing [ENTER] start forward search, if a match is found pressing [<] or [>] continue the search in the indicated direction. (TODO: enable F3 and [SHIFT+F3] too). The introduced patterns are considered case insensitive partial strings and look for matches in all fields.

Pressing the [Y] button enables filter which works much in the same way than search, but the [<] and [>] buttons are disabled, so filtering starts by pressing [ENTER]. Once the commit logs are filtered, successive filtering is enabled by introducing new terms, in that case filtering acts only on the already filtered records. It is possible to enable searching over the filtered set by clicking the [S] button. In order to cancel the filter mode, one have to make sure the [Y] button is pressed (if searching the filter set has been active) and delete all terms and then pressing [ENTER], in other words, filtering an empty input string cancels filtering.

while filtering, the graph is hidden as it is of little use in this scenario.

![Log Filter](../images/logfilter.png)<br>
Screen after the term "taskdialog" is entered and [ENTER] is pressed, it finds 66 commits matching the term.

![Log Filter](../images/logfiltermore.png)
Screen after the term "bart" is added to the filter and [ENTER] is pressed. The previous 66 commits are now reduced to 12.

***Show links in the commit subject***

LazGitGui can employ regular expressions for detecting interesting things in the commit message, so a text like #12345 can be detected and converted to a clickable link that allows quickly navigating into the details of the linked bug report. The same way links that point to commits in the git repository hosting can be detected with the corresponding regular expression pattern. 

Another use is to detect referred commits within the same repository, when a patch is reverted or mentioned for any purpose, have links to these commits is helpful because as you know, in git commits are not assigned a revision number but a commit hash. Hashes may contain from a minimum number up to 40 hexadecimal digits and may be difficult to locate in a glimpse.

As currently there is not yet a config editor for this program, the rules for detecting links have to be introduced directly in the config file (see [Launching LazGitGui]() on where to find the config file). Rules are repository dependent.

Each rule can be configured with one of two actions: open and goto.

<u>open</u> will open the browser with the produced link after replacement, if replacement is empty, it will use matched text as the link.

<u>goto</u> will try to find a matching SHA-1 hash within the history of the repository.

As an example, here is how it should be declared the "Links" object within the corresponding object for the Lazarus repository, in this case, located at /home/$USER/dev/lazarus directory, just replace $USER and the rest of the path by the right path in your case. It should be trivial the needed changes to adapt it for other repositories.

    "/home/$USER/dev/lazarus/" : {
    "Links" : [
      {
        "Name" : "Bug tracker links",
        "Pattern" : "#(?:0{1,})?(\\d{5})",
        "Replace" : "https://gitlab.com/freepascal.org/lazarus/lazarus/-/issues/$1",
        "Action" : "open",
        "Color" : "clBlue"
      },
      {
        "Name" : "url links",
        "Pattern" : "(?:https?):\\/\\/(?:[^\\s])+",
        "Replace" : "",
        "Action" : "open",
        "Color" : "clBlue"
      },
      {
        "Name" : "Commit References",
        "Pattern" : "(\\s|#)([0-9A-Fa-f]{6,})",
        "Replace" : "$2",
        "Action" : "goto",
        "Color" : "clMaroon"
      }
    ]
    },

This should be simplified once a config screen is added to LazGitGui.

**Log context menu**


***Bisect***

Use binary search to find the commit that introduced a bug.

Bisect consists in finding at what commit a bug (or feature) was first introduced. It first require that you select a know commit where the bug is exposed, call this the "bad commit" (frequently this is the most recent commit). Then you select an older commit that you know don't have that bug (or feature) and this is called the "good commit". The bisect process is then started and git will load commits into the working area so you can test if the bug is present or not.

This is done by calling git bisect good or git bisect bad at the appropriate moment, the process continue until no more alternatives remain and you found the first bad commit (or the commit that started the feature).

This process is done intuitively in LazGitGui through the log screen, context menu: Bisect, as illustrated in the following <u>mini bisect tutorial</u>:

![bisect step 00: Initial screen](../images/bisect_step00.png)<br>
Step 0. This is the initial screen, the plan is apply a bisect procedure to find at what commit things started to go wrong, in this case, we will mark the "C" commit as bad and the "B" commit as good.

![bisect step 01: Selecting initial bad commit](../images/bisect_step01.png)<br>
Step 1. While the commit "B" is selected, right click on it to show the context menu. Choose: Bisect -> Select this commit as 'bad'. 

![bisect step 02: Screen after bad commit selected](../images/bisect_step02.png)<br>
Step 2. LazGitGui will highlight the commit with a redish color, this means that a initial bad commit has been selected and what commit is that.

![bisect step 03: Selecting initial good commit](../images/bisect_step03.png)<br>
Step 3. Now select the "B" commit and from the context menu, choose "Select this commit as good". Note how the previous bad commit is now marked as "'Bad' selected (cee1484 C)", where a reduced commit SHA-1 and a fragment of the commit message are displayed. In a long range of commits, the initial bad commit could go away from the view, so this helps as an indicator that the bad commit has been already chosen.

![bisect step 04: Screen after good commit selected](../images/bisect_step04.png)<br>
Step 4. Once both the good and the bad commits are selected, the log will highlight the rest of the commits that will be excluded from the bisect operation, git will have to choose one of the un-highlighted commits as the proposal for testing. 

Note also that is irrelevant what to choose first if the good or the bad commit. Also, at this point the bisect operation has not yet started. You are free to choose another commit as bad instead of the already selected, simply select a new commit and choose Bisect -> 'Bad' selected (xxxx). The same regarding the initial good commit.

![bisect step 05: Selecting bisect start](../images/bisect_step05.png)<br>
Step 5. Now is the time for really starting the bisect process through the option Bisect -> Start. Or if you wish, select reset to cancel the previous steps and return to the initial state.

![bisect step 06: After bisect start, proposal 1](../images/bisect_step06.png)<br>
Step 6. After Bisect start has been selected, git will put the repository in detached head state, and will checkout a proposed commit for testing, it will also create a couple of branches: one labeled bisect/bad, and one labeled bisect/good-SHA1. The bisect/bad represents, at all moment, the currently selected first bad commit.

Now is your turn for testing the proposed commit that git has checked out for you in the working area. The next step is to confirm if the proposed commit is good or bad.

![bisect step 07: Select proposal 1 as bad](../images/bisect_step07.png)<br>
Step 7. In this case we will select the proposed commit as bad.

Note also that the context menu has changed once the bisect process has started. What used to be a Bisect menu with sub-menus, it's now expanded to show directly the next available options, either the proposed commit is good, bad or if you prefer, you can see the list of actions the bisect process has completed at the moment through the bisect log. Or if you prefer, you can cancel the bisect process by choosing the Reset option.

In this case we select the proposed commit as bad.

![bisect step 08: Git response showing remaining steps](../images/bisect_step08.png)<br>
Step 8. The git response to our selection, it gives an estimate of how many steps remain, and what would be the next proposal.

![bisect step 09: Screen showing git proposal 2](../images/bisect_step09.png)<br>
Step 9. This screen shows the second proposal, note how the un-highlighted region is reduced with every selection we do.

![bisect step 10: Select proposal 2 as bad](../images/bisect_step10.png)<br>
Step 10. In this case we select the git proposal as bad too.

![bisect step 11: Git response, the first bad commit found](../images/bisect_step11.png)<br>
Step 11. The git response to our selection is shown in this confirmation dialog, this tell us the bisect process has finished because the "first bad commit" has been found. It shows the header of the bad commit and a resume of what changes the commit introduced. Note also that the log window title now include the text "The bisect operation has ended"

![bisect step 12: Final screen showing bisect/bad](../images/bisect_step12.png)<br>
Step 12. This is the look of the final state, note how there are no more un-highlighted regions anymore and bisect/bad is pointing to the just found bad commit. 

This is the time you should create a tag marking to the bad commit so it is remembered, because in the end the repository will be returned to its initial state.

The red highlighted area could mean "from this point on, everything started to go wrong", conversely, the green highlighted area could mean "from the latest bisect/good-sha1 branch and older, everything was fine."

![bisect step 13: To end bisect session select bisect reset](../images/bisect_step13.png)<br>
Step 13. The last step in the bisect process is to reset the repository to the initial state, this is done through the Bisect: Reset menu.

![bisect step 14: The log is restored to the initial state](../images/bisect_step14.png)<br>
Step 14. The last screen should match the initial state.

**Repository Browser**

Launch it with:

  * command line: -b or --browser
  * File menu -> Repository Browser 

![Repository Browser](../images/repositorybrowser.png)