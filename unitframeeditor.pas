{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  This implements a text editor.
}
unit unitframeeditor;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  Forms, Dialogs, Controls, ComCtrls, ActnList, SynEdit, SynEditTypes,
  unitcommon, unitifaces, unithighlighterhelper;

type
  TUpdateWhat = set of (uwCaret, uwModified, uwSelection, uwInsertMode, uwClipboard);

  { TframeEditor }

  TframeEditor = class(TFrame, IViewer)
    actFileOpen: TAction;
    actCopy: TAction;
    actCut: TAction;
    actNew: TAction;
    actReplaceNext: TAction;
    actReplace: TAction;
    actSearchNext: TAction;
    actSearch: TAction;
    actSelectAll: TAction;
    actPaste: TAction;
    actRedo: TAction;
    actUndo: TAction;
    actQuit: TAction;
    actFileSave: TAction;
    actFileSaveAs: TAction;
    actlstFEditor: TActionList;
    sDlg: TSaveDialog;
    stbar: TStatusBar;
    txtEditor: TSynEdit;
    barTools: TToolBar;
    btnCopy: TToolButton;
    btnPaste: TToolButton;
    ToolButton12: TToolButton;
    btnSearch: TToolButton;
    btnReplace: TToolButton;
    ToolButton15: TToolButton;
    btnQuit: TToolButton;
    btnOpen: TToolButton;
    btnNew: TToolButton;
    btnSave: TToolButton;
    btnSaveAs: TToolButton;
    ToolButton5: TToolButton;
    btnUndo: TToolButton;
    btnRedo: TToolButton;
    ToolButton8: TToolButton;
    btnCut: TToolButton;
    procedure actCopyExecute(Sender: TObject);
    procedure actCutExecute(Sender: TObject);
    procedure actFileSaveAsExecute(Sender: TObject);
    procedure actFileSaveExecute(Sender: TObject);
    procedure actPasteExecute(Sender: TObject);
    procedure actQuitExecute(Sender: TObject);
    procedure actRedoExecute(Sender: TObject);
    procedure actReplaceExecute(Sender: TObject);
    procedure actSearchExecute(Sender: TObject);
    procedure actUndoExecute(Sender: TObject);
    procedure txtEditorStatusChange(Sender: TObject; Changes: TSynStatusChanges);
  private
    fEmbedded: boolean;
    fFileName: String;
    fhlHelper: THighlighterHelper;
    fOnQuit: TNotifyEvent;
    fReadOnly: boolean;
    procedure SetEmbedded(AValue: boolean);
    procedure SetOnQuit(AValue: TNotifyEvent);
    procedure SetModified(AValue: boolean);
    procedure SetReadOnly(AValue: boolean);
    procedure UpdateState(what: TUpdateWhat);
    procedure AttachTo(aParent: TObject);
    procedure SetProperty(aProperty: string; aValue: Variant); overload;
    procedure SetProperty(aProperty: string; aValue: TNotifyEvent); overload;
    procedure SetProperty(aProperty: string; aValue: TObject); overload;
  public
    function CanEdit(aFile: string; isText: boolean): boolean;
    function CanOpen(aFile: string; isText: boolean): boolean;
    function LoadFromFile(aFile: string): boolean;
    function LoadFromStream(stream: TStream; metaFilename:String=''): boolean;

    property Embedded: boolean read fEmbedded write SetEmbedded;
    property HlHelper: THighlighterHelper read fhlHelper write fhlHelper;
    property OnQuit: TNotifyEvent read fOnQuit write SetOnQuit;
    property ReadOnly: boolean read fReadOnly write SetReadOnly;
  end;

implementation

{$R *.lfm}

const
  uwAll = [uwCaret..uwClipboard];

{ TframeEditor }

procedure TframeEditor.txtEditorStatusChange(Sender: TObject;
  Changes: TSynStatusChanges);
var
  uw: TUpdateWhat;
begin
  uw := [];

  if [scCaretX, scCaretY] * Changes <> [] then
    Include(uw, uwCaret);

  if scModified in Changes then
    Include(uw, uwModified);

  if scSelection in Changes then
    Include(uw, uwSelection);

  if scInsertMode in Changes then
    Include(uw, uwInsertMode);

  UpdateState(uw);
end;

procedure TframeEditor.actQuitExecute(Sender: TObject);
begin
  if Assigned(fOnQuit) then
    fOnQuit(Self);
end;

procedure TframeEditor.actFileSaveExecute(Sender: TObject);
begin
  txtEditor.Lines.SaveToFile(fFileName);
  UpdateState(uwAll);
end;

procedure TframeEditor.actPasteExecute(Sender: TObject);
begin
  if not ReadOnly then
    txtEditor.PasteFromClipboard();
end;

procedure TframeEditor.actFileSaveAsExecute(Sender: TObject);
begin
  if sDlg.InitialDir='' then
    sDlg.InitialDir := ExtractFilePath(fFilename);
  sDlg.Options := sDlg.Options + [ofOverwritePrompt];
  if sDlg.Execute then
    txtEditor.Lines.SaveToFile(sDlg.FileName);
end;

procedure TframeEditor.actCutExecute(Sender: TObject);
begin
  if not ReadOnly then
    txtEditor.CutToClipboard;
end;

procedure TframeEditor.actCopyExecute(Sender: TObject);
begin
  txtEditor.CopyToClipboard;
end;

procedure TframeEditor.actRedoExecute(Sender: TObject);
begin
  txtEditor.Redo;
end;

procedure TframeEditor.actReplaceExecute(Sender: TObject);
begin
  ShowMessage(rsThisFeatureWillBeImplementedASAP);
end;

procedure TframeEditor.actSearchExecute(Sender: TObject);
begin
  ShowMessage(rsThisFeatureWillBeImplementedASAP);
end;

procedure TframeEditor.actUndoExecute(Sender: TObject);
begin
  txtEditor.Undo;
end;

procedure TframeEditor.SetEmbedded(AValue: boolean);
begin
  if fEmbedded = AValue then Exit;
  fEmbedded := AValue;
  btnNew.visible := not fEmbedded;
  btnOpen.visible := not fEmbedded;
end;

procedure TframeEditor.SetOnQuit(AValue: TNotifyEvent);
begin
  if fOnQuit = AValue then Exit;
  fOnQuit := AValue;
end;

procedure TframeEditor.SetModified(AValue: boolean);
begin
  txtEditor.Modified := AValue;
  actFileSave.Enabled := AValue;
end;

procedure TframeEditor.SetReadOnly(AValue: boolean);
begin
  if fReadOnly = AValue then Exit;
  fReadOnly := AValue;
  txtEditor.ReadOnly := fReadOnly;
end;

procedure TframeEditor.UpdateState(what: TUpdateWhat);
begin
  if uwCaret in What then
    stBar.Panels[0].Text := format('Line: %d Col: %d',[txtEditor.CaretX, txtEditor.CaretY]);

  if uwModified in What then begin
    actUndo.Enabled := txtEditor.CanUndo;
    actRedo.Enabled := txtEditor.CanRedo;
    stBar.Panels[2].Text := BoolToStr(txtEditor.Modified, 'Modified', '');
    actFileSave.Enabled := txtEditor.Modified;
  end;

  if uwSelection in What then begin
    actCopy.Enabled := txtEditor.SelAvail;
    actCut.Enabled := actCopy.Enabled;
  end;

  if uwInsertMode in What then
    stBar.Panels[1].Text := BoolToStr(txtEditor.InsertMode, 'INS', 'OVR');

  if uwClipboard in What then
    actPaste.Enabled := txtEditor.CanPaste;
end;

procedure TframeEditor.AttachTo(aParent: TObject);
begin
  Parent := TWinControl(aParent);
  Align := alClient;
end;

function TframeEditor.LoadFromFile(aFile: string): boolean;
begin
  fFileName := aFile;
  fhlHelper.SetHighlighter(txtEditor, fFileName);
  txtEditor.Lines.LoadFromFile(fFileName);
  stBar.Panels[3].Text := fFileName;
  UpdateState(uwAll);
  result := true;
end;

function TframeEditor.LoadFromStream(stream: TStream; metaFilename: String
  ): boolean;
begin
  fHlHelper.SetHighlighter(txtEditor, metaFilename);
  txtEditor.Lines.LoadFromStream(stream);
  stBar.Panels[3].Text := metaFilename;
  UPdateState(uwAll);
  result := true;
end;

procedure TframeEditor.SetProperty(aProperty: string; aValue: Variant);
begin
  case lowercase(aProperty) of
    'embedded': Embedded := AValue;
    'readonly': ReadOnly := AValue;
  end;
end;

procedure TframeEditor.SetProperty(aProperty: string; aValue: TNotifyEvent);
begin
  case lowercase(aProperty) of
    'onquit': OnQuit := aValue;
  end;
end;

procedure TframeEditor.SetProperty(aProperty: string; aValue: TObject);
begin
  case lowercase(aProperty) of
    'hlhelper': HlHelper := THighlighterHelper(AValue);
  end;
end;

function TframeEditor.CanEdit(aFile: string; isText: boolean): boolean;
begin
 result := isText;
end;

function TframeEditor.CanOpen(aFile: string; isText: boolean): boolean;
begin
  result := isText;
end;

end.

