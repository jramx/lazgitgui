{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  TfrmOutput.
  This is a form is an alternative to the main txtDiff for showing git commands
  output, is mainly used when several parts are launched in standalone mode where
  the main status screen is not available.
}
unit unitoutput;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ButtonPanel, SynEdit,
  unitcommon, unitconfig, unitansiescapes, unitsyneditextras;

type

  { TfrmOutput }

  TfrmOutput = class(TForm)
    ButtonPanel1: TButtonPanel;
    txtOutput: TSynEdit;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    fAnsiHandler: TAnsiEscapesHandler;
  public
    procedure ShowText(aText: string);
  end;

  procedure ShowOutput(aText: string; aCaption:string='');

var
  frmOutput: TfrmOutput;

implementation

procedure ShowOutput(aText: string; aCaption: string);
var
  F: TfrmOutput;
begin
  F := TfrmOutput.Create(Application);
  F.Caption := aCaption;
  F.ShowText(aText);
  try
    F.ShowModal;
  finally
    F.Free;
  end;
end;

{$R *.lfm}

{ TfrmOutput }

procedure TfrmOutput.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  fConfig.WriteWindow(Self, 'outputform', SECTION_GEOMETRY);
end;

procedure TfrmOutput.FormCreate(Sender: TObject);
begin
  fConfig.OpenConfig;
  fConfig.ReadWindow(Self, 'outputform', SECTION_GEOMETRY);
  fConfig.ReadFont(txtOutput.Font, 'outputform.txtoutput', fpFixed, SECTION_FONTS);
  fAnsiHandler := TAnsiEscapesHandler.Create(txtOutput);
  AddPopupMenu(txtOutput);
  fConfig.CloseConfig;
end;

procedure TfrmOutput.FormDestroy(Sender: TObject);
begin
  fAnsiHandler.Free;
end;

procedure TfrmOutput.ShowText(aText: string);
var
  L: TStringList;
  s: String;
begin
  L := TStringList.Create;
  try
    L.Text := aText;
    txtOutput.Clear;
    for s in L do
      fAnsiHandler.ProcessLine(s, LineEnding);
  finally
    L.Free;
  end;
end;

end.

