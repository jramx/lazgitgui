unit tstsstartup;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry,
  unitcommon, unitcustomconfig, unitgitmgr,
  tstsmain;

type

  TTestStartup = class(TTestCase)
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestHookUp;
  end;

implementation

procedure TTestStartup.TestHookUp;
begin
  Fail('Write your own test');
end;

procedure TTestStartup.SetUp;
begin

end;

procedure TTestStartup.TearDown;
begin

end;

initialization

  RegisterTest(TTestStartup);
end.

