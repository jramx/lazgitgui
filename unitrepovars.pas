{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  This class process a string replacing some variables in the form of $var
  to their corresponding value in the current repository.
}
unit unitrepovars;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, fgl,
  unitgitmgr;

type

  { TRepoVars }

  TRepoVars = class
  private
    type
      TVarsMap = specialize TFPGMap<string, string>;
  private
    //fVars: array of TVarItem;
    fVars: TVarsMap;
    fGitMgr: TGitMgr;
    procedure Update;
  public
    destructor Destroy; override;
    function ReplaceVars(cmd: string): string;
    property GitMgr: TGitMgr read fGitMgr write fGitMgr;
  end;

implementation

{ TRepoVars }

procedure TRepoVars.Update;
var
  aRemote, aRemoteBranch, aRemoteURL: String;
  i: Integer;
begin
  if fVars=nil then begin
    fVars := TVarsMap.Create;
    fVars.Sorted := true;
  end;

  aRemote := fGitMgr.Upstream;
  i := pos('/', aRemote);
  if i>0 then begin
    aRemoteBranch := copy(aRemote, i+1, MAXINT);
    aRemote := copy(aRemote, 1, i-1);
  end else begin
    aRemoteBranch := '';
  end;

  i := fGitMgr.RemoteIndex;
  if i<0 then begin
    fGitMgr.UpdateRemotes;
    i := fGitMgr.RemoteIndex;
  end;
  if i>=0 then aRemoteURL := fGitMgr.Remotes[i].fetch
  else         aRemoteURL := '';

  fVars.AddOrSetData('branch', fGitMgr.Branch);
  fVars.AddOrSetData('branchoid', fGitMgr.BranchOID);
  fVars.AddOrSetData('upstream', fGitMgr.Upstream);
  fVars.AddOrSetData('remote', aRemote);
  fVars.AddOrSetData('remotebranch', aRemoteBranch);
  fVars.AddOrSetData('remoteurl', aRemoteURL);
  fVars.AddOrSetData('tag', fGitMgr.LastTag);
  fVars.AddOrSetData('tagoid', fGitMgr.LastTagOID);

end;

destructor TRepoVars.Destroy;
begin
  fVars.Free;
  inherited Destroy;
end;

function TRepoVars.ReplaceVars(cmd: string): string;
var
  i: Integer;
begin
  result := cmd;
  Update;
  for i := 0 to fVars.Count-1 do
    result := StringReplace(result, '$'+fVars.Keys[i], fVars.Data[i], [rfReplaceAll, rfIgnoreCase]);
end;

end.

