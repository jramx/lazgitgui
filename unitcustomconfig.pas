{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  TCustomConfig.
  Base class for handling config support, no functions for saving or retrieving
  GUI properties should be added to this class because is shared by the lazgitgui
  and testing framework programs.
}
unit unitcustomconfig;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, {$ifdef UseINI}IniFiles,{$endif} fpjson, jsonparser,
  LazLoggerBase,
  unitcommon, unitifaces;

type

  { TCustomConfig }

  TCustomConfig = class(TInterfacedObject, IConfig)
  private
    fConfigFile: string;
    fConfigFileOpenCount: Integer;
    {$IFDEF UseINI}
    fIniFile: TIniFile;
    {$ENDIF}
    fJsonFile: TJsonObject;
    fShowTags: boolean;
    fViewIgnoredFiles: boolean;
    fViewUntrackedFiles: boolean;
    fViewTrackedFiles: boolean;
    procedure CheckConfigFile;
    procedure SetShowTags(AValue: boolean);
    procedure SetViewIgnoredFiles(AValue: boolean);
    procedure SetViewTrackedFiles(AValue: boolean);
    procedure SetViewUntrackedFiles(AValue: boolean);
    function  GetLastParent(var aKey: string): TJsonObject;
    function  GetObject(section: string; var aKey: string): TJsonObject; overload;
    function  GetArray(aKey: string): TJsonArray;
    function  GetObject(aKey: string): TJsonObject; overload;
  public
    procedure OpenConfig;
    procedure CloseConfig;
    function ReadString(aKey:string; default:string=''; section:string=SECTION_DEFAULT): string;
    function ReadBoolean(aKey:string; default:boolean=false; section:string=SECTION_DEFAULT): boolean;
    function ReadInteger(aKey:string; default:Integer=0; section:string=SECTION_DEFAULT): Integer;
    function ReadInt64(aKey:string; default:Int64=0; section:string=SECTION_DEFAULT): Int64;
    procedure WriteString(aKey:string; avalue:string; section:string=SECTION_DEFAULT);
    procedure WriteBoolean(aKey:string; avalue:boolean; section:string=SECTION_DEFAULT);
    procedure WriteInteger(aKey:string; avalue:Integer; section:string=SECTION_DEFAULT);
    procedure WriteInt64(aKey:string; avalue:Int64; section:string=SECTION_DEFAULT);
    {$IFDEF UseINI}
    procedure ReadSection(section:string; strings:TStrings);
    procedure WriteSection(section:string; strings:TStrings);
    {$ENDIF}
    procedure ReadPreferences;

    function  ReadArray(section: string): TJsonArray;
    procedure WriteArray(section: string; arr: TJsonArray);

    property ViewUntrackedFiles: boolean read fViewUntrackedFiles write SetViewUntrackedFiles;
    property ViewIgnoredFiles: boolean read fViewIgnoredFiles write SetViewIgnoredFiles;
    property ViewTrackedFiles: boolean read fViewTrackedFiles write SetViewTrackedFiles;
    property ShowTags: boolean read fShowTags write SetShowTags;
  end;

implementation


procedure JSonToFile(Obj: TJSONData; aFilename: string; Opt: TFormatOptions = DefaultFormat);
var
  l: TStringList;
begin
  l := TStringList.Create;
  l.Text := Obj.FormatJSON(Opt);
  l.SaveToFile(aFilename);
  l.Free;
end;

function JsonFromFile(aFilename: string): TJsonObject;
var
  stream: TFileStream;
  obj: TJsonData;
begin
  if not FileExists(aFilename) then
    result := TJsonObject.Create
  else begin
    stream := TFileStream.Create(aFilename, fmOpenRead);
    try
      obj := GetJSON(stream);
      if obj is TJsonObject then
        result := TJsonObject(obj)
      else begin
        result := TJsonObject.Create;
        result.Add(ExtractFileName(aFilename), obj);
      end;
    finally
      stream.free;
    end;
  end;
end;

{ TCustomConfig }

procedure TCustomConfig.CheckConfigFile;
begin
  if fConfigFile='' then begin
    fConfigFile := GetAppConfigFile(false, true);
    ForceDirectories(ExtractFilePath(fConfigFile));
  end;
end;

procedure TCustomConfig.SetShowTags(AValue: boolean);
begin
  if fShowTags = AValue then Exit;
  fShowTags := AValue;
  WriteBoolean('ShowTags', fShowTags);
end;

procedure TCustomConfig.SetViewIgnoredFiles(AValue: boolean);
begin
  if fViewIgnoredFiles = AValue then Exit;
  fViewIgnoredFiles := AValue;
  WriteBoolean('ViewIgnored', fViewIgnoredFiles);
end;

procedure TCustomConfig.SetViewTrackedFiles(AValue: boolean);
begin
  if fViewTrackedFiles = AValue then Exit;
  fViewTrackedFiles := AValue;
  WriteBoolean('ViewTracked', fViewTrackedFiles);
end;

procedure TCustomConfig.SetViewUntrackedFiles(AValue: boolean);
begin
  if fViewUntrackedFiles = AValue then Exit;
  fViewUntrackedFiles := AValue;
  WriteBoolean('ViewUntracked', fViewUntrackedFiles);
end;

function TCustomConfig.GetLastParent(var aKey: string): TJsonObject;
var
  i: Integer;
  arr: TStringArray;
  obj: TJSONObject;
begin
  arr := aKey.Split('.');

  result := fJsonFile;
  for i := 0 to Length(arr)-2 do begin
    obj := result.Get(arr[i], TJsonObject(nil));
    if obj=nil then begin
      obj := TJsonObject.Create;
      result.Objects[arr[i]] := obj;
    end;
    result := obj;
  end;

  if length(arr)>1 then
    aKey := arr[Length(arr)-1];
end;

function TCustomConfig.GetObject(section: string; var aKey: string): TJsonObject;
var
  p: SizeInt;
  s: string;
  obj: TJsonObject;
begin
  if (Section<>SECTION_GEOMETRY) and (Section<>SECTION_FONTS) then begin
    result := fJsonFile.Get(section, TJsonObject(nil));
    if result=nil then begin
      result := TJsonObject.Create;
      fJsonFile.Objects[section] := result;
    end;
  end else
    result := fJsonFile;

  p := pos('.', aKey);
  while p>0 do begin
    s := copy(aKey, 1, p-1);
    obj := result.Get(s, TJsonObject(nil));
    if obj=nil then begin
      obj := TJsonObject.Create;
      result.Objects[s] := obj;
    end;
    result := obj;
    delete(aKey, 1, p);
    p := pos('.', aKey);
  end;
end;

function TCustomConfig.GetArray(aKey: string): TJsonArray;
var
  obj: TJSONObject;
begin
  obj := GetLastParent(aKey);
  if obj<>nil then
    result := obj.Get(aKey, TJsonArray(nil))
  else
    result := nil;
end;

function TCustomConfig.GetObject(aKey: string): TJsonObject;
begin
  result := GetLastParent(aKey);
  if result<>nil then
    result := result.Get(aKey, TJsonObject(nil));
end;

procedure TCustomConfig.OpenConfig;
begin
  if fConfigFileOpenCount=0 then begin
    CheckConfigFile;
    {$IFDEF UseINI}
    fIniFile := TIniFile.Create(fConfigFile);
    {$ENDIF}
    fJsonFile := JsonFromFile(fConfigFile+'.json');
  end;
  inc(fConfigFileOpenCount);
end;

procedure TCustomConfig.CloseConfig;
begin
  dec(fConfigFileOpenCount);
  if fConfigFileOpenCount<=0 then begin
    {$IFDEF UseINI}
    if fIniFIle<>nil then
      FreeAndNil(fIniFile);
    {$ENDIF}
    JSonToFile(fJsonFile, fConfigFile+'.json');
    FreeAndNil(fJsonFile);
    fConfigFileOpenCount := 0;
  end;
end;

function TCustomConfig.ReadString(aKey: string; default: string; section: string
  ): string;
var
  obj: TJSONObject;
begin
  OpenConfig;
  obj := GetObject(section, aKey);
  {$IFDEF UseINI}
  result := fIniFile.ReadString(section, aKey, default);
  //if result<>default then
    obj.Strings[aKey] := result;
  {$ENDIF}
  result := obj.Get(aKey, default);
  CloseConfig;
end;

function TCustomConfig.ReadBoolean(aKey: string; default: boolean; section: string
  ): boolean;
var
  obj: TJSONObject;
begin
  OpenConfig;
  obj := GetObject(section, aKey);
  {$IFDEF UseINI}
  result := fIniFile.ReadBool(section, aKey, default);
  //if result<>default then
    obj.Booleans[aKey] := result;
  {$ENDIF}
  result := obj.Get(aKey, default);
  CloseConfig;
end;

function TCustomConfig.ReadInteger(aKey: string; default: Integer; section: string
  ): Integer;
var
  obj: TJSONObject;
begin
  OpenConfig;
  obj := GetObject(section, aKey);
  {$IFDEF UseINI}
  result := fIniFile.ReadInteger(Section, aKey, default);
  //if result<>default then
    obj.Integers[aKey] := result;
  {$ENDIF}
  result := obj.Get(aKey, default);
  CloseConfig;
end;

function TCustomConfig.ReadInt64(aKey: string; default: Int64; section: string
  ): Int64;
var
  obj: TJSONObject;
begin
  OpenConfig;
  obj := GetObject(section, aKey);
  {$IFDEF UseINI}
  result := fIniFile.ReadInteger(Section, aKey, default);
  //if result<>default then
    obj.Integers[aKey] := result;
  {$ENDIF}
  result := obj.Get(aKey, default);
  CloseConfig;
end;

procedure TCustomConfig.WriteString(aKey: string; avalue: string; section: string);
var
  obj: TJSONObject;
begin
  OpenConfig;
  obj := GetObject(section, aKey);
  {$IFDEF UseINI}
  fIniFile.WriteString(section, aKey, aValue);
  {$ENDIF}
  obj.Strings[aKey] := aValue;
  CloseConfig;
end;

procedure TCustomConfig.WriteBoolean(aKey: string; avalue: boolean; section: string);
var
  obj: TJSONObject;
begin
  OpenConfig;
  obj := GetObject(section, aKey);
  {$IFDEF UseINI}
  fIniFile.WriteBool(Section, aKey, aValue);
  {$ENDIF}
  obj.Booleans[aKey] := aValue;
  CloseConfig;
end;

procedure TCustomConfig.WriteInteger(aKey: string; avalue: Integer; section: string);
var
  obj: TJSONObject;
begin
  OpenConfig;
  obj := GetObject(section, aKey);
  {$IFDEF UseINI}
  fIniFile.WriteInteger(Section, aKey, aValue);
  {$ENDIF}
  obj.Integers[aKey] := aValue;
  CloseConfig;
end;

procedure TCustomConfig.WriteInt64(aKey: string; avalue: Int64; section: string
  );
var
  obj: TJSONObject;
begin
  OpenConfig;
  obj := GetObject(section, aKey);
  {$IFDEF UseINI}
  fIniFile.WriteInteger(Section, aKey, aValue);
  {$ENDIF}
  obj.Int64s[aKey] := aValue;
  CloseConfig;
end;

{$IFDEF UseINI}
procedure TConfig.ReadSection(section: string; strings: TStrings);
begin
  OpenConfig;
  fIniFile.ReadSectionRaw(section, strings);
  CloseConfig;
end;

procedure TConfig.WriteSection(section: string; strings: TStrings);
var
  i, p: Integer;
  s, key, value: String;
begin
  OpenConfig;
  if fIniFile.SectionExists(section) then
    fIniFile.EraseSection(section);
  if (strings<>nil) then begin
    for i:=0 to strings.count-1 do begin
      s := strings[i];
      p := pos('=', s);
      if p=0 then begin
        key := IntToStr(i+1);
        value := s;
      end else begin
        key := trim(copy(s, 1, p-1));
        value := trim(copy(s, p+1, MAXINT));
      end;
      fIniFile.WriteString(section, key, value);
    end;
  end;
  CloseConfig;
end;
{$ENDIF}

procedure TCustomConfig.ReadPreferences;
begin
  fViewUntrackedFiles := ReadBoolean('ViewUntracked', true);
  fViewIgnoredFiles := ReadBoolean('ViewIgnored', false);
  fViewTrackedFiles := ReadBoolean('ViewTracked', false);
  fShowTags := ReadBoolean('ShowTags', true);
end;

function TCustomConfig.ReadArray(section: string): TJsonArray;
begin
  result := GetArray(section);
end;

procedure TCustomConfig.WriteArray(section: string; arr: TJsonArray);
var
  obj: TJSONObject;
begin
  obj := GetLastParent(section);
  obj.Arrays[section] := arr;
end;

end.

