unit unitmarkeditem;

{$mode ObjFPC}{$H+}
{$ModeSwitch advancedrecords}

interface

uses
  Classes, SysUtils,
  menus,
  unitconfig, unitgittypes, unitgitutils, unitifaces, unitgitmgr, unitdifftool;

type

  { TMarkedItem }

  PMarkedItem = ^TMarkedItem;
  TMarkedItem = record
    filename: string; // full item filename
    treehash: string; // file tree hash
    CommitOID: RawByteString;
    CommiterDate: Int64;
    procedure Store(key, section: string);
    procedure Restore(key, section: string);
    function LooksValid: boolean;
  end;

  { TMarkedItemHelper }

  TMarkedItemHelper = class
  private
    fGitMgr: TGitMgr;
    fMarkedItem: TMarkedItem;
    fKey: String;
  public
    constructor Create(aGitMgr: TGitMgr; storeKey:string);
    procedure SetMarkedItem(aFilename, aTreeHash: string; aItem: TLogItem); overload;
    procedure SetMarkedItem(aFilename, aTreeHash, commitOID: string; commiterDate:Int64); overload;
    procedure AddPopMenuCompare(pop: TPopupMenu; handler: TNotifyEvent; marked:boolean);

    property MarkedItem: TMarkedItem read fMarkedItem;
  end;

implementation

{ TMarkedItem }

procedure TMarkedItem.Store(key, section: string);
begin
  if key<>'' then
    key += '.';
  fConfig.WriteString(key + 'MarkedItem.Name', filename, section);
  fConfig.WriteString(key + 'MarkedItem.Hash', treehash, section);
  fConfig.WriteString(key + 'MarkedItem.Commit', CommitOID, section);
  fConfig.WriteInt64(key + 'MarkedItem.Date', CommiterDate, section);
end;

procedure TMarkedItem.Restore(key, section: string);
begin
  if key<>'' then
    key += '.';
  filename := fConfig.ReadString(key + 'MarkedItem.Name', '', section);
  treehash := fConfig.ReadString(key + 'MarkedItem.Hash', '', section);
  CommitOID := fConfig.ReadString(key + 'MarkedItem.Commit', '', section);
  CommiterDate := fConfig.ReadInt64(key + 'MarkedItem.Date', 0, section);
end;

function TMarkedItem.LooksValid: boolean;
begin
  result := (filename<>'') and (CommitOID<>'');
end;

{ TMarkedItemHelper }

procedure TMarkedItemHelper.SetMarkedItem(aFilename, aTreeHash: string;
  aItem: TLogItem);
begin
  fMarkedItem.filename := aFilename;
  fMarkedItem.treehash := aTreeHash;
  fMarkedItem.CommitOID := aItem.CommitOID;
  fMarkedItem.CommiterDate := aItem.CommiterDate;
  fMarkedItem.Store(fKey, fGitMgr.Git.TopLevelDir);
end;

procedure TMarkedItemHelper.SetMarkedItem(aFilename, aTreeHash,
  commitOID: string; commiterDate: Int64);
begin
  fMarkedItem.filename := aFilename;
  fMarkedItem.treehash := aTreeHash;
  fMarkedItem.CommitOID := CommitOID;
  fMarkedItem.CommiterDate := CommiterDate;
  fMarkedItem.Store(fKey, fGitMgr.Git.TopLevelDir);
end;

constructor TMarkedItemHelper.Create(aGitMgr: TGitMgr; storeKey: string);
var
  aGit: IGit;
begin
  inherited Create;
  fGitMgr := aGitMgr;
  fKey := storeKey;
  aGit := fGitMgr.Git;

  fMarkedItem.Restore(fKey, aGit.TopLevelDir);
end;

procedure TMarkedItemHelper.AddPopMenuCompare(pop: TPopupMenu;
  handler: TNotifyEvent; marked: boolean);
var
  s: string;
begin

  if marked and (not fMarkedItem.LooksValid) then
    exit;

  if marked then begin
    s := GetTempName(fMarkedItem.filename, fMarkedItem.CommitOID);
    s := format('Compare with %s', [s]);
  end else
    s := 'Compare with working tree';

  AddDiffToolsMenu(pop, handler, s);
end;

end.

