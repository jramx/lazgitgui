unit unitdifftool;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, fpjson,
  Menus,
  unitcommon, unitconfig, unitgitutils, unitgitmgr, unitguitools, unitrunthread;

type
  TDiffToolFileItem = record
    filename: string;           // filename to compare
    tag: string;                // tag to add to filename if it's to be extracted from git
    tree: string;               // tree hash of the file if it's to be extracted
    meta: string;               // label or metadata to use for filename in the diff tool (if it's supported)
  end;

  TDiffToolLauncherRec = record
    toolIndex: Integer;                   // index of diff tool to use (according to difftool config array)
    gitMgr: TGitMgr;                      // git manager
    base, comp: TDiffToolFileItem;        // data from base and comp files
  end;

  { TDiffTools }

  TDiffTools = class
  private
    function GetCount: Integer;
    function ValidateTool(obj:TJsonData): boolean;
    function GetCommand(aIndex:Integer; out cmd, args: string): boolean;
  public
    Tools: array of TJsonObject;
    destructor Destroy; override;

    procedure Open;
    procedure AddMenu(pop: TPopupMenu; handler: TNotifyEvent; Caption:string);
    function FormatCommand(aIndex:Integer; out cmd, args:String; baseFile, compFile, baseMeta, compMeta: string): boolean;

    property Count: Integer read GetCount;
  end;

  procedure AddDiffToolsMenu(pop: TPopupMenu; handler: TNotifyEvent; caption: string);
  function LaunchDiffTool(dtl: TDiffToolLauncherRec; out errLog:RawByteString): boolean;

implementation

procedure AddDiffToolsMenu(pop: TPopupMenu; handler: TNotifyEvent; caption: string);
var
  diffTools: TDiffTools;
begin
  diffTools := TDiffTools.Create;
  try
    diffTools.Open;
    if diffTools.Count>0 then
      diffTools.AddMenu(pop, handler, caption);
  finally
    diffTools.Free;
  end;

end;

function LaunchDiffTool(dtl: TDiffToolLauncherRec; out errLog: RawByteString
  ): boolean;
var
  diffTools: TDiffTools;
  baseExtract, compExtract, baseOk, compOk: Boolean;
  cmd, args: String;
begin

  result := false;

  with dtl do begin

    baseExtract := (base.tag<>'');
    compExtract := (comp.tag<>'');

    if (baseExtract or compExtract) and (gitMgr=nil) then begin
      errLog := 'LaunchDiffTool without GitMgr';
      exit;
    end;

    if not baseExtract and not FileExists(base.filename) then begin
      errLog := format('%s does not exits', [base.filename]);
      exit;
    end;

    if not compExtract and not FileExists(comp.filename) then begin
      errLog := format('%s does not exists', [comp.filename]);
      exit;
    end;

  end;

  diffTools := TDiffTools.Create;
  try

    with dtl do begin

      if baseExtract then
        base.filename := unitgitutils.GetTempFilename(base.filename, base.tag);

      if compExtract then
        comp.filename := unitgitutils.GetTempFilename(comp.filename, comp.tag);

      if diffTools.FormatCommand(toolIndex, cmd, args, base.filename, comp.filename, base.meta, comp.meta) then begin

        baseOk := not baseExtract or gitMgr.ExtractToFile(base.tree, base.filename);
        compOk := not compExtract or gitMgr.ExtractToFile(comp.tree, comp.filename);

        if baseOk and compOk then begin

          cmd += ' ' + args;

          runcommand(cmd, '', errLog);

          if baseExtract then DeleteFile(base.filename);
          if compExtract then DeleteFile(comp.filename);

        end;

      end;

    end;

    result := true;

  finally
    diffTools.Free;
  end;

end;

{ TDiffTools }

function TDiffTools.GetCount: Integer;
begin
  result := Length(Tools);
end;

function TDiffTools.ValidateTool(obj: TJsonData): boolean;
begin
  result :=
    (obj is TJsonObject) and
    (TJSonObject(obj).Get('name', '')<>'') and
    (TJSonObject(obj).Get('command', '')<>'');
end;

destructor TDiffTools.Destroy;
var
  i: Integer;
begin
  for i:=Count-1 downto 0 do
    Tools[i].Free;
  Tools := nil;
  inherited Destroy;
end;

procedure TDiffTools.Open;
var
  arr: TJSONArray;
  i: Integer;
  j: SizeInt;
begin

  fConfig.OpenConfig;
  try

    arr := fConfig.ReadArray('difftools');
    if arr=nil then
      exit;

    for i := 0 to arr.count-1 do begin
      if ValidateTool(arr[i]) then begin
        j := Length(Tools);
        SetLength(Tools, j+1);
        Tools[j] := TJsonObject(arr[i].Clone);
        Tools[j].Integers['OriginalIndex'] := i;
      end;
    end;

  finally
    fConfig.CloseConfig;
  end;
end;

procedure TDiffTools.AddMenu(pop: TPopupMenu; handler: TNotifyEvent;
  Caption: string);
var
  s: string;
  i: Integer;
begin

  if count=0 then
    exit;

  if count=1 then begin
    s := format('%s using %s',[Caption, Tools[0].Strings['name']]);
    AddPopItem(pop, s, handler, Tools[0].Integers['OriginalIndex']);
  end else begin
    s := format('%s using',[Caption]);
    for i := 0 to Count-1 do
      AddPopItem(pop, Tools[i].Strings['name'], handler, Tools[i].Integers['OriginalIndex'], s);
  end;

end;

function TDiffTools.GetCommand(aIndex: Integer; out cmd, args: string
  ): boolean;
var
  arr: TJSONArray;
  obj: TJSONObject;
begin
  result := false;
  try
    fConfig.OpenConfig;
    arr := fConfig.ReadArray('difftools');
    result := (aIndex>=0) and (aIndex<arr.Count);
    if result then begin
      obj := arr.objects[aIndex];
      cmd := obj.Strings['command'];
      args := obj.Get('args', '');
      result := (args<>'') and (cmd<>'');
    end;
  finally
    fConfig.CloseConfig;
  end;
end;

function TDiffTools.FormatCommand(aIndex: Integer; out cmd, args: String;
  baseFile, compFile, baseMeta, compMeta: string): boolean;
begin
  result := GetCommand(aIndex, cmd, args);
  if result then begin
    args := StringReplace(args, '$Meta1', Sanitize(baseMeta), [rfReplaceAll, rfIgnoreCase]);
    args := StringReplace(args, '$Meta2', Sanitize(compMeta), [rfReplaceAll,
      rfIgnoreCase]);
    args := StringReplace(args, '$1', Sanitize(baseFile), [rfReplaceAll]);
    args := StringReplace(args, '$2', Sanitize(compFile), [rfReplaceAll]);
  end;
end;

end.

