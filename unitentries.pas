{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  Git status entries unit
}
unit unitentries;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LazLogger;

  {

  reference:
    https://git-scm.com/docs/git-status

  # (1) Changed Tracked Entries (Ordinary changed)
    see unitparsers.ParseOrdinaryChanged()

  # (2) Renamed or copied entries:
    see unitparsers.ParseRenamedCopied()

  # (u) Unmerged entries
    see unitparsers.ParseUnmerged()

  Fields meaning for 1 and 2:

    1 <XY> <sub> <mH> <mI> <mW> <hH> <hI> <path>
    2 <XY> <sub> <mH> <mI> <mW> <hH> <hI> <X><score> <path><sep><origPath>

  Field       Meaning
  --------------------------------------------------------
  <XY>        A 2 character field containing the staged and
  	    unstaged XY values described in the short format,
  	    with unchanged indicated by a "." rather than
  	    a space.
  <sub>       A 4 character field describing the submodule state.
  	    "N..." when the entry is not a submodule.
  	    "S<c><m><u>" when the entry is a submodule.
  	    <c> is "C" if the commit changed; otherwise ".".
  	    <m> is "M" if it has tracked changes; otherwise ".".
  	    <u> is "U" if there are untracked changes; otherwise ".".
  <mH>        The octal file mode in HEAD.
  <mI>        The octal file mode in the index.
  <mW>        The octal file mode in the worktree.
  <hH>        The object name in HEAD.
  <hI>        The object name in the index.
  <X><score>  The rename or copy score (denoting the percentage
  	    of similarity between the source and target of the
  	    move or copy). For example "R100" or "C75".
  <path>      The pathname.  In a renamed/copied entry, this
  	    is the target path.
  <sep>       When the `-z` option is used, the 2 pathnames are separated
  	    with a NUL (ASCII 0x00) byte; otherwise, a tab (ASCII 0x09)
  	    byte separates them.
  <origPath>  The pathname in the commit at HEAD or in the index.
  	    This is only present in a renamed/copied entry, and
  	    tells where the renamed/copied contents came from.
  --------------------------------------------------------

  Fields meaning for u:

    u <XY> <sub> <m1> <m2> <m3> <mW> <h1> <h2> <h3> <path>

  Field       Meaning
  --------------------------------------------------------
  <XY>        A 2 character field describing the conflict type
  	    as described in the short format.
  <sub>       A 4 character field describing the submodule state
  	    as described above.
  <m1>        The octal file mode in stage 1.
  <m2>        The octal file mode in stage 2.
  <m3>        The octal file mode in stage 3.
  <mW>        The octal file mode in the worktree.
  <h1>        The object name in stage 1.
  <h2>        The object name in stage 2.
  <h3>        The object name in stage 3.
  <path>      The pathname.
  --------------------------------------------------------
  }

type

  TEntryTypeSource = (
    etsUnknown,       // it's not any of the following ...
    etsUnstaged,      // the entry source is the unstaged list (don't distinguish amend state)
    etsStaged,        // the entry source is the staged list (don't distinsguish amend state)
    etsStagedAmend,   // the entry source is the staged list and will overwrite an amended item
    etsAmend          // the entry source is the staged list and it's an amended item
  );

  TEntryKind = (
    ekUnknown,
    ekOrdinaryChanged,
    ekRenamedCopied,
    ekUnmerged,
    ekUntracked,
    ekIgnored
  );

  TEntryType = (
    etUnknown,

    etNotUpdatedA,
    etNotUpdatedM,
    etNotUpdatedD,

    etUpdatedInIndex,
    etUpdatedInIndexM,
    etUpdatedInIndexT,
    etUpdatedInIndexD,
    etTypeChangedInIndex,
    etTypeChangedInIndexM,
    etTypeChangedInIndexT,
    etTypeChangedInIndexD,
    etAddedToIndex,
    etAddedToIndexM,
    etAddedToIndexT,
    etAddedToIndexD,
    etDeletedFromIndex,
    etRenamedInIndex,
    etRenamedInIndexM,
    etRenamedInIndexT,
    etRenamedInIndexD,
    etCopiedInIndex,
    etCopiedInIndexM,
    etCopiedInIndexT,
    etCopiedInIndexD,

    etIndexAndWorktreeMatchesM,
    etIndexAndWorktreeMatchesT,
    etIndexAndWorktreeMatchesA,
    etIndexAndWorktreeMatchesR,
    etIndexAndWorktreeMatchesC,

    etWorktreeChangedSinceIndex,
    etWorktreeChangedSinceIndexM,
    etWorktreeChangedSinceIndexT,
    etWorktreeChangedSinceIndexA,
    etWorktreeChangedSinceIndexR,
    etWorktreeChangedSinceIndexC,
    etTypeChangedInWorktreeSinceIndex,
    etTypeChangedInWorktreeSinceIndexM,
    etTypeChangedInWorktreeSinceIndexT,
    etTypeChangedInWorktreeSinceIndexA,
    etTypeChangedInWorktreeSinceIndexR,
    etTypeChangedInWorktreeSinceIndexC,
    etDeletedInWorktree,
    etDeletedInWorktreeM,
    etDeletedInWorktreeT,
    etDeletedInWorktreeA,
    etDeletedInWorktreeR,
    etDeletedInWorktreeC,
    etRenamedInWorkTree,
    etCopiedInWorkTree,

    etUnmergedAddedByUs,
    etUnmergedBothAdded,
    etUnmergedBothDeleted,
    etUnmergedDeletedByUs,
    etUnmergedDeletedByThem,
    etUnmergedAddedByThem,
    etUnmergedBothModified,

    etUntracked,
    etIgnored
  );

  TSetOfEntryType = set of TEntryType;

  TSubmoduleType = set of (smtSubmodule, smtCommitChanged, smtTrackedChanges, smtUntrackedChanges);

  PFileEntry = ^TFileEntry;
  TFileEntry = record
    EntryKind: TEntryKind;
    EntryTypeStaged: TEntryType;
    EntryTypeUnStaged: TEntryType;
    SubModule: TSubmoduleType;
    x, y: char;
    m1, m2, m3, mW: Integer;    // m1=mH and m2=mI for ekOrdinaryChanged and ekRenamedCopied
    h1, h2, h3: string[41];     // h1=hH and h2=hI for ekOrdinaryChanged and ekRenamedCopied
    xScore: string[7];
    path: string;
    origPath: string;
  end;
  TPFileEntryArray = array of PFileEntry;

function XYToEntryType(x, y: char; staged:boolean; merging:boolean=false): TEntryType;
function EntryTypeToStr(X, Y: char): string;
procedure DumpEntry(Entry: PFileEntry);
procedure ClearEntries(fEntries: TfpList); overload;
procedure ClearEntries(list: TStrings); overload;
function EntryTypeToIndex(aItem: TEntryType; src: TEntryTypeSource): Integer;
function EntryTypeInList(entry: TEntryType; staged: boolean): boolean;

const
  ChangedInWorktreeSet = [ etWorktreeChangedSinceIndex .. etCopiedInWorkTree ];
  DeletedInWorktreeSet = [ etDeletedInWorktree .. etRenamedInWorkTree ];
  ChangedInIndexSet = [ etUpdatedInIndex .. etCopiedInIndexD ];
  UnmergedSet = [ etUnmergedAddedByUs .. etUnmergedBothModified ];

implementation

{
X          Y     Meaning
-------------------------------------------------
         [AMD]   not updated
M        [ MTD]  updated in index
T        [ MTD]  type changed in index
A        [ MTD]  added to index
D                deleted from index
R        [ MTD]  renamed in index
C        [ MTD]  copied in index
[MTARC]          index and work tree matches
[ MTARC]    M    work tree changed since index
[ MTARC]    T    type changed in work tree since index
[ MTARC]    D    deleted in work tree
            R    renamed in work tree
            C    copied in work tree
-------------------------------------------------
D           D    unmerged, both deleted
A           U    unmerged, added by us
U           D    unmerged, deleted by them
U           A    unmerged, added by them
D           U    unmerged, deleted by us
A           A    unmerged, both added
U           U    unmerged, both modified
-------------------------------------------------
?           ?    untracked
!           !    ignored
-------------------------------------------------
}

resourcestring
  rsLazGitGuiUnmodified = 'Unmodified';
  rsLazGitGuiNewIntentedToAdd = 'New, intended to add';
  rsLazGitGuiModifiedNotStaged = 'Modified, not staged';
  rsLazGitGuiStagedForCommit = 'Staged for commit';
  rsLazGitGuiPortionsStagedForCommit = 'Portions staged for commit';
  rsLazGitGuiStagedForCommitMissing = 'Staged for commit, missing';
  rsLazGitGuiFileTypeChangedNotStaged = 'File type changed, not staged';
  rsLazGitGuiFileTypeChangedOldTypeStagedForCommit = 'File type changed, old type staged for commit';
  rsLazGitGuiFileTypeChangedStaged = 'File type changed, staged';
  rsLazGitGuiFileTypeChangeStagedModificationNotStaged = 'File type change staged, Modification not staged';
  rsLazGitGuiFileTypeChangeFileMissing = 'File type change staged, Missing';
  rsLazGitGuiUntrackedNotStaged = 'Untracked, not staged';
  rsLazGitGuiMissing = 'Missing';
  rsLazGitGuiStagedForRemoval = 'Staged for removal';
  rsLazGitGuiStagedForRemovalStillPresent = 'Staged for removal, still present';
  rsLazGitGuiStagedForRename = 'Staged for Rename';
  rsLazGitGuiRequiresMergeResolution = 'Requires merge resolution';
  rsLazGitGuiFileStateMissingDescription = 'File state missing description';
  rsLazGitGuiIgnored = 'Ignored';
  rsLazGitGuiUnmergedBothDeleted = 'Unmerged, Both deleted';
  rsLazGitGuiUnmergedAddedByUs = 'Unmerged, Added by Us';
  rsLazGitGuiUnmergedDeletedByThem = 'Unmerged, Deleted by Them';
  rsLazGitGuiUnmergedAddedByThem = 'Unmerged, Added by Them';
  rsLazGitGuiUnmergedDeletedByUs = 'Unmerged, Deleted by Us';
  rsLazGitGuiUnmergedBothAdded  = 'Unmerged, Both added';
  rsLazGitGuiUnmergedBothModified = 'Unmerged, Both Modified';

function XYToEntryType(x, y: char; staged:boolean; merging:boolean=false): TEntryType;
begin
  if x=' ' then x := '.';
  if y=' ' then y := '.';
  result := etUnknown;
  if staged and (not merging) then begin
    case x+y of
      '.M': result := etNotUpdatedM;
      '.D': result := etNotUpdatedD;
      '.R': result := etRenamedInWorkTree;
      '.C': result := etCopiedInWorkTree;
      'M.': result := etUpdatedInIndex;
      'MM': result := etUpdatedInIndexM;
      'MT': result := etUpdatedInIndexT;
      'MD': result := etUpdatedInIndexD;
      'T.': result := etTypeChangedInIndex;
      'TM': result := etTypeChangedInIndexM;
      'TT': result := etTypeChangedInIndexT;
      'TD': result := etTypeChangedInIndexD;
      'A.': result := etAddedToIndex;
      'AM': result := etAddedToIndexM;
      'AT': result := etAddedToIndexT;
      'AD': result := etAddedToIndexD;
      'D.': result := etDeletedFromIndex;
      'R.': result := etRenamedInIndex;
      'RM': result := etRenamedInIndexM;
      'RT': result := etRenamedInIndexT;
      'RD': result := etRenamedInIndexD;
      'C.': result := etCopiedInIndex;
      'CM': result := etCopiedInIndexM;
      'CT': result := etCopiedInIndexT;
      'CD': result := etCopiedInIndexD;
    end;

  end else begin

    case x+y of
      '.A': result := etNotUpdatedA;
      'M.': result := etIndexAndWorktreeMatchesM;
      'T.': result := etIndexAndWorktreeMatchesT;
      'A.': result := etIndexAndWorktreeMatchesA;
      'R.': result := etIndexAndWorktreeMatchesR;
      'C.': result := etIndexAndWorktreeMatchesC;
      '.M': result := etWorktreeChangedSinceIndex;
      'MM': result := etWorktreeChangedSinceIndexM;
      'TM': result := etWorktreeChangedSinceIndexT;
      'AM': result := etWorktreeChangedSinceIndexA;
      'RM': result := etWorktreeChangedSinceIndexR;
      'CM': result := etWorktreeChangedSinceIndexC;
      '.T': result := etTypeChangedInWorktreeSinceIndex;
      'MT': result := etTypeChangedInWorktreeSinceIndexM;
      'TT': result := etTypeChangedInWorktreeSinceIndexT;
      'AT': result := etTypeChangedInWorktreeSinceIndexA;
      'RT': result := etTypeChangedInWorktreeSinceIndexR;
      'CT': result := etTypeChangedInWorktreeSinceIndexC;
      '.D': result := etDeletedInWorktree;
      'MD': result := etDeletedInWorktreeM;
      'TD': result := etDeletedInWorktreeT;
      'AD': result := etDeletedInWorktreeA;
      'RD': result := etDeletedInWorktreeR;
      'CD': result := etDeletedInWorktreeC;
      '??': result := etUntracked;
      '!!': result := etIgnored;
      'DD': result := etUnmergedBothDeleted;
      'AU': result := etUnmergedAddedByUs;
      'UD': result := etUnmergedDeletedByThem;
      'UA': result := etUnmergedAddedByThem;
      'DU': result := etUnmergedDeletedByUs;
      'AA': result := etUnmergedBothAdded;
      'UU': result := etUnmergedBothModified;
    end;

  end;
end;

function EntryTypeToStr(X, Y: char): string;
begin
  // ref: https://github.com/git/git/blob/master/git-gui/git-gui.sh (c. 2098)
  case x+y of
    '..': result := rsLazGitGuiUnmodified;
    '.A': result := rsLazGitGuiNewIntentedToAdd;
    '.M': result := rsLazGitGuiModifiedNotStaged;
    'M.': result := rsLazGitGuiStagedForCommit;
    'MM': result := rsLazGitGuiPortionsStagedForCommit;
    'MD': result := rsLazGitGuiStagedForCommitMissing;
    '.T': result := rsLazGitGuiFileTypeChangedNotStaged;
    'MT': result := rsLazGitGuiFileTypeChangedOldTypeStagedForCommit;
    'AT': result := rsLazGitGuiFileTypeChangedOldTypeStagedForCommit;
    'T.': result := rsLazGitGuiFileTypeChangedStaged;
    'TM': result := rsLazGitGuiFileTypeChangeStagedModificationNotStaged;
    'TD': result := rsLazGitGuiFileTypeChangeFileMissing;
    '.O': result := rsLazGitGuiUntrackedNotStaged; // 'O'='?' ???
    'A.': result := rsLazGitGuiStagedForCommit;
    'AM': result := rsLazGitGuiPortionsStagedForCommit;
    'AD': result := rsLazGitGuiStagedForCommitMissing;
    '.D': result := rsLazGitGuiMissing;
    'D.': result := rsLazGitGuiStagedForRemoval;
    'DO': result := rsLazGitGuiStagedForRemovalStillPresent; // 'O'='?' ???
    'R.': result := rsLazGitGuiStagedForRename;

    'DD': result := rsLazGitGuiUnmergedBothDeleted;
    'AU': result := rsLazGitGuiUnmergedAddedByUs;
    'UD': result := rsLazGitGuiUnmergedDeletedByThem;
    'UA': result := rsLazGitGuiUnmergedAddedByThem;
    'DU': result := rsLazGitGuiUnmergedDeletedByUs;
    'AA': result := rsLazGitGuiUnmergedBothAdded;
    'UU': result := rsLazGitGuiUnmergedBothModified;
    '.U': result := rsLazGitGuiRequiresMergeResolution;
    'U.': result := rsLazGitGuiRequiresMergeResolution;
    'UM': result := rsLazGitGuiRequiresMergeResolution;
    'UT': result := rsLazGitGuiRequiresMergeResolution;

    '?.': result := rsLazGitGuiUntrackedNotStaged;
    '!.': result := rsLazGitGuiIgnored;
    else  result := rsLazGitGuiFileStateMissingDescription;
  end;
end;

procedure DumpEntry(Entry: PFileEntry);
var
  s: string;
begin
  with Entry^ do begin
    WriteStr(s, EntryKind);
    DbgOut('Entry: kind: %s', [s]);
    WriteStr(s, EntryTypeStaged);
    DbgOut(' TypeStaged: %s', [s]);
    WriteStr(s, EntryTypeUnStaged);
    DbgOut(' TypeUnStaged: %s', [s]);
    DbgOut(' SubModule: [');
    if smtSubmodule in SubModule then DbgOut('smtSubmodule ');
    if smtCommitChanged in SubModule then DbgOut('smtCommitChanged ');
    if smtUnTrackedChanges in SubModule then dbgOut('smtUntrackedChanged ');
    if smtTrackedChanges in SubModule then dbgOut('smtTrackedChanged ');
    DbgOut('] XY=%s%s',[X,Y]);
    DbgOut(' FileModes HI3W: %s %s %s %s ', [OctStr(m1, 5), OctStr(m2, 5), OctStr(m3, 5), OctStr(mW, 5)]);
    DbgOut(' ObjNames: HI3: %s %s %s ', [h1, h2, h3]);
    DbgOut(' XScore: %s',[xScore]);
    DbgOut(' Path: %s',[path]);
    DebugLn(' OrigPath: %s', [OrigPath]);
  end;
end;

procedure ClearEntries(fEntries: TfpList);
var
  i: Integer;
  entry: PFileEntry;
begin
  if fEntries<>nil then begin
    for i:=0 to fEntries.Count-1 do begin
      entry := PFileEntry(fEntries[i]);
      if entry<>nil then
        Dispose(entry)
    end;
    fEntries.Clear;
  end;
end;

procedure ClearEntries(list: TStrings);
var
  i: Integer;
  entry: PFileEntry;
begin
  for i:=0 to list.Count-1 do begin
    entry := PFileEntry(list.Objects[i]);
    if entry<>nil then
      Dispose(entry)
  end;
  list.clear;
end;

// This function takes a TEntryType argument and returns the corresponding
// Image index from the main image list.
function EntryTypeToIndex(aItem: TEntryType; src: TEntryTypeSource): Integer;
begin

  result := 0;

  case src of

    etsUnstaged:
      case aItem of
        etNotUpdatedA:                                              result := 2;
        etWorktreeChangedSinceIndex..etWorktreeChangedSinceIndexT:  result := 6;
        etDeletedInWorktree..etDeletedInWorktreeC:                  result := 3;
        etIgnored:                                                  result := 5;
        etUnmergedAddedByUs..etUnmergedBothModified:                result := 11;
      end;

    etsStaged:
      case aItem of
        etUpdatedInIndex..etUpdatedInIndexD:                        result := 7;
        etAddedToIndex..etAddedToIndexD:                            result := 2;
        etDeletedFromIndex:                                         result := 3;
        etCopiedInIndex..etCopiedInIndexD:                          result := 4;
        etRenamedInIndex..etRenamedInIndexD:                        result := 10;
      end;

    etsAmend:
      case aItem of
        etUpdatedInIndex..etUpdatedInIndexD:                        result := 12 + 7;
        etAddedToIndex..etAddedToIndexD:                            result := 12 + 2;
        etDeletedFromIndex:                                         result := 12 + 3;
        etCopiedInIndex..etCopiedInIndexD:                          result := 12 + 4;
        etRenamedInIndex..etRenamedInIndexD:                        result := 12 + 10;
      end;

    etsStagedAmend:
      case aItem of
        etUpdatedInIndex..etUpdatedInIndexD:                        result := 24 + 7;
        etAddedToIndex..etAddedToIndexD:                            result := 24 + 2;
        etDeletedFromIndex:                                         result := 24 + 3;
        etCopiedInIndex..etCopiedInIndexD:                          result := 24 + 4;
        etRenamedInIndex..etRenamedInIndexD:                        result := 24 + 10;
      end;

  end;
end;

// given a entry type this funcion returns wether the entry belongs to
// to list
function EntryTypeInList(entry: TEntryType; staged: boolean): boolean;
begin

  result := false;

  if staged then
    case entry of
      etUpdatedInIndex..etDeletedFromIndex,
      etRenamedInIndex..etCopiedInIndexD:
        result := true;
    end
  else
    case entry of
      etUnknown:;
      etUpdatedInIndex..etCopiedInIndexD:;
      etIndexAndWorktreeMatchesM..etIndexAndWorktreeMatchesC:;
      else
        result := true;
    end;

end;

end.

