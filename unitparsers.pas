{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}
unit unitparsers;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  unitcommon, unitgittypes, unitentries;

procedure ParsePorcelainV1(var head:pchar; tail: pchar; out entry: PFileEntry);
procedure ParseOrdinaryChanged(var head: pchar; tail: pchar; out entry: PFileEntry);
procedure ParseRenamedCopied(var head: pchar; tail: pchar; out entry: PFileEntry);
procedure ParseUnmerged(var head: pchar; tail: pchar; out entry: PFileEntry);
procedure ParseOther(var head: pchar; tail: pchar; out entry: PFileEntry);
procedure ParseBranches(var head: pchar; tail: pchar; out fBranch, fBranchOID, fUpstream: string; out fCommitsAhead, fCommitsBehind: Integer);
procedure ParseStatus(var head: pchar; tail: pchar; lstUnstaged, lstStaged: TStrings; fEntries:TfpList; out fMergingConflict:boolean; v1: boolean);
procedure ParseDescribe(cmd: string; out fLastTag, fLastTagOID: string; out fLastTagCommits:Integer);
procedure ParseBisectLog(cmd: string; out startBad, startGood, lastBad, lastGood, termBad, termGood: string; out finished:boolean);
procedure ParseCommit(var head:pchar; tail: pchar; fEntries: TStrings; out commit, author, email, date, subject: string);
procedure ParseLogItem(aBuf: pchar; out aItem: TLogItem);
procedure ParseTreeItem(var head:pchar; tail:pchar; out mode1, mode2:Integer; out h1,h2:string; out change:char; out path,origPath,score:string);

implementation

function NextField(var head:pchar; separator:pchar=' '): string;
var
  p: pchar;
begin
  p := strpos(head, separator);
  if p<>nil then begin
    SetString(result, head, p-head);
    head := p + 1;
  end else
    result := '';
end;

procedure AddEntryToList(list: TStrings; entry:PFileEntry; staged:boolean);
begin

  if staged then

    case entry^.EntryTypeStaged of
      etUpdatedInIndex..etDeletedFromIndex:
        list.AddObject(entry^.path, TObject(entry));
      etRenamedInIndex..etCopiedInIndexD:
        list.AddObject(entry^.origPath + ' -> ' + entry^.path, TObject(entry));
    end

  else

    case entry^.EntryTypeUnStaged of
      etUnknown:;
      etUpdatedInIndex..etCopiedInIndexD:;
      etIndexAndWorktreeMatchesM..etIndexAndWorktreeMatchesC:;
      else
        list.AddObject(entry^.path, TObject(entry));
    end;

end;

procedure ParsePorcelainV1(var head: pchar; tail: pchar; out entry: PFileEntry);
var
  q: pchar;
begin
  // <XY> <path> [ <origpath ]
  new(Entry);
  entry^.EntryTypeStaged := XYToEntryType(head^, (head+1)^, true);
  entry^.EntryTypeUnStaged := XYToEntryType(head^, (head+1)^, false);
  entry^.x := head^;
  entry^.y := (head+1)^;

  if (entry^.EntryTypeStaged in UnmergedSet) or (entry^.EntryTypeUnStaged in UnmergedSet) then
    entry^.EntryKind := ekUnmerged
  else if (entry^.x='R') or (entry^.y='R') then
    entry^.EntryKind := ekRenamedCopied // todo: Check porcelain v1 EntryKind ekRenamedCopied
  else if (entry^.x='?') or (entry^.y='?') then
    entry^.EntryKind := ekUntracked
  else if (entry^.x='!') or (entry^.y='!') then
    entry^.EntryKind := ekIgnored
  else
    entry^.EntryKind := ekOrdinaryChanged;  // todo: Check porcelain v1 EntryKind ekOrdinaryChanged

  inc(head, 3);
  q := strpos(head, ' ');
  if (q<>nil) then begin
    entry^.EntryKind := ekRenamedCopied;
    SetString(entry^.path, head, q-head);
    inc(head, q-head);
    entry^.origPath := head;
  end else
    entry^.path := head
end;

procedure ParseOrdinaryChanged(var head: pchar; tail: pchar; out
  entry: PFileEntry);
var
  s: string;
  n: Integer;
begin
  New(entry);
  entry^.EntryKind := ekOrdinaryChanged;
  // 1 <XY> <sub> <mH> <mI> <mW> <hH> <hI> <path>
  Inc(head, 2);
  entry^.EntryTypeStaged := XYToEntryType(head^, (head+1)^, true);
  entry^.EntryTypeUnStaged := XYToEntryType(head^, (head+1)^, false);
  entry^.x := head^;
  entry^.y := (head+1)^;

  Inc(head, 3);
  entry^.SubModule := [];
  if head^='S' then begin
    include(entry^.Submodule, smtSubmodule);
    if (head+1)^ = 'C' then include(entry^.Submodule, smtCommitChanged);
    if (head+2)^ = 'M' then include(entry^.Submodule, smtTrackedChanges);
    if (head+3)^ = 'U' then include(entry^.Submodule, smtUntrackedChanges);
  end;

  inc(head, 5);
  s := NextField(head);
  entry^.m1 := StrToInt('&' + s);
  s := NextField(head);
  entry^.m2 := StrToInt('&' + s);
  s := NextField(head);
  entry^.mW := StrToInt('&' + s);

  entry^.h1 := NextField(head);
  entry^.h2 := NextField(head);

  n := strlen(head);
  entry^.path := head;
  inc(head, n+1);
  entry^.origPath := '';
end;

procedure ParseRenamedCopied(var head: pchar; tail: pchar; out entry: PFileEntry
  );
var
  s: string;
  n: Integer;
begin
  New(entry);
  entry^.EntryKind := ekRenamedCopied;

  // 2 <XY> <sub> <mH> <mI> <mW> <hH> <hI> <X><score> <path><sep><origPath>
  Inc(head, 2);
  entry^.EntryTypeStaged := XYToEntryType(head^, (head+1)^, true);
  entry^.EntryTypeUnStaged := XYToEntryType(head^, (head+1)^, false);
  entry^.x := head^;
  entry^.y := (head+1)^;

  Inc(head, 3);
  entry^.SubModule := [];
  if head^='S' then begin
    include(entry^.Submodule, smtSubmodule);
    if (head+1)^ = 'C' then include(entry^.Submodule, smtCommitChanged);
    if (head+2)^ = 'M' then include(entry^.Submodule, smtTrackedChanges);
    if (head+3)^ = 'U' then include(entry^.Submodule, smtUntrackedChanges);
  end;

  inc(head, 5);
  s := NextField(head);
  entry^.m1 := StrToInt('&' + s);
  s := NextField(head);
  entry^.m2 := StrToInt('&' + s);
  s := NextField(head);
  entry^.mW := StrToInt('&' + s);

  entry^.h1 := NextField(head);
  entry^.h2 := NextField(head);
  entry^.xScore := NextField(head);

  entry^.path := head;
  n := strlen(head);
  inc(head, n+1);
  entry^.origPath := head;
  n := strlen(head);
  inc(head, n+1);
end;

procedure ParseUnmerged(var head: pchar; tail: pchar; out entry: PFileEntry);
var
  s: string;
begin
  New(entry);
  entry^.EntryKind := ekUnmerged;

  // u <XY> <sub> <m1> <m2> <m3> <mW> <h1> <h2> <h3> <path>
  Inc(head, 2);
  entry^.EntryTypeStaged := XYToEntryType(head^, (head+1)^, true, true);
  entry^.EntryTypeUnStaged := XYToEntryType(head^, (head+1)^, false, true);
  entry^.x := head^;
  entry^.y := (head+1)^;

  Inc(head, 3);
  entry^.SubModule := [];
  if head^='S' then begin
    include(entry^.Submodule, smtSubmodule);
    if (head+1)^ = 'C' then include(entry^.Submodule, smtCommitChanged);
    if (head+2)^ = 'M' then include(entry^.Submodule, smtTrackedChanges);
    if (head+3)^ = 'U' then include(entry^.Submodule, smtUntrackedChanges);
  end;

  inc(head, 5);
  s := NextField(head);
  entry^.m1 := StrToInt('&' + s);
  s := NextField(head);
  entry^.m2 := StrToInt('&' + s);
  s := NextField(head);
  entry^.m3 := StrToInt('&' + s);
  s := NextField(head);
  entry^.mW := StrToInt('&' + s);

  entry^.h1 := NextField(head);
  entry^.h2 := NextField(head);
  entry^.h2 := NextField(head);

  entry^.path := head;

end;

procedure ParseOther(var head: pchar; tail: pchar; out entry: PFileEntry);
begin
  New(entry);
  entry^.x := head^;
  entry^.y := '.';

  case head^ of
    '?':
      begin
        entry^.EntryKind := ekUntracked;
        entry^.EntryTypeUnStaged := etUntracked;
        entry^.EntryTypeStaged := etUnknown;
      end;
    '!':
      begin
        entry^.EntryKind := ekIgnored;
        entry^.EntryTypeUnStaged := etIgnored;
        entry^.EntryTypeStaged := etUnknown;
      end
    else
      entry^.EntryKind := ekUnknown;
  end;

  inc(head, 2);
  entry^.path := head;
end;

function GetNextString(var head: pchar; var s: string; nextdelim: pchar): boolean;
var
  q: pchar;
  n: Integer;
begin
  q := strpos(head, nextdelim);
  result := q<>nil;
  if result then begin
    SetString(s, head, q-head);
    inc(head, q-head + strlen(nextdelim));
    result := true;
  end else begin
    n := strlen(head);
    SetString(s, head, n);
    inc(head, n);
  end;

end;

procedure ParseBranches(var head: pchar; tail: pchar; out fBranch, fBranchOID,
  fUpstream: string; out fCommitsAhead, fCommitsBehind: Integer);
var
  ab: string;
  i, n: Integer;
  q: pchar;
begin
  fBranch := '';
  fBranchOID := '';
  fUpstream := '';
  fCommitsAhead := 0;
  fCommitsBehind := 0;
  ab := '';

  // scan header lines
  while (head<tail) and (head^='#') do begin

    n := strlen(head);

    if (fBranchOID='') and (strlcomp(head, '# branch.oid', 12)=0) then begin
      SetString(fBranchOID, head + 13, n - 13);
    end else
    if (fBranch='') and (strlcomp(head, '# branch.head', 13)=0) then begin
      SetString(fBranch, head + 14, n - 14);
    end else
    if (fUpstream='') and (strlcomp(head, '# branch.upstream', 17)=0) then begin
      SetString(fUpstream, head + 18, n - 18);
    end else
    if (ab='') and (strlcomp(head, '# branch.ab', 11)=0) then begin
      SetString(ab, head + 12, n - 12);
      i := pos(' ', ab);
      fCommitsAhead := StrToIntDef(copy(ab, 1, i-1), 0);
      fCommitsBehind := StrToIntDef(copy(ab, i+1, Length(ab)), 0);
    end else
    if (fBranch='') and (strlcomp(head, '## ', 3)=0) then begin
      q := head;
      inc(q, 3);
      if GetNextString(q, fBranch, '...') then
      if GetNextString(q, fUpstream, ' [') then
      if GetNextString(q, ab, ']') then begin
        // ahead x behind y
        // ahead x
        i := pos('behind ', ab);
        if i>0 then begin
          fCommitsBehind := -StrToIntDef(copy(ab, i+7, Length(ab)), 0);
          delete(ab, i, length(ab));
        end;
        i := pos('ahead ', ab);
        if i>0 then
          fCommitsAhead := StrToIntDef(copy(ab, i+6, length(ab)), 0);
        // gone
      end;
    end;

    inc(head, n + 1);
  end;
end;

procedure ParseStatus(var head: pchar; tail: pchar; lstUnstaged,
  lstStaged: TStrings; fEntries: TfpList; out fMergingConflict: boolean;
  v1: boolean);
var
  n: Integer;
  entry: PFileEntry;
  start: pchar;
  needResetHead: boolean;
begin
  // clear lists
  ClearEntries(fEntries);
  lstUnstaged.Clear;
  lstStaged.Clear;
  fMergingConflict := false;

  // scan header lines
  while (head<tail) do begin

    start := head;
    n := strlen(head);
    //DebugLn(start);
    needResetHead := true;

    if v1 then begin

      ParsePorcelainV1(head, tail, entry);
      if entry^.EntryTypeUnStaged in UnmergedSet then
        fMergingConflict := true;

    end else
      case head^ of
        '1': ParseOrdinaryChanged(head, tail, entry);
        '2':
          begin
            ParseRenamedCopied(head, tail, entry);
            needResetHead := false;
          end;
        'u':
          begin
            fMergingConflict := true;
            ParseUnmerged(head, tail, entry);
          end;
        '?',
        '!': ParseOther(head, tail, entry);
        else entry := nil;
      end;

    if entry<>nil then begin
      fEntries.Add(entry);

      AddEntryToList(lstStaged, entry, true);
      AddEntryToList(lstUnstaged, entry, false);
    end;

    if needResetHead then
      head := start + n + 1;
  end;
end;

procedure ParseDescribe(cmd: string; out fLastTag, fLastTagOID: string; out
  fLastTagCommits: Integer);
var
  p: Integer;
begin
  cmd := trim(cmd);

  fLastTag := ''; fLastTagOID := ''; fLastTagCommits := 0;

  p := cmd.LastIndexOf('-g');
  if p>=0 then begin
    fLastTagOID := Trim(copy(cmd, p+3, MAXINT));
    delete(cmd, p+1, MAXINT);
    p := cmd.LastIndexOf('-');
    if p>=0 then begin
      fLastTagCommits := StrToIntDef(copy(cmd, p+2, MAXINT), 0);
      fLastTag := copy(cmd, 1, p);
    end;
    exit;
  end;

  p := pos('~', cmd);
  if p>0 then begin
    fLastTag := copy(cmd, 1, p-1);
    fLastTagCommits := StrToIntDef(copy(cmd, p+1, MAXINT), 0);
    exit;
  end;

  fLastTag := trim(cmd);

end;

procedure ParseBisectLog(cmd: string; out startBad, startGood, lastBad,
  lastGood, termBad, termGood: string; out finished: boolean);
var
  s: string;
  i: Integer;
  arr: TStringArray;

  procedure SetTerm(line: string; alt1, alt2: string; var term: string);
  var
    j, k, len: Integer;
  begin

    j := pos('--term-'+alt1+'=', line);
    if j=0 then begin
      j := pos('--term-'+alt2+'=', line);
      if j=0 then exit;
      len := length(alt2)
    end else
      len := length(alt1);

    j += 7 + len + 1;

    k := line.IndexOf('''', j);

    if k>0 then
      term := line.Substring(j-1, k-j+1);
  end;

  function IsLineBad(aLine:string; out oid:string): boolean;
  var
    p: Integer;
  begin
    // # bad: [oid]
    result := pos('# '+termBad+': ', aLine)=1;
    if result then
      oid := copy(aLine, 6+Length(termBad), 40);
  end;

  function IsLineGood(aLine:string; out oid:string): boolean;
  var
    p: Integer;
  begin
    // # good: [oid]
    result := pos('# '+termGood+': ', aLine)=1;
    if result then
      oid := copy(aLine, 6+Length(termGood), 40);
  end;

begin

  startBad := '';
  startGood := '';
  lastBad := '';
  lastGood := '';
  termBad := 'bad';
  termGood := 'good';
  finished := false;

  arr := cmd.Split([#10]);

  for i:=0 to Length(arr)-1 do begin
    if pos('git bisect start', arr[i])>0 then begin
      SetTerm(arr[i], 'good', 'old', termGood);
      SetTerm(arr[i], 'bad', 'new', termBad);
      break;
    end;
  end;

  for i:=Length(arr)-1 downto 0 do begin
    if not finished and (pos('# first '+termBad+' commit', arr[i])=1) then finished := true;
    if (lastBad='') and IsLineBad(arr[i], s) then lastBad := s;
    if (lastGood='') and IsLineGood(arr[i], s) then lastGood := s;
    if (lastBad<>'') and (lastGood<>'') then
      break;
  end;

  for i:=0 to Length(arr)-1 do begin
    if (startBad='') and IsLineBad(arr[i], s) then startBad := s;
    if (startGood='') and IsLineGood(arr[i], s) then startGood := s;
    if (startBad<>'') and (startGood<>'') then
      break;
  end;

end;

procedure ParseCommit(var head: pchar; tail: pchar; fEntries: TStrings; out
  commit, author, email, date, subject: string);
var
  p: pchar;
  s: string;
  entry: PFileEntry;
begin
  {
  An output line is formatted this way:

  in-place edit  :100644 100644 bcd1234 0123456 M file0
  copy-edit      :100644 100644 abcd123 1234567 C68 file1 file2
  rename-edit    :100644 100644 abcd123 1234567 R86 file1 file3
  create         :000000 100644 0000000 1234567 A file4
  delete         :100644 000000 1234567 0000000 D file5
  unmerged       :000000 000000 0000000 0000000 U file6

  That is, from the left to the right:

  a colon.
  mode for "src"; 000000 if creation or unmerged.
  a space.
  mode for "dst"; 000000 if deletion or unmerged.
  a space.
  sha1 for "src"; 0{40} if creation or unmerged.
  a space.
  sha1 for "dst"; 0{40} if deletion, unmerged or "work tree out of sync with the index".
  a space.
  status, followed by optional "score" number.
  a tab or a NUL when -z option is used.
  path for "src"
  a tab or a NUL when -z option is used; only exists for C or R.
  path for "dst"; only exists for C or R.
  an LF or a NUL when -z option is used, to terminate the record.

  Possible status letters are:

  A: addition of a file
  C: copy of a file into a new one
  D: deletion of a file
  M: modification of the contents or mode of a file
  R: renaming of a file
  T: change in the type of the file (regular file, symbolic link or submodule)
  U: file is unmerged (you must complete the merge before it can be committed)
  X: "unknown" change type (most probably a bug, please report it)
  }

  commit := '';
  author := '';
  email := '';
  date := '';
  subject := '';

  if strlcomp(head, 'commit ', 7)<>0 then exit;
  SetString(commit, head + 7, 40);
  inc(head, 47);

  p := strpos(head, 'Author:'); if p=nil then exit;
  head := p + 8;
  p := strpos(head, '<'); if p=nil then exit;
  SetString(author, head, p - head - 1);

  head := p + 1;
  p := strpos(head, '>'); if p=nil then exit;
  SetString(email, head, p - head);

  head := p + 1;
  p := strpos(head, 'Date:'); if p=nil then exit;
  head := p + 6;

  Date := Trim(NextField(head, #10));

  // now process the commit message
  s := NextField(head, #10); // s should be empty
  while (s<>'') or (subject='') do begin
    if subject<>'' then subject += LineEnding;
    subject += copy(s, 5, MAXINT);
    s := NextField(head, #10);
  end;

  // head should be now at the start of file changes
  while (head<>nil) and (head^=':') do begin

    New(entry);
    FillChar(entry^, SizeOf(TFileEntry), 0);
    entry^.y := '.';

    inc(head);

    s := NextField(head);
    entry^.m1 := StrToInt('&' + s);
    s := NextField(head);
    entry^.m2 := StrToInt('&' + s);

    entry^.h1 := NextField(head);
    entry^.h2 := NextField(head);

    s := NextField(head, #9);
    entry^.x := s[1];
    case s[1] of
      'C', 'R':
        begin
          Delete(s, 1, 1);
          entry^.xScore := s;
          entry^.origPath := NextField(head, #9);
          entry^.y := '.';
        end;
      'M':
        entry^.y := 'M';
    end;
    entry^.path := NextField(head, #10);
    entry^.EntryTypeStaged := XYToEntryType(entry^.x, entry^.y, true);

    AddEntryToList(fEntries, entry, true);
  end;


end;

procedure ParseLogItem(aBuf: pchar; out aItem: TLogItem);
var
  p: PChar;

  function NextString: RawbyteString;
  var
    q: pchar;
  begin
    q := strpos(p, #2);
    if q<>nil then begin
      SetString(result, p, q-p);
      p := q + 1;
    end else
      result := p;
  end;

begin

  p := aBuf;
  aItem.CommiterDate := StrToInt64Def(NextString, 0);
  aItem.ParentOID := NextString;
  aItem.CommitOID := NextString;
  aItem.Author := NextString;
  aItem.Email := NextString;
  aItem.Subject := NextString;

end;

procedure ParseTreeItem(var head: pchar; tail: pchar; out mode1,
  mode2: Integer; out h1, h2: string; out change: char; out path, origPath,
  score: string);
var
  s: string;
begin

  mode1 := 0;
  mode2 := 0;
  h1 := '';
  h2 := '';
  change := #0;
  path := '';
  origPath := '';
  score := '';

  if (head=nil) or (head^<>':') then
    exit;

  inc(head);

  s := NextField(head);
  mode1 := StrToInt('&' + s);
  s := NextField(head);
  mode2 := StrToInt('&' + s);

  h1 := NextField(head);
  h2 := NextField(head);

  s := NextField(head, #9);
  change := s[1];
  case change of
    'C', 'R':
      begin
        Delete(s, 1, 1);
        score := s;
        origPath := NextField(head, #9);
      end;
  end;
  path := NextField(head, #10);
end;



end.

