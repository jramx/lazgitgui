unit tstsreflist;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry,
  unitcommon, unitcustomconfig, unitgitmgr,
  tstsmain;

type

  TRefListTests = class(TTestCase)
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestHookUp;
  end;

implementation

procedure TRefListTests.TestHookUp;
begin
  Fail('Write your own test');
end;

procedure TRefListTests.SetUp;
begin

end;

procedure TRefListTests.TearDown;
begin

end;

initialization

  RegisterTest(TRefListTests);
end.

