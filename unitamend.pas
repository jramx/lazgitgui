{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  Amend feature helper
}
unit unitamend;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  unitcommon, unitgittypes, unitcommit, unitentries, unitifaces, unitgitmgr;

type

  PAmendEntry = ^TAmendEntry;
  TAmendEntry = record
    StageEntry: PFileEntry;
    CommitEntry: PFileEntry;
    Index: Integer;
  end;

  { TAmend }

  TAmend = class
  private
    fGitMgr: TGitMgr;
    fCommit: TCommit;
    fEntries: TfpList;
    procedure SetGitMgr(AValue: TGitMgr);
  public
    constructor Create;
    destructor destroy; override;
    procedure LoadLastCommit;
    procedure Merge(staged, outList: TStrings);

    property Commit: TCommit read fCommit;
    property GitMgr: TGitMgr read fGitMgr write SetGitMgr;
  end;

  function AmendEntryToStr(entry: PAmendEntry): string;

implementation

function AmendEntryToStr(entry: PAmendEntry): string;
var
  sEntry, cEntry: PFileEntry;
begin
  if entry=nil then begin
    result := 'Amend entry invalid';
    exit;
  end;

  sEntry := entry^.StageEntry;
  cEntry := entry^.CommitEntry;

  if (sEntry<>nil) and (cEntry<>nil) then

    result := EntryTypeToStr(sEntry^.x, sEntry^.y) + ' will overwrite commited ' + EntryTypeToStr(cEntry^.x, cEntry^.y)

  else
  if (sEntry<>nil) then

    result := EntryTypeToStr(sEntry^.x, sEntry^.y) + ' will be added to commit'

  else

    result := EntryTypeToStr(cEntry^.x, cEntry^.y) + ' will remain in the commit';
end;

{ TAmend }

procedure TAmend.SetGitMgr(AValue: TGitMgr);
begin
  if fGitMgr = AValue then Exit;
  fGitMgr := AValue;

  fCommit.GitMgr := fGitMgr;
end;

constructor TAmend.Create;
begin
  inherited Create;
  fCommit := TCommit.Create;
  fEntries := TFpList.Create;
end;

destructor TAmend.destroy;
var
  i: Integer;
  entry: PAmendEntry;
begin
  for i:=0 to fEntries.Count-1 do begin
    entry := PAmendEntry(fEntries[i]);
    Dispose(entry);
  end;
  fEntries.Free;
  fCommit.Free;
  inherited destroy;
end;

procedure TAmend.LoadLastCommit;
var
  fGit: IGit;
  cmdOut: RawByteString;
  aCommit: string;
  i: Integer;
begin
  fGit := fGitMgr.Git;
  if fGit.Any('rev-list -n 1 ' + fGitMgr.Branch + ' --not --remotes --', cmdOut)<=0 then begin
    aCommit := trim(cmdOut);
    fCommit.Load(aCommit);
  end;
  fGit := nil;
end;

procedure TAmend.Merge(staged, outList: TStrings);
var
  entry: PAmendEntry;
  i, j: Integer;
begin
  // now we got two lists and the outList
  // staged:        the more recent list of the stage area
  // fCommitedList: the list of changes of the last commit
  // outList:       basically it should be the lstStaged.Items list


  // outlist should have:
  //
  // * New items:     new staged items that have not match in fCommitedList
  // * Matched items: items that will overwrite the commited ones
  // * Commit items:  The rest of fCommitedList items

  outlist.BeginUpdate;
  try
    outList.Clear;

    for i:=0 to staged.Count-1 do begin
      new(entry);
      fEntries.Add(entry);
      j := fCommit.Entries.IndexOf(staged[i]);
      if j<0 then begin
        entry^.StageEntry := PFileEntry(staged.Objects[i]);
        entry^.CommitEntry := nil;
        entry^.Index := EntryTypeToIndex(entry^.StageEntry^.EntryTypeStaged, etsStaged);
        outList.AddObject(staged[i], TObject(entry));
      end else begin
        entry^.StageEntry := PFileEntry(staged.Objects[i]);;
        entry^.CommitEntry := PFileEntry(fCommit.Entries.Objects[j]);
        entry^.Index := EntryTypeToIndex(entry^.StageEntry^.EntryTypeStaged, etsStagedAmend);
        outList.AddObject(staged[i] + ' => ' + fCommit.Entries[j], TObject(entry));
      end;
    end;

    for i:=0 to fCommit.Entries.Count-1 do begin
      j := staged.IndexOf(fCommit.Entries[i]);
      if j<0 then begin
        new(entry);
        entry^.StageEntry := nil;
        entry^.CommitEntry := PFileEntry(fCommit.Entries.Objects[i]);
        entry^.Index := EntryTypeToIndex(entry^.CommitEntry^.EntryTypeStaged, etsAmend);
        fEntries.Add(entry);
        outList.AddObject(fCommit.Entries[i], TObject(entry));
      end;
    end;

  finally
    outlist.EndUpdate;
  end;

end;

end.

