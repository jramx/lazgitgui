{ LazGitGui: An interface to git status with some additional tools
             with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  Program file
}
program lazgitgui;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  SysUtils, Classes, Interfaces, // this includes the LCL widgetset
  Forms, main, unitconfig, unitprocess, unitentries, unitgit, unitnewbranch,
  unitruncmd, unitansiescapes, unitnewtag, unitdebug, unitlogcache, unitdbindex,
  unitgitutils, unitdrafts, unitframelog, unitgitmgr, unitgittypes,
  unitcheckouttag, unitcommitbrowser, unitvfs, unithighlighterhelper,
  unitgraphbuild, unitfilehistory, unitformlog, unitcustcmdform, unitcustomcmds,
  unitreset, LCLTranslator, unitcommon, unittextchunks, unitlinkmgr, unitgitcmd,
  unitpush, unitremotes, unitsyneditextras, unittoposort, unitcolumnscroller,
  unitclone, unitrepovars, unitoutput, unitformmgr, unitgitstate,
  unitframestateactions, unitframeeditor, unitparsers, unitcustomconfig,
  unitrunthread, unitbisect, unitamend, unitcommit, unitrepobrowser,
  unitsrceditor, unitframeimage, unitviewermgr, unitdifftool, unitguitools,
  unitmarkeditem, unitwidgetset, unitdiffactions;

{$R *.res}

{.$define drafts}

function ConcatArr(arr: TStringArray): string;
var
  s: String;
begin
  result := '';
  for s in arr do begin
    if result<>'' then
      result += ' ';
    result += s;
  end;
end;

function GetTargetDir(aDir: string; var targetDir:string): boolean;
begin
  result := true;
  if DirectoryExists(aDir) then
    targetDir := aDir
  else if FileExists(aDir) then
    targetDir := ExtractFilePath(aDir)
  else
    result := false;
end;

var
  ConfigOpenOnce: boolean;
  aDir, fileHistory: string;
  fFormMgr: TFormMgr;
  nonOpts: TStringArray;
  n: Integer;
begin
  SetDefaultLang('', 'translations', '', false);

  Application.Scaled := True;

  {$ifdef drafts}
  //TestParams;
  //AnalizeColumns;
  //TestVfs;
  //TestLinks;
  TestRegExpr;
  {$else}

  fConfig := TConfig.Create;

  nonOpts := Application.GetNonOptions('f::clbq',
    [
      'filehistory::',      // start lazgitgui in file or directory history mode
      'clone',              // start lazgitgui by cloning a URL using the clipboard if available
      'log',                // start lazgitgui in log mode
      'browser',            // start lazgitgui in repository browser mode
      'logfile:',           // specify where to save the produced debug output
      'locked',             // start lazgitgui in directory lock mode
      'clearcache',         // clear the log cache at start
      'quiet',              // filehistory mode launched with an untracked file, do not show a msg dialog
      'git:',               // Don't search for git exe, use this instead
      'lang:'               // Specify preferred locale (Handled by LCLTranslator)
    ]
  );

  TDebugging.Config := fConfig;
  TDebugging.Setup;

  targetDir := '.';
  if Length(nonOpts)>0 then begin
    if not GetTargetDir(nonOpts[Length(nonOpts)-1], targetDir) then
      GetTargetDir(ConcatArr(nonOpts), targetDir);
  end;

  targetDir := IncludeTrailingPathDelimiter(targetDir);

  ConfigOpenOnce := fConfig.ReadBoolean('ConfigOpenOnce');
  try
    if ConfigOpenOnce then
      fConfig.OpenConfig;
    RequireDerivedFormResource:=True;
    Application.Scaled := True;
    Application.Initialize;

    fileHistory := '';
    if Application.HasOption('f', 'filehistory') then begin
      fileHistory := Application.GetOptionValue('f', 'filehistory');
      if fileHistory='' then begin
        WriteLn(StdErr, 'Filehistory requires a filename or a directory');
        exit;
      end;
      if (not FileExists(fileHistory)) and (not DirectoryExists(fileHistory)) then begin
        WriteLn(StdErr, format('Filehistory requires an existing filename or directory, file %s not found', [fileHistory]));
        exit;
      end;
    end;

    fFormMgr := nil;

    if Application.HasOption('c', 'clone') then begin

      fFormMgr := TFormMgr.Create(Application);
      fFormMgr.LaunchProc := @CloneLauncher;
      fFormMgr.Target := targetDir;

    end else
    if fileHistory<>'' then begin

      fFormMgr := TFormMgr.Create(Application);
      fFormMgr.LaunchProc := @FileHistoryLauncher;
      fFormMgr.Target := fileHistory;

    end else
    if Application.HasOption('l', 'log') then begin

      fFormMgr := TFormMgr.Create(Application);
      fFormMgr.LaunchProc := @LogLauncher;
      fFormMgr.Target := targetDir;

    end else
    if Application.HasOption('b', 'browser') then begin

      fFormMgr := TFormMgr.Create(Application);
      fFormMgr.LaunchProc := @RepoBrowserLauncher;
      fFormMgr.Target := targetDir;

    end;

    if fFormMgr<>nil then
      fFormMgr.Launch
    else
    Application.CreateForm(TfrmMain, frmMain);
    Application.Run;

  finally
    if ConfigOpenOnce then
      fConfig.CloseConfig;
    fConfig.Free;
  end;


  {$endif}

end.

