{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  Git manager.
  It creates the git instance and provides the git interface to several parts
  that need it. Starts operations and distribute results.
}
unit unitgitmgr;

{$mode ObjFPC}{$H+}
{.$define DebugRefList}

interface

uses
  Classes, SysUtils, LazLogger,
  unitcommon, unitgittypes, unitgitutils, unitgit, unitifaces, unitprocess,
  unitrunthread, unitentries, unitgitstate, unitparsers;

const
  GITMGR_EVENT_UPDATESTATUS         = 1;
  GITMGR_EVENT_REFLISTCHANGED       = 2;
  GITMGR_EVENT_NEWBRANCH            = 6;
  GITMGR_EVENT_LOADREPOSITORY       = 7;
  GITMGR_EVENT_BISECTACTION         = 8;
  GITMGR_EVENT_UPDATECURRENTDIFF    = 9;

type

  { TObserverMgr }

  TObserverMgr = class
  private
    fObservers: array of IObserver;
    function IndexOf(who: IObserver): Integer;
  public
    procedure AddObserver(who: IObserver);
    procedure RemoveObserver(who: IObserver);
    procedure NotifyObservers(sender: TObject; what: Integer; data: PtrInt);
  end;

  { TGitMgr }

  TGitMgr = class
  private
    fAlternateGitExe: string;
    fBranch: string;
    fBranchOID: string;
    fCommitsAhead: Integer;
    fCommitsBehind: Integer;
    fEntries: TFPList;
    fLastTag: string;
    fLastTagOID: string;
    fLastTagCommits: Integer;
    fConfig: IConfig;
    fGit: TGit;
    fLocked: boolean;
    fOnUpdateStatusDone: TNotifyEvent;
    fShowTags: boolean;
    fUpstream: string;
    fViewIgnoredFiles: boolean;
    fViewTrackedFiles: boolean;
    fViewUntrackedFiles: boolean;
    fDescribed: boolean;
    fLastDescribedTag: string;
    fUnstagedList, fStagedList: TStringList;
    fObserverMgr: TObserverMgr;
    fRemotes: TRemotesArray;
    fWithStatusAheadBehihd: Boolean;
    fWithStatusPorcelainV2: Boolean;
    fWithStatusIgnored: Boolean;
    fInternalRefList: TStringList;
    fRefsMap: TRefsMap;
    fGitState: TGitState;
    function GetGit: IGit;
    function GetLocked: boolean;
    function GetRemotesList: string;
    procedure OnCommandsDone(Sender: TObject);
    procedure SetConfig(AValue: IConfig);
    procedure OnCommandProgress(sender: TObject; item: TCommandItem; percent: single);
    function  GetRemoteIndex: Integer;
    function RefListEnabledField(aField: string): boolean;
    procedure UpdateRefsMap;
    function GetRefList: TStringList;
    function GetRefsMap: TRefsMap;
    procedure DoUpdateStatus(ondone:TNotifyEvent; sync:boolean);
  public
    constructor create;
    destructor destroy; override;

    function  AddToIgnoreFile(aFile:string; justType:boolean): boolean;
    function  Initialize: boolean;
    procedure UpdateStatus(ondone: TNotifyEvent = nil);
    procedure UpdateStatusSync;
    procedure UpdateDiff(aPath: string; target: Integer);
    procedure UpdateRefList;
    function  Bisect(bAction: Integer; out cmdOut:RawByteString; commitBad:string=''; commitGood:string=''): Integer;
    procedure AddObserver(who: IObserver);
    procedure RemoveObserver(who: IObserver);
    function  IndexOfLocalBranch(aName: string): Integer;
    procedure CreateBranch(sender: TObject; branchName, command: string; switch, fetch:boolean);
    procedure ForceTagDescription;
    procedure UpdateRemotes;
    function  FillRefList(list: TStrings; pattern:string; fields:array of string; keyField:string=''): Integer; overload;
    function  RefsFilter(commitOID: string; filter: TRefFilterProc): TRefInfoArray;
    procedure QueueLoadRepository(aDir: string);
    function  SwitchTo(cmd: string): Integer;
    function  TagTo(tagName, tagCommit:string; annotated:boolean; tagMsg:string): Integer;
    function  GetStateCommitMsg(out msg: string): boolean;
    function  OpenRepository(aDir: string): boolean;
    function  GetRelativeFilename(aFilename: string): string;
    function  GetCommit(commitOID: string; out item: TLogItem): Integer;
    function  GetFileStream(filetree: string): TStream;
    function  ExtractToFile(filetree, dstFile:string; assumeTextContent:boolean=true): boolean;
    function  GetRefInfo(aRef: string = ''): PRefInfo;

    property CommitsAhead: Integer read fCommitsAhead;
    property CommitsBehind: Integer read fCommitsBehind;
    property Branch: string read fBranch;
    property BranchOID: string read fBranchOID;
    property Upstream: string read fUpstream;
    property LastTag: string read fLastTag;
    property LastTagCommits: Integer read fLastTagCommits;
    property LastTagOID: string read fLastTagOID;
    property AlternateGitExe: string read fAlternateGitExe write fAlternateGitExe;

    property Git: IGit read GetGit;
    property Config: IConfig read fConfig write SetConfig;
    property ShowTags: boolean read fShowTags write fShowTags;
    property ViewIgnoredFiles: boolean read fViewIgnoredFiles write fViewIgnoredFiles;
    property ViewUntrackedFiles: boolean read fViewUntrackedFiles write fViewUntrackedFiles;
    property ViewTrackedFiles: boolean read fViewTrackedFiles write fViewTrackedFiles;
    property UnstagedList: TStringList read fUnstagedList;
    property StagedList: TStringList read fStagedList;
    property Remotes: TRemotesArray read fRemotes;
    property RemotesList: string read GetRemotesList;
    property RemoteIndex: Integer read GetRemoteIndex;
    property RefList: TStringList read GetRefList;
    property RefsMap: TRefsMap read GetRefsMap;
    property GitState: TGitState read fGitState;
    property Locked: boolean read GetLocked write fLocked;
  end;

implementation

type
  TRefFieldType = ( ftUnknown, ftRefName, ftObjectType, ftObjectName, ftUpstream,
                    ftPush, ftHead, ftWorktreePath, ftContents, ftSubject,
                    ftAuthorName, ftAutorDate, ftCommiterDate, ftCreatorDate,
                    ftReferedObjectType, ftReferedObjectName, ftReferedAuthorName,
                    ftReferedAuthorDate, ftReferedContent, ftReferedSubject);

function FieldToRefFieldType(aField: string): TRefFieldType;
begin
  case aField of
    'refname':        result := ftRefName;
    'objecttype':     result := ftObjectType;
    'objectname':     result := ftObjectName;
    'upstream':       result := ftUpstream;
    'push':           result := ftPush;
    'HEAD':           result := ftHead;
    'worktreepath':   result := ftWorktreePath;
    'contents':       result := ftContents;
    'subject':        result := ftSubject;
    'authorname':     result := ftAuthorName;
    'authordate':     result := ftAutorDate;
    'committerdate':  result := ftCommiterDate;
    'creatordate':    result := ftCreatorDate;
    '*objecttype':    result := ftReferedObjectType;
    '*objectname':    result := ftReferedObjectName;
    '*authorname':    result := ftReferedAuthorName;
    '*authordate':    result := ftReferedAuthorDate;
    '*contents':      result := ftReferedContent;
    '*subject':       result := ftReferedSubject;
  end;
end;

procedure ClearRefList(list: TStrings);
var
  i: Integer;
  info: PRefInfo;
begin
  if List<>nil then begin
    for i:=0 to list.Count-1 do begin
      info := PRefInfo(list.Objects[i]);
      if info^.refered<>nil then begin
        Finalize(info^.refered^);
        Dispose(info^.refered);
      end;
      Finalize(info^);
      Dispose(info);
    end;
    list.clear;
  end;
end;

function CleanRefField(aField: string; out opt:string): TRefFieldType;
var
  i: SizeInt;
begin
  opt := '';
  delete(aField, 1, 2);
  i := pos(':', aField);
  if i<=0 then
    i := pos(')', aField)
  else
    opt := copy(aField, i+1, Length(aField)-i-1);
  if i>0 then
    delete(aField, i, Length(aField));

  result := FieldToRefFieldType(aField);
end;

function StrToRefObjType(objType: string): TRefObjectType;
begin
  case objType of
    'commit': result := rotCommit;
    'blob': result := rotBlob;
    'tree': result := rotTree;
    'tag': result := rotTag;
  end;
end;

function ParseRefList(M: TMemoryStream; list: TStrings; sep:string;
  fields:array of string; keyField:string=''): boolean;
var
  p, q, r, t: pchar;
  info: PRefInfo;
  fieldIndex, n: Integer;
  value, opt, item: string;
  field: TRefFieldType;
  isFirstField: boolean;

  procedure AddReferedField;
  begin
    if (value<>'') and (field in [ftReferedObjectType..ftReferedSubject]) then begin
      if info^.refered=nil then begin
        new(info^.refered);
        FillChar(info^.refered^, SizeOf(TRefInfo), 0);
      end;
      case field of
        ftReferedObjectType:  info^.refered^.objType := StrToRefObjType(value);
        ftReferedObjectName:  info^.refered^.objName := value;
        ftReferedAuthorName:  info^.refered^.authorName := value;
        ftReferedAuthorDate:  info^.refered^.authorDate := GitDateToDateTime(value);
        ftReferedContent:     info^.refered^.subject := value; // ?
        ftReferedSubject:     info^.refered^.subject := value;
      end;
    end;
  end;

begin


  result := false;
  p := m.Memory;
  t := p + m.size;

  while p<t do begin

    // find length of single line "record"
    n := strlen(p);
    q := p + n;

    new(info);
    fillchar(info^, SizeOf(TRefInfo), 0);
    info^.subType := rostOther;
    info^.isTracking := false;
    info^.refered := nil;
    isFirstField := true;

    // each returned field should correspond to every fields[] requested
    fieldIndex := 0;
    while (p<q) and (fieldIndex<Length(fields)) do begin

      // skip empty fields as they are not supported by git version
      if fields[fieldIndex]='' then begin
        inc(fieldIndex);
        continue;
      end;

      // get field value, the start of separator is the end of the field
      r := strpos(p, pchar(sep));
      if r=nil then begin
        DebugLn('Error: field separator not found at %d',[t-p]);
        dispose(info);
        exit;
      end else
        r^ := #0;

      value := p;

      // the keyField is used to select a field value as the 'string' in
      // the resulting list, if not provided, use the first field value
      if ((keyField='') and isFirstField) or
         ((keyField<>'') and (fields[fieldIndex]=keyField))
      then
        item := value;

      isFirstField := false;

      // match requested fields
      field := CleanRefField(fields[fieldIndex], opt);

      case field of
        ftRefname:
          begin
            case opt of
              '':
                begin
                  if pos('refs/tags', value)=1 then info^.subType := rostTag else
                  if pos('refs/heads', value)=1 then info^.subType := rostLocal else
                  if pos('refs/remotes', value)=1 then info^.subType := rostTracking;
                end;
              'short': info^.refName := value;
              'rstrip=-2':
                case value of
                  'refs/tags': info^.subType := rostTag;
                  'refs/heads': info^.subType := rostLocal;
                  'refs/remotes': info^.subType := rostTracking;
                end;
            end;
          end;
        ftObjectType: info^.objType := StrToRefObjType(value);
        ftObjectName:
          begin
            info^.objName := value;
            info^.objNameInt := OIDToQWord(value);
          end;
        ftUpstream: info^.upstream := value;
        ftPush: info^.push := value;
        ftHead: info^.head := value='*';
        ftWorktreePath: info^.worktreepath := value;
        ftContents: info^.subject := value;
        ftSubject: info^.subject := value;
        ftAuthorName: info^.authorName := value;
        ftAutorDate: info^.authorDate := GitDateToDateTime(value);
        ftCommiterDate: info^.committerDate := GitDateToDateTime(value);
        ftCreatorDate: info^.creatorDate := GitDateToDateTime(value);
        else
          AddReferedField;
      end;

      inc(fieldIndex);
      p := r + Length(sep);
    end;

    p := q + 1;
    // skip the eol that for-each-ref always add
    while (p<t) and (p^ in [#10, #13]) do inc(p);

    if info^.subType=rostOther then
      case info^.objType of
        rotCommit:
          if info^.isTracking then  info^.subType := rostTracking
          else                      info^.subType := rostLocal;
        rotTag:                     info^.subType := rostTag;
      end;

    list.addObject(item, TObject(info));
  end;

  result := true;
end;

{ TObserverMgr }

function TObserverMgr.IndexOf(who: IObserver): Integer;
var
  i: Integer;
begin
  for i:=0 to Length(fObservers)-1 do
    if fObservers[i]=who then
      exit(i);
  result := -1;
end;

procedure TObserverMgr.AddObserver(who: IObserver);
var
  i: SizeInt;
begin
  i := IndexOf(who);
  if i<0 then begin
    i := Length(fObservers);
    SetLength(fObservers, i+1);
    fObservers[i] := who
  end;
end;

procedure TObserverMgr.RemoveObserver(who: IObserver);
var
  i: Integer;
begin
  i := IndexOf(who);
  if i>=0 then
    Delete(fObservers, i, 1);
end;

procedure TObserverMgr.NotifyObservers(sender: TObject; what: Integer;
  data: PtrInt);
var
  who: IObserver;
begin
  for who in fObservers do
    who.ObservedChanged(sender, what, data);
end;

{ TGitMgr }

procedure TGitMgr.SetConfig(AValue: IConfig);
begin
  if fConfig = AValue then Exit;
  fConfig := AValue;
  fGit.Config := fConfig;
end;

function TGitMgr.GetGit: IGit;
begin
  result := fGit;
end;

function TGitMgr.GetLocked: boolean;
begin
  result := fLocked and (fGit.StartDir<>'');
end;

function TGitMgr.GetRemotesList: string;
var
  r: TRemoteInfo;
begin
  result := '';
  for r in fRemotes do begin
    if result<>'' then result += ',';
    result += r.name;
  end;
end;

constructor TGitMgr.create;
begin
  inherited Create;
  fGit := TGit.Create;
  fUnstagedList := TStringList.Create;
  fStagedList := TStringList.Create;
  fObserverMgr := TObserverMgr.Create;
  fEntries := TFpList.Create;
  fGitState := TGitState.Create;
  fGitState.git := fGit;
end;

destructor TGitMgr.destroy;
begin
  fGitState.Free;
  ClearEntries(fEntries);
  fEntries.Free;
  fObserverMgr.Free;
  fStagedList.Free;
  fUnstagedList.Free;
  if fRefsMap<>nil then
    fRefsMap.Clear;
  fRefsMap.Free;
  ClearRefList(fInternalRefList);
  fInternalRefList.Free;
  fGit.Free;
  inherited destroy;
end;

function TGitMgr.AddToIgnoreFile(aFile: string; justType: boolean): boolean;
var
  l: TStringList;
  aPath, gitIgnoreFile: string;
begin
  result := false;

  l := TStringList.Create;
  try
    aPath := fGit.TopLevelDir;
    if Locked then
      aPath += fGit.StartDir;
    aPath += '.gitignore';

    if FileExists(aPath) then
      l.LoadFromFile(aPath);

    if justType then begin
      aFile := ExtractFileExt(aFile);
      if aFile='' then
        exit; // refuse to add '*.' to ignore list
      aFile := '*' + aFile;
    end else
      aFile := GetRelativeFilename(aFile);

    result := l.IndexOf(aFile)<0;
    if result then begin
      l.Add(aFile);
      l.SaveToFile(aPath);
    end;

  finally
    l.Free;
  end;
end;

function TGitMgr.Initialize: boolean;
begin
  result := fGit.Initialize(fAlternateGitExe);
  fWithStatusPorcelainV2 := false;
  fWithStatusAheadBehihd := false;
  fWithStatusIgnored     := false;
  if result then begin
    fWithStatusPorcelainV2 := fGit.AtLeastVersion('2.11');
    fWithStatusAheadBehihd := fGit.AtLeastVersion('2.17');
    fWithStatusIgnored     := fGit.AtLeastVersion('2.21'); // TODO: check this..
  end;
end;

procedure TGitMgr.OnCommandProgress(sender: TObject; item: TCommandItem; percent: single);
var
  cmd: string;
  p: Integer;
  head, tail: pchar;
  M: TMemoryStream;
  cmdOut: RawByteString;
  hasMergingConflict: boolean;
begin
  DebugLn('%3f%%: %s',[percent, item.description]);
  case item.description of
    'describe':
      begin
        fLastTag := '';
        fLastTagCommits := 0;
        fLastTagOID := '';
        if item.Result<=0 then begin
          // tag-commits-'g'OID
          cmd := item.Output;
          ParseDescribe(cmd, fLastTag, fLastTagOID, fLastTagCommits);
        end;
      end;

    'status':
      begin
        M := TMemoryStream(item.tag);
        head := M.Memory;
        tail := head + M.Size;
        //M.SaveToFile('laststatus.txt');

        ParseBranches(head, tail, fBranch, fBranchOID, fUpstream, fCommitsAhead, fCommitsBehind);
        ParseStatus(head, tail, fUnstagedList, fStagedList, fEntries, hasMergingConflict, not fWithStatusPorcelainV2);

        if not fWithStatusPorcelainV2 then begin
          if fGit.Any('rev-parse HEAD', cmdOut)<=0 then
            fBranchOID := trim(cmdOut);
        end;

        if fCommitsAhead=0 then begin
          if fGit.Any('rev-list --count '+fBranch+' --not --remotes', cmdOut)<=0 then
            fCommitsAhead := StrToIntDef(Trim(cmdOut), 0);
        end;

        fGitState.Update;
        fGitState.MergingConflict :=  hasMergingConflict;

        fObserverMgr.NotifyObservers(self, GITMGR_EVENT_UPDATESTATUS, 0);
      end;
  end;
end;

function TGitMgr.GetRemoteIndex: Integer;
var
  arr: TStringArray;
  i: Integer;
begin
  if (fUpstream<>'') and (fRemotes<>nil) then begin
    arr := fUpstream.Split('/');
    if arr<>nil then
      for i:=0 to Length(fRemotes)-1 do
        if fRemotes[i].name=arr[0] then begin
          result := i;
          exit;
        end;
  end;
  result := -1;
end;

function TGitMgr.RefListEnabledField(aField: string): boolean;
begin
  result := true;
  if pos('%(worktreepath', aField)=1 then
    // TODO: check the real version worktreepath appeared
    //       here I'm only registering the version it worked for me.
    result := fGit.AtLeastVersion('2.25')

  else if pos(':rstrip', aField)>0 then
    result := fGit.AtLeastVersion('2.13');
end;

procedure TGitMgr.UpdateRefsMap;
var
  i, j, aIndex: Integer;
  info: PRefInfo;
  arr: TRefInfoArray;
  exists: boolean;
  s, sub, objName: string;
begin
  if fRefsMap=nil then begin
    fRefsMap := TRefsMap.Create;
    fRefsMap.Sorted := true;
  end else
    fRefsMap.Clear;

  for i:= 0 to RefList.Count-1 do begin
    info := PRefInfo(RefList.Objects[i]);
    objName := info^.Reference;

    exists := fRefsMap.Find(objName, aIndex);
    if exists then
      arr := fRefsMap.Data[aIndex]
    else
      arr := nil;

    j := Length(arr);
    SetLength(arr, j+1);
    arr[j] := info;

    if not exists then
      fRefsMap.Add(objName, arr)
    else
      fRefsMap[objName] := arr;
  end;

  {$ifdef DebugRefList}
  DebugLn;
  for i:=0 to RefList.Count-1 do begin
    info := PRefInfo(RefList.Objects[i]);
    WriteStr(s, info^.objType);
    WriteStr(sub, info^.subType);
    DebugLn('%3d. %40s | %12s %12s | %s',[i, info^.objName, s, sub, info^.refName]);
    if info^.refered<>nil then begin
      info := info^.refered;
      WriteStr(s, info^.objType);
      WriteStr(sub, info^.subType);
      DebugLn('  -> %40s | %12s %12s | %s',[info^.objName, s, sub, info^.refName]);
    end;
  end;
  DebugLn;
  for i:=0 to fRefsMap.Count-1 do begin
    arr := fRefsMap.Data[i];
    DebugLn('%3d. %s : %d refs', [i, fRefsMap.Keys[i], Length(arr)]);
    for j:=0 to Length(Arr)-1 do
      DebugLn('     %s',[arr[j]^.refName]);
  end;
  {$endif}
end;

function TGitMgr.GetRefList: TStringList;
begin
  if fInternalRefList=nil then
    fInternalRefList := TStringList.Create;
  result := fInternalRefList;
end;

function TGitMgr.GetRefsMap: TRefsMap;
begin
  result := fRefsMap;
end;

procedure TGitMgr.DoUpdateStatus(ondone: TNotifyEvent; sync: boolean);
var
  i: Integer;
  commands: TCommandsArray;
  s: string;
begin

  DebugLn('TGitMgr.DoUpdateStatus: sync=%s',[dbgs(sync)]);

  commands := nil;

  // get the more recent tag
  if fShowTags and (not fDescribed) then begin
    i := Length(commands);
    SetLength(commands, i+1);
    commands[i].description := 'describe';
    commands[i].command := fGit.Exe + ' describe --tags';
    commands[i].RedirStdErr := false;
    commands[i].PreferredOutputType := cipotString;
    commands[i].Enviroment := '';
  end;

  i := Length(commands);
  SetLength(commands, i+1);
  commands[i].description := 'status';
  s := fGit.Exe + ' status -b --long --porcelain';
  if fWithStatusPorcelainV2 then s += '=2';
  if fWithStatusAheadBehihd then s += ' --ahead-behind';
  if fWithStatusIgnored then s += ' --ignored=' + BoolToStr(fViewIgnoredFiles, 'traditional', 'no');
  s += format(' --untracked-files=%s -z', [BoolToStr(fViewUntrackedFiles, 'all', 'no')]);
  if Locked then s +=  ' -- ' + fGit.StartDir;
  commands[i].command := s;
  commands[i].RedirStdErr := false;
  commands[i].PreferredOutputType := cipotStream;
  commands[i].Enviroment := 'LANG=C';

  fOnUpdateStatusDone := OnDone;

  if sync then
    RunCommands(commands, fGit.TopLevelDir, @OnCommandProgress, @OnCommandsDone, true)
  else
    RunInThread(commands, fGit.TopLevelDir, @OnCommandProgress, @OnCommandsDone);
end;

procedure TGitMgr.OnCommandsDone(Sender: TObject);
var
  thread: TRunThread absolute sender;
begin
  if Assigned(fOnUpdateStatusDone) then
    fOnUpdateStatusDone(Self);
end;

procedure TGitMgr.UpdateStatus(ondone: TNotifyEvent);
begin
  DoUpdateStatus(OnDone, false);
end;

procedure TGitMgr.UpdateStatusSync;
begin
  DoUpdateStatus(nil, true);
end;

// update the current diff view according to list identified by the target
// 0=UnStaged List, 1=Staged List
procedure TGitMgr.UpdateDiff(aPath: string; target: Integer);
var
  info: PTagInfo; // reuse an existing structure
begin

  new(info);
  info^.data := aPath;
  info^.result := target;


  // update only the current diff view
  fObserverMgr.NotifyObservers(self, GITMGR_EVENT_UPDATECURRENTDIFF, PtrInt(info));
end;

// Update the internal reflist
procedure TGitMgr.UpdateRefList;
var
  res: Integer;
begin
  GetRefList;

  res := FillRefList(
      fInternalRefList, '', [
      '%(refname:short)',
      '%(refname)',
      '%(objecttype)',
      '%(objectname)',
      '%(upstream:short)',
      '%(HEAD)',
      '%(worktreepath)',
      '%(contents)',
      '%(authorname)',
      '%(authordate)',
      '%(committerdate)',
      '%(creatordate)',
      '%(*objecttype)',
      '%(*objectname)',
      '%(*authorname)',
      '%(*authordate)',
      '%(*contents)'
      ]);

  UpdateRefsMap;

  fObserverMgr.NotifyObservers(Self, GITMGR_EVENT_REFLISTCHANGED, res);
end;

function TGitMgr.Bisect(bAction: Integer; out cmdOut: RawByteString;
  commitBad: string; commitGood: string): Integer;
var
  cmd: string;
  info: PBisectInfo;
begin

  case bAction of
    ACTION_BISECT_START:    cmd := format('start %s %s',[commitBad, commitGood]);
    ACTION_BISECT_GOOD:     cmd := GitState.BisectTermGood;
    ACTION_BISECT_BAD:      cmd := GitState.BisectTermBad;
    ACTION_BISECT_LOG:      cmd := 'log';
    ACTION_BISECT_RESET:    cmd := 'reset';
    ACTION_BISECT_GOOD_SET: cmd := format('%s %s', [GitState.BisectTermGood, commitGood]);
    ACTION_BISECT_BAD_SET:  cmd := format('%s %s', [GitState.BisectTermBad, commitBad]);
  end;

  //DebugLn('TGitMgr.Bisect: cmd=%s',[cmd]);
  new(info);
  info^.Action := bAction;
  info^.result := fGit.Any('bisect '+cmd, cmdOut);
  info^.Log := cmdOut;

  result := info^.result;

  fObserverMgr.NotifyObservers(Self, GITMGR_EVENT_BISECTACTION, PtrInt(info));
end;

procedure TGitMgr.AddObserver(who: IObserver);
begin
  fObserverMgr.AddObserver(who);
end;

procedure TGitMgr.RemoveObserver(who: IObserver);
begin
  fObserverMgr.RemoveObserver(who);
end;

function TGitMgr.IndexOfLocalBranch(aName: string): Integer;
var
  i: Integer;
  info: PRefInfo;
begin
  result := -1;
  aName := lowercase(aName);
  for i:=0 to RefList.Count-1 do begin
    info := PRefInfo(RefList.Objects[i]);
    if (info^.subType=rostLocal) and (aName=info^.refName) then begin
      result := i;
      break;
    end;
  end;
end;

procedure TGitMgr.CreateBranch(sender: TObject; branchName, command: string;
  switch, fetch: boolean);
var
  cmdOut: RawByteString;
  info: PBranchInfo;
  needReflistUpdate: boolean;
  needUpdateStatus: boolean;
begin

  new(info);
  info^.sender := sender;
  info^.refreshBranch := false;
  needReflistUpdate := false;
  needUpdateStatus := false;

  try
    info^.result := fGit.Any('branch ' + Command, cmdOut);
    if info^.result>0 then
      exit;

    needReflistUpdate := true;
    info^.refreshBranch := true;
    if not switch then
      exit;

    info^.result := fGit.Switch(branchName);
    if info^.result>0 then
      exit;

    // switched to the new branch, even if fetching from the
    // upstream is not requested, force an update status
    needUpdateStatus := true;
    if not fetch then
      exit;

    info^.result := fGit.Any('fetch', cmdOut);
    if info^.result>0 then
      exit;

  finally
    fObserverMgr.NotifyObservers(self, GITMGR_EVENT_NEWBRANCH, PtrInt(info));
    if needUpdateStatus then UpdateStatus;
    if needReflistUpdate then UpdateRefList;
  end;
end;

procedure TGitMgr.ForceTagDescription;
begin
  fDescribed := false;
end;

procedure TGitMgr.UpdateRemotes;
var
  cmdOut: RawByteString;
  L: TStringList;
  i, j: Integer;
  arr: TStringArray;
begin
  if fGit.Any('remote -v show', cmdOut)<=0 then begin
    fRemotes := nil;
    L := TStringList.Create;
    try
      L.Text := cmdOut;
      if L.Count div 2 > 0 then begin
        i := 0;
        while i<L.Count do begin

          j := Length(fRemotes);
          SetLength(fRemotes, j+1);

          arr := L[i].Split([' ', #9]);
          fRemotes[j].name := arr[0];
          fRemotes[j].fetch := arr[1];
          inc(i);

          arr := L[i].Split([' ', #9]);
          fRemotes[j].push := arr[1];
          inc(i);
        end;
      end;
    finally
      L.Free;
    end;
  end;
end;

function TGitMgr.FillRefList(list: TStrings; pattern: string;
  fields: array of string; keyField: string): Integer;
var
  cmd: String;
  i: Integer;
  M: TMemoryStream;
begin

  ClearRefList(list);

  M := TMemoryStream.Create;
  try

    // collect valid fields, mark unsupported fields
    cmd := '';
    for i:=0 to Length(fields)-1 do
      if RefListEnabledField(fields[i]) then
        cmd += fields[i] + '%02'
      else
        fields[i] := '';

    cmd := ' for-each-ref --format="' + cmd + '%00"';
    if pattern<>'' then
      cmd += ' ' + pattern;

    //DebugLn('Command: ', cmd);

    result := cmdLine.RunProcess(fGit.Exe + cmd, fGit.TopLevelDir, M);
    if result>0 then
      exit;
    //M.SaveToFile('lookatme.reflist');
    ParseRefList(M, list, #2, fields);
  finally
    M.Free;
  end;
end;

function TGitMgr.RefsFilter(commitOID: string; filter: TRefFilterProc
  ): TRefInfoArray;
var
  aIndex, i: Integer;
  arr: TRefInfoArray;
  info: PRefInfo;

  procedure AddInfo;
  var
    j: Integer;
  begin
    j := Length(result);
    SetLength(result, j+1);
    result[j] := info;
  end;

begin
  result := nil;
  if not Assigned(filter) then
    exit;
  // if commit is not empty and refsMap is loaded, use it
  if (commitOID<>'') and (fRefsMap<>nil) then begin
    if fRefsMap.Find(commitOID, aIndex ) then begin
      arr := fRefsMap.Data[aIndex];
      for info in arr do
        if Filter(info) then AddInfo
      //for i:=0 to Length(arr)-1 do begin
      //  info := arr[i];
      //  if Filter(info) then AddInfo;
      //end;
    end;
  end else
  // use the internalRefList if loaded
  if (fInternalRefList<>nil) then begin
    for i := 0 to fInternalRefList.Count-1 do begin
      info := PRefInfo(fInternalRefList.Objects[i]);
      if (commitOID='') or (commitOID=info^.objName) then begin
        if Filter(info) then AddInfo;
      end;
    end;
  end;

end;

procedure TGitMgr.QueueLoadRepository(aDir: string);
var
  info: PTagInfo;
begin
  new(info);
  info^.data := aDir;
  fObserverMgr.NotifyObservers(Self, GITMGR_EVENT_LOADREPOSITORY, PtrInt(info));
end;

function TGitMgr.SwitchTo(cmd: string): Integer;
begin
  result := fGit.Switch(cmd);
  if result<=0 then begin
    ForceTagDescription;
    UpdateStatus;
    UpdateRefList;
  end;
end;

function TGitMgr.TagTo(tagName, tagCommit: string; annotated: boolean;
  tagMsg: string): Integer;
begin
  result := fGit.Tag(tagName, tagCommit, annotated, tagMsg);
  if result<=0 then begin
    ForceTagDescription;
    UpdateStatus;
    UpdateRefList;
  end;
end;

function TGitMgr.GetStateCommitMsg(out msg: string): boolean;
begin
  result := fGitState.Merging and FileExists(fGit.GitDir + 'MERGE_MSG');
  if result then
    msg := GetFileContent(fGit.GitDir + 'MERGE_MSG', false);
end;

function TGitMgr.OpenRepository(aDir: string): boolean;
begin
  result := fGit.OpenDir(aDir)<=0;
  if result then begin
    UpdateStatus;
    UpdateRemotes;
  end;
end;

// If the repository is locked, return a filename relative to StartDir
function TGitMgr.GetRelativeFilename(aFilename: string): string;
begin
  if Locked then  result := copy(aFilename, Length(fGit.StartDir)+1, MAXINT)
  else            result := aFilename;
end;

function TGitMgr.GetCommit(commitOID: string; out item: TLogItem): Integer;
var
  M: TMemoryStream;
  cmd: string;
begin
  M := TMemoryStream.Create;
  try
    cmd := 'log ' + LOG_SINGLE_CMD + ' ' + commitOID;
    result := fGit.Any(cmd, M);
    if result<=0 then
      ParseLogItem(M.Memory, item);
  finally
    M.Free;
  end;
end;

function TGitMgr.GetFileStream(filetree: string): TStream;
begin
  result := TMemoryStream.Create;
  if fGit.Any('cat-file -p ' + filetree, result)>0 then
    FreeAndNil(result);
end;

function TGitMgr.ExtractToFile(filetree, dstFile: string;
  assumeTextContent: boolean): boolean;
var
  stream: TStream;
  L: TStringList;
  isWin: boolean;
begin
  isWin := {$ifdef MsWindows}true{$else}false{$endif};
  stream := GetFileStream(filetree);
  result := stream<>nil;
  if result then begin
    if isWin and assumeTextContent then begin
      stream.Position := 0;
      // we take this extra work here because git cat-file does not convert
      // line endings under windows, we have to do it ourselves.
      L := TStringList.Create;
      L.LoadFromStream(stream);
      L.SaveToFile(dstFile);
      L.Free;
    end else
      TMemoryStream(stream).SaveToFile(dstFile);
    stream.Free;
  end;
end;

function TGitMgr.GetRefInfo(aRef: string): PRefInfo;
var
  aIndex: Integer;
begin
  result := nil;

  if aRef='' then aRef := Branch;
  if aRef='' then exit;

  aIndex := fInternalRefList.IndexOf(aRef);
  if aIndex<0 then exit;

  result := PRefInfo(fInternalRefList.Objects[aIndex]);
end;

end.

