{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  Main unit
}
unit main;

{$mode objfpc}{$H+}
{$ModeSwitch nestedprocvars}

interface

uses
  Classes, SysUtils,  LCLIntf,
  LazLogger, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls, ActnList,
  SynEdit, StrUtils, FileUtil, Clipbrd, lclType, Menus, Buttons, ComCtrls,
  Types, unitgittypes, unitifaces, unitconfig, unitprocess, unitwidgetset,
  unithighlighterhelper, unitentries, unitgitutils, unitcommon, unitdebug,
  unitnewbranch, unitrunthread, unitruncmd, unitsyneditextras, unitnewtag, LConvEncoding,
  unitdbindex, unitgitmgr, unitcheckouttag, unitformlog, unitcustomcmds,
  unitcustcmdform, unittextchunks, unitgitcmd, unitpush, unitclone, unitremotes,
  unitrepovars, unitfilehistory, unitgitstate, unitframestateactions,
  unitformmgr, unitcommit, unitamend, unitrepobrowser, unitoutput,
  unitviewermgr, unitguitools, unitdifftool, unitdiffactions;

type

  { TfrmMain }

  TfrmMain = class(TForm, IObserver)
    actCommit: TAction;
    actFetch: TAction;
    actInsertBranchName: TAction;
    actAddCmd: TAction;
    actGitCmd: TAction;
    actClone: TAction;
    actRepoBrowser: TAction;
    actRemotes: TAction;
    actRepoInfo: TAction;
    actRestoreCommitMsg: TAction;
    actNewLog: TAction;
    actPushDialog: TAction;
    actQuit: TAction;
    actPull: TAction;
    actPush: TAction;
    actRescan: TAction;
    ActionList1: TActionList;
    btnNewLog: TSpeedButton;
    btnRescan: TButton;
    btnStageChanged: TButton;
    btnSignOff: TButton;
    btnCommit: TButton;
    btnPush: TButton;
    btnPushDlg: TButton;
    frmActions: TframeStateActions;
    imgList: TImageList;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    lblTag: TLabel;
    lblState: TLabel;
    lblRemote: TLabel;
    lblAheadBehind: TLabel;
    lblBranch: TLabel;
    lstUnstaged: TListBox;
    lstStaged: TListBox;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    mnuMain: TMainMenu;
    panCommitState: TPanel;
    panBranch: TPanel;
    panHolder: TPanel;
    panPush: TPanel;
    panStatus: TPanel;
    panFileState: TPanel;
    panUnstaged: TPanel;
    panStagedContainer: TPanel;
    panStaged: TPanel;
    popBranch: TPopupMenu;
    popLists: TPopupMenu;
    popCommands: TPopupMenu;
    prgBar: TProgressBar;
    btnRepoInfo: TSpeedButton;
    dlgSave: TSaveDialog;
    Separator1: TMenuItem;
    splitterMain: TSplitter;
    barCustomCmds: TToolBar;
    btnGitCmd: TToolButton;
    btnAddCustomCmd: TToolButton;
    toggleAmend: TToggleBox;
    txtComment: TMemo;
    panLeft: TPanel;
    panContent: TPanel;
    panCommit: TPanel;
    panCommitButtons: TPanel;
    splitterStaged: TSplitter;
    splitterCommit: TSplitter;
    txtDiff: TSynEdit;
    procedure actAddCmdExecute(Sender: TObject);
    procedure actCloneExecute(Sender: TObject);
    procedure actCommitExecute(Sender: TObject);
    procedure actFetchExecute(Sender: TObject);
    procedure actGitCmdExecute(Sender: TObject);
    procedure actInsertBranchNameExecute(Sender: TObject);
    procedure actNewLogExecute(Sender: TObject);
    procedure actPullExecute(Sender: TObject);
    procedure actPushDialogExecute(Sender: TObject);
    procedure actPushExecute(Sender: TObject);
    procedure actQuitExecute(Sender: TObject);
    procedure actRemotesExecute(Sender: TObject);
    procedure actRepoBrowserExecute(Sender: TObject);
    procedure actRepoInfoExecute(Sender: TObject);
    procedure actRescanExecute(Sender: TObject);
    procedure actRestoreCommitMsgExecute(Sender: TObject);
    procedure btnGitCmdArrowClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var {%H-}CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormDropFiles(Sender: TObject; const FileNames: array of string);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure lblBranchClick(Sender: TObject);
    procedure lblBranchContextPopup(Sender: TObject; {%H-}MousePos: TPoint;
      var {%H-}Handled: Boolean);
    procedure lblInfoOldClick(Sender: TObject);
    procedure lstUnstagedContextPopup(Sender: TObject; MousePos: TPoint;
      var {%H-}Handled: Boolean);
    procedure lstUnstagedDblClick(Sender: TObject);
    procedure lstUnstagedDrawItem(Control: TWinControl; Index: Integer;
      ARect: TRect; State: TOwnerDrawState);
    procedure lstUnstagedMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure toggleAmendClick(Sender: TObject);
    procedure txtDiffContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
  private
    fAmend: TAmend;
    fEnableDropFiles: Boolean;
    fGitMgr: TGitMgr;
    fGit: IGit;
    fClickedIndex: Integer;
    fPopPoint: TPoint;
    fListAlwaysDrawSelection: boolean;
    fhlHelper: THighlighterHelper;
    fCustomCommands: TCustomCommandsMgr;
    fViewerMgr: TViewerMgr;
    fDiffActions: TDiffActions;
    function  CreatePatchFromListbox(const lb: TListbox): TStringList;
    procedure DelayedShowMenu({%H-}Data: PtrInt);
    procedure DoGitDiff(Data: PtrInt);
    procedure DoItemAction(Data: PtrInt);
    procedure DoCommit;
    procedure DoClone;
    procedure DoNewLog;
    procedure DoPush;
    function  DoPushDialog(withUpstream:boolean): TModalResult;
    procedure DoFetch;
    procedure DoPull;
    procedure DoRepoInfo;
    function  DoRemotes: TModalResult;
    procedure DoRepoBrowser;
    procedure FrameEditorQuitHandler(Data: PtrInt);
    procedure OnCreatePatchClick(Sender: TObject);
    procedure OnCopyPatchClick(Sender: TObject);
    procedure OnCustomCommandClick(Sender: TObject);
    procedure OnDebugLnInterceptor(Sender: TObject; S: string;
      var Handled: Boolean);
    procedure OnDiffUnstagedInExternalViewer(Sender: TObject);
    procedure OnFrameActionsError(Sender: TObject);
    procedure OnFrameEditorQuit(Sender: TObject);
    procedure OnIdleMe(Sender: TObject; var Done: Boolean);
    procedure OnMRECommand(Sender: TObject);
    procedure OnPopupItemClick(Sender: TObject);
    procedure OnBranchSwitch(Data: PtrInt);
    procedure OnIgnoreFileClick(Sender: TObject);
    procedure OnIgnoreTypeClick(Sender: TObject);
    procedure OnReloadBranchMenu({%H-}Data: PtrInt);
    procedure OnRepoInfoStatusDone(Sender: TObject);
    procedure OnRestoreFileClick(Sender: TObject);
    procedure OnDeleteFilesClick(Sender: TObject);
    procedure OnShowFileHistoryUnstaged(Sender: TObject);
    procedure OnShowFileHistoryStaged(Sender: TObject);
    procedure OnEditFile(Sender: TObject);
    procedure OnCustomEditFile(sender: TObject);
    procedure OnSystemEditFile(sender: TObject);
    procedure OnStageAllClick(Sender: TObject);
    procedure OnStageItemClick(Sender: TObject);
    procedure OnUnstageItemClick(Sender: TObject);
    function  OpenDirectory(aDir: string): boolean;
    procedure UpdateBranch;
    procedure RestoreGui;
    procedure SaveGui;
    procedure ItemAction(sender: TListbox; aIndex: Integer);
    procedure UpdateBranchMenu;
    procedure ShowError;
    procedure ViewFile(filename: string);
    procedure ComingSoon;
    function  MakeMenuItemUnstagedEntryArray(mi: TMenuItem): TPFileEntryArray;
    function  MakeMenuItemStagedEntryArray(mi: TMenuItem): TPFileEntryArray;
    procedure NewBranch;
    procedure RenameBranch;
    procedure InvalidateBranchMenu;
    procedure NewTag;
    procedure CheckMenuDivisorInLastPosition(pop:TPopupMenu);
    procedure ObservedChanged(Sender:TObject; what: Integer; data: PtrInt);
    procedure SaveCommitMessage;
    procedure RestoreCommitMessage;
    procedure UpdateCommandsBar;
    procedure OpenInternalEditor(aFile: string);
    function  GetListEntry(lb: TListBox; aIndex: Integer; out entry:PFileEntry): TEntryTypeSource;
    function  GetListEntryInfo(lb: TListBox; aIndex: Integer; out entry:PFileEntry; out info:string): TEntryTypeSource;
    procedure AddBranchWorkTree;
    procedure RemoveBranchWorkTree;
  public

  end;

  procedure CloneLauncher(sender:TObject);

var
  frmMain: TfrmMain;
  targetDir: string = '';

implementation

{$R *.lfm}

const
  MENU_INVALID                = -1;

  MENU_BRANCH_NEW             = 1;
  MENU_BRANCH_RELOAD          = 2;
  MENU_BRANCH_SWITCH          = 3;
  MENU_BRANCH_NEW_TAG         = 4;
  MENU_BRANCH_RENAME          = 5;
  MENU_BRANCH_ADD_WORKTREE    = 6;
  MENU_BRANCH_REMOVE_WORKTREE = 7;

  MENU_LIST_VIEW_UNTRACKED    = 14;
  MENU_LIST_VIEW_IGNORED      = 15;
  MENU_LIST_VIEW_TRACKED      = 16;
  MENU_LIST_STAGE_CHANGED     = 17;
  MENU_LIST_STAGE_ALL         = 18;
  MENU_LIST_UNSTAGE_ALL       = 19;

  LIST_TAG_ALL_SELECTED       = -1;
  LIST_TAG_ALL_CHANGED        = -2;
  LIST_TAG_ALL                = -3;

function GetSelectedIndex(lb: TListbox): Integer;
var
  i: Integer;
begin
  result := -1;
  for i:=0 to lb.Count-1 do
    if lb.Selected[i] then begin
      result := i;
      break;
    end;
end;

function CountListItemsOfEntryType(lb: TListbox; unstaged, onlySel:boolean; entrySet: TSetOfEntryType): Integer;
var
  Entry: PFileEntry;
  i: Integer;
begin
  result := 0;
  for i:=0 to lb.Count-1 do
    if (not OnlySel) or lb.Selected[i] then begin
      Entry := PFileEntry(lb.Items.Objects[i]);
      if (Entry<>nil) then begin
        if (unstaged and (Entry^.EntryTypeUnStaged in entrySet)) or
           (not unstaged and (Entry^.EntryTypeStaged in entrySet)) then
        inc(result);
      end;
    end;
end;

function AreAllSelectedItemsOfEntryType(lb: TListbox; unstaged:boolean; entrySet: TSetOfEntryType): boolean;
var
  n: Integer;
begin
  n := CountListItemsOfEntryType(lb, unstaged, true, entrySet);
  result := (n>0) and (n=lb.SelCount);
end;

function FirstSelectedItemExcluding(lb: TListBox; unstaged:boolean; entryset: TSetOfEntryType): Integer;
var
  i: Integer;
  Entry: PFileEntry;
  inSet: Boolean;
begin
  result := -1;
  for i:=0 to lb.Count-1 do
    if lb.Selected[i] then begin
      Entry := PFileEntry(lb.Items.Objects[i]);
      if (Entry<>nil) then begin
        inSet := (unstaged and (Entry^.EntryTypeUnStaged in entrySet)) or
               (not unstaged and (Entry^.EntryTypeStaged in entrySet));
        if not inSet then begin
          result := i;
          break;
        end;
      end;
    end;
end;

procedure ChangeItemSelectionOfEntryType(lb: TListbox; unstaged:boolean; unselect:boolean; entryset: TSetOfEntryType);
var
  i: Integer;
  Entry: PFileEntry;
  inSet: boolean;
begin
  for i:=0 to lb.Count-1 do begin
    Entry := PFileEntry(lb.Items.Objects[i]);
    if (Entry<>nil) then begin
      inSet := (unstaged and (Entry^.EntryTypeUnStaged in entrySet)) or
             (not unstaged and (Entry^.EntryTypeStaged in entrySet));
      if inSet then
        lb.Selected[i] := not unselect
    end;
  end;
end;

procedure CloneLauncher(sender: TObject);
var
  formMgr: TformMgr absolute sender;
  F: TfrmClone;
  launchit: boolean;
  aDir: string;
begin
  F := TfrmClone.Create(Application);
  F.GitMgr := formMgr.GitMgr;
  F.LoadRepositoryEnabled := false;
  if targetDir<>'' then begin
    aDir := ExpandFileName(targetDir);
    if DirectoryExists(aDir) then
      F.CloneDir := aDir;
  end;
  try
    launchIt := (F.ShowModal=mrOk) and F.chkLoad.Checked;
    if launchIt then
      targetDir := F.CloneDir + F.RepoName;
  finally
    F.Free;
  end;

  // formMgr is of no use now
  formMgr.Free;

  if launchIt then begin
    // even when there are no more forms to show (TfrmClone has by now been finished)
    // Application.CreateForm(TfrmMain, frmMain) fails to recognize frmMain as a
    // main form, is this a bug?
    // Launch it as normal form...
    frmMain := TfrmMain.Create(Application);
    try
      frmMain.ShowModal;
    finally
      FreeAndNil(frmMain);
    end;
  end;

  PreserveClipboard;

  Application.Terminate;

end;

{ TfrmMain }

procedure TfrmMain.FormShow(Sender: TObject);
begin
  if not OpenDirectory(targetDir) then begin
    Application.Terminate;
    exit;
  end;
  fTextLinks.LoadFromConfig(fGit.TopLevelDir);
  UpdateCommandsBar;
  Application.AddOnIdleHandler(@OnIdleMe);
end;

procedure TfrmMain.lblBranchClick(Sender: TObject);
begin
  if popBranch.Items.Count=0 then
    UpdateBranchMenu;
  popBranch.PopUp;
end;

procedure TfrmMain.OnPopupItemClick(Sender: TObject);
var
  mi: TMenuItem;
begin
  mi := TMenuItem(sender);

  case mi.tag of
    MENU_BRANCH_NEW:
      NewBranch;

    MENU_BRANCH_RENAME:
      RenameBranch;

    MENU_BRANCH_NEW_TAG:
      NewTag;

    MENU_BRANCH_RELOAD:
      begin
        fPopPoint := popBranch.PopupPoint;
        Application.QueueAsyncCall(@OnReloadBranchMenu, 0);
      end;

    MENU_BRANCH_SWITCH:
      begin
        Application.QueueAsyncCall(@OnBranchSwitch, PtrInt(Sender));
      end;

    MENU_LIST_VIEW_UNTRACKED:
      begin
        fGitMgr.ViewUntrackedFiles := mi.checked;
        fConfig.ViewUntrackedFiles := fGitMgr.ViewUntrackedFiles;
        fGitMgr.UpdateStatus;
      end;

    MENU_LIST_VIEW_IGNORED:
      begin
        fGitMgr.ViewIgnoredFiles := mi.checked;
        fConfig.ViewIgnoredFiles := fGitMgr.ViewIgnoredFiles;
        fGitMgr.UpdateStatus;
      end;

    MENU_BRANCH_ADD_WORKTREE:
      AddBranchWorkTree;

    MENU_BRANCH_REMOVE_WORKTREE:
      RemoveBranchWorkTree;

    //MENU_LIST_VIEW_TRACKED:
    //  begin
    //    fGitMgr.ViewTrackedFiles := mi.Checked;
    //    fConfig.ViewTrackedFiles := fGitMgr.ViewTrackedFiles;
    //    fGitMgr.UpdateStatus;
    //  end;

    else
      ComingSoon;
  end;
end;

procedure TfrmMain.OnBranchSwitch(Data: PtrInt);
var
  mi: TMenuItem;
begin
  mi := TMenuItem(TObject(Data));
  if fGitMgr.SwitchTo(mi.Caption)<=0 then
    InvalidateBranchMenu
  else
    ShowError;
end;

procedure TfrmMain.OnIgnoreFileClick(Sender: TObject);
var
  ignored: string;
  mi: TMenuItem absolute Sender;
begin
  // what file?
  ignored := lstUnstaged.Items[mi.Tag];
  if fGitMgr.AddToIgnoreFile(ignored, false) then
    fGitMgr.UpdateStatus
  else
    ShowMessage(Format(rsSIsAlreadyInIgnoredList, [ignored]));
end;

procedure TfrmMain.OnIgnoreTypeClick(Sender: TObject);
var
  ignored: string;
  mi: TMenuItem absolute Sender;
begin
  // what file?
  ignored := lstUnstaged.Items[mi.Tag];
  if fGitMgr.AddToIgnoreFile(ignored, true) then
    fGitMgr.UpdateStatus
  else
    ShowMessage(Format(rsTheTypeSIsAlreadyInTheIgnoredList, [ExtractFileExt(ignored)]));
end;

procedure TfrmMain.OnReloadBranchMenu(Data: PtrInt);
begin
  UpdateBranchMenu;
  popBranch.PopUp(fPopPoint.x, fPopPoint.y);
end;

procedure TfrmMain.OnRepoInfoStatusDone(Sender: TObject);
  function FilterLocal(info: PRefInfo): boolean;
  begin
    result := info^.subType=rostLocal;
  end;
  function FilterTracking(info: PRefInfo): boolean;
  begin
    result := info^.subType=rostTracking;
  end;
  function FilterTags(info: PRefInfo): boolean;
  begin
    result := info^.subType=rostTag;
  end;

var
  i: Integer;
  s: string;
  log: TLazLoggerFile;
  refItems: TRefInfoArray;
  cmdOut: RawByteString;
  entry: PFileEntry;
begin
  log := DebugLogger;
  fGitMgr.UpdateRefList;
  fGitMgr.UpdateRemotes;
  txtDiff.Clear;
  Log.OnDebugLn := @OnDebugLnInterceptor;
  try
    DebugLnEnter('Repository Information:');
    DebugLn('Date: %s', [DateTimeToStr(Now)]);
    DebugLn('Top Level Directory: %s', [fGit.TopLevelDir]);
    DebugLn('Repo .git Directory: %s', [fGit.GitDir]);
    DebugLn('Git program: %s', [fGit.Exe]);
    DebugLn('Git version: %s', [fGit.Version]);
    DebugLnExit('');
    DebugLnEnter('Status:');
    DebugLn('Branch: %s',[fGitMgr.Branch]);
    DebugLn('Tracking Branch: %s',[fGitMgr.Upstream]);
    DebugLn('Commits Ahead: %d',[fGitMgr.CommitsAhead]);
    DebugLn('Commits Behind: %d',[Abs(fGitMgr.CommitsBehind)]);
    DebugLn('Branch Commit Sha: %s',[fGitMgr.BranchOID]);
    DebugLn('Tag: %s',[fGitMgr.LastTag]);
    DebugLn('Commits Since Tag %s: %d',[fGitMgr.LastTag, fGitMgr.LastTagCommits]);
    DebugLn('Tag Commit Sha: %s',[fGitMgr.LastTagOID]);
    DebugLn('State: %s', [fGitMgr.GitState.AsString]);
    DebugLnExit('');
    DebugLnEnter('Unstaged Area');
    DebugLn('%d files',[fGitMgr.UnstagedList.Count]);
    for i:=0 to fGitMgr.UnstagedList.Count-1 do begin
      entry := PFileEntry(fGitMgr.UnstagedList.Objects[i]);
      DebugLn('%-40s %s', [
        EntryTypeToStr(entry^.x, entry^.y),
        BoolToStr(entry^.OrigPath<>'', entry^.Path+' -> '+entry^.OrigPath, entry^.Path)
        ]);
    end;
    DebugLnExit('');
    DebugLnEnter('Staged Area');
    DebugLn('%d files',[fGitMgr.StagedList.Count]);
    for i:=0 to fGitMgr.StagedList.Count-1 do begin
      entry := PFileEntry(fGitMgr.StagedList.Objects[i]);
      DebugLn('%-40s %s', [
        EntryTypeToStr(entry^.x, entry^.y),
        BoolToStr(entry^.OrigPath<>'', entry^.Path+' -> '+entry^.OrigPath, entry^.Path)
        ]);
    end;
    DebugLnExit('');

    DebugLnEnter('Local Branches:');
    refItems := fGitMgr.RefsFilter('', @FilterLocal);
    for i := 0 to Length(refItems)-1 do
    with refItems[i]^ do begin
      s := '';
      if Head then s := ' (HEAD)';
      if upstream<>'' then  DebugLn('%s %s -> %s%s', [objName, refName, upstream, s])
      else                  DebugLn('%s %s%s',[objName, refName, s]);
    end;
    DebugLnExit('');
    DebugLnEnter('Tracking branches:');
    refItems := fGitMgr.RefsFilter('', @FilterTracking);
    for i := 0 to Length(refItems)-1 do
    with refItems[i]^ do
      DebugLn('%s %s',[objName, refName]);
    DebugLnExit('');
    DebugLnEnter('Tags:');
    refItems := fGitMgr.RefsFilter('', @FilterTags);
    for i := 0 to Length(refItems)-1 do
    with refItems[i]^ do
      DebugLn('%s %s',[objName, refName]);
    DebugLnExit('');
    DebugLnEnter('Remotes:');
    for i := 0 to Length(fGitMgr.Remotes)-1 do
      DebugLn('%s %s', [fGitMgr.Remotes[i].name, fGitMgr.Remotes[i].fetch]);
    DebugLnExit('');
    if gblReportLooseObjects then begin
      DebugLnEnter('Loose Objects:');
      if fGit.Any('count-objects -v -H', cmdOut)<=0 then DebugLnMultiline(cmdOut)
      else                                               DebugLnMultiline(fGit.LogError);
      DebugLnExit('');
    end;
  finally
    Log.OnDebugLn := nil;
  end;
end;

procedure TfrmMain.OnRestoreFileClick(Sender: TObject);
var
  mi: TMenuItem;
  entryArray: TPFileEntryArray;
  aFile: String;
  res: TModalResult;
begin
  mi := TMenuItem(Sender);
  entryArray := MakeMenuItemUnstagedEntryArray(mi);

  if Length(entryArray)=1 then
    aFile := MakePathList(entryArray, false)
  else
    aFile := format(rsDFiles, [Length(entryArray)]);

  // this is necessary because we normally hide lists selection
  // when they are unfocused, but in this case the next dialog
  // will unfocus the lists and we want to see the selection of
  // files that will be restored.
  fListAlwaysDrawSelection := true;

  res := QuestionDlg(rsRestoringWorkFiles, format(rsRestoringWorkFilesWarning, [aFile]), mtWarning,
    [mrYes, rsDiscardChanges, mrCancel, rsCancel], 0 );

  fListAlwaysDrawSelection := false;

  if res<>mrYes then
    exit;

  {$IFDEF DEBUG}
  aFile := MakePathList(entryArray);
  DebugLn('Restoring: ',aFile);
  {$ENDIF}

  fGit.ResetLogError;
  if fGit.Restore(entryArray, false)<=0 then
    fGitMgr.UpdateStatus;
  txtDiff.Text := fGit.LogError;
end;

procedure TfrmMain.OnDeleteFilesClick(Sender: TObject);
var
  mi: TMenuItem absolute sender;
  aFile: string;
  res: TModalResult;
  i: Integer;
  ok, someDeleted: boolean;
begin

  if mi.Tag>=0 then
    aFile := lstUnstaged.Items[mi.Tag]
  else
    aFile := format(rsDFiles, [lstUnstaged.SelCount]);

  res := QuestionDlg(rsDeletingWorkFiles, format(rsDeletingWorkFilesWarning, [aFile]), mtWarning,
    [mrYes, rsDeleteFiles, mrCancel, rsCancel], 0 );

  if res<>mrYes then
    exit;

  if mi.Tag>=0 then begin
    ok := DeleteFile(fGit.TopLevelDir + aFile);
    someDeleted := ok;
  end else begin
    someDeleted := false;
    for i := 0 to lstUnstaged.Count-1 do
      if lstUnstaged.Selected[i] then begin
        ok := DeleteFile(fGit.TopLevelDir + lstUnstaged.Items[i]);
        someDeleted := someDeleted or ok;
        if not ok then
          break;
      end;
  end;

  if not ok then
    ShowMessage(rsSomeFilesCouldNotBeDeleted);

  if someDeleted then
    fGitMgr.UpdateStatus;
end;

procedure TfrmMain.OnShowFileHistoryUnstaged(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  aFile: string;
begin
  aFile := lstUnStaged.Items[mi.Tag];
  ShowFileHistory(aFile, fGitMgr, fhlHelper);
end;

procedure TfrmMain.OnShowFileHistoryStaged(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  aFile: string;
begin
  aFile := lstStaged.Items[mi.Tag];
  ShowFileHistory(aFile, fGitMgr, fhlHelper);
end;


procedure TfrmMain.OnEditFile(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
begin
  OpenInternalEditor(fGit.TopLevelDir + lstUnStaged.Items[mi.Tag]);
end;

procedure TfrmMain.OnCustomEditFile(sender: TObject);
var
  mi: TMenuItem absolute Sender;
  aFile, cEditor, cmd, args: string;
  cmdOut: RawByteString;
begin
  aFile := fGit.TopLevelDir + lstUnStaged.Items[mi.Tag];

  cmd := fConfig.ReadString('command', '', 'editor');
  if cmd='' then
    raise Exception.Create('No custom editor has beed defined');

  cEditor := fConfig.ReadString('name', '', 'editor');
  args := fConfig.ReadString('args', '', 'editor');

  if args<>'' then begin
    if not args.EndsWith('=') then
      args += ' ';
    args := ' ' + args;
  end else
    args := ' ';

  cmd += args + Sanitize(aFile);

  RunCommand(cmd, '', cmdOut, true);
end;

procedure TfrmMain.OnSystemEditFile(sender: TObject);
var
  mi: TMenuItem absolute Sender;
  aFile: string;
begin
  aFile := fGit.TopLevelDir + lstUnStaged.Items[mi.Tag];
  OpenDocument(aFile);
end;

procedure TfrmMain.OnStageAllClick(Sender: TObject);
var
  mi: TMenuItem;
  cmd, cmdOut: RawByteString;
  entryArray: TPFileEntryArray;
  entry: PFileEntry;
begin
  cmd := '';
  mi := TMenuItem(sender);
  case mi.tag of
    MENU_LIST_STAGE_CHANGED:  cmd := 'add -u';
    MENU_LIST_STAGE_ALL:      cmd := 'add -A';
    MENU_LIST_UNSTAGE_ALL:    cmd := 'reset';
  end;

  if mi.tag=MENU_LIST_STAGE_ALL then begin
    mi.Tag := LIST_TAG_ALL;
    entryArray := MakeMenuItemUnstagedEntryArray(mi);
    for entry in entryArray do
      if entry^.EntryKind=ekIgnored then begin
        cmd += ' -f';
        break;
      end;
  end;

  if cmd<>'' then begin
    fGit.ResetLogError;
    if fGit.Any(cmd, cmdOut)<=0 then
      fGitMgr.UpdateStatus;
    txtDiff.Text := fGit.LogError;
  end;
end;

procedure TfrmMain.OnStageItemClick(Sender: TObject);
var
  mi: TMenuItem;
  entryArray: TPFileEntryArray;
begin
  mi := TMenuItem(Sender);
  entryArray := MakeMenuItemUnstagedEntryArray(mi);

  fGit.ResetLogError;
  if fGit.Add(entryArray)<=0 then
    fGitMgr.UpdateStatus;
  txtDiff.Text := fGit.LogError;
end;

procedure TfrmMain.OnUnstageItemClick(Sender: TObject);
var
  mi: TMenuItem;
  entryArray: TPFileEntryArray;
begin
  mi := TMenuItem(Sender);
  entryArray := MakeMenuItemStagedEntryArray(mi);

  fGit.ResetLogError;
  if fGit.Restore(entryArray, true)<=0 then
    fGitMgr.UpdateStatus;
  txtDiff.Text := fGit.LogError;
end;

procedure TfrmMain.lblBranchContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin

  if popBranch.Items.Count>0 then
    exit;

  UpdateBranchMenu;
end;

procedure TfrmMain.lblInfoOldClick(Sender: TObject);
begin
  //fLogCache.NotifyMe;
end;

procedure TfrmMain.lstUnstagedContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
var
  lb: TListBox absolute Sender;
  mi: TMenuItem;
  entry: PFileEntry;
  isUnstaged: Boolean;
  aIndex, selCount: Integer;
  aFile: string;
  entrySrc: TEntryTypeSource;

  procedure AddViewItems;
  begin
    if isUnstaged then begin
      mi := AddPopItem(popLists, rsViewUntrackedFiles, @OnPopupItemClick, MENU_LIST_VIEW_UNTRACKED);
      mi.AutoCheck := true;
      mi.Checked := fConfig.ViewUntrackedFiles;
      mi := AddPopItem(popLists, rsViewIgnoredFiles, @OnPopupItemClick, MENU_LIST_VIEW_IGNORED);
      mi.AutoCheck := true;
      mi.Checked := fConfig.ViewIgnoredFiles;
      mi := AddPopItem(popLists, rsViewTrackedFiles, @OnPopupItemClick, MENU_LIST_VIEW_TRACKED);
      mi.AutoCheck := true;
      mi.Checked := fConfig.ViewTrackedFiles;
      mi.Enabled := false;
    end;
  end;

  procedure AddFileHistory;
  var
    aCaption: string;
  begin
    if entrySrc=etsAmend then exit;
    AddPopItem(popLists, '-', nil, 0);
    aCaption := format(rsShowFileHistoryOfS, [QuotedStr(aFile)]);
    if isUnstaged then AddPopItem(popLists, aCaption, @OnShowFileHistoryUnstaged, aIndex)
    else               AddPopItem(popLists, aCaption, @OnShowFileHistoryStaged, aIndex);
  end;

  procedure AddEditFile;
  var
    cEditor: String;
    vmflags: TViewerMgrFlags;
  begin
    if isUnstaged and (aIndex>=0) then begin
      AddPopItem(popLists, '-', nil, 0);
      AddPopItem(popLists, format('Open %s', [QuotedStr(aFile)]), @OnSystemEditFile, aIndex);

      cEditor := fGit.TopLevelDir + lstUnstaged.Items[aIndex];
      vmflags := fViewerMgr.Analyze(cEditor);
      if vmfCanEdit in vmflags then
        AddPopItem(popLists, format('Edit %s with the internal editor', [QuotedStr(aFile)]), @OnEditFile, aIndex);
      if vmfIsText in vmFlags then begin
        cEditor := fConfig.ReadString('name', '', 'editor');
        if (cEditor<>'') then
          AddPopItem(popLists, format('Edit %s with %s', [QuotedStr(aFile), QuotedStr(cEditor)]), @OnCustomEditFile, aIndex);
        // TODO: Add Open With ...
      end;
    end;
  end;

  procedure AddStageFile;
  begin
    AddPopItem(popLists, format(rsStageS, [aFile]), @OnStageItemClick, aIndex);
  end;

  procedure AddUnstageFiles;
  begin
    if entrySrc=etsAmend then exit;
    AddPopItem(popLists, '-', nil, 0);
    if aIndex>=0 then
      AddPopItem(popLists, format(rsUnstageS, [aFile]), @OnUnstageItemClick, aIndex);
    AddPopItem(popLists, rsUnstageAll, @OnStageAllClick, MENU_LIST_UNSTAGE_ALL);
  end;

  procedure AddStageAll;
  begin
    AddPopItem(popLists, rsStageChanged, @OnStageAllClick, MENU_LIST_STAGE_CHANGED);
    AddPopItem(popLists, rsStageAll, @OnStageAllClick, MENU_LIST_STAGE_ALL);
  end;

  procedure AddIgnoreUntracked;
  var
    ext: string;
  begin
    AddPopItem(popLists, '-', nil, 0);
    AddPopItem(popLists, format(rsAddSToIgnoreList, [aFile]), @OnIgnoreFileClick, aIndex);
    ext :=ExtractFileExt(aFile);
    if ext<>'' then
      AddPopItem(popLists, format(rsAddSFilesToIgnoreList, [ext]), @OnIgnoreTypeClick, aIndex);
  end;

  procedure AddRestoreFiles;
  var
    n: Integer;
  begin
    if isUnstaged then begin
      AddPopItem(popLists, '-', nil, 0);
      // this covers the one (index>=0) or all selected (index<0)
      if ((aIndex>=0)and(entry^.EntryTypeUnStaged in ChangedInWorktreeSet)) or
         AreAllSelectedItemsOfEntryType(lstUnstaged, true, ChangedInWorktreeSet)
      then
        AddPopItem(popLists, format(rsRestoreS, [aFile]), @OnRestoreFileClick, aIndex);
      // we need an option for all changed
      n := CountListItemsOfEntryType(lstUnstaged, true, false, ChangedInWorktreeSet);
      if n > 0 then
        AddPopItem(popLists, rsRestoreAllChanged, @OnRestoreFileClick, LIST_TAG_ALL_CHANGED);
    end;
  end;

  procedure CheckWhatToProcess;
  begin
    if selCount=1 then begin
      aFile := ExtractFileName(lb.GetSelectedText);
      aIndex := GetSelectedIndex(lb);
      entrySrc := GetListEntry(lb, lb.ItemIndex, entry);
    end else begin
      aFile := format(rsDFiles, [selCount]);
      aIndex := LIST_TAG_ALL_SELECTED;
      entry := nil;
      entrySrc := etsUnknown;
    end;
  end;

  procedure AddDeleteFiles;
  var
    safeCount: Integer;
    n, i: Integer;
    entrySet: TSetOfEntryType;
  begin
    if isUnstaged and (SelCount>0) then begin
      // check if there is only (in the index) already deleted files
      entrySet := [etUntracked];
      if gblAllowDeleteChanged then
        entrySet := entrySet + ChangedInWorktreeSet - DeletedInWorktreeSet;
      safeCount := CountListItemsOfEntryType(lstUnstaged, true, true, entrySet);
      if SelCount=safeCount then begin
        AddPopItem(popLists, '-', nil, 0);
        AddPopItem(popLists, format(rsDeleteS, [aFile]), @OnDeleteFilesClick, aIndex)
      end;
      //else begin
      //  n := SelCount - safeCount;
      //  if n=1 then
      //    AddPopItem(popLists, format('Delete ''%s'' denied. Is changed', [aFile]), nil, aIndex)
      //  else
      //    AddPopItem(popLists, format('Delete ''%s'' denied. %d Are changed', [aFile, n]), nil, aIndex);
      //end;
    end;
  end;

  procedure AddCreateAPatch;
  begin
    if (SelCount>0) and (entrySrc<>etsAmend) then begin
      AddPopItem(popLists, '-', nil, 0);
      AddPopItem(popLists, format(rsCreateAPatchFileFromS, [aFile]), @OnCreatePatchClick, PtrInt(lb));
      AddPopItem(popLists, format(rsCopyPatchFromSToTheClipboard, [aFile]), @OnCopyPatchClick, PtrInt(lb));
    end;
  end;

  procedure AddDiffUnstagedInExternalViewer;
  begin
    if (entrySrc=etsUnstaged) and (entry^.EntryKind=ekOrdinaryChanged) and (aIndex>=0) then begin
      // AddMenu will create a menu item with OnDiffUnstagedInExternalViewer as a handler,
      // that handler will be called with a menuitem with a tag pointing to the selected
      // diffTool index, so how do we know what was the list item triggering the action?
      // a new variable fLastSelectedIndex could be introduced, but having all those
      // controls with unused tags, just use one of them ....
      // TODO: find a better way
      lstUnstaged.Tag := aIndex;
      AddDiffToolsMenu(popLists, @OnDiffUnstagedInExternalViewer, format('Diff %s Side to Side',[aFile]));
    end;
  end;

begin

  isUnstaged := Sender=lstUnstaged;
  aIndex := lb.GetIndexAtXY(MousePos.x, MousePos.y);
  if aIndex>=0 then begin
    aFile := ExtractFileName(lb.Items[aIndex]);
    entrySrc := GetListEntry(lb, aIndex, entry);
  end else begin
    aFile := '';
    entry := nil;
    aIndex := LIST_TAG_ALL_SELECTED;
    entrySrc := etsUnknown;
  end;

  popLists.Items.Clear;
  selCount := lb.SelCount;

  mi := AddPopItem(popLists, '', nil, 0);
  mi.Action := actRescan;

  AddDiffUnstagedInExternalViewer;

  AddPopItem(popLists, '-', nil, 0);

  if selCount=0 then begin

    if entry<>nil then begin
      if isUnstaged then begin
        AddStageFile;
        AddStageAll;
      end else
        AddUnstageFiles;
      AddRestoreFiles;
      if entry^.EntryKind in [ekUntracked, ekIgnored] then
        AddIgnoreUntracked
      else if entry^.EntryKind in [ekOrdinaryChanged] then
        AddFileHistory;
      AddPopItem(popLists, '-', nil, 0);
    end else
    if lb.Count>0 then begin
      if isUnstaged then begin
        AddStageAll;
        AddRestoreFiles;
        AddPopItem(popLists, '-', nil, 0);
      end else
        AddUnstageFiles
    end;

    AddViewItems;

  end else begin

    CheckWhatToProcess;

    if isUnstaged then begin
      AddStageFile;
      AddStageAll;
      AddRestoreFiles;
      AddDeleteFiles;
      AddEditFile;
      AddCreateAPatch;

      if (entry<>nil) and (entry^.EntryKind in [ekUntracked, ekIgnored]) then
        AddIgnoreUntracked
      else if (SelCount=1) and (entry^.EntryKind in [ekOrdinaryChanged]) then
        AddFileHistory;

      AddPopItem(popLists, '-', nil, 0);
      AddViewItems;
    end else begin
      AddCreateAPatch;
      AddUnstageFiles;
      if SelCount=1 then
        AddFileHistory;
    end;

  end;

  CheckMenuDivisorInLastPosition(popLists);

end;

procedure TfrmMain.lstUnstagedDblClick(Sender: TObject);
var
  lb: TListBox;
  entry: PFileEntry;
  entrySrc: TEntryTypeSource;
  aIndex: Integer;
  dtl: TDiffToolLauncherRec;
  cmdOut: RawByteString;
begin

  lb := TListBox(Sender);
  aIndex := lb.ItemIndex;
  if aIndex<0 then
    exit;

  entrySrc := GetListEntry(lb, aIndex, entry);
  if (entrySrc<>etsUnstaged) or (entry=nil) or (entry^.EntryKind<>ekOrdinaryChanged) then
    exit;

  if gblLastDiffToolIndex<0 then begin
    ShowMessage('No DiffTool has been selected yet!');
    exit;
  end;

  initialize(dtl);

  dtl.toolIndex := gblLastDiffToolIndex;
  dtl.gitMgr := fGitMgr;
  dtl.base.filename := ExtractFileName(entry^.path);
  dtl.base.tag := fGitMgr.BranchOID;
  dtl.base.tree := entry^.h1; // hash in index
  dtl.base.meta := rsBase;
  dtl.comp.filename := fGit.TopLevelDir + entry^.path;
  dtl.comp.meta := rsWorkingTree;

  LaunchDiffTool(dtl, cmdOut);
end;

procedure TfrmMain.ItemAction(sender: TListbox; aIndex: Integer);
var
  entry: PFileEntry;
  cmdOut: RawByteString;
  theAction: (taAdd, taRm, taReset);
  res: Integer;
  src: TEntryTypeSource;
begin
  src := GetListEntry(Sender, aIndex, entry);
  if sender=lstUnstaged then begin
    case entry^.EntryTypeUnStaged of
      etUntracked, etIgnored, etNotUpdatedA,
      etWorktreeChangedSinceIndex..etTypeChangedInWorktreeSinceIndexC:
        theAction := taAdd;

      etDeletedInWorktree,
      // automatic resolution of merge conflicts:
      etUnmergedbothDeleted,
      etUnmergedDeletedByThem:
        theAction := taRm;

      etUnmergedAddedByThem,
      etUnmergedBothAdded:
        theAction := taAdd;

      // other merge conflicts:
      //etUnmergedAddedByUs,
      //etUnmergedDeletedByUs,
      //etUnmergedBothModified,
      else begin
        WriteStr(cmdOut, entry^.EntryTypeUnStaged);
        ShowMessageFmt(rsNotYetImplementedForUnstagedS, [cmdOut]);
        exit;
      end;
    end;
  end else begin

    if src=etsAmend then begin
      ShowMessage('This action is not yet implemented');
      exit;
    end;

    case entry^.EntryTypeStaged of
      etAddedToIndex..etAddedToIndexD,
      etUpdatedInIndex..etTypeChangedInIndexD,
      etRenamedInIndex..etRenamedInIndexD,
      etDeletedFromIndex:
        theAction := taReset;

      else begin
        WriteStr(cmdOut, entry^.EntryTypeStaged);
        ShowMessageFmt(rsNotYetImplementedForStagedS, [cmdOut]);
        exit;
      end;
    end;
  end;

  fGit.ResetLogError;

  case theAction of
    taAdd:    res := fGit.Add(entry);
    taRm:     res := fGit.Rm(entry);
    taReset:  res := fGit.Restore(entry, sender=lstStaged);
  end;

  if res<=0 then fGitMgr.UpdateStatus
  else           txtDiff.Text := fGit.LogError;

end;

procedure TfrmMain.UpdateBranchMenu;
var
  list, branchLine: TStringList;
  i: integer;
  mi: TMenuItem;
  ref: PRefInfo;
  aState: TGitState;

  function FilterLocalBranches(info: PRefInfo): boolean;
  begin
    result := (info^.subType=rostLocal);
  end;

begin
  InvalidateBranchMenu;

  try

    fGitMgr.UpdateRefList;

    AddPopItem(popBranch, rsNewBranch, @OnPopupItemClick, MENU_BRANCH_NEW);
    AddPopItem(popBranch, rsRenameBranch, @OnPopUpItemClick, MENU_BRANCH_RENAME);
    AddPopItem(popBranch, rsNewTag, @OnPopupItemClick, MENU_BRANCH_NEW_TAG);
    AddPopItem(popBranch, rsReload, @OnPopupItemClick, MENU_BRANCH_RELOAD);
    aState := fGitMgr.GitState;
    if (aState<>nil) then begin
      if not aState.InWorkTree then
        AddPopItem(popBranch, format(rsAddWorkTree, [QuotedStr(fGitMgr.Branch)]), @OnPopupItemClick, MENU_BRANCH_ADD_WORKTREE)
      else
        AddPopItem(popBranch, rsRemoveWorkTree, @OnPopupItemClick, MENU_BRANCH_REMOVE_WORKTREE);
    end;
    AddPopItem(popBranch, '-', nil, MENU_INVALID);


    for ref in fGitMgr.RefsFilter('', @FilterLocalBranches) do begin
      mi := AddPopItem(popBranch, ref^.refName, @OnPopupItemClick, MENU_BRANCH_SWITCH);
      mi.GroupIndex := 1;
      mi.AutoCheck := true;
      mi.Checked := ref^.head;
      if mi.Checked then
        mi.OnClick := nil;
      mi.RadioItem := true;
    end;

  finally
    CheckMenuDivisorInLastPosition(popBranch);
  end;
end;

procedure TfrmMain.ShowError;
begin
  txtDiff.Text := cmdLine.ErrorLog;
end;

procedure TfrmMain.ViewFile(filename: string);
var
  stream: TStream;
  flags: TViewerMgrFlags;
begin
  flags := fViewerMgr.Analyze(filename);
  if (vmfIsText in flags) or (not (vmfCanOpen in flags)) then begin
    fViewerMgr.Hide;
    SampleOfFile(filename, stream);
    stream.Position := 0;
    txtDiff.Lines.LoadFromStream(stream);
    txtDiff.Visible := true;
    stream.Free;
  end else
  if vmfCanOpen in flags then begin
    txtDiff.Visible := false;
    fViewerMgr.AttachTo := panStatus;
    fViewerMgr.LoadFromFile(filename);
  end;
end;

procedure TfrmMain.ComingSoon;
begin
  ShowMessage(rsThisFeatureWillBeImplementedASAP);
end;

function TfrmMain.MakeMenuItemUnstagedEntryArray(mi: TMenuItem): TPFileEntryArray;
var
  aIndex: PtrInt;
  i, n: Integer;
  entry: PFileEntry;
begin
  n := 0;
  result := nil;
  SetLength(result, lstUnstaged.Count);

  aIndex := mi.Tag;
  for i:=0 to lstUnstaged.Count-1 do begin
    entry := PFileEntry(lstUnstaged.Items.Objects[i]);
    if (aIndex=i) or
       ((aIndex=LIST_TAG_ALL)) or
       ((aIndex=LIST_TAG_ALL_SELECTED) and lstUnstaged.Selected[i]) or
       ((aIndex=LIST_TAG_ALL_CHANGED) and (entry^.EntryTypeUnStaged in ChangedInWorktreeSet))
    then begin
      result[n] := entry;
      inc(n);
      if aIndex=i then break;
    end
  end;

  SetLength(result, n);
end;

function TfrmMain.MakeMenuItemStagedEntryArray(mi: TMenuItem): TPFileEntryArray;
var
  aIndex: PtrInt;
  i, n: Integer;
  entry: PFileEntry;
begin
  n := 0;
  result := nil;
  SetLength(result, lstStaged.Count);

  aIndex := mi.Tag;
  for i:=0 to lstStaged.Count-1 do begin
    entry := PFileEntry(lstStaged.Items.Objects[i]);
    if (aIndex=i) or
       ((aIndex=LIST_TAG_ALL_SELECTED) and lstStaged.Selected[i]) or
       ((aIndex=LIST_TAG_ALL_CHANGED) and (entry^.EntryTypeStaged in ChangedInIndexSet))
    then begin
      result[n] := entry;
      inc(n);
      if aIndex=i then break;
    end
  end;

  SetLength(result, n);
end;

procedure TfrmMain.NewBranch;
var
  f: TfrmNewBranch;
begin
  f := TfrmNewBranch.Create(Self);
  f.GitMgr := fGitMgr;
  try
    if f.ShowModal=mrOk then
      fGitMgr.CreateBranch(self, f.BranchName, f.GetBranchCommandOptions, f.Switch, f.Fetch);
  finally
    f.Free;
  end;
end;

procedure TfrmMain.RenameBranch;
var
  aBranch:string;
begin
  if InputQuery(rsRenameBranch, format(rsRenameBranchTo, [QuotedStr(fGitMgr.Branch)]), aBranch) then begin
    aBranch := trim(aBranch);
    if (aBranch='') or SameText(aBranch, fGitMgr.Branch) then begin
      ShowMessage(rsInvalidBranchName);
      exit;
    end;
    fGitMgr.CreateBranch(Self, '', '-m '+aBranch, true, false);
  end;
end;

procedure TfrmMain.InvalidateBranchMenu;
begin
  popBranch.Items.Clear;
end;

procedure TfrmMain.NewTag;
begin
  if ShowNewTagForm(Self, fGitMgr, fGitMgr.BranchOID)>0 then
    ShowError;
end;

procedure TfrmMain.CheckMenuDivisorInLastPosition(pop: TPopupMenu);
begin
  if pop.Items.Count>1 then begin
    if pop.Items[pop.Items.Count-1].Caption='-' then
      pop.Items.Delete(pop.Items.Count-1);
  end;
end;

procedure TfrmMain.ObservedChanged(Sender:TObject; what: Integer; data: PtrInt);
var
  info: PTagInfo;
  binfo: PBranchInfo;
  msg: string;
  bisectInfo: PBisectInfo;
  lb: TListBox;
begin
  case what of

    GITMGR_EVENT_UpdateStatus:
      begin
        prgBar.Visible := false;
        if data>0 then
          ShowError
        else begin
          lstUnstaged.Items.Assign(fGitMgr.UnstagedList);
          if fAmend=nil then  lstStaged.Items.Assign(fGitMgr.StagedList)
          else                fAmend.Merge(fGitMgr.StagedList, lstStaged.Items);
          UpdateBranch;
          if fGitMgr.GetStateCommitMsg(msg) then
            txtComment.Text := msg;
        end;
      end;

    GITMGR_EVENT_BISECTACTION:
      begin
        bisectInfo := PBisectInfo(data);

        txtDiff.Tag := 1;
        if bisectInfo^.result<=0 then
          txtDiff.Text := bisectInfo^.Log
        else begin
          ShowError;
          ShowOutput(cmdLine.ErrorLog, 'Bisect action failed');
        end;

        Dispose(bisectInfo);

        fGitMgr.UpdateStatusSync;
        fGitMGr.UpdateRefList;
      end;

    GITMGR_EVENT_NEWBRANCH:
      begin
        binfo := {%H-}PBranchInfo(data);

        if binfo^.refreshBranch then
          // there is a new branch, the next time branch list
          // is requested, recreate it
          InvalidateBranchMenu;

        if binfo^.result>0 then begin
          if binfo^.sender=self then  ShowError
          else                        ShowMessage(fGit.ErrorLog);
        end;

        Dispose(binfo);
      end;

    GITMGR_EVENT_LOADREPOSITORY:
      begin
        info := {%H-}PTagInfo(data);
        OpenDirectory(info^.data);
        Dispose(info);
      end;

    GITMGR_EVENT_UPDATECURRENTDIFF:
      begin
        info := {%H-}PTagInfo(data);

        if info^.result=0 then lb := lstUnstaged
        else                   lb := lstStaged;

        if info^.data<>'' then begin
          what := lb.Items.IndexOf(info^.data);
          if what>=0 then
            lb.ItemIndex := what;
        end;

        DoGitDiff(PtrInt(lb));

        Dispose(info);
      end;
  end;
end;

procedure TfrmMain.SaveCommitMessage;
begin
  fConfig.WriteString('CommitMsg', txtComment.Text, fGit.TopLevelDir);
end;

procedure TfrmMain.RestoreCommitMessage;
begin
  txtComment.Text := fConfig.ReadString('CommitMsg', '', fGit.TopLevelDir);
end;

procedure TfrmMain.UpdateCommandsBar;
var
  btn: TToolButton;
  i: Integer;
begin

  // remove the current buttons
  while barCustomCmds.ButtonCount>2 do
    barCustomCmds.Buttons[2].Free;

  // add a divider
  if fCustomCommands.Count>0 then begin
    btn := TToolButton.Create(self);
    btn.Style := tbsDivider;
    btn.Parent := barCustomCmds;
  end;

  // add the new buttons
  for i:=0 to fCustomCommands.Count-1 do
    with fCustomCommands[i] do begin
      btn := TToolButton.Create(self);
      if description='-' then
        btn.Style := tbsDivider
      else begin
        btn.Style := tbsButton;
        btn.Tag := i;
        btn.OnClick := @OnCustomCommandClick;
        btn.Hint := description;
        btn.Caption := description;
        btn.Parent := barCustomCmds;
      end;
    end;

  // layout buttons, this trick makes them appear in order
  btnAddCustomCmd.Left := 0;
  btnGitCmd.Left := 0;
  for i:=barCustomCmds.ButtonCount-1 downto 0 do
    barCustomCmds.Buttons[i].Left := 0;

end;

procedure TfrmMain.OpenInternalEditor(aFile: string);
//var
//  frm: TframeEditor;
begin

  lstStaged.Enabled := false;
  lstUnstaged.Enabled := false;
  panBranch.Enabled := false;

  fViewerMgr.AttachTo := panHolder;
  fViewerMgr.Readonly := false;
  fViewerMgr.Embedded := true;
  fViewerMgr.LoadFromFile(aFile);
  //frm := TframeEditor.Create(Self);
  //frm.OnQuit := @OnFrameEditorQuit;
  //frm.Align := alClient;
  //frm.Parent := panHolder;
  //frm.HlHelper := fhlHelper;
  //frm.Embedded := true;
  //frm.LoadFromFile(aFile);

  panHolder.BringToFront;
end;

function TfrmMain.GetListEntry(lb: TListBox; aIndex: Integer; out
  entry: PFileEntry): TEntryTypeSource;
var
  sEntry, cEntry: PFileEntry;
begin

  entry := PFileEntry(lb.Items.Objects[aIndex]);

  if lb=lstUnstaged then

    result := etsUnstaged

  else if fAmend=nil then

    result := etsStaged

  else begin

    sEntry := PAmendEntry(entry)^.StageEntry;
    cEntry := PAmendEntry(entry)^.CommitEntry;

    if (sEntry<>nil) and (cEntry<>nil) then begin
      result := etsStagedAmend;
      entry := sEntry;
    end else
    if (sEntry<>nil) then begin
      result := etsStaged;
      entry := sEntry;
    end else
    if (cEntry<>nil) then begin
      result := etsAmend;
      entry := cEntry;
    end else begin
      // This should never happen as this means that entry=nil
      // fAmend.Merge() has not been called?
      result := etsUnknown;
      entry := nil;
    end;

  end;

end;

function TfrmMain.GetListEntryInfo(lb: TListBox; aIndex: Integer; out
  entry: PFileEntry; out info: string): TEntryTypeSource;
begin
  result := GetListEntry(lb, aIndex, entry);
  if (fAmend=nil) or (lb=lstUnstaged) then
    info := EntryTypeToStr(entry^.x, entry^.y)
  else
    info := AmendEntryToStr(PAmendEntry(lb.Items.Objects[aIndex]));
end;

procedure TfrmMain.AddBranchWorkTree;
begin
  ComingSoon;
end;

procedure TfrmMain.RemoveBranchWorkTree;
begin
  ComingSoon;
end;

function OwnerDrawStateToStr(State: TOwnerDrawState): string;
  procedure Add(st: string);
  begin
    if result<>'' then result += ',';
    result += st;
  end;
var
  stateItem: TOwnerDrawStateType;
  s: string;
begin
  for stateItem in State do begin
    WriteStr(s, stateItem);
    Add(s);
  end;
  result := '[' + result + ']';
end;

procedure TfrmMain.lstUnstagedDrawItem(Control: TWinControl; Index: Integer;
  ARect: TRect; State: TOwnerDrawState);
var
  lb: TListBox;
  entry: PFileEntry;
  aCanvas: TCanvas;
  aColor: TColor;
  sel, isFocused: boolean;
begin
  if index<0 then
    exit;

  lb := TListBox(Control);
  entry := PFileEntry(lb.Items.Objects[index]);

  isFocused := lb.Focused or fListAlwaysDrawSelection;
  sel := (odSelected in State) and isFocused;

  aCanvas := lb.Canvas;
  if isFocused then
    if sel then aColor := clHighlight
    else        aColor := lb.Color
  else          aColor := lb.Color;

  aCanvas.Brush.Color := aColor;
  aCanvas.FillRect(aRect);

  if isFocused then
    if sel then aColor := clHighlightText
    else        aColor := clWindowText
  else          aColor := clWindowText;

  lb.Canvas.Font.Color := aColor;
  lb.Canvas.Brush.Style := bsClear;
  lb.Canvas.TextRect(aRect, aRect.Left + 22, aRect.Top, fGitMgr.GetRelativeFilename(lb.Items[Index]));

  if lb=lstUnstaged then index := EntryTypeToIndex(entry^.EntryTypeUnStaged, etsUnstaged) else
  if fAmend<>nil then    index := PAmendEntry(entry)^.Index
  else                   index := EntryTypeToIndex(entry^.EntryTypeStaged, etsStaged);

  imgList.Draw(lb.Canvas, aRect.Left + 2, aRect.Top + 1, index);
end;

procedure TfrmMain.lstUnstagedMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  aIndex: Integer;
begin
  if button=mbLeft then begin
    aIndex := TListBox(Sender).GetIndexAtXY(x, y);
    if aIndex>=0 then begin
      if (x>0) and (x < 20) then begin
        fClickedIndex := aIndex;
        Application.QueueAsyncCall(@DoItemAction, ptrInt(Sender));
      end else
        Application.QueueAsyncCall(@DoGitDiff, PtrInt(Sender));
    end;
  end;
end;

procedure TfrmMain.toggleAmendClick(Sender: TObject);
begin
  if toggleAmend.Checked then begin
    fAmend := TAmend.Create;
    fAmend.GitMgr := fGitMgr;
    fAmend.LoadLastCommit;
    txtComment.Text := fAmend.Commit.Subject;
    fAmend.Merge(fGitMgr.StagedList, lstStaged.Items);
  end else begin
    FreeAndNil(fAmend);
    txtComment.Text := '';
    txtDiff.Text := '';
    lstStaged.Items.Assign(fGitMgr.StagedList);
  end;

  lstStaged.Invalidate;

  actPush.Enabled := not toggleAmend.Checked;
  actPushDialog.Enabled := actPush.Enabled;
  lblBranch.Enabled := actPush.Enabled;
end;

procedure TfrmMain.txtDiffContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin
  fDiffActions.AddPopupItems(MousePos);
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  prgBar.Style := pbstMarquee;

  fhlHelper := THighlighterHelper.create;

  fGitMgr := TGitMgr.Create;
  fGitMgr.Config := fConfig;
  fGitMgr.AddObserver(Self);
  fGitMgr.Locked := Application.HasOption('locked');
  fGitMgr.AlternateGitExe := Application.GetOptionValue('git');

  fGit := fGitMgr.Git;

  fConfig.OpenConfig;

  if not fGitMgr.Initialize then begin
    fConfig.CloseConfig;
    DebugLn('Error: Could not find git command');
    Application.Terminate;
    exit;
  end;

  //DebugLn('git=', fGit.Exe);

  txtDiff.Clear;

  fConfig.ReadFont(txtDiff.Font, 'viewer', fpFixed, SECTION_FONTS);

  AddPopupMenu(txtDiff);

  panFileState.Caption := '';

  RestoreGui;

  fConfig.ReadPreferences;
  fGitMgr.ViewUntrackedFiles := fConfig.ViewUntrackedFiles;
  fGitMgr.ViewIgnoredFiles := fConfig.ViewIgnoredFiles;
  fGitMgr.ViewTrackedFiles := fConfig.ViewTrackedFiles;
  fGitMgr.ShowTags := fConfig.ShowTags;

  if fConfig.ReadBoolean('NeedsMenuWorkaround'{$ifdef Linux},true{$endif}) then begin
    Self.Menu := nil;
    Application.QueueAsyncCall(@DelayedShowMenu, 0);
  end;

  fCustomCommands := TCustomCommandsMgr.create;
  fCustomCommands.LoadFromConfig;

  fTextLinks := TTextLinks.Create;

  fViewerMgr := TViewerMgr.create;
  fViewerMgr.AttachTo := panHolder;
  fViewerMgr.HlHelper := fHlHelper;
  fViewerMgr.OnQuit := @OnFrameEditorQuit;

  fConfig.MenuMRE(popCommands, false, @OnMRECommand, '', SECTION_MRECOMMANDS);

  gblCutterMode := fConfig.ReadBoolean('CutterMode', gblCutterMode);
  gblTopologicalMode := fConfig.ReadBoolean('TopologicalMode', gblTopologicalMode);
  gblAllowDeleteChanged := fConfig.ReadBoolean('AllowDeleteChanged', gblAllowDeleteChanged);
  gblReportLooseObjects := fConfig.ReadBoolean('ReportLooseObjects', gblReportLooseObjects);
  gblLastDiffToolIndex := fConfig.ReadInteger('DiffToolIndex', gblLastDiffToolIndex);

  fConfig.CloseConfig;
  frmActions.GitMgr := fGitMgr;
  frmActions.OnError := @OnFrameActionsError;
  frmActions.ChildSizing.ControlsPerLine := 8;

  gblInvalidateCache := Application.HasOption('ClearCache');

  fDiffActions := TDiffActions.Create(txtDiff);
  fDiffActions.GitMgr := fGitMgr;
end;

procedure TfrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  SaveGui;
end;

procedure TfrmMain.actRescanExecute(Sender: TObject);
begin
  fGitMgr.UpdateStatus;
end;

procedure TfrmMain.actRestoreCommitMsgExecute(Sender: TObject);
var
  s: String;
  i: SizeInt;
begin
  RestoreCommitMessage;
  if txtComment.Lines.Count>0 then begin
    s := txtComment.Lines[0];
    i := RPos(':', S);
    if i>0 then begin
      txtComment.SelStart := i;
      txtComment.SelLength := Length(s);
    end;
  end;
end;

procedure TfrmMain.btnGitCmdArrowClick(Sender: TObject);
begin
  ShowMessage('Arrow click');
end;

procedure TfrmMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  PreserveClipboard;
end;

procedure TfrmMain.actCommitExecute(Sender: TObject);
begin
  DoCommit;
end;

procedure TfrmMain.actAddCmdExecute(Sender: TObject);
var
  F: TfrmCustomCommands;
begin
  F := TfrmCustomCommands.Create(Self);
  F.Commands := fCustomCommands;
  F.AddNew := true;
  try
    if F.ShowModal=mrOk then
      UpdateCommandsBar;
  finally
    F.Free;
  end;
end;

procedure TfrmMain.actCloneExecute(Sender: TObject);
begin
  DoClone;
end;

procedure TfrmMain.actFetchExecute(Sender: TObject);
begin
  DoFetch;
end;

procedure TfrmMain.actGitCmdExecute(Sender: TObject);
var
  F: TfrmGitCmd;
  s: string;
begin
  F := TfrmGitCmd.Create(Self);
  try
    if F.ShowModal=mrOk then begin
      s := StringReplace(F.txtGitCmd.Text, 'git', fGit.Exe, []);
      if RunInteractive(s, fGit.TopLevelDir, 'Run a git command', F.txtGitCmd.Text)<=0 then begin
        if F.chkRemember.Checked then
          fConfig.MenuMRE(popCommands, true, @OnMRECommand, F.txtGitCmd.Text, SECTION_MRECOMMANDS);
      end
    end;
  finally
    F.Free;
  end;
end;

procedure TfrmMain.actInsertBranchNameExecute(Sender: TObject);
begin
  txtComment.SelText := fGitMgr.Branch + ': ';
end;

procedure TfrmMain.actNewLogExecute(Sender: TObject);
begin
  DoNewLog;
end;

procedure TfrmMain.actPullExecute(Sender: TObject);
begin
  DoPull;
end;

procedure TfrmMain.actPushDialogExecute(Sender: TObject);
begin
  DoPushDialog(false);
end;

procedure TfrmMain.actPushExecute(Sender: TObject);
begin
  DoPush;
end;

procedure TfrmMain.actQuitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.actRemotesExecute(Sender: TObject);
begin
  DoRemotes;
end;

procedure TfrmMain.actRepoBrowserExecute(Sender: TObject);
begin
  DoRepoBrowser;
end;

procedure TfrmMain.actRepoInfoExecute(Sender: TObject);
begin
  DoRepoInfo;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  fDiffActions.Free;
  fViewerMgr.Free;
  fCustomCommands.Free;
  frmLog.Free;
  fGit := nil;
  fGitMgr.RemoveObserver(Self);
  fhlHelper.Free;
  fGitMgr.Free;
  fTextLinks.Free;
end;

procedure TfrmMain.FormDropFiles(Sender: TObject;
  const FileNames: array of string);
var
  aFile, cmd: String;
  F: TFileStream;
  readbytes: Int64;
  buffer: pchar;
  serialCount, simpleCount: Integer;
  res: TModalResult;
  isDiff: boolean;
begin
  if not fEnableDropFiles then
    exit;
  GetMem(buffer, BIN_BUFSIZE+1);
  try
    serialCount := 0;
    simpleCount := 0;

    // check dropped files are all text diff files
    for aFile in Filenames do begin
      F := TFileStream.Create(aFile, fmOpenRead + fmShareDenyNone);
      try
        readBytes := F.Read(buffer^, BIN_BUFSIZE);
        buffer[readBytes] := #0;
        if IsBinBuffer(pbyte(buffer), readBytes) then
          exit;
        isDiff := strpos(buffer, '@@ ')<>nil;
        if isDiff and (strlcomp('From ', buffer, 5)=0) and (strpos(buffer, 'From: ')<>nil) then
          inc(serialCount)
        else if isDiff then
          inc(simpleCount);
      finally
        F.Free;
      end;
    end;

    if (SimpleCount=0) and (SerialCount=0) then
      exit; // dropped a non diff/patch file, exit without complaining

    // we can process either a simple diff for git apply
    // or a group of serial diff files for git am
    if (serialCount>0) then begin
      ShowMessage(rsApplyingSerialPatchesIsNotYetImplemented);
      exit;
    end;

    if (simpleCount<>Length(Filenames)) or (SimpleCount>1) then begin
      ShowMessage(rsInvalidAmountOfPatchesTooApply);
      exit;
    end;

    res := QuestionDlg(rsPatchingTheWorkArea,
      format(rsYouAreTryingToApply, [QuotedStr(ExtractFileName(Filenames[0]))]),
       mtConfirmation, [mrYes, rsApplyPatch, mrCancel, rsCancel], 0 );

    if res=mrYes then begin
      cmd := fGit.Exe + ' apply '+ Sanitize(Filenames[0]);

      if RunInteractive(cmd, fGit.TopLevelDir, rsApplyingAPatch, cmd)<=0 then
        fGitMgr.UpdateStatus();
    end;
  finally
    FreeMem(buffer);
  end;
end;

procedure TfrmMain.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then
    close;
end;

function TfrmMain.OpenDirectory(aDir: string): boolean;
begin
  result := fGitMgr.OpenRepository(aDir);
  if not result then begin
    ShowMessageFmt(rsCouldnTGetToplevelDirectoryOfS, [aDir]);
    exit;
  end;
  prgBar.Visible := true;
  if frmLog<>nil then
    frmLog.clear;
end;

procedure TfrmMain.DoItemAction(Data: PtrInt);
begin
  ItemAction(TListBox(Data), fClickedIndex);
end;

procedure TfrmMain.DoCommit;
var
  opts: String;
begin

  if lstStaged.Count=0 then begin
    ShowMessage(rsYouHaveToStageSomething);
    exit;
  end;
  if Trim(txtComment.Text)='' then begin
    ShowMessage(rsYourCommitMessageIsEmpt);
    exit;
  end;

  if fAmend<>nil then opts := '--amend'
  else                opts := '';

  if fGit.Commit(txtComment.Text, opts)>0 then
    ShowError
  else begin
    if fAmend<>nil then begin
      FreeAndNil(fAmend);
      toggleAmend.Checked := false;
    end;
    SaveCommitMessage;
    fGitMgr.ForceTagDescription;
    fGitMgr.UpdateStatus;
    fGitMgr.UpdateRefList;
    txtDiff.Clear;
    txtComment.Clear;
  end;

end;

procedure TfrmMain.DoClone;
var
  F: TfrmClone;
begin
  F := TfrmClone.Create(Self);
  F.GitMgr := fGitMgr;
  try
    F.ShowModal;
  finally
    F.Free;
  end;
end;

procedure TfrmMain.DoNewLog;
begin

  if ssShift in KeyboardStateToShiftState then
    gblInvalidateCache := true;

  if frmLog=nil then begin
    frmLog := TfrmLog.Create(Self);
    frmLog.GitMgr := fGitMgr;
    frmLog.HlHelper := fHlHelper;
  end;

  if actNewLog.Checked then begin
    frmLog.Show;
    frmLog.BringToFront;
  end else begin
    frmLog.close
  end;
end;

procedure TfrmMain.DoPush;
var
  res: TModalResult;
  cmd, aRemote: string;
  n: Integer;
begin
  //if fConfig.ReadBoolean('FetchBeforePush', false) then
  //  doFetch;
  if fGitMgr.CommitsBehind<0 then begin
    res := QuestionDlg(rsPushingYourCommits, rsThereAreCommitsBehind, mtConfirmation,
      [mrYes, 'Push', mrCancel, 'Cancel'], 0 );
    if res<>mrYes then
      exit;
  end;

  aRemote := '';

  if (fGitMgr.Upstream='') then begin

    if fGitMgr.Remotes=nil then
      fGitMgr.UpdateRemotes;

    if fGitMgr.Remotes=nil then begin
      ShowMessage(rsThisRepositoryHasNoRemotes);
      if DoRemotes<>mrOk then
        exit;
    end;

    n := Length(fGitMgr.Remotes);
    if n>1 then begin
      DoPushDialog(true);
      exit;
    end;

    aRemote := fGitMgr.Remotes[0].name;

    res := QuestionDlg( rsPushingBranchWithout,
             format(rsDoYouWantToPushSToSA, [fGitMgr.Branch, aRemote]), mtConfirmation,
             [mrYes, rsYesDoIt, mrCancel, rsCancel], 0 );
    if res<>mrYes then
      exit;

    cmd := ' push --progress --set-upstream '+aRemote+' '+fGitMgr.Branch;

  end else begin

    n := pos('/', fGitMgr.Upstream);
    if n=0 then begin
      // should never happen ...
      ShowMessage('Error: unexpected upstream layout');
      exit;
    end;

    aRemote := copy(fGitMgr.Upstream, 1, n-1);
    cmd := copy(fGitMgr.Upstream, n+1, MAXINT);

    cmd := ' push --progress ' + aRemote + ' HEAD:' + cmd;
  end;

  RunInteractive(fGit.Exe + cmd, fGit.TopLevelDir, format(rsPushingToRemoteS, [aRemote]), cmd);
  fGitMgr.UpdateStatus;
  fGitMgr.UpdateRefList;
end;

function TfrmMain.DoPushDialog(withUpstream: boolean): TModalResult;
var
  F: TfrmPush;
begin
  F := TfrmPush.Create(Self);
  F.GitMgr := fGitMgr;
  F.WithUpstream := withUpstream;
  try
    result := F.ShowModal;
  finally
    F.Free;
  end;
end;

procedure TfrmMain.DoFetch;
begin
  RunInteractive(fGit.Exe + ' -c color.ui=always fetch --progress', fGit.TopLevelDir, rsFetchingFromRemote, 'Fetch');
  fGitMgr.UpdateStatus;
  fGitMgr.UpdateRefList;
end;

procedure TfrmMain.DoPull;
begin
  RunInteractive(fGit.Exe + ' -c color.ui=always pull --progress', fGit.TopLevelDir, rsPullingFromRemote, 'Pull');
  fGitMgr.UpdateStatus;
  fGitMgr.UpdateRefList;
end;

procedure TfrmMain.DoRepoInfo;
begin
  fGitMgr.UpdateStatusSync;
  OnRepoInfoStatusDone(self);
end;

function TfrmMain.DoRemotes: TModalResult;
var
  F: TfrmRemotes;
begin
  F := TfrmRemotes.Create(self);
  F.GitMgr := fGitMgr;
  F.ReadOnly := false;
  try
    result := F.ShowModal;
  finally
    F.Free;
  end;
end;

procedure TfrmMain.DoRepoBrowser;
begin
  if frmRepoBrowser=nil then begin
    frmRepoBrowser := TfrmRepoBrowser.Create(Self);
    frmRepoBrowser.GitMgr := fGitMgr;
    frmRepoBrowser.HlHelper := fhlHelper;
  end;
  frmRepoBrowser.Show;
  frmRepoBrowser.BringToFront;
end;

procedure TfrmMain.FrameEditorQuitHandler(Data: PtrInt);
begin

  panStatus.BringToFront;

  lstStaged.Enabled := true;
  lstUnstaged.Enabled := true;
  panBranch.Enabled := true;

end;

procedure TfrmMain.OnCreatePatchClick(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  lb: TListbox;
  diff: TStringList;
begin
  lb := TListBox(mi.Tag);
  diff := CreatePatchFromListbox(lb);
  //txtDiff.Text := diff.Text;
  if dlgSave.Execute then
    diff.SaveToFile(dlgSave.FileName);
  diff.free;
end;

procedure TfrmMain.OnCopyPatchClick(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  lb: TListbox;
  diff: TStringList;
begin
  lb := TListBox(mi.Tag);
  diff := CreatePatchFromListbox(lb);
  //txtDiff.Text := diff.Text;
  Clipboard.AsText := diff.Text;
  diff.Free;
end;

procedure TfrmMain.OnCustomCommandClick(Sender: TObject);
var
  comp: TComponent absolute Sender;
  cmd: TCustomCmdItem;
  res: TModalResult;
  s, c: String;
  vars: TRepoVars;
  arr: TStringArray;
  aRes: Integer;
  cmdOut: RawByteString;
  lout: TStringList;
begin
  cmd := fCustomCommands[comp.Tag];

  lout := TStringList.Create;
  vars := TRepoVars.Create;
  vars.GitMgr := fGitMgr;
  try
    s := vars.ReplaceVars(cmd.command);

    arr := s.Split([#13#10, ';', #10]);

    // simple checks
    c := '';
    aRes := 0;
    for s in arr do begin
      if pos('git ', s)=1 then begin
        inc(aRes);
        if aRes=1 then c := s
        else            c += ^M+s;
        continue;
      end;
    end;

    if aRes=0 then begin
      ShowMessage('Found no git commands to execute');
      exit;
    end else
      c := format('%d git commands:', [aRes]) + ^M^M + c;

    if cmd.Ask then begin
      res := QuestionDlg(
        rsExecutingACustomCommand,
        format(rsYouAreAboutToExecute, [QuotedStr(cmd.description), c]),
        mtConfirmation, [mrYes, rsYesDoIt, mrCancel, rsCancel], 0 );
      if res<>mrYes then
        exit;
    end;

    for c in arr do begin

      if pos('git ', c)<>1 then
        continue;

      s := StringReplace(c, 'git', fGit.Exe, []);
      if (length(arr)=1) and cmd.RunInDlg then begin
        RunInteractive(s, fGit.TopLevelDir, rsExecutingACustomCommand, cmd.description);
        break;
      end;

      aRes := RunCommand(s, fGit.TopLevelDir, cmdOut);

      lout.Add('');

      lout.AddObject(rsCommandSS, [QuotedStr(c), boolToStr(aRes<=0, rsWasSuccessful, rsFailed)], nil);

      if Length(cmdOut)>0 then
        lout.Append(cmdOut);

      if aRes>0 then begin
        lout.AddObject(rsSProducedAnErrorExecutionStoped, [c], nil);
        lout.Append(cmdLine.ErrorLog);
        break;
      end;

    end;

    txtDiff.Text := lout.Text;

    if cmd.updatestatus then begin
      // UpdateStatus is ran asynchronically, on completion it will
      // clear txtDiff signaling the update was successful, but this
      // clears the accumulated output of the custom command contained
      // in lout.Text.
      // set txtDiff.Tag=1 meaning that UpdateStatus should not clear
      // the screen. See ObservedChanged() -> UpdateBranch()
      txtDiff.Tag := 1;

      fGitMgr.UpdateStatus;
      fGitMgr.UpdateRefList;
    end;


  finally
    vars.free;
    lout.free;
  end;
end;

procedure TfrmMain.OnDebugLnInterceptor(Sender: TObject; S: string;
  var Handled: Boolean);
begin
  txtDiff.Lines.Add(s);
  handled := true;
end;

procedure TfrmMain.OnDiffUnstagedInExternalViewer(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  entry: PFileEntry;
  dtl: TDiffToolLauncherRec;
  cmdOut: RawByteString;
begin

  gblLastDiffToolIndex := mi.Tag;
  fConfig.WriteInteger('DiffToolIndex', gblLastDiffToolIndex);

  GetListEntry(lstUnstaged, lstUnstaged.Tag, entry);
  if entry=nil then
    exit; // should not happen

  initialize(dtl);

  dtl.toolIndex := gblLastDiffToolIndex;
  dtl.gitMgr := fGitMgr;
  dtl.base.filename := ExtractFileName(entry^.path);
  dtl.base.tag := fGitMgr.BranchOID;
  dtl.base.tree := entry^.h1; // hash in index
  dtl.base.meta := rsBase;
  dtl.comp.filename := fGit.TopLevelDir + entry^.path;
  dtl.comp.meta := rsWorkingTree;

  LaunchDiffTool(dtl, cmdOut);
end;

procedure TfrmMain.OnFrameActionsError(Sender: TObject);
begin
  ShowError;
end;

procedure TfrmMain.OnFrameEditorQuit(Sender: TObject);
begin
  Application.QueueAsyncCall(@FrameEditorQuitHandler, PtrInt(Sender));
end;

procedure TfrmMain.OnIdleMe(Sender: TObject; var Done: Boolean);
begin
  fEnableDropFiles := true;
end;

procedure TfrmMain.OnMRECommand(Sender: TObject);
var
  mi: TMenuItem absolute Sender;
  s: string;
begin
  s := StringReplace(mi.Caption, 'git', fGit.Exe, []);
  RunInteractive(s, fGit.TopLevelDir, 'Run a git command', mi.Caption);
end;

procedure TfrmMain.DoGitDiff(Data: PtrInt);
var
  lb: TListbox absolute Data;
  entry: PFileEntry;
  aType: TEntryTypeSource;
  unstaged: Boolean;
  info: string;
begin
  if lb.ItemIndex>=0 then begin

    aType := GetListEntryInfo(lb, lb.ItemIndex, entry, info);

    panFileState.Caption := info;

    if aType=etsAmend then begin
      // how to resolve this?
      txtDiff.Text := 'Showing information of already commited ' + lb.Items[lb.ItemIndex];
      exit;
    end;

    unstaged := aType=etsUnstaged;

    if unstaged and (entry^.EntryTypeUnStaged in [etUntracked, etIgnored]) then begin
      fhlHelper.SetHighlighter(txtDiff, entry^.path);
      ViewFile(fGit.TopLevelDir + entry^.path);
      fDiffActions.Enabled := false;
    end else begin
      fViewerMgr.Hide;
      txtDiff.Visible := true;
      fhlHelper.SetHighlighter(txtDiff, 'x.diff');
      if fGit.Diff(entry, unstaged, txtDiff.Lines)>0 then
        ShowError;
      fDiffActions.ParseDiff(entry, unstaged);
    end;

  end;
end;

procedure TfrmMain.DelayedShowMenu(Data: PtrInt);
begin
  Self.Menu := mnuMain;
end;

function TfrmMain.CreatePatchFromListbox(const lb: TListbox): TStringList;
var
  i: Integer;
  s: string;
  stagedList: string;
  unstagedList: string;
  untrackedList, list: TStringList;
  entry: PFileEntry;
  isStaged, succeed: boolean;

  procedure DoDiff(cmdfmt: string; args:array of const; isUntracked: boolean);
  var
    cmd: string;
    Code: Integer;
  begin
    cmd := format(cmdfmt, args);
    list.clear;
    Code := fGit.Diff(cmd, list);
    succeed := Code<=0;
    {$ifdef MSWindows}
    // this is crazy, in windows it apparently fails with code=1 when diffing
    // an untracked file, but the output is ok, if we blindly check the code
    // for success it wont work...
    // could it related to the use of /dev/null?
    succeed := succeed or ((isUntracked) and (code=1));
    {$endif}
    if succeed then begin
      //DebugLn(list.Text);
      result.AddStrings(list);
    end else begin
      DebugLn(fGit.ErrorLog);
      DebugLn(list.Text);
    end;
  end;

  procedure Add(var strList:string; what:string);
  begin
    if strList<>'' then strList += ' ';
    strList += what;
  end;

begin

  result := nil;

  isStaged := (lb = lstStaged);

  untrackedList := TStringList.Create;
  list := TStringList.Create;
  try

    unstagedList := '';
    stagedList := '';
    for i := 0 to lb.Count - 1 do begin
      if lb.Selected[i] then begin
        s := lb.Items[i];
        if isStaged then
          Add(stagedList, s)
        else begin
          entry := PFileEntry(lb.Items.Objects[i]);
          if entry^.EntryTypeUnStaged = etUntracked then
            untrackedList.Add(s)
          else
            Add(unstagedList, s);
        end;
      end;
    end;

    result := TStringList.Create;

    if isStaged then
      DoDiff('--cached -- %s',[stagedList], false)
    else begin
      if unstagedList<>'' then
        DoDiff('-- %s',[unstagedList], false);
      for s in untrackedList do begin
        DoDiff('--no-index -- /dev/null %s', [s], true);
        if not succeed then
          break;
      end;
    end;

    if not succeed then
      txtDiff.Text := fGit.ErrorLog;

  finally
    list.Free;
    untrackedList.Free;
  end;
end;

procedure TfrmMain.UpdateBranch;
var
  s: string;
  ahead, behind, upstream: boolean;
  i: Integer;
begin
  ahead := fGitMgr.CommitsAhead>0;
  behind := fGitMgr.CommitsBehind<0;
  upstream := fGitMgr.Upstream<>'';

  label1.Visible := not ahead and not behind;
  lblBranch.Caption := fGitMgr.Branch;
  lblBranch.Hint := fGitMgr.Branch + LineEnding + fGitMgr.BranchOID;

  s := '';
  if fGitMgr.GitState.StateType<>gstNone then begin
    s += '(';
    s += UpperCase(fGitMgr.GitState.AsString);
    s += ')';
  end;
  lblState.Caption := s;

  frmActions.UpdateState;

  s := '';
  if ahead then
    if upstream then  s += format(rsDCommitsAhead, [fGitMgr.CommitsAhead])
    else              s += format(rsHasDCommits, [fGitMgr.CommitsAhead]);
  if ahead and behind then s += ', ';
  if behind then s += format(rsDCommitsBehind, [ - fGitMgr.CommitsBehind]);
  if (ahead and upstream) or behind then s += ' ' + rsOf;

  if s='' then s:=' ';
  lblAheadBehind.Caption := s;

  label2.Visible := (not ahead and not behind) and upstream;
  lblRemote.Caption := fGitMgr.Upstream;

  if fConfig.ShowTags then begin
    if fGitMgr.LastTag='' then begin
      lblTag.Caption := rsNoTagAvailable;
      lblTag.Hint := '';
      label3.Caption := '';
    end else begin
      lblTag.Caption := fGitMgr.LastTag;
      if fGitMgr.LastTagCommits=0 then
        label3.Caption := rsAtTag
      else
        label3.Caption := format(rsDCommitsSince, [fGitMgr.LastTagCommits]);
      lblTag.Hint := fGitMgr.LastTagOID;
    end;
  end else begin
    lblTag.Caption :='';
    lblTag.Hint := '';
    label3.Caption := '';
  end;

  i := fGitMgr.RemoteIndex;
  if i>=0 then begin
    with fGitMgr.Remotes[i] do begin
      if Fetch=Push then
        lblRemote.Hint := format('Url: %s',[Fetch])
      else
        lblRemote.Hint := format('Fetch Url: %s'^M'Push Url: %s', [Fetch, Push]);
    end;
  end else
    lblRemote.Hint := '';

  // see comment in OnCustomCommandClick about txtDiff.Tag
  if txtDiff.Tag=0 then
    txtDiff.Clear;
  txtDiff.Tag := 0;

  toggleAmend.Enabled := ahead;

  s := 'LazGitGUI v1.0 (git '+fGit.Version+') - ' + fGit.TopLevelDir;
  if fGitMgr.Locked then
    s += ' [' + fGit.StartDir + ']';
  Caption := s;
end;

procedure TfrmMain.RestoreGui;
begin
  fConfig.OpenConfig;

  fConfig.ReadWindow(Self, 'mainform', SECTION_GEOMETRY);

  lstUnstaged.Height := fConfig.ReadInteger('lstUnstaged.Height', lstUnstaged.Height, SECTION_GEOMETRY);
  panLeft.Width := fConfig.ReadInteger('panleft.width', panLeft.Width, SECTION_GEOMETRY);
  pancommit.Height := fConfig.ReadInteger('pancommit.height', pancommit.Height, SECTION_GEOMETRY);

  fConfig.CloseConfig;
end;

procedure TfrmMain.SaveGui;
begin
  fConfig.OpenConfig;
  fConfig.WriteWindow(Self, 'mainform', SECTION_GEOMETRY);
  fConfig.WriteInteger('lstUnstaged.Height', lstUnstaged.Height, SECTION_GEOMETRY);
  fConfig.WriteInteger('panleft.width', panLeft.Width, SECTION_GEOMETRY);
  fConfig.WriteInteger('pancommit.height', pancommit.Height, SECTION_GEOMETRY);
  fConfig.CloseConfig;
end;

end.

