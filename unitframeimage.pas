unit unitframeimage;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  LazLoggerBase, Graphics, Forms, Controls, ComCtrls, ExtCtrls, ActnList,
  unitcommon, unitifaces;

type

  TFitType = (ft1x1, ftFit);

  { TfrmImage }

  TfrmImage = class(TFrame, IViewer)
    act1x1: TAction;
    actFit: TAction;
    ActionList1: TActionList;
    img: TImage;
    scrBox: TScrollBox;
    sBar: TStatusBar;
    toolBar: TToolBar;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    procedure act1x1Execute(Sender: TObject);
    procedure actFitExecute(Sender: TObject);
    procedure imgClick(Sender: TObject);
  private
    fFitType: TFitType;
    procedure SetFit(aFitType: TFitType);
    function CanOpen(aFile: string; isText:boolean): boolean;
    function CanEdit(aFile: string; isText:boolean): boolean;
    procedure SetProperty(aProperty: string; aValue: TNotifyEvent); overload;
    procedure SetProperty(aProperty: string; aValue: Variant); overload;
    procedure SetProperty(aProperty: string; aValue: TObject); overload;
    procedure AttachTo(aParent: TObject);
    procedure UpdateInfo;
  public
    function LoadFromFile(aFile: string): boolean;
    function LoadFromStream(stream: TStream; metaFilename:string=''): boolean;
  end;

implementation

{$R *.lfm}

function FitTypeToStr(ft: TFitType): string;
begin
  case ft of
    ft1x1:  result := '1:1';
    ftFit:  result := 'Fit';
  end;
end;

{ TfrmImage }

procedure TfrmImage.imgClick(Sender: TObject);
var
  aFitType: TFitType;
begin

  if fFitType=ftFit then
    aFitType := ft1x1
  else
    aFitType := ftFit;

  SetFit(aFitType);
  UpdateInfo;
end;

procedure TfrmImage.act1x1Execute(Sender: TObject);
begin
  SetFit(ft1x1);
end;

procedure TfrmImage.actFitExecute(Sender: TObject);
begin
  SetFit(ftFit);
end;

procedure TfrmImage.SetFit(aFitType: TFitType);
var
  scrWidth, scrHeight: Integer;
begin

  scrWidth := scrBox.ClientWidth;
  scrHeight := scrBox.ClientHeight;
  fFitType := aFitType;

  case aFitType of
    ft1x1:
      begin
        img.Stretch := false;
        img.Width := img.Picture.width;
        img.Height := img.Picture.Height;

        if img.Width>scrWidth then  img.Left := 0
        else                        img.Left := scrWidth div 2 - img.Width div 2;

        if img.Height>scrHeight then  img.Top := 0
        else                          img.Top := scrHeight div 2 - img.Height div 2;
      end;

    ftFit:
      begin
        img.Left := 0;
        img.Top := 0;
        img.Width := scrBox.ClientWidth;
        img.Height := scrBox.ClientHeight;
        img.Stretch := true;
        img.Proportional := true;
        img.Center := true;
      end;

  end;

end;

function TfrmImage.CanOpen(aFile: string; isText: boolean): boolean;
var
  ext: String;
begin
  ext := ExtractFileExt(aFile);
  result := GetGraphicClassForFileExtension(ext) <> nil;
end;

function TfrmImage.CanEdit(aFile: string; isText: boolean): boolean;
begin
  result := false;
end;

procedure TfrmImage.AttachTo(aParent: TObject);
begin
  Parent := TWinControl(aParent);
  Align := alClient;
end;

procedure TfrmImage.UpdateInfo;
begin
  sbar.Panels[0].Text := format('%dx%d',[img.Picture.Width, img.Picture.Height]);
  sbar.Panels[1].Text := FitTypeToStr(fFitType);
end;

function TfrmImage.LoadFromFile(aFile: string): boolean;
begin
  result := CanOpen(aFile, false{dummy});
  if result then begin
    img.Picture.LoadFromFile(aFile);
    SetFit(ft1x1);
    UpdateInfo;
  end;
end;

function TfrmImage.LoadFromStream(stream: TStream; metaFilename: string): boolean;

  procedure LoadIt;
  begin
    SetFit(ft1x1);
    UpdateInfo;
    result := true;
  end;

begin

  // first chance
  try
    img.Picture.LoadFromStream(stream);
    LoadIt;
  except
    result := false;
  end;

  if result then
    exit;

  // last chance ...
  try
    img.Picture.LoadFromStreamWithFileExt(stream, ExtractFileExt(metaFilename));
    LoadIt;
  except
    result := false;
  end;
end;

procedure TfrmImage.SetProperty(aProperty: string; aValue: TNotifyEvent);
begin

end;

procedure TfrmImage.SetProperty(aProperty: string; aValue: Variant);
begin

end;

procedure TfrmImage.SetProperty(aProperty: string; aValue: TObject);
begin

end;

end.

