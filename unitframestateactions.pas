{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  TframeStateActions.
  This frame uses the git state for presenting the user with options to handle
  several options that normally the user types in the command line, for example
  'continue', 'skip', 'abort', 'quit', etc.
}
unit unitframestateactions;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Graphics, Forms, Dialogs, Controls, StdCtrls,
  unitcommon, unitgitutils, unitrunthread, unitgitmgr, unitifaces, unitgitstate;

type

  { TframeStateActions }

  TframeStateActions = class(TFrame)
    lblCaption: TLabel;
  private
    fGitMgr: TGitMgr;
    fOnError: TNotifyEvent;
    procedure Clear;
    procedure Setup(aTitle,aHint: string; options:array of const);
    procedure OnActionClick(Sender: TObject);
  public

    function  ActionByTag(aTag: Integer): TComponent;
    procedure UpdateState;

    property GitMgr: TGitMgr read fGitMgr write fGitMgr;
    property OnError: TNotifyEvent read fOnError write fOnError;
  end;

implementation

{$R *.lfm}

{ TframeStateActions }

procedure TframeStateActions.Clear;
var
  i: Integer;
begin
  for i := ComponentCount-1 downto 0 do
    if Components[i]<>lblCaption then
      Components[i].Free;
end;

procedure TframeStateActions.Setup(aTitle, aHint: string;
  options: array of const);
var
  i, aTag: Integer;
  lbl: TLabel;
  aCaption: string;
begin

  lblCaption.Caption := aTitle;
  lblCaption.Hint := aHint;

  i := 0;
  while i<Length(Options)-1 do begin

    case Options[i].VType of
      vtString: aCaption := Options[i].VString^;
      vtAnsiString: aCaption := AnsiString(Options[i].VAnsiString);
      else exit; // error
    end;

    inc(i);

    aTag := Options[i].VInteger;

    inc(i);

    if (aTag=ACTION_QUIT) and (not fGitMgr.Git.AtLeastVersion('2.23')) then
      continue;

    lbl := TLabel.Create(Self);
    lbl.OnClick := @OnActionClick;
    lbl.Caption := aCaption;
    lbl.Font.Style := [fsBold, fsUnderline];
    lbl.Font.Color := clBlue;
    lbl.Cursor := crHandPoint;
    lbl.Tag := aTag;
    lbl.Parent := Self;

  end;

  Visible := true;
end;

procedure TframeStateActions.OnActionClick(Sender: TObject);
var
  lbl: TLabel absolute Sender;
  s, cmd, aTitle: string;
  res: TModalResult;
  cmdOut: RawByteString;
  aGit: IGit;
begin
  if fGitMgr.GitState.Rebasing then begin

    case lbl.Tag of
      ACTION_CONTINUE:
        begin
          s := rsContinueRebasing;
          cmd := 'rebase --continue';
        end;
      ACTION_SKIP:
        begin
          s := rsSkipThisPatch;
          cmd := 'rebase --skip';
        end;
      ACTION_ABORT:
        begin
          s := rsAbortTheRebase;
          cmd := 'rebase --abort';
        end;
      ACTION_QUIT:
        begin
          s := rsQuitTheRebase;
          cmd := 'rebase --quit';
        end;
    end;

    aTitle := rsRebaseAction;

  end else
  if fGitMgr.GitState.Reverting then begin

    case lbl.Tag of
      ACTION_CONTINUE:
        begin
          s := rsContinueReverting;
          cmd := 'revert --continue';
        end;
      ACTION_SKIP:
        begin
          s := rsSkipThisPatch;
          cmd := 'revert --skip';
        end;
      ACTION_ABORT:
        begin
          s := rsAbortTheRevert;
          cmd := 'revert --abort';
        end;
      ACTION_QUIT:
        begin
          s := rsQuitTheRevert;
          cmd := 'revert --quit';
        end;
    end;

    aTitle := rsRevertAction;

  end else
  if fGitMgr.GitState.Bisecting then begin

    fGitMgr.Bisect(lbl.Tag, cmdOut);
    exit;

  end else
  if fGitMgr.GitState.Merging then begin

    s := rsAbortTheMerge;
    cmd := 'merge --abort';
    aTitle := rsMergeAction;

  end;

  res := QuestionDlg(aTitle, format(rsYouSelectedToSIWillDo, [s, cmd]),
    mtConfirmation, [mrYes, rsYesDoIt, mrCancel, rsCancel], 0 );
  if res<>mrYes then
    exit;

  aGit := fGitMgr.Git;
  if RunCommand(aGit.Exe + ' ' + cmd, aGit.TopLevelDir, cmdOut)<=0 then begin
    fGitMgr.UpdateStatus();
    fGitMgr.UpdateRefList;
  end else
    if Assigned(OnError) then
      fOnError(Self);
end;

function TframeStateActions.ActionByTag(aTag: Integer): TComponent;
var
  i: Integer;
begin
  for i:=0 to ComponentCount-1 do
    if Components[i].Tag=aTag then begin
      result := Components[i];
      break;
    end;
end;

procedure TframeStateActions.UpdateState;
var
  lbl: TLabel;
  state: TGitState;
  aTitle: string;
begin
  Clear;
  state := fGitMgr.GitState;

  if state.Rebasing or state.Reverting then begin

    if state.Rebasing then aTitle := rsREBASE
    else                   aTitle := rsREVERT;

    Setup(format(rsSInProgress, [aTitle]), '',
      [rsContinue, ACTION_CONTINUE,
       rsSkip, ACTION_SKIP,
       rsAbort, ACTION_ABORT,
       rsQuit, ACTION_QUIT]);

    lbl := TLabel(ActionByTag(ACTION_CONTINUE));
    lbl.Enabled := not state.MergingConflict;
    if state.MergingConflict then
      lbl.Hint := rsPleaseResolveAllConflictsFirst;

  end else
  if state.Bisecting then begin

    if state.BisectFinished then begin
      Setup(rsTheBisectOperationHasEnded, '',
        ['Log', ACTION_BISECT_LOG,
         'Reset', ACTION_BISECT_RESET]);
    end else
      Setup(format(rsSInProgress, [rsBISECTING]), '',
        ['Log', ACTION_BISECT_LOG,
         TitleCase(state.BisectTermGood), ACTION_BISECT_GOOD,
         TitleCase(state.BisectTermBad), ACTION_BISECT_BAD,
         'Reset', ACTION_BISECT_RESET]);

  end else
  if state.Merging then begin

    Setup(format(rsSInProgress, [rsMERGING]), rsFixConflictsAndCommitYourChanges,
      [rsAbort, ACTION_ABORT]);

  end else
    visible := false;
end;

end.

