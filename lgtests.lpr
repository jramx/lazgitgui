program lgtests;

{$mode objfpc}{$H+}

uses
  Classes, consoletestrunner,
  unitcustomconfig, unitgitmgr,
  tstsmain, tstsstartup, tstsreflist;

type

  { TMyTestRunner }

  TMyTestRunner = class(TTestRunner)
  protected
  // override the protected methods of TTestRunner to customize its behavior
  end;

var
  Application: TMyTestRunner;

begin

  fMain := TMain.Create;
  try
    Application := TMyTestRunner.Create(nil);
    Application.Initialize;
    Application.Title := 'FPCUnit Console test runner';
    Application.Run;
    Application.Free;
  finally
    fMain.Free;
  end;

end.
