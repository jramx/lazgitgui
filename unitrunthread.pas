{ LazGitGui: An interface to git status with some additional tools
             and with a familiar git gui interface.

  Copyright (C) 2023 Jesus Reyes Aguilar (jesusrmx@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.

  TRunThread.
  Runs one or several consecutive commands in a thread.
}
unit unitrunthread;

{$mode ObjFPC}{$H+}
{$ModeSwitch nestedprocvars}

interface

uses
  Classes, SysUtils,
  unitprocess;

type
  TLineEnding = string[3];

  TCommandItemPreferredOutputType = (cipotString, cipotStream);
  TCommandItem = record
    description: string;
    command: string;
    RedirStdErr: boolean;
    PreferredOutputType: TCommandItemPreferredOutputType;
    Enviroment: string;
    output: RawByteString;
    tag: pointer;
    result: Integer;
  end;
  TCommandsArray = array of TCommandItem;
  TCommandProgressEvent = procedure(sender: TObject; item:TCommandItem; percent:single) of object;
  TCommandProgressEventNested = procedure(sender: TObject; item:TCommandItem; percent:single) is nested;

  { TRunThread }

  TRunThread = class(TThread)
  private
    fCommand: string;
    fCommands: TCommandsArray;
    fHaveProgress: boolean;
    fLineEnding: TLineEnding;
    fOnCommandProgress: TCommandProgressEvent;
    fOnCommandProgressNested: TCommandProgressEventNested;
    fOnOutput: TNotifyInterruptEvent;
    fResult: Integer;
    fIndex: Integer;
    fErrorLog: string;
    fStartDir: string;
    fLine: RawByteString;
    fCurrentOutput: RawByteString;
    fCmdLine: TCmdLine;
    procedure Notify;
    procedure RunCommand;
    procedure RunCommandsArray;
    procedure DoCommandProgress;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Execute; override;
    property Command: string read fCommand write fCommand;
    property Commands: TCommandsArray read fCommands write fCommands;
    property StartDir: string read fStartDir write fStartDir;
    property Result: Integer read fResult;
    property ErrorLog: string read fErrorLog;
    property CurrentOutput: RawByteString read fCurrentOutput;
    property OnOutput: TNotifyInterruptEvent read fOnOutput write fOnOutput;
    property HaveProgress: boolean read fHaveProgress write fHaveProgress;
    property LineEnding: TLineEnding read fLineEnding;
    property Line: string read fLine;
    property OnCommandProgress: TCommandProgressEvent read fOnCommandProgress write fOnCommandProgress;
    property OnCommandProgressNested: TCommandProgressEventNested read fOnCommandProgressNested write fOnCommandProgressNested;
  end;

  function RunInThread(Command, startDir: string; OnOutput: TNotifyInterruptEvent; OnDone: TNotifyEvent; startIt:boolean=true): TRunThread; overload;
  function RunInThread(Commands: TCommandsArray; startDir: string; OnProgress: TCommandProgressEvent; OnDone: TNotifyEvent; startIt:boolean=true; allowFails:boolean=false): TRunThread; overload;
  function RunInThread(Commands: TCommandsArray; startDir: string; OnProgress: TCommandProgressEventNested; OnDone: TNotifyEvent; startIt:boolean=true; allowFails:boolean=false): TRunThread; overload;
  function RunCommand(command, startDir: string; out cmdOut: RawByteString; ForgetIt:boolean=false): Integer;
  procedure RunCommands(Commands: TCommandsArray; startDir: string; OnProgress: TCommandProgressEvent; OnDone: TNotifyEvent; allowFails:boolean=false); overload;

implementation

function RunInThread(Command, startDir: string; OnOutput: TNotifyInterruptEvent;
  OnDone: TNotifyEvent; startIt: boolean): TRunThread;
begin
  Result := TRunThread.Create;
  Result.Command := Command;
  Result.StartDir := startDir;
  Result.FreeOnTerminate := true;
  Result.OnOutput := OnOutput;
  Result.OnTerminate := OnDone;
  if startIt then
    Result.Start;
end;

function RunInThread(Commands: TCommandsArray; startDir: string;
  OnProgress: TCommandProgressEvent; OnDone: TNotifyEvent; startIt: boolean;
  allowFails: boolean): TRunThread;
begin
  Result := TRunThread.Create;
  Result.Commands := Commands;
  Result.StartDir := startDir;
  Result.FreeOnTerminate := true;
  Result.OnCommandProgress := OnProgress;
  Result.OnTerminate := OnDone;
  if startIt then
    Result.Start;
end;

function RunInThread(Commands: TCommandsArray; startDir: string;
  OnProgress: TCommandProgressEventNested; OnDone: TNotifyEvent;
  startIt: boolean; allowFails: boolean): TRunThread;
begin
  Result := TRunThread.Create;
  Result.Commands := Commands;
  Result.StartDir := startDir;
  Result.FreeOnTerminate := true;
  Result.OnCommandProgressNested := OnProgress;
  Result.OnTerminate := OnDone;
  if startIt then
    Result.Start;
end;

function RunCommand(command, startDir: string; out cmdOut: RawByteString;
  ForgetIt: boolean): Integer;
begin
  if ForgetIt then begin
    CmdLine.UsePipes := false;
    CmdLine.StdErrorClosed := true;
    cmdLine.StdInputClosed := true;
    CmdLine.StdOutputClosed := true;
  end;
  Result := CmdLine.RunProcess(command, startdir, cmdOut);
end;

procedure RunCommands(Commands: TCommandsArray; startDir: string;
  OnProgress: TCommandProgressEvent; OnDone: TNotifyEvent;
  allowFails: boolean);
var
  M: TMemoryStream;
  i: Integer;
begin

  if not Assigned(OnProgress) then
    exit;

  for i := 0 to Length(commands)-1 do begin
    CmdLine.RedirStdErr := commands[i].RedirStdErr;
    CmdLine.Environment := commands[i].Enviroment;
    try

      M := nil;
      if commands[i].PreferredOutputType=cipotStream then begin
        M := TMemoryStream.Create;
        commands[i].result := CmdLine.RunProcess(commands[i].command, startDir, M);
        commands[i].tag := M;
      end else
        commands[i].result := CmdLine.RunProcess(commands[i].command, startDir, commands[i].output);

      if Assigned(OnProgress) then
        OnProgress(nil, commands[i], (i+1)/Length(commands)*100);

      if (commands[i].result>0) and (not allowFails) then
        break;

    finally
      M.Free;
    end;
  end;

  if Assigned(OnDone) then
    OnDone(nil);
end;

{ TRunThread }

procedure TRunThread.Notify;
var
  interrupt: boolean;
begin
  if assigned(fOnOutput) then begin
    {$IFDEF DEBUG}
    DebugLn('Notifying: %s',[DbgStr(fLine)]);
    {$ENDIF}
    interrupt := false;
    fOnOutput(Self, interrupt);
    if interrupt then
      terminate;
  end;
end;

constructor TRunThread.Create;
begin
  inherited Create(true);
  fCmdLine := TCmdLine.Create;
end;

destructor TRunThread.Destroy;
begin
  {$IFDEF DEBUG}
  debugln('TRunThread.Destroy: ');
  {$ENDIF}
  fCmdLine.Free;
  inherited Destroy;
end;

procedure TRunThread.RunCommand;
var
  outText: string;

  function NextLineEnding(p: pchar): pchar;
  var
    x, y: pchar;
  begin
    x := strpos(p, #10);
    y := strpos(p, #13);
    if (x=nil) and (y=nil) then
      result := nil
    else if (x=nil) and (y<>nil) then
      result := y
    else if (x<>nil) and (y=nil) then
      result := x
    else if x<y then
      result := x
    else
      result := y;
  end;

  procedure CollectOutput(const buffer; size:Longint; var interrupt: boolean);
  var
    aPos: Integer;
    p, q: pchar;
  begin
    if terminated or interrupt then
      exit;
    {$IFDEF DEBUG}
    DebugLn('Collecting %d bytes',[size]);
    {$ENDIF}
    aPos := Length(outText);
    SetLength(outText, aPos + size);
    Move(Buffer, OutText[aPos+1], size);

    // buffer is guaranteed to end with #0
    while (outText<>'') and (not Terminated) do begin

      p := @outText[1];
      q := NextLineEnding(p);
      if q=nil then
        break;

      if (q^=#10) and ((q-1)^=#13) then
        dec(q);

      if (q^=#10) then
        fLineEnding := #10
      else if (q^=#13) and ((q+1)^=#10) then
        fLineEnding := #13#10
      else
        fLineEnding := #13;

      fLine := copy(outText, 1, q-p);

      Synchronize(@Notify);

      interrupt := terminated;
      if interrupt then
        break;

      delete(outText, 1, (q-p) + length(fLineEnding));

    end;
  end;

begin
  {$IFDEF DEBUG}
  DebugLnEnter('RunThread START Command=%s', [fCommand]);
  {$ENDIF}
  outText := '';
  //fCmdLine^.WaitOnExit := true;
  fCmdLine.RedirStdErr := true;
  fResult := fCmdLine.RunProcess(fCommand, fStartDir, @CollectOutput);
  if (outText<>'') and (not terminated) then begin
    fLine := outText;
    Synchronize(@Notify);
  end;
  fErrorLog := fCmdLine.ErrorLog;
  {$IFDEF DEBUG}
  DebugLnExit('RunThread DONE result=%d', [fResult]);
  {$ENDIF}
end;

procedure TRunThread.RunCommandsArray;
var
  M: TMemoryStream;
begin
  if (not Assigned(fOnCommandProgress)) and
     (not Assigned(fOnCommandProgressNested))
  then
    exit;
  fIndex := 0;
  while not terminated and (fIndex<Length(fCommands)) do begin
    fCmdLine.RedirStdErr := fCommands[fIndex].RedirStdErr;
    fCmdLine.Environment := fCommands[fIndex].Enviroment;
    try
      M := nil;
      if fCommands[fIndex].command<>'' then begin
        if fCommands[fIndex].PreferredOutputType=cipotStream then begin
          M := TMemoryStream.Create;
          fResult := fCmdLine.RunProcess(fCommands[fIndex].command, fStartDir, M);
          fCommands[fIndex].tag := M;
        end else begin
          fResult := fCmdLine.RunProcess(fCommands[fIndex].command, fStartDir, fCurrentOutput);
          fCommands[fIndex].output := fCurrentOutput;
        end;
        fCommands[fIndex].result := fResult;
        Synchronize(@DoCommandProgress);
      end else
        // run the notification in the thread
        DoCommandProgress;
    finally
      M.Free;
    end;
    inc(fIndex);
  end;
end;

procedure TRunThread.DoCommandProgress;
begin
  if not terminated and Assigned(fOnCommandProgress) then
    fOnCommandProgress(self, fCommands[fIndex], (fIndex+1)/Length(fCommands)*100)
  else if not terminated and Assigned(fOnCommandProgressNested) then
    fOnCommandProgressNested(self, fCommands[fIndex], (fIndex+1)/Length(fCommands)*100);
end;

procedure TRunThread.Execute;
begin
  if Length(fCommands)=0 then
    RunCommand
  else
    RunCommandsArray;
  Terminate;
end;

end.

